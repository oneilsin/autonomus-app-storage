﻿using Storage.Datos.Interfaces;
using Storage.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class CategoriaRepository : Repository<Categoria>, ICategoriaRepository
    {
        public CategoriaRepository(StorageContext storageContext) : base(storageContext)
        {
        }

        public IEnumerable<Categoria> GetList()
        {
            return GetList("General.SpCategoriaList");
        }
    }
}
