﻿using Storage.Datos.Interfaces;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class ClienteRepository : Repository<Cliente>, IClienteRepository
    {
        public ClienteRepository(StorageContext storageContext) : base(storageContext)
        {
        }

        public int Crud(Cliente entity, string option) 
        {
            try
            {
                var r = ObjContext.ExecuteNonQuery("SpClienteMaintenance", 
                    System.Data.CommandType.StoredProcedure,
                    new SqlParameter[]
                    {
                        new SqlParameter("@IdCliente", entity.IdCliente),
                        new SqlParameter("@IdTipoCliente", entity.IdTipoCliente),
                        new SqlParameter("@IdTipoDocumento", entity.IdTipoDocumento),
                        new SqlParameter("@NumeroDocumento", entity.NumeroDocumento),
                        new SqlParameter("@Nombres", entity.Nombres),
                        new SqlParameter("@Apellidos", entity.Apellidos),
                        new SqlParameter("@Sexo", entity.Sexo),
                        new SqlParameter("@FechaNacimiento", entity.FechaNacimiento),
                        new SqlParameter("@Rol", entity.Rol),
                        new SqlParameter("@EditorUser", entity.EditorUser),
                        new SqlParameter("@IdNivelPrecio", entity.IdNivelPrecio),
                        new SqlParameter("@LineaCredito", entity.LineaCredito),
                        new SqlParameter("@FlagCredito", entity.FlagCredito),
                        new SqlParameter("@Option", option)
                    });

                return r;
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public Cliente GetById(string id)
        {
            string querySql = "SpClienteById";

            return ObjContext.ToList<Cliente>(
                ObjContext.GetData(querySql, new SqlParameter("@IdCliente", id)).Tables[0]
            ).FirstOrDefault();
        }
        
        public IEnumerable<ClienteNivelprecioReport> GetClienteNivelprecios()
        {
            return ObjContext.ToList<ClienteNivelprecioReport>(
                 ObjContext.GetData("SpClienteNivelPrecioList").Tables[0]);
        }

        public IEnumerable<ClienteReport> GetList()
        {
            return ObjContext.ToList<ClienteReport>(
                    ObjContext.GetData("SpClienteList").Tables[0]);
        }
    }
}
