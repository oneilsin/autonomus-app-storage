﻿using Storage.Datos.AdoNet.Helper;
using Storage.Datos.Interfaces;
using Storage.Datos.Interfaces.Helper;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class EntradaRepository : Repository<Entrada>, IEntradaRepository
    {
        ICorrelativo10D _helper;
        public EntradaRepository(StorageContext storageContext) : base(storageContext)
        {
            _helper = new Correlativo10D(storageContext);
        }

        public int Crud(Entrada entity, Inventario inventario)
        {
            try
            {
                return ObjContext.ExecuteNonQuery("SpEntradaInsert",
                    System.Data.CommandType.StoredProcedure,
                    new SqlParameter[]
                    {
                        new SqlParameter("@IdEntrada", entity.IdEntrada),
                        new SqlParameter("@IdTienda", entity.IdTienda),
                        new SqlParameter("@IdProveedor", entity.IdProveedor),
                        new SqlParameter("@Documento", entity.Documento),
                        //Sección para el inventario
                        new SqlParameter("@IdTipoMov", inventario.IdTipoMov),
                        new SqlParameter("@IdProducto", inventario.IdProducto),
                        new SqlParameter("@Cantidad", inventario.Cantidad),
                        new SqlParameter("@PrecioUnitario", inventario.PrecioUnitario),
                      //  new SqlParameter("@Condicion", inventario.Condicion),
                        //Para historico.
                        new SqlParameter("@EditorUser", entity.EditorUser)
                    });
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public string GetLastId()
        {
            return _helper.GetLastId("Entrada", "IdEntrada");
        }

        public IEnumerable<EntradaReport> GetList(DateTime desde, DateTime hasta)
        {
            return ObjContext.ToList<EntradaReport>(
                      ObjContext.GetData("SpEntradaList", new SqlParameter[]
                      {
                          new SqlParameter("@Desde",desde),
                          new SqlParameter("@Hasta",hasta),
                      }).Tables[0]);
        }

        public string Prueba()
        {
            return (string)ObjContext.ExecuteEscalar("select Descripcion from Producto where IdProducto='P00001'"
                , System.Data.CommandType.Text);
        }
    }
}
