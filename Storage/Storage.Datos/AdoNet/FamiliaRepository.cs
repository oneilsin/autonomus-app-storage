﻿using Storage.Datos.Interfaces;
using Storage.Entidad;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class FamiliaRepository : Repository<Familia>, IFamiliaRepository
    {
        public FamiliaRepository(StorageContext storageContext) : base(storageContext)
        {
        }

        public int Crud(Familia entity, string option)
        {
            try
            {
                return ObjContext.ExecuteNonQuery("General.SpFamiliaMaintenace",
                    System.Data.CommandType.StoredProcedure,
                    new SqlParameter[]
                    {
                        new SqlParameter("@IdFamilia",entity.IdFamilia),
                        new SqlParameter("@Descripcion",entity.Descripcion),
                        new SqlParameter("@EditorUser",entity.EditorUser),
                        new SqlParameter("@Option",option)
                    });
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public IEnumerable<Familia> GetList()
        {
            return GetList("General.SpFamiliaList");
        }

       
    }
}
