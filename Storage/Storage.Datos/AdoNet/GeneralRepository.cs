﻿using Storage.Datos.Interfaces;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class GeneralRepository : Repository<General>, IGeneralRepository
    {
        public GeneralRepository(StorageContext storageContext) : base(storageContext)
        {
        }

        public IEnumerable<GeneralReport> GetGeneralList(string numeroTabla)
        {
            try
            {
                return ObjContext.ToList<GeneralReport>(
                    ObjContext.GetData($"execute General.SpTablaList '{numeroTabla}'").Tables[0]);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public IEnumerable<PrecioNivel> GetPrecioNivels()
        {
            try
            {
                return ObjContext.ToList<PrecioNivel>(
                    ObjContext.GetData($"General.SpPrecioNivelList").Tables[0]);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public IEnumerable<PrecioProducto> GetPrecios()
        {
            try
            {
                return ObjContext.ToList<PrecioProducto>(
                    ObjContext.GetData($"General.SpPrecioProductoList").Tables[0]);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public IEnumerable<PrecioProductoDetail> GetPrecioProductoDetails(string idProducto)
        {
            try
            {
                return ObjContext.ToList<PrecioProductoDetail>(
                    ObjContext.GetData($"General.SpPrecioProductoDetail",
                    new SqlParameter("@IdProducto", idProducto)).Tables[0]);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public IEnumerable<TipoMovimiento> GetTipoMovimientoList()
        {
            try
            {
                return ObjContext.ToList<TipoMovimiento>(
                    ObjContext.GetData($"execute General.SpTipoMovimientoList").Tables[0]);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public IEnumerable<UbigeoReport> GetUbigeoList()
        {
            try
            {
                return ObjContext.ToList<UbigeoReport>(
                    ObjContext.GetData("execute General.SpUbigeoList").Tables[0]);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public int UpdatePrecios(string id, decimal precio)
        {
            string sqlQuery = "General.SpPrecioProductoUpdate";

            return ObjContext.ExecuteNonQuery(sqlQuery,
                CommandType.StoredProcedure,
                new SqlParameter[] {
                new SqlParameter("@IdPrecio",id),
                new SqlParameter("@Precio",precio),
                });
        }
    }
}
