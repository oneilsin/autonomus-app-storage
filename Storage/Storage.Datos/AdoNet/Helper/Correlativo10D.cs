﻿using Storage.Datos.Interfaces.Helper;
using System.Data.SqlClient;

namespace Storage.Datos.AdoNet.Helper
{
    public class Correlativo10D : ICorrelativo10D
    {
        protected readonly StorageContext ObjContext;
        public Correlativo10D(StorageContext storageContext)
        {
            ObjContext = storageContext;
        }

        public string GetLastId(string table, string column)
        {
            string command = "SpGenerateNewId10digits";
            var salida = ObjContext.ExecuteEscalar(command,
                System.Data.CommandType.StoredProcedure,
                new SqlParameter[]
                {
                    new SqlParameter("@tableName",table),
                    new SqlParameter("@columnIdName",column)
                });

            return (string)salida;
        }
    }
}
