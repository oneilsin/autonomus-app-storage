﻿using Storage.Datos.Interfaces;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class InventarioRepository : Repository<Inventario>, IInventarioRepository
    {
        public InventarioRepository(StorageContext storageContext) : base(storageContext)
        {
        }

        public IEnumerable<InventarioMovimiento> GetDetalleOrden(string documento, string movimiento)
        {
            return GetMovimientos(documento, movimiento)
                .Where(o => o.IdTipoMov == "00001");
        }

        public IEnumerable<InventarioMovimiento> GetMovimientos(string documento, string movimiento)
        {
            string comandSql = "SpInventarioMovimiento";
            return ObjContext.ToList<InventarioMovimiento>(
                ObjContext.GetData(comandSql, new System.Data.SqlClient.SqlParameter[]
                {
                    new System.Data.SqlClient.SqlParameter("@Documento",documento),
                    new System.Data.SqlClient.SqlParameter("@Movimiento",movimiento)
                }).Tables[0]);
        }

        public IEnumerable<StockStore> GetStockByStore(string idTienda)
        {
            string comandSql = "SpStockByStore";
            return ObjContext.ToList<StockStore>(
                    ObjContext.GetData(comandSql,
                    new SqlParameter("@IdTienda", idTienda)
                    ).Tables[0]
                );
        }

        public List<StockDetail> GetStockDetails(string idTienda = "")
        {
            string comandSql = "SpStockDetail";
            return ObjContext.ToList<StockDetail>(
                ObjContext.GetData(comandSql, new SqlParameter("@IdTienda", idTienda)
                ).Tables[0]);
        }

        public int UpdateProductoPrecioporNivel(string idNivel, string idOrden, string idCliente)
        {
            string sqlQuery = "SpInventarioUpdateNivelPrecio";
            return ObjContext.ExecuteNonQuery(sqlQuery,
                System.Data.CommandType.StoredProcedure,
                new SqlParameter[]{
                new SqlParameter("@IdNivel",idNivel),
                new SqlParameter("@IdOrdenVenta",idOrden),
                new SqlParameter("@IdCliente",idCliente)
                });
        }
    }
}
