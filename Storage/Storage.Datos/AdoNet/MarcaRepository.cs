﻿using Storage.Datos.Interfaces;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class MarcaRepository : Repository<Marca>, IMarcaRepository
    {
        public MarcaRepository(StorageContext storageContext) : base(storageContext)
        {
        }

        public int Crud(Marca entity, string option)
        {
            try
            {
                return ObjContext.ExecuteNonQuery("General.SpMarcaMaintenace",
                    System.Data.CommandType.StoredProcedure,
                    new SqlParameter[]
                    {
                        new SqlParameter("@IdMarca",entity.IdMarca),
                        new SqlParameter("@NombreMarca",entity.NombreMarca),
                        new SqlParameter("@Descripcion",entity.Descripcion),
                        new SqlParameter("@Prioridad",entity.Prioridad),
                        new SqlParameter("@IdProveedor",entity.IdProveedor),
                        new SqlParameter("@EditorUser",entity.EditorUser),
                        new SqlParameter("@Option",option),
                    });
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public IEnumerable<MarcaReport> GetList()
        {
            try
            {
                return ObjContext.ToList<MarcaReport>
                (ObjContext.GetData("execute General.SpMarcaList").Tables[0]);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
    }
}
