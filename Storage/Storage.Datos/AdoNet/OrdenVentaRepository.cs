﻿using Storage.Datos.AdoNet.Helper;
using Storage.Datos.Interfaces;
using Storage.Datos.Interfaces.Helper;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class OrdenVentaRepository : Repository<OrdenVenta>, IOrdenVentaRepository
    {
        ICorrelativo10D _helper;
        public OrdenVentaRepository(StorageContext storageContext) : base(storageContext)
        {
            _helper = new Correlativo10D(storageContext);
        }

        public int Crud(OrdenVenta orden, Inventario inventario)
        {
            try
            {
                string querySql = "SpOrdenVentaInsert";
                return ObjContext.ExecuteNonQuery(querySql, System.Data.CommandType.StoredProcedure,
                    new SqlParameter[]{
                  new SqlParameter("@IdOrden",orden.IdOrden),
                  new SqlParameter("@IdTienda",orden.IdTienda),
                  new SqlParameter("@IdVendedor",orden.IdVendedor),
                  new SqlParameter("@IdCliente",orden.IdCliente),
                  new SqlParameter("@Observacion",orden.Observacion),
                  new SqlParameter("@EditorUser",orden.EditorUser),
                  //Sección para inventario
                  new SqlParameter("@IdProducto",inventario.IdProducto),
                  new SqlParameter("@Cantidad",inventario.Cantidad),
                  new SqlParameter("@PrecioUnitario",inventario.PrecioUnitario),
                    });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string GetLastId()
        {
            return _helper.GetLastId("OrdenVenta", "IdOrden");
        }

        public IEnumerable<OrdenVentaReport> GetList(DateTime desde, DateTime hasta,
            OrdenVenta orden = null)//string idTien da="", string situacion = "")
        {
            string querySql = "SpOrdenVentaList";
            return ObjContext.ToList<OrdenVentaReport>(
                ObjContext.GetData(querySql, new SqlParameter[]
                {
                    new SqlParameter("@Desde",desde),
                    new SqlParameter("@Hasta",hasta),
                    new SqlParameter("@IdTienda", orden.IdTienda!=null? orden.IdTienda:""),
                    new SqlParameter("@Situacion",orden.Situacion!=null? orden.Situacion:""),
                    new SqlParameter("@IdOrden",orden.IdOrden!=null?orden.IdOrden:"")
                }).Tables[0]
                ).ToList();
        }
    }
}
