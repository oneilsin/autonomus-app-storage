﻿using Storage.Datos.Interfaces;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class ProductoRepository : Repository<Producto>, IProductoRepository
    {
        public ProductoRepository(StorageContext storageContext) : base(storageContext)
        {
        }

        public int Crud(Producto entity, string option)
        {
            try
            {
                return (int)ObjContext.ExecuteNonQuery("SpProductoMaintenace", System.Data.CommandType.StoredProcedure,
                    new SqlParameter[]
                    {
                        new SqlParameter("@IdProducto",entity.IdProducto),
                        new SqlParameter("@IdCategoria",entity.IdCategoria),
                        new SqlParameter("@IdSubCategoria",entity.IdSubCategoria),
                        new SqlParameter("@IdMarca",entity.IdMarca),
                        new SqlParameter("@IdTalla",entity.IdTalla),
                        new SqlParameter("@IdFamilia",entity.IdFamilia),
                        new SqlParameter("@NombreProducto",entity.NombreProducto),
                        new SqlParameter("@Descripcion",entity.Descripcion),
                        new SqlParameter("@Genero",entity.Genero),
                        new SqlParameter("@EnlaceWeb",entity.EnlaceWeb),
                        new SqlParameter("@Sku",entity.Sku),
                        new SqlParameter("@StokMinimo",entity.StokMinimo),
                        new SqlParameter("@PrecioBase",entity.PrecioBase),
                        new SqlParameter("@EditorUser",entity.EditorUser),
                        new SqlParameter("@Option",option)
                    });
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<ProductoReport> GetList()
        {
            return ObjContext.ToList<ProductoReport>(
                    ObjContext.GetData("SpProductoList").Tables[0]);
        }
    }
}
