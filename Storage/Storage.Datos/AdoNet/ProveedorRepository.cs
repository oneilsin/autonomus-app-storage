﻿using Storage.Datos.Interfaces;
using Storage.Entidad;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class ProveedorRepository : Repository<Proveedor>, IProveedorRepository
    {
        public ProveedorRepository(StorageContext storageContext) : base(storageContext)
        {
        }

        public int Crud(Proveedor entity, string option)
        {
            try
            {
                return ObjContext.ExecuteNonQuery("General.SpProveedorMaintenace",
                        System.Data.CommandType.StoredProcedure,
                        new SqlParameter[]
                        {
                            new SqlParameter("@IdProveedor",entity.IdProveedor),
                            new SqlParameter("@NombreProveedor",entity.NombreProveedor),
                            new SqlParameter("@Ubigeo",entity.Ubigeo),
                            new SqlParameter("@Direccion",entity.Direccion),
                            new SqlParameter("@Urbanizacion",entity.Urbanizacion),
                            new SqlParameter("@TelefFijo",entity.TelefFijo),
                            new SqlParameter("@TelefMovil",entity.TelefMovil),
                            new SqlParameter("@Email",entity.Email),
                            new SqlParameter("@EditorUser",entity.EditorUser),
                            new SqlParameter("@Option",option)
                        });
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public IEnumerable<Proveedor> GetList()
        {
            return GetList("General.SpProveedorList");
        }
    }
}
