﻿using Storage.Datos.AdoNet.Helper;
using Storage.Datos.Interfaces;
using Storage.Datos.Interfaces.Helper;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class SalidaRepository : Repository<Salida>, ISalidaRepository
    {
        ICorrelativo10D _helper;
        public SalidaRepository(StorageContext storageContext) : base(storageContext)
        {
            _helper = new Correlativo10D(storageContext);
        }

        public int Crud(Salida entity, Inventario inventario)
        {
            try
            {
                return ObjContext.ExecuteNonQuery("SpSalidaInsert",
                    System.Data.CommandType.StoredProcedure,
                    new SqlParameter[]
                    {
                        new SqlParameter("@IdSalida", entity.IdSalida),
                        new SqlParameter("@IdTienda", entity.IdTienda),
                        new SqlParameter("@Documento", entity.Documento),
                        //Sección para el inventario
                        new SqlParameter("@IdTipoMov", inventario.IdTipoMov),
                        new SqlParameter("@IdProducto", inventario.IdProducto),
                        new SqlParameter("@Cantidad", inventario.Cantidad),
                        //Para historico.
                        new SqlParameter("@EditorUser", entity.EditorUser)
                    });
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public string GetLastId()
        {
            return _helper.GetLastId("Salida", "IdSalida");
        }

        public IEnumerable<SalidaReport> GetList(DateTime desde, DateTime hasta)
        {
            return ObjContext.ToList<SalidaReport>(
                         ObjContext.GetData("SpSalidaList", new SqlParameter[]
                         {
                             new SqlParameter("@Desde",desde),
                             new SqlParameter("@Hasta",hasta),
                         }).Tables[0]);
        }
    }
}
