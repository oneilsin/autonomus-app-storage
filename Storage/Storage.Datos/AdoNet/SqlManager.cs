﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;

namespace Storage.Datos.AdoNet
{
    public class SqlManager
    {
        SqlConnection sql;
        private bool IsClosed;
        private string ConnectionString;
        public SqlManager(string connectionString)
        {
            this.ConnectionString = connectionString;
            sql = new SqlConnection(connectionString);
            IsClosed = false;
        }

        protected void Close(bool value)
        {
            this.IsClosed = value;
        }

        public void Dispose() { sql.Dispose(); }

        public DataSet GetData(string command)
        {
            try
            {
                using (var newConnection = IsClosed ? new SqlConnection(ConnectionString) : sql)
                {
                    newConnection.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(command, newConnection))
                    {
                        var ds = new DataSet();
                        da.Fill(ds);
                        Close(true);
                        return ds;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public DataSet GetData(string command, params SqlParameter[] sqlParameters)
        {
            try
            {
                using (var newConnection = IsClosed ? new SqlConnection(ConnectionString) : sql)
                {
                    newConnection.Open();
                    using (SqlDataAdapter da = new SqlDataAdapter(command, newConnection))
                    {
                        da.SelectCommand.CommandType = CommandType.StoredProcedure;
                        da.SelectCommand.Parameters.AddRange(sqlParameters);

                        var ds = new DataSet();
                        da.Fill(ds);
                        Close(true);
                        return ds;
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public List<T> ToList<T>(DataTable table) where T :class, new()
        {
            try
            {
                var list = new List<T>();
                foreach (var row in table.AsEnumerable())
                {
                    T obj = new T();
                    foreach (var property in obj.GetType().GetProperties())
                    {
                        try
                        {
                            PropertyInfo propertyInfo = obj.GetType().GetProperty(property.Name);
                            propertyInfo.SetValue(obj, 
                                Convert.ChangeType(row[property.Name], 
                                propertyInfo.PropertyType, 
                                null));
                        }
                        catch
                        {
                            continue;
                        }
                    }
                    list.Add(obj);
                }
                return list;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Int32 ExecuteNonQuery(string querySql, CommandType commandType, params SqlParameter[] sqlParameters)
        {
            try
            {
                using (var newConnection = IsClosed ? new SqlConnection(ConnectionString) : sql)
                {
                    newConnection.Open();

                    using (SqlCommand cm = new SqlCommand(querySql, newConnection))
                    {
                        cm.CommandType = commandType;
                        cm.Parameters.AddRange(sqlParameters);
                        
                        Close(true);
                        return cm.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Int32 ExecuteNonQuery(string querySql, CommandType commandType)
        {
            try
            {
                using (var newConnection = IsClosed ? new SqlConnection(ConnectionString) : sql)
                {
                    newConnection.Open();
                    using (SqlCommand cm = new SqlCommand(querySql, newConnection))
                    {
                        cm.CommandType = commandType;
                        Close(true);
                        return cm.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

        public Object ExecuteEscalar(string querySql, CommandType commandType, params SqlParameter[] sqlParameters)
        {
            try
            {
                using (var newConnection = IsClosed ? new SqlConnection(ConnectionString) : sql)
                {
                    newConnection.Open();
                    using (SqlCommand cm = new SqlCommand(querySql, newConnection))
                    {
                        cm.CommandType = commandType;
                        cm.Parameters.AddRange(sqlParameters);
                        Close(true);
                        return cm.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public Object ExecuteEscalar(string querySql, CommandType commandType)
        {
            try
            {
                using (var newConnection = IsClosed ? new SqlConnection(ConnectionString) : sql)
                {
                    newConnection.Open();
                    using (SqlCommand cm = new SqlCommand(querySql, newConnection))
                    {
                        cm.CommandType = commandType;
                        Close(true);
                        return cm.ExecuteScalar();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
