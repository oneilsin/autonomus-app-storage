﻿using Storage.Datos.Interfaces;
using Storage.Entidad;
using Storage.Entidad.Report;
using System.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class SubCategoriaRepository : Repository<SubCategoria>, ISubCategoriaRepository
    {
        public SubCategoriaRepository(StorageContext storageContext) : base(storageContext)
        {        }

        public int Crud(SubCategoria entity, string option)
        {
            return ObjContext.ExecuteNonQuery("General.SpSubCategoriaMaintenace",
                System.Data.CommandType.StoredProcedure,
                new SqlParameter[]
                {
                    new SqlParameter("@IdSubCategoria",entity.IdSubCategoria),
                    new SqlParameter("@NombreSubCategoria",entity.NombreSubCategoria),
                    new SqlParameter("@Descripcion",entity.Descripcion),
                    new SqlParameter("@IdCategoria",entity.IdCategoria),
                    new SqlParameter("@EditorUser",entity.EditorUser),
                    new SqlParameter("@Option",option)
                });
        }

        public IEnumerable<SubCategoriaReport> GetList()
        {
            var ds = ObjContext.GetData("exec General.SpSubCategoriaList");
            return ObjContext.ToList<SubCategoriaReport>(ds.Tables[0]);
        }
    }
}
