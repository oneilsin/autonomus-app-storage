﻿using Storage.Datos.Interfaces;
using Storage.Entidad;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class TallaRepository : Repository<Talla>, ITallaRepository
    {
        public TallaRepository(StorageContext storageContext) : base(storageContext)
        {
        }

        public int Crud(Talla entity, string option)
        {
            try
            {
                return ObjContext.ExecuteNonQuery("General.SpTallaMaintenace",
                    System.Data.CommandType.StoredProcedure,
                    new SqlParameter[]
                    {
                        new SqlParameter("@IdTalla",entity.IdTalla),
                        new SqlParameter("@NombreTalla",entity.NombreTalla),
                        new SqlParameter("@Descripcion",entity.Descripcion),
                        new SqlParameter("@EditorUser",entity.EditorUser),
                        new SqlParameter("@Option",option)
                    });

            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public IEnumerable<Talla> GetList()
        {
            return GetList("General.SpTallaList");
        }
    }
}
