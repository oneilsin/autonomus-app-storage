﻿using Storage.Datos.Interfaces;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class TiendaRepository : Repository<Tienda>, ITiendaRepository
    {
        public TiendaRepository(StorageContext storageContext) : base(storageContext)
        {
        }

        public int Crud(Tienda entity, string option)
        {
            try
            {
                return ObjContext.ExecuteNonQuery("General.SpTiendaMaintenance",
                    System.Data.CommandType.StoredProcedure,
                    new SqlParameter[]
                    {
                        new SqlParameter("@IdTienda",entity.IdTienda),
                        new SqlParameter("@NombreTienda",entity.NombreTienda),
                        new SqlParameter("@Descripcion",entity.Descripcion),
                        new SqlParameter("@IdResponsable",entity.IdResponsable),
                        new SqlParameter("@Consigna",entity.Consigna),
                        new SqlParameter("@EditorUser",entity.EditorUser),
                        new SqlParameter("@Option",option),
                    });
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }

        public IEnumerable<TiendaReport> GetList()
        {
            try
            {
                return ObjContext.ToList<TiendaReport>(
                    ObjContext.GetData($"execute General.SpTiendaList").Tables[0]);
            }
            catch (Exception ex)
            { throw new Exception(ex.Message); }
        }
    }
}
