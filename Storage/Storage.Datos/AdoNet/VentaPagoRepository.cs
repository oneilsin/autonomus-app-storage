﻿using Storage.Datos.Interfaces;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class VentaPagoRepository : Repository<VentaPago>, IVentaPagoRepository
    {
        public VentaPagoRepository(StorageContext storageContext) : base(storageContext)
        {
        }

        public IEnumerable<VentaPagoReport> GetDetail(string idVenta)
        {
            string SqlQuery = "SpVentaPagoList";
            
            return ObjContext.ToList<VentaPagoReport>
                (
                    ObjContext.GetData
                    (
                        SqlQuery, 
                        new SqlParameter("@IdVenta", idVenta)
                    ).Tables[0]
                );
        }
    }
}
