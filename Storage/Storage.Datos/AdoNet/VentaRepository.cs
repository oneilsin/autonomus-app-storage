﻿using Storage.Datos.AdoNet.Helper;
using Storage.Datos.Interfaces;
using Storage.Datos.Interfaces.Helper;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.AdoNet
{
    public class VentaRepository : Repository<Venta>, IVentaRepository
    {
        ICorrelativo10D _helper;
        public VentaRepository(StorageContext storageContext) : base(storageContext)
        {
            _helper = new Correlativo10D(storageContext);
        }

        public int Crud(Venta venta, VentaPago pago)
        {
            try
            {
                string sqlQuery = "SpVentaInsert";
                return ObjContext.ExecuteNonQuery
                    (
                        sqlQuery,
                        CommandType.StoredProcedure,
                        new SqlParameter[]
                        {
                            new SqlParameter("@IdTienda",venta.IdTienda),
                            new SqlParameter("@IdVenta",venta.IdVenta),
                            new SqlParameter("@IdOrden",venta.IdOrden),
                            new SqlParameter("@IdComprobante",venta.IdComprobante),
                            new SqlParameter("@NumComprobante",venta.NumComprobante),
                            new SqlParameter("@NivelPrecio",venta.NivelPrecio),
                            new SqlParameter("@ImporteTotal",venta.ImporteTotal),
                            new SqlParameter("@Observacion",venta.Observacion),
                            new SqlParameter("@EditorUser",venta.EditorUser),

                            new SqlParameter("@IdFormaPago",pago.IdFormaPago),
                            new SqlParameter("@MetodoPago",pago.MetodoPago),
                            new SqlParameter("@NumOperacion",pago.NumOperacion),
                            new SqlParameter("@Moneda",pago.Moneda),
                            new SqlParameter("@Cambio",pago.Cambio),
                            new SqlParameter("@Importe",pago.Importe)
                        }
                    );
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public string GetCorrelativoComprobante(string idCompobante)
        {
            string sqlQuery = "SpVentaGenerarComprobanteCorrelativo";
            var obj = ObjContext.ExecuteEscalar(sqlQuery,
                CommandType.StoredProcedure,
                new SqlParameter("@IdComprobante", idCompobante));

            return (string)obj;
        }

        public string GetLastId()
        {
            return _helper.GetLastId("Venta", "IdVenta");
        }

        public IEnumerable<VentaReport> GetList(DateTime desde, DateTime hasta, string idTienda = "")
        {
            string sqlQuery = "SpVentaList";

            return ObjContext.ToList<VentaReport>
                (
                    ObjContext.GetData(sqlQuery, new SqlParameter[]
                    {
                        new SqlParameter("@Desde",desde),
                        new SqlParameter("@Hastadsd",hasta),
                        new SqlParameter("@IdTienda",idTienda),
                    }).Tables[0]
                );
        }
    }
}
