﻿namespace Storage.Datos.Interfaces.Helper
{
    public interface ICorrelativo10D
    {
        string GetLastId(string table, string column);
    }
}
