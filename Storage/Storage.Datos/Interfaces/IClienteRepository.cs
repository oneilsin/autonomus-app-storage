﻿using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface IClienteRepository : IRepository<Cliente>
    {
        IEnumerable<ClienteReport> GetList();
        IEnumerable<ClienteNivelprecioReport> GetClienteNivelprecios();
        int Crud(Cliente entity, string option);
        Cliente GetById(string id);
    }
}
