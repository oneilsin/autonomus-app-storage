﻿using Storage.Datos.Interfaces.Helper;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface IEntradaRepository : IRepository<Entrada>
    {
        IEnumerable<EntradaReport> GetList(DateTime desde, DateTime hasta);
       
        /// <summary>
        /// Método para insertar Entras y Detalles, manejo de inventario.
        /// </summary>
        /// <param name="entity">Entidad de la clase Entrada.</param>
        /// <param name="inventario">Entidad de la clase Inventario.</param>
        /// <returns>Retorna x>0 si se ha instardo correctamente.</returns>
        int Crud(Entrada entity, Inventario inventario);
        string GetLastId();

        string Prueba();
    }
}
