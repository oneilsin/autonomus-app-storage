﻿using Storage.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface IFamiliaRepository : IRepository<Familia>
    {
        IEnumerable<Familia> GetList();
        int Crud(Familia entity, string option);
    }
}
