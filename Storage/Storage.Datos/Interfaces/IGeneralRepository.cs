﻿using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface IGeneralRepository : IRepository<General>
    {
        IEnumerable<UbigeoReport> GetUbigeoList();
        IEnumerable<GeneralReport> GetGeneralList(string numeroTabla);
        IEnumerable<TipoMovimiento> GetTipoMovimientoList();



        IEnumerable<PrecioNivel> GetPrecioNivels();
        IEnumerable<PrecioProducto> GetPrecios();

        IEnumerable<PrecioProductoDetail> GetPrecioProductoDetails(string idProducto);
        int UpdatePrecios(string id, decimal precio);
    }
}
