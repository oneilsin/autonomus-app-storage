﻿using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface IInventarioRepository : IRepository<Inventario>
    {
        /// <summary>
        /// Método para obtener los movimientos por entrada o salida.
        /// </summary>
        /// <param name="documento">Documento del movimiento, referencia: Id de la Entrada o Salida.</param>
        /// <param name="movimiento">Tipo del movimiento, referencia: "E" Entrada y "S" Salida.</param>
        /// <returns>Muestra el listado detallado según tipo de movimiento.</returns>
        IEnumerable<InventarioMovimiento> GetMovimientos(string documento, string movimiento);
        IEnumerable<InventarioMovimiento> GetDetalleOrden(string documento, string movimiento);
        IEnumerable<StockStore> GetStockByStore(string idTienda);


        List<StockDetail> GetStockDetails(string idTienda = "");

        /// <summary>
        /// Método para actualizar los precios de produtos que corresponda a una orden de venta... según el nivel de pecio.
        /// </summary>
        /// <param name="idNivel"></param>
        /// <param name="idOrden"></param>
        /// <returns></returns>
        int UpdateProductoPrecioporNivel(string idNivel, string idOrden,string idCliente);


    }
}
