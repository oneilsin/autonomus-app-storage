﻿using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface IMarcaRepository : IRepository<Marca>
    {
        IEnumerable<MarcaReport> GetList();
        int Crud(Marca entity, string option);
    }
}
