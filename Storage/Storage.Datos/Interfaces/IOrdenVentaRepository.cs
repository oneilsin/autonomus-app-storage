﻿using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface IOrdenVentaRepository : IRepository<OrdenVenta>
    {
        IEnumerable<OrdenVentaReport> GetList(DateTime desde, DateTime hasta
            , OrdenVenta orden = null);
        int Crud(OrdenVenta orden, Inventario inventario);
        string GetLastId();
    }
}
