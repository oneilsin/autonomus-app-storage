﻿using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface IProductoRepository : IRepository<Producto>
    {
        IEnumerable<ProductoReport> GetList();
        int Crud(Producto entity, string option);
    }
}
