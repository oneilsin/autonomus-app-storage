﻿using Storage.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface IProveedorRepository : IRepository<Proveedor>
    {
        IEnumerable<Proveedor> GetList();
        int Crud(Proveedor entity, string option);
    }
}
