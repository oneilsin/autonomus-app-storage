﻿using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface ISalidaRepository : IRepository<Salida>
    {
        IEnumerable<SalidaReport> GetList(DateTime desde, DateTime hasta);
        int Crud(Salida salida, Inventario inventario);
        string GetLastId();
    }
}
