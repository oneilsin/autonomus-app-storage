﻿using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface ISubCategoriaRepository : IRepository<SubCategoria>
    {
        IEnumerable<SubCategoriaReport> GetList();
        int Crud(SubCategoria entity, string option); // Insert, Update, Delete
    }
}
