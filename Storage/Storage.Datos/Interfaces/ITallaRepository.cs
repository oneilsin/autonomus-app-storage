﻿using Storage.Entidad;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface ITallaRepository : IRepository<Talla>
    {
        IEnumerable<Talla> GetList();
        int Crud(Talla entity, string option);
    }
}
