﻿using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface ITiendaRepository : IRepository<Tienda>
    {
        IEnumerable<TiendaReport> GetList();
        int Crud(Tienda entity, string option);
    }
}
