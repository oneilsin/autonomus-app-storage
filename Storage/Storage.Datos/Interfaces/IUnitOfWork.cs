﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        ICategoriaRepository Categorias { get; }
        ISubCategoriaRepository SubCategorias { get; }
        IFamiliaRepository Familias { get; }
        ITallaRepository Tallas { get; }
        IProveedorRepository Proveedors { get; }
        IMarcaRepository Marcas { get; }
        IGeneralRepository Generals { get; }
        IProductoRepository Productos { get; }
        IClienteRepository Clientes { get; }
        ITiendaRepository Tiendas { get; }
        
        IEntradaRepository Entrada { get; }
        ISalidaRepository Salida { get; }
        IInventarioRepository Inventario { get; }

        IOrdenVentaRepository OrdenVenta { get; }

        IVentaPagoRepository VentaPago { get; }
        IVentaRepository Venta { get; }
    }
}
