﻿using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface IVentaPagoRepository :IRepository<VentaPago>
    {
        IEnumerable<VentaPagoReport> GetDetail(string idVenta);
    }
}
