﻿using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos.Interfaces
{
    public interface IVentaRepository : IRepository<Venta>
    {
        IEnumerable<VentaReport> GetList(DateTime desde, DateTime hasta, string idTienda = "");
        int Crud(Venta venta, VentaPago pago);
        String GetLastId();

        string GetCorrelativoComprobante(string idCompobante);
    }
}
