﻿using Storage.Datos.AdoNet;
using Storage.Datos.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos
{
    public class Repository<T> : IRepository<T> where T : class, new()
    {
        protected readonly StorageContext ObjContext;
        public Repository(StorageContext storageContext)
        {
            this.ObjContext = storageContext;
        }

        public int Crud(string querySql, params SqlParameter[] sqlParameters)
        {
            try
            {
                return ObjContext.ExecuteNonQuery(querySql, System.Data.CommandType.StoredProcedure, sqlParameters);
            }
            catch (Exception ex)
            { throw ex; }
        }

        public int Delete(string querySql, params SqlParameter[] sqlParameters)
        {
            return ObjContext.ExecuteNonQuery(querySql, System.Data.CommandType.StoredProcedure, sqlParameters);
        }

        public IEnumerable<T> GetList(string querySql)
        {
            var ds = ObjContext.GetData(querySql);
            return ObjContext.ToList<T>(ds.Tables[0]);
        }

        public int Insert(string querySql, params SqlParameter[] sqlParameters)
        {
            return ObjContext.ExecuteNonQuery(querySql, System.Data.CommandType.StoredProcedure, sqlParameters);
        }

        public int Update(string querySql, params SqlParameter[] sqlParameters)
        {
            return ObjContext.ExecuteNonQuery(querySql, System.Data.CommandType.StoredProcedure, sqlParameters);
        }
    }
}
