﻿using Storage.Datos.AdoNet;
using Storage.Datos.Interfaces;
using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Datos
{
    public class UnitOfWork : IUnitOfWork
    {
        private string stringConnection = ConfigurationManager.ConnectionStrings["conexion"].ConnectionString;
        protected StorageContext ObjContext;
        public UnitOfWork()
        {
            this.ObjContext = new StorageContext(stringConnection);

            Categorias = new CategoriaRepository(ObjContext);
            SubCategorias = new SubCategoriaRepository(ObjContext);
            Familias = new FamiliaRepository(ObjContext);
            Tallas = new TallaRepository(ObjContext);
            Proveedors = new ProveedorRepository(ObjContext);
            Marcas = new MarcaRepository(ObjContext);
            Generals = new GeneralRepository(ObjContext);
            Productos = new ProductoRepository(ObjContext);
            Clientes = new ClienteRepository(ObjContext);
            Tiendas = new TiendaRepository(ObjContext);
            Entrada = new EntradaRepository(ObjContext);
            Salida = new SalidaRepository(ObjContext);
            Inventario = new InventarioRepository(ObjContext);
            OrdenVenta = new OrdenVentaRepository(ObjContext);
            Venta = new VentaRepository(ObjContext);
            VentaPago = new VentaPagoRepository(ObjContext);
        }

        public ICategoriaRepository Categorias { get; private set; }
        public ISubCategoriaRepository SubCategorias { get; private set; }
        public IFamiliaRepository Familias { get; private set; }
        public ITallaRepository Tallas { get; private set; }
        public IProveedorRepository Proveedors { get; private set; }
        public IMarcaRepository Marcas { get; private set; }
        public IGeneralRepository Generals { get; private set; }
        public IProductoRepository Productos { get; private set; }
        public IClienteRepository Clientes { get; private set; }
        public ITiendaRepository Tiendas { get; private set; }

        public IEntradaRepository Entrada { get; private set; }
        public ISalidaRepository Salida { get; private set; }
        public IInventarioRepository Inventario { get; private set; }
        public IOrdenVentaRepository OrdenVenta { get; private set; }

        public IVentaRepository Venta { get; private set; }
        public IVentaPagoRepository VentaPago { get; private set; }
        public void Dispose()
        {
            ObjContext.Dispose();
        }
    }
}
