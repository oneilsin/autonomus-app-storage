﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class Cliente : BaseEntitty
    {
        public string IdCliente { get; set; }
        public string IdTipoCliente { get; set; }
        public string IdTipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string CodigoCliente { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string RazonSocial { get; set; }
        public string Sexo { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Rol { get; set; }
        public string IdNivelPrecio { get; set; }
        public decimal LineaCredito { get; set; }
        public bool FlagCredito { get; set; }             
    }
}
