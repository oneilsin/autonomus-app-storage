﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class Entrada: BaseMovement
    {
        public string IdEntrada { get; set; }
        public string IdTienda { get; set; }
        public DateTime FechaEntrada { get; set; }
        public string IdProveedor { get; set; }
    }
}
