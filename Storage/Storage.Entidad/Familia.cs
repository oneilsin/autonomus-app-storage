﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class Familia : BaseEntitty
    {
        public string IdFamilia { get; set; }
        public string Descripcion { get; set; }
    }
}
