﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class Inventario
    {
        public string IdInventario { get; set; }
        public string IdTienda { get; set; }
        public DateTime FechaMovimiento { get; set; }
        public string IdTipoMov { get; set; }
        public string FlagMov { get; set; }
        public string IdProducto { get; set; }
        public decimal Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }
        public string DocMovimiento { get; set; }
        public string Condicion { get; set; }
    }
}
