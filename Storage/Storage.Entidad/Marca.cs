﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class Marca : BaseEntitty
    {
        public string IdMarca { get; set; }
        public string NombreMarca { get; set; }
        public string Descripcion { get; set; }
        public int Prioridad { get; set; }
        public string IdProveedor { get; set; }
    }
}
