﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class OrdenVenta : BaseEntitty
    {
        public string IdOrden { get; set; }
        public string IdTienda { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string IdVendedor { get; set; }
        public string IdCliente { get; set; }
        public string Situacion { get; set; }
        public DateTime? FechaDespacho { get; set; }
        public string IdDespachador { get; set; }
        public decimal ImporteParcial { get; set; }
        public string Observacion { get; set; }            
    }
}
