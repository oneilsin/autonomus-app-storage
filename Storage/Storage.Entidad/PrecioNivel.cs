﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class PrecioNivel
    {
        public string IdNivel { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
    }
}
