﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class PrecioProducto
    {
        public string IdPrecio { get; set; }
        public string IdProducto { get; set; }
        public string IdNivel { get; set; }
        public decimal Precio { get; set; }
        public bool Estado { get; set; }
    }
}
