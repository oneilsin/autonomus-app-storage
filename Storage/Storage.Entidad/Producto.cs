﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class Producto : BaseEntitty
    {
        public string IdProducto { get; set; }
        public string IdCategoria { get; set; }
        public string IdSubCategoria { get; set; }
        public string IdMarca { get; set; }
        public string IdTalla { get; set; }
        public string IdFamilia { get; set; }
        public string NombreProducto { get; set; }
        public string Descripcion { get; set; }
        public string Genero { get; set; }
        public string EnlaceWeb { get; set; }
        public string Sku { get; set; }
        public decimal StokMinimo { get; set; }
        public decimal PrecioBase { get; set; }
        public string CodigoBarra { get; set; }
    }
}
