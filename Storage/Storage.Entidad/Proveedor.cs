﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class Proveedor : BaseEntitty
    {
        public string IdProveedor { get; set; }
        public string NombreProveedor { get; set; }   
        public string TelefFijo { get; set; }
        public string TelefMovil { get; set; }
        public string Email { get; set; }
        public string Ubigeo { get; set; }
        public string Urbanizacion { get; set; }
        public string Direccion { get; set; }
    }
}
