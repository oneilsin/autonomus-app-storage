﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class ClienteNivelprecioReport
    {
        public string IdCliente { get; set; }
        public string NumeroDocumento { get; set; }
        public string CodigoCliente { get; set; }
        public string RazonSocial { get; set; }
        public string NivelPrecio { get; set; }
    }
}
