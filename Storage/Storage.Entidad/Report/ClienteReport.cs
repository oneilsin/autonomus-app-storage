﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class ClienteReport
    {
        public string IdCliente { get; set; }
        public string IdTipoCliente { get; set; }
        public string TipoCliente { get; set; }
        public string IdTipoDocumento { get; set; }
        public string TipoDocumento { get; set; }
        public string NumeroDocumento { get; set; }
        public string CodigoCliente { get; set; }
        public string Nombres { get; set; }
        public string Apellidos { get; set; }
        public string RazonSocial { get; set; }
        public string Sexo { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Rol { get; set; }
        public bool Estado { get; set; }
        public string EditorUser { get; set; }
        public string NivelPrecio { get; set; }
        public decimal LineaCredito { get; set; }
        public bool FlagCredito { get; set; }
    }
}
