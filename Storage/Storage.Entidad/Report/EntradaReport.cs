﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class EntradaReport
    {
        public string IdEntrada { get; set; }
        public string NombreTienda { get; set; }
        public DateTime FechaEntrada { get; set; }
        public string NombreProveedor { get; set; }
        public string Documento { get; set; }               
    }
}
