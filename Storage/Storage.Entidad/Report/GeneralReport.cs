﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class GeneralReport
    {
        public string IdTabla { get; set; }
        public string Descripcion { get; set; }
        public string Abreviatura { get; set; }
        public int Valor { get; set; }
        public int Dimension { get; set; }              
    }
}
