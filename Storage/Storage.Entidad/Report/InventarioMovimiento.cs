﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class InventarioMovimiento
    {
        public string IdInventario { get; set; }
        public string IdTipoMov { get; set; }
        public string TipoMovimiento { get; set; }
        public string Producto { get; set; }
        public decimal Cantidad { get; set; }
        public decimal PrecioUnitario { get; set; }                
    }
}
