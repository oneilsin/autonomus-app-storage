﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class MarcaReport
    {
        public string IdMarca { get; set; }
        public string NombreMarca { get; set; }
        public string Descripcion { get; set; }
        public int Prioridad { get; set; }
        public string IdProveedor { get; set; }
        public string NombreProveedor { get; set; }
        public bool Estado { get; set; }
        public string EditorUser { get; set; }
    }
}
