﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class OrdenVentaReport
    {
        public string IdOrden { get; set; }
        public string Tienda { get; set; }
        public DateTime FechaRegistro { get; set; }
        public string Vendedor { get; set; }
        public string IdCliente { get; set; }
        public string Cliente { get; set; }
        public string Situacion { get; set; }
        public DateTime? FechaDespacho { get; set; }
        public string Despachador { get; set; }
        public decimal ImporteParcial { get; set; }
    }
}
