﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class PrecioProductoDetail
    {
        public string IdPrecio { get; set; }
        public string NivelPrecio { get; set; }
        public decimal Precio { get; set; }
    }
}
