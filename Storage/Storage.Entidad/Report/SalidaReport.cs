﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class SalidaReport
    {
        public string IdSalida { get; set; }
        public string NombreTienda { get; set; }
        public DateTime FechaSalida { get; set; }
        public string Documento { get; set; }
    }
}
