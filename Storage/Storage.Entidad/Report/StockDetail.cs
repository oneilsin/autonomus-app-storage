﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class StockDetail
    {
        public string Tienda { get; set; }
        public string IdProducto { get; set; }
        public string Producto { get; set; }
        public string Categoria { get; set; }
        public string Marca { get; set; }
        public decimal PrecioBase { get; set; }
        public decimal StokMinimo { get; set; }
        public decimal StokFinal { get; set; }
        public string Descripcion { get; set; }
    }
}
