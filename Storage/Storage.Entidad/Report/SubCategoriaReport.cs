﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class SubCategoriaReport
    {
        public string IdSubCategoria { get; set; }
        public string IdCategoria { get; set; }
        public string NombreCategoria { get; set; }
        public string NombreSubCategoria { get; set; }
        public string Descripcion { get; set; }
        public bool Estado { get; set; }
        public string EditorUser { get; set; }
    }
}
