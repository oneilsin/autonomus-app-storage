﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class TiendaReport
    {
        public string IdTienda { get; set; }
        public string NombreTienda { get; set; }
        public string Descripcion { get; set; }
        public int IdResponsable { get; set; }
        public string RazonSocial { get; set; }
        public bool Consigna { get; set; }
        public string EditorUser { get; set; }
        public bool Estado { get; set; }
    }
}
