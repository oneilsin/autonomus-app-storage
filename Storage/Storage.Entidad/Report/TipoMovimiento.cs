﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class TipoMovimiento
    {
        public string IdTipoMov { get; set; }
        public string Descripcion { get; set; }
        public string Movimiento { get; set; }
    }
}
