﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class UbigeoReport
    {
        public string Ubigeo { get; set; }
        public string IdDepartamento { get; set; }
        public string Departamento { get; set; }
        public string IdProvincia { get; set; }
        public string Provincia { get; set; }
        public string Distrito { get; set; }
    }
}
