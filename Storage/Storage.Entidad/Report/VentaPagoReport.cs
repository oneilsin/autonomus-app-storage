﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class VentaPagoReport
    {
        public string IdPago { get; set; }
        public string IdVenta { get; set; }
        public string FormaPago { get; set; }
        public string MetodoPago { get; set; }
        public string NumOperacion { get; set; }
        public string Moneda { get; set; }
        public decimal Cambio { get; set; }
        public decimal Importe { get; set; }
    }
}
