﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad.Report
{
    public class VentaReport
    {
        public string IdVenta { get; set; }
        public string Tienda { get; set; }
        public string IdOrden { get; set; }
        public string Cliente { get; set; }
        public string Vendedor { get; set; }
        public DateTime FechaRegistrro { get; set; }
        public string Comprobante { get; set; }
        public string NumComprobante { get; set; }
        public string NivelPrecio { get; set; }
        public decimal ImporteTotal { get; set; }
        public string Observacion { get; set; }
        public bool Anulado { get; set; }
        public string MotivoAnulacion { get; set; }
    }
}
