﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class Salida: BaseMovement
    {
        public string IdSalida { get; set; }
        public string IdTienda { get; set; }
        public DateTime FechaSalida { get; set; }                
    }
}
