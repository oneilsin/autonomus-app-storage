﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class SubCategoria : BaseEntitty
    {
        public string IdSubCategoria { get; set; }
        public string NombreSubCategoria { get; set; }
        public string Descripcion { get; set; }
        public string IdCategoria { get; set; }
    }
}
