﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class Talla : BaseEntitty
    {
        public string IdTalla { get; set; }
        public string NombreTalla { get; set; }
        public string Descripcion { get; set; }
    }
}
