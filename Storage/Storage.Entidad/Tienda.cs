﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class Tienda : BaseEntitty
    {
        public string IdTienda { get; set; }
        public string NombreTienda { get; set; }
        public string Descripcion { get; set; }
        public int IdResponsable { get; set; }
        public bool Consigna { get; set; }
    }
}
