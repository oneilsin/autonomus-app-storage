﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class Venta : BaseEntitty
    {
        public string IdVenta { get; set; }
        public string IdTienda { get; set; }
        public string IdOrden { get; set; }
        public DateTime FechaRegistrro { get; set; }
        public string IdComprobante { get; set; }
        public string NumComprobante { get; set; }
        public string NivelPrecio { get; set; }
        public decimal ImporteTotal { get; set; }
        public string Observacion { get; set; }
        public bool Facturado { get; set; }
        public DateTime? FechaFacturado { get; set; }
        public string IdClienteFinal { get; set; }
        public string CorrelativoVenta { get; set; }
        public string CorrelativoBoleta { get; set; }
        public string CorrelativoFactura { get; set; }
        public string ComprobanteInterno { get; set; }
        public bool Anulado { get; set; }
        public string MotivoAnulacion { get; set; }
    }
}
