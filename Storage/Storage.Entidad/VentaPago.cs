﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Entidad
{
    public class VentaPago
    {
        public string IdPago { get; set; }
        public string IdVenta { get; set; }
        public string IdFormaPago { get; set; }
        public string MetodoPago { get; set; }
        public string NumOperacion { get; set; }
        public string Moneda { get; set; }
        public decimal Cambio { get; set; }
        public decimal Importe { get; set; }
        public bool Estado { get; set; }
    }
}
