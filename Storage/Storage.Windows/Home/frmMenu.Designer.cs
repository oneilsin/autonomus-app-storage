﻿namespace Storage.Windows.Home
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMenu));
            this.btnCategoria = new Autonomo.CustomControls.LabelButton();
            this.btnSubCategoria = new Autonomo.CustomControls.LabelButton();
            this.btnFamilia = new Autonomo.CustomControls.LabelButton();
            this.btnTallas = new Autonomo.CustomControls.LabelButton();
            this.btnMarca = new Autonomo.CustomControls.LabelButton();
            this.btnProducto = new Autonomo.CustomControls.LabelButton();
            this.btnTienda = new Autonomo.CustomControls.LabelButton();
            this.labelButton3 = new Autonomo.CustomControls.LabelButton();
            this.btnProveedor = new Autonomo.CustomControls.LabelButton();
            this.btnTransporte = new Autonomo.CustomControls.LabelButton();
            this.btnCliente = new Autonomo.CustomControls.LabelButton();
            this.btnEntrada = new Autonomo.CustomControls.LabelButton();
            this.btnSalida = new Autonomo.CustomControls.LabelButton();
            this.btnOrden = new Autonomo.CustomControls.LabelButton();
            this.btnVenta = new Autonomo.CustomControls.LabelButton();
            this.btnStock = new Autonomo.CustomControls.LabelButton();
            this.btnPrecioProducto = new Autonomo.CustomControls.LabelButton();
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).BeginInit();
            this.PanelVenta.SuspendLayout();
            this.PanelConfiguracion.SuspendLayout();
            this.PanelAlmacen.SuspendLayout();
            this.PanelCliente.SuspendLayout();
            this.PanelProveedor.SuspendLayout();
            this.PanelMovimiento.SuspendLayout();
            this.SuspendLayout();
            // 
            // UserDisplayText
            // 
            this.UserDisplayText.Text = "Bienvenido ";
            // 
            // PanelVenta
            // 
            this.PanelVenta.Controls.Add(this.btnStock);
            this.PanelVenta.Controls.Add(this.btnVenta);
            this.PanelVenta.Controls.Add(this.btnOrden);
            this.PanelVenta.Size = new System.Drawing.Size(212, 162);
            this.PanelVenta.Controls.SetChildIndex(this.btnOrden, 0);
            this.PanelVenta.Controls.SetChildIndex(this.btnVenta, 0);
            this.PanelVenta.Controls.SetChildIndex(this.btnStock, 0);
            // 
            // PanelConfiguracion
            // 
            this.PanelConfiguracion.Controls.Add(this.btnPrecioProducto);
            this.PanelConfiguracion.Location = new System.Drawing.Point(0, 1240);
            this.PanelConfiguracion.Size = new System.Drawing.Size(212, 167);
            this.PanelConfiguracion.Controls.SetChildIndex(this.btnPrecioProducto, 0);
            // 
            // PanelCompra
            // 
            this.PanelCompra.Location = new System.Drawing.Point(0, 465);
            this.PanelCompra.Size = new System.Drawing.Size(212, 168);
            // 
            // PanelAlmacen
            // 
            this.PanelAlmacen.Controls.Add(this.labelButton3);
            this.PanelAlmacen.Controls.Add(this.btnTienda);
            this.PanelAlmacen.Controls.Add(this.btnProducto);
            this.PanelAlmacen.Controls.Add(this.btnMarca);
            this.PanelAlmacen.Controls.Add(this.btnTallas);
            this.PanelAlmacen.Controls.Add(this.btnFamilia);
            this.PanelAlmacen.Controls.Add(this.btnSubCategoria);
            this.PanelAlmacen.Controls.Add(this.btnCategoria);
            this.PanelAlmacen.Location = new System.Drawing.Point(0, 793);
            this.PanelAlmacen.Size = new System.Drawing.Size(212, 395);
            this.PanelAlmacen.Controls.SetChildIndex(this.btnCategoria, 0);
            this.PanelAlmacen.Controls.SetChildIndex(this.btnSubCategoria, 0);
            this.PanelAlmacen.Controls.SetChildIndex(this.btnFamilia, 0);
            this.PanelAlmacen.Controls.SetChildIndex(this.btnTallas, 0);
            this.PanelAlmacen.Controls.SetChildIndex(this.btnMarca, 0);
            this.PanelAlmacen.Controls.SetChildIndex(this.btnProducto, 0);
            this.PanelAlmacen.Controls.SetChildIndex(this.btnTienda, 0);
            this.PanelAlmacen.Controls.SetChildIndex(this.labelButton3, 0);
            // 
            // PanelReporte
            // 
            this.PanelReporte.Location = new System.Drawing.Point(0, 1188);
            this.PanelReporte.Size = new System.Drawing.Size(212, 52);
            // 
            // PanelCliente
            // 
            this.PanelCliente.Controls.Add(this.btnCliente);
            this.PanelCliente.Location = new System.Drawing.Point(0, 214);
            this.PanelCliente.Size = new System.Drawing.Size(212, 117);
            this.PanelCliente.Controls.SetChildIndex(this.btnCliente, 0);
            // 
            // PanelProveedor
            // 
            this.PanelProveedor.Controls.Add(this.btnTransporte);
            this.PanelProveedor.Controls.Add(this.btnProveedor);
            this.PanelProveedor.Location = new System.Drawing.Point(0, 633);
            this.PanelProveedor.Size = new System.Drawing.Size(212, 160);
            this.PanelProveedor.Controls.SetChildIndex(this.btnProveedor, 0);
            this.PanelProveedor.Controls.SetChildIndex(this.btnTransporte, 0);
            // 
            // PanelCaja
            // 
            this.PanelCaja.Size = new System.Drawing.Size(212, 52);
            // 
            // SubContenedor
            // 
            this.SubContenedor.Size = new System.Drawing.Size(773, 804);
            // 
            // PanelMovimiento
            // 
            this.PanelMovimiento.Controls.Add(this.btnSalida);
            this.PanelMovimiento.Controls.Add(this.btnEntrada);
            this.PanelMovimiento.Location = new System.Drawing.Point(0, 331);
            this.PanelMovimiento.Size = new System.Drawing.Size(212, 134);
            this.PanelMovimiento.Controls.SetChildIndex(this.btnEntrada, 0);
            this.PanelMovimiento.Controls.SetChildIndex(this.btnSalida, 0);
            // 
            // btnCategoria
            // 
            this.btnCategoria.BackColor = System.Drawing.Color.Transparent;
            this.btnCategoria.ColorDetailText = System.Drawing.Color.Gray;
            this.btnCategoria.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnCategoria.DetailText = "Registro de datos";
            this.btnCategoria.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCategoria.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCategoria.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCategoria.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnCategoria.ImageDown")));
            this.btnCategoria.Location = new System.Drawing.Point(0, 44);
            this.btnCategoria.Name = "btnCategoria";
            this.btnCategoria.Size = new System.Drawing.Size(212, 42);
            this.btnCategoria.TabIndex = 5;
            this.btnCategoria.Text = "Categorías";
            this.btnCategoria.ButtonClick += new System.EventHandler(this.btnCategoria_ButtonClick);
            // 
            // btnSubCategoria
            // 
            this.btnSubCategoria.BackColor = System.Drawing.Color.Transparent;
            this.btnSubCategoria.ColorDetailText = System.Drawing.Color.Gray;
            this.btnSubCategoria.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnSubCategoria.DetailText = "Registro de datos";
            this.btnSubCategoria.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSubCategoria.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubCategoria.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSubCategoria.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnSubCategoria.ImageDown")));
            this.btnSubCategoria.Location = new System.Drawing.Point(0, 86);
            this.btnSubCategoria.Name = "btnSubCategoria";
            this.btnSubCategoria.Size = new System.Drawing.Size(212, 42);
            this.btnSubCategoria.TabIndex = 6;
            this.btnSubCategoria.Text = "Sub-Categoría";
            this.btnSubCategoria.ButtonClick += new System.EventHandler(this.btnSubCategoria_ButtonClick);
            // 
            // btnFamilia
            // 
            this.btnFamilia.BackColor = System.Drawing.Color.Transparent;
            this.btnFamilia.ColorDetailText = System.Drawing.Color.Gray;
            this.btnFamilia.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnFamilia.DetailText = "Registro de datos";
            this.btnFamilia.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnFamilia.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFamilia.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnFamilia.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnFamilia.ImageDown")));
            this.btnFamilia.Location = new System.Drawing.Point(0, 128);
            this.btnFamilia.Name = "btnFamilia";
            this.btnFamilia.Size = new System.Drawing.Size(212, 42);
            this.btnFamilia.TabIndex = 7;
            this.btnFamilia.Text = "Familia";
            this.btnFamilia.ButtonClick += new System.EventHandler(this.btnFamilia_ButtonClick);
            // 
            // btnTallas
            // 
            this.btnTallas.BackColor = System.Drawing.Color.Transparent;
            this.btnTallas.ColorDetailText = System.Drawing.Color.Gray;
            this.btnTallas.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnTallas.DetailText = "Registro de datos";
            this.btnTallas.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTallas.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTallas.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTallas.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnTallas.ImageDown")));
            this.btnTallas.Location = new System.Drawing.Point(0, 170);
            this.btnTallas.Name = "btnTallas";
            this.btnTallas.Size = new System.Drawing.Size(212, 42);
            this.btnTallas.TabIndex = 8;
            this.btnTallas.Text = "Tallas";
            this.btnTallas.ButtonClick += new System.EventHandler(this.btnTallas_ButtonClick);
            // 
            // btnMarca
            // 
            this.btnMarca.BackColor = System.Drawing.Color.Transparent;
            this.btnMarca.ColorDetailText = System.Drawing.Color.Gray;
            this.btnMarca.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnMarca.DetailText = "Registro de datos";
            this.btnMarca.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnMarca.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarca.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnMarca.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnMarca.ImageDown")));
            this.btnMarca.Location = new System.Drawing.Point(0, 212);
            this.btnMarca.Name = "btnMarca";
            this.btnMarca.Size = new System.Drawing.Size(212, 42);
            this.btnMarca.TabIndex = 9;
            this.btnMarca.Text = "Marca / Brands";
            this.btnMarca.ButtonClick += new System.EventHandler(this.btnMarca_ButtonClick);
            // 
            // btnProducto
            // 
            this.btnProducto.BackColor = System.Drawing.Color.Transparent;
            this.btnProducto.ColorDetailText = System.Drawing.Color.Gray;
            this.btnProducto.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnProducto.DetailText = "Registro de datos";
            this.btnProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProducto.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProducto.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProducto.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnProducto.ImageDown")));
            this.btnProducto.Location = new System.Drawing.Point(0, 254);
            this.btnProducto.Name = "btnProducto";
            this.btnProducto.Size = new System.Drawing.Size(212, 42);
            this.btnProducto.TabIndex = 10;
            this.btnProducto.Text = "Productos";
            this.btnProducto.ButtonClick += new System.EventHandler(this.btnProducto_ButtonClick);
            // 
            // btnTienda
            // 
            this.btnTienda.BackColor = System.Drawing.Color.Transparent;
            this.btnTienda.ColorDetailText = System.Drawing.Color.Gray;
            this.btnTienda.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnTienda.DetailText = "Registro de datos";
            this.btnTienda.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTienda.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTienda.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTienda.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnTienda.ImageDown")));
            this.btnTienda.Location = new System.Drawing.Point(0, 296);
            this.btnTienda.Name = "btnTienda";
            this.btnTienda.Size = new System.Drawing.Size(212, 42);
            this.btnTienda.TabIndex = 11;
            this.btnTienda.Text = "Tienda / Local";
            this.btnTienda.ButtonClick += new System.EventHandler(this.btnTienda_ButtonClick);
            // 
            // labelButton3
            // 
            this.labelButton3.BackColor = System.Drawing.Color.Transparent;
            this.labelButton3.ColorDetailText = System.Drawing.Color.Gray;
            this.labelButton3.ColorText = System.Drawing.SystemColors.ControlText;
            this.labelButton3.DetailText = "Description button";
            this.labelButton3.Dock = System.Windows.Forms.DockStyle.Top;
            this.labelButton3.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelButton3.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelButton3.ImageDown = ((System.Drawing.Image)(resources.GetObject("labelButton3.ImageDown")));
            this.labelButton3.Location = new System.Drawing.Point(0, 338);
            this.labelButton3.Name = "labelButton3";
            this.labelButton3.Size = new System.Drawing.Size(212, 42);
            this.labelButton3.TabIndex = 12;
            this.labelButton3.Text = "labelButton3";
            this.labelButton3.ButtonClick += new System.EventHandler(this.labelButton3_ButtonClick);
            // 
            // btnProveedor
            // 
            this.btnProveedor.BackColor = System.Drawing.Color.Transparent;
            this.btnProveedor.ColorDetailText = System.Drawing.Color.Gray;
            this.btnProveedor.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnProveedor.DetailText = "Registro de datos";
            this.btnProveedor.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProveedor.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProveedor.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnProveedor.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnProveedor.ImageDown")));
            this.btnProveedor.Location = new System.Drawing.Point(0, 44);
            this.btnProveedor.Name = "btnProveedor";
            this.btnProveedor.Size = new System.Drawing.Size(212, 42);
            this.btnProveedor.TabIndex = 11;
            this.btnProveedor.Text = "Proveedores";
            this.btnProveedor.ButtonClick += new System.EventHandler(this.btnProveedor_ButtonClick);
            // 
            // btnTransporte
            // 
            this.btnTransporte.BackColor = System.Drawing.Color.Transparent;
            this.btnTransporte.ColorDetailText = System.Drawing.Color.Gray;
            this.btnTransporte.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnTransporte.DetailText = "Registro de datos";
            this.btnTransporte.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnTransporte.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransporte.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransporte.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnTransporte.ImageDown")));
            this.btnTransporte.Location = new System.Drawing.Point(0, 86);
            this.btnTransporte.Name = "btnTransporte";
            this.btnTransporte.Size = new System.Drawing.Size(212, 42);
            this.btnTransporte.TabIndex = 12;
            this.btnTransporte.Text = "Transporte";
            this.btnTransporte.ButtonClick += new System.EventHandler(this.btnTransporte_ButtonClick);
            // 
            // btnCliente
            // 
            this.btnCliente.BackColor = System.Drawing.Color.Transparent;
            this.btnCliente.ColorDetailText = System.Drawing.Color.Gray;
            this.btnCliente.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnCliente.DetailText = "Registro de datos";
            this.btnCliente.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCliente.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCliente.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCliente.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnCliente.ImageDown")));
            this.btnCliente.Location = new System.Drawing.Point(0, 44);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(212, 42);
            this.btnCliente.TabIndex = 13;
            this.btnCliente.Text = "Clientes";
            this.btnCliente.ButtonClick += new System.EventHandler(this.btnCliente_ButtonClick);
            // 
            // btnEntrada
            // 
            this.btnEntrada.BackColor = System.Drawing.Color.Transparent;
            this.btnEntrada.ColorDetailText = System.Drawing.Color.Gray;
            this.btnEntrada.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnEntrada.DetailText = "Registro de datos";
            this.btnEntrada.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnEntrada.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEntrada.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEntrada.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnEntrada.ImageDown")));
            this.btnEntrada.Location = new System.Drawing.Point(0, 44);
            this.btnEntrada.Name = "btnEntrada";
            this.btnEntrada.Size = new System.Drawing.Size(212, 42);
            this.btnEntrada.TabIndex = 14;
            this.btnEntrada.Text = "Entrada";
            this.btnEntrada.ButtonClick += new System.EventHandler(this.btnEntrada_ButtonClick);
            // 
            // btnSalida
            // 
            this.btnSalida.BackColor = System.Drawing.Color.Transparent;
            this.btnSalida.ColorDetailText = System.Drawing.Color.Gray;
            this.btnSalida.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnSalida.DetailText = "Registro de datos";
            this.btnSalida.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSalida.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalida.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSalida.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnSalida.ImageDown")));
            this.btnSalida.Location = new System.Drawing.Point(0, 86);
            this.btnSalida.Name = "btnSalida";
            this.btnSalida.Size = new System.Drawing.Size(212, 42);
            this.btnSalida.TabIndex = 15;
            this.btnSalida.Text = "Salida";
            this.btnSalida.ButtonClick += new System.EventHandler(this.btnSalida_ButtonClick);
            // 
            // btnOrden
            // 
            this.btnOrden.BackColor = System.Drawing.Color.Transparent;
            this.btnOrden.ColorDetailText = System.Drawing.Color.Gray;
            this.btnOrden.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnOrden.DetailText = "Registro de datos";
            this.btnOrden.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnOrden.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrden.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnOrden.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnOrden.ImageDown")));
            this.btnOrden.Location = new System.Drawing.Point(0, 44);
            this.btnOrden.Name = "btnOrden";
            this.btnOrden.Size = new System.Drawing.Size(212, 42);
            this.btnOrden.TabIndex = 14;
            this.btnOrden.Text = "Orden-Venta";
            this.btnOrden.ButtonClick += new System.EventHandler(this.btnOrden_ButtonClick);
            // 
            // btnVenta
            // 
            this.btnVenta.BackColor = System.Drawing.Color.Transparent;
            this.btnVenta.ColorDetailText = System.Drawing.Color.Gray;
            this.btnVenta.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnVenta.DetailText = "Registro de datos";
            this.btnVenta.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnVenta.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVenta.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnVenta.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnVenta.ImageDown")));
            this.btnVenta.Location = new System.Drawing.Point(0, 86);
            this.btnVenta.Name = "btnVenta";
            this.btnVenta.Size = new System.Drawing.Size(212, 42);
            this.btnVenta.TabIndex = 15;
            this.btnVenta.Text = "Gestionar Ventas";
            this.btnVenta.ButtonClick += new System.EventHandler(this.btnVenta_ButtonClick);
            // 
            // btnStock
            // 
            this.btnStock.BackColor = System.Drawing.Color.Transparent;
            this.btnStock.ColorDetailText = System.Drawing.Color.Gray;
            this.btnStock.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnStock.DetailText = "Detale de datos";
            this.btnStock.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnStock.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStock.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStock.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnStock.ImageDown")));
            this.btnStock.Location = new System.Drawing.Point(0, 128);
            this.btnStock.Name = "btnStock";
            this.btnStock.Size = new System.Drawing.Size(212, 42);
            this.btnStock.TabIndex = 16;
            this.btnStock.Text = "Stock";
            this.btnStock.ButtonClick += new System.EventHandler(this.btnStock_ButtonClick);
            // 
            // btnPrecioProducto
            // 
            this.btnPrecioProducto.BackColor = System.Drawing.Color.Transparent;
            this.btnPrecioProducto.ColorDetailText = System.Drawing.Color.Gray;
            this.btnPrecioProducto.ColorText = System.Drawing.SystemColors.ControlText;
            this.btnPrecioProducto.DetailText = "Actualizar precios";
            this.btnPrecioProducto.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPrecioProducto.FontDetailText = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrecioProducto.FontText = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrecioProducto.ImageDown = ((System.Drawing.Image)(resources.GetObject("btnPrecioProducto.ImageDown")));
            this.btnPrecioProducto.Location = new System.Drawing.Point(0, 44);
            this.btnPrecioProducto.Name = "btnPrecioProducto";
            this.btnPrecioProducto.Size = new System.Drawing.Size(212, 42);
            this.btnPrecioProducto.TabIndex = 11;
            this.btnPrecioProducto.Text = "Precio Productos";
            this.btnPrecioProducto.ButtonClick += new System.EventHandler(this.btnPrecioProducto_ButtonClick);
            // 
            // frmMenu
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(1042, 870);
            this.Name = "frmMenu";
            this.Text = "frmMenu";
            this.Load += new System.EventHandler(this.frmMenu_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Logo)).EndInit();
            this.PanelVenta.ResumeLayout(false);
            this.PanelConfiguracion.ResumeLayout(false);
            this.PanelAlmacen.ResumeLayout(false);
            this.PanelCliente.ResumeLayout(false);
            this.PanelProveedor.ResumeLayout(false);
            this.PanelMovimiento.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Autonomo.CustomControls.LabelButton btnMarca;
        private Autonomo.CustomControls.LabelButton btnTallas;
        private Autonomo.CustomControls.LabelButton btnFamilia;
        private Autonomo.CustomControls.LabelButton btnSubCategoria;
        private Autonomo.CustomControls.LabelButton btnCategoria;
        private Autonomo.CustomControls.LabelButton labelButton3;
        private Autonomo.CustomControls.LabelButton btnTienda;
        private Autonomo.CustomControls.LabelButton btnProducto;
        private Autonomo.CustomControls.LabelButton btnProveedor;
        private Autonomo.CustomControls.LabelButton btnTransporte;
        private Autonomo.CustomControls.LabelButton btnCliente;
        private Autonomo.CustomControls.LabelButton btnSalida;
        private Autonomo.CustomControls.LabelButton btnEntrada;
        private Autonomo.CustomControls.LabelButton btnStock;
        private Autonomo.CustomControls.LabelButton btnVenta;
        private Autonomo.CustomControls.LabelButton btnOrden;
        private Autonomo.CustomControls.LabelButton btnPrecioProducto;
    }
}