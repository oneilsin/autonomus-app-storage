﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Storage.Windows.Home
{
    public partial class frmMenu : Autonomo.Object.MenuLeftPanel
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void frmMenu_Load(object sender, EventArgs e)
        {
            ThemeStyle(Theme.BlueDark);
           // new w() { ParentForm=this }.Show();
        }

        private void btnCategoria_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Almacen.frmCategoria();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }

        private void btnSubCategoria_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Almacen.frmSubCategoria();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }

        private void btnFamilia_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Almacen.frmFamilia();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }

        private void btnProveedor_ButtonClick(object sender, EventArgs e)
        {
            //var f = new Modulo.Proveedor.frmProveedor();
            var f = new Modulo.Clientes.frmCliente();
            f.Rol = "Proveedor";
            f.Title.Text = "Registro de Proveedores";
            //Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
            Autonomo.Class.Fomulary.ShowNewFormInPanel(f, this, SubContenedor);
        }

        private void btnTallas_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Almacen.frmTalla();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }

        private void btnMarca_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Almacen.frmMarca();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }

        private void btnTransporte_ButtonClick(object sender, EventArgs e)
        {

        }

        private void btnProducto_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Almacen.frmProducto();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }

        private void btnCliente_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Clientes.frmCliente();
            f.Rol = "Cliente";
            f.Title.Text = "Registro de Clientes";
            //Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
            Autonomo.Class.Fomulary.ShowNewFormInPanel(f, this, SubContenedor);
        }

        private void labelButton3_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Proveedor.frmProveedor();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }
        private void btnTienda_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Almacen.frmTienda();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }

        private void btnEntrada_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Movimiento.frmEntrada();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }

        private void btnSalida_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Movimiento.frmSalida();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }

        private void btnStock_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Ventas.Stock.frmStockDetalle();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }

        private void btnOrden_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Ventas.Orden.frmOrdenVenta();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }

        private void btnVenta_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Ventas.Gestion.frmVenta();
            f.ParentForm = this;
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);

        }

        private void btnPrecioProducto_ButtonClick(object sender, EventArgs e)
        {
            var f = new Modulo.Configuracion.frmPrecioProducto();
            Autonomo.Class.Fomulary.ShowFormInPanel(f, SubContenedor);
        }
    }
}
