﻿
namespace Storage.Windows.Home
{
    partial class w
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // w
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(355, 343);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "w";
            this.Opacity = 0.6D;
            this.Text = "Home - Sistema de ventas";
            this.TransparencyKey = System.Drawing.Color.Black;
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.MaximumSizeChanged += new System.EventHandler(this.w_MaximumSizeChanged);
            this.MinimumSizeChanged += new System.EventHandler(this.w_MinimumSizeChanged);
            this.Enter += new System.EventHandler(this.w_Enter);
            this.ResumeLayout(false);

        }

        #endregion
    }
}