﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Home
{
    public partial class w : Form
    {
        public new Form ParentForm;
        public w()
        {
            InitializeComponent();
        }

        void Tasking()
        {
            var f = (frmMenu)this.ParentForm;
            f.WindowState = FormWindowState.Maximized;
        }
        private void w_MinimumSizeChanged(object sender, EventArgs e)
        {
            MessageBox.Show("#");
            Tasking();
        }

        private void w_MaximumSizeChanged(object sender, EventArgs e)
        {
            MessageBox.Show("#");
            Tasking();
        }
        private void w_Enter(object sender, EventArgs e)
        {
            MessageBox.Show("#");
            Tasking();
        }
    }
}
