﻿using Storage.Datos;
using Storage.Entidad;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmCategoria : Autonomo.Object.Registry
    {
        List<Categoria> categorias;
        public frmCategoria()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                categorias = new List<Categoria>();
                categorias = uow.Categorias.GetList().ToList();
                LoadFilter();
            }
        }

        private void LoadFilter()
        {
            if (categorias != null && categorias.Count() > 0)
            {
                //LinQ
                var newList = from o in categorias
                              select new
                              {
                                  IdCategoria = o.IdCategoria,
                                  NombreCategoria = o.NombreCategoria,
                                  Prioridad = o.Prioridad,
                                  Descripcion = o.Descripcion
                              };

                grdData.DataSource = newList
                    .Where(o => o.NombreCategoria.Contains(txFilter.Text))
                    .ToList();
                //  select * from Categoria Where NombreCtg like'%'+@filtro+'%'
            }
        }

        private void LoadModal(string opcion, string title)
        {
            var f = new frmCategoriaModal();
            f.Title.Text = title;
            if (opcion == "Update")
            {
                string id = grdData.CurrentRow.Cells["IdCategoria"].Value.ToString();
                var ctg = categorias.FirstOrDefault(o => o.IdCategoria.Equals(id));
                //SQL: select * from Categoria where IdCategoria='id'
                if (ctg != null)
                {
                    f.LoadData(ctg.IdCategoria,
                        ctg.NombreCategoria,
                        ctg.Descripcion,
                        ctg.Prioridad.ToString());
                }
            }
            Autonomo.Class.Fomulary.ShowModal(f, opcion, false);
            if (f.Tag.ToString() == "Get")
            {
                LoadData();
            }
        }

        private void frmCategoria_Load(object sender, EventArgs e)
        {
            ThemeStyle(Theme.White);
            LoadData();
        }

        private void txFilter_TextBoxChanged(object sender, EventArgs e)
        {
            LoadFilter();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LoadModal("Insert", "Registro de Categoría");
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (grdData.Rows.Count > 0)
                LoadModal("Update", "Actualizar Categoría");
        }
    }
}
