﻿using Storage.Datos;
using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmCategoriaModal : Autonomo.Object.Modal
    {
        private string IdCategoria;
        public frmCategoriaModal()
        {
            InitializeComponent();
            this.IdCategoria = string.Empty;

        }

        public void LoadData(string idCategoria,string nombre,
            string descripcion, string prioridad)
        {
            this.IdCategoria = idCategoria;
            txNombre.Text = nombre;
            txDescripcion.Text = descripcion;
            txPrioridad.Text = prioridad;
        }

        private void SaveChange()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                string option = this.Tag.ToString();

                var result = uow.Categorias.Crud("General.SpCategoriaMaintenace",
                    new SqlParameter[]
                    {
                        new SqlParameter("@IdCategoria",IdCategoria),
                        new SqlParameter("@NombreCategoria",txNombre.Text.Trim()),
                        new SqlParameter("@Descripcion",txDescripcion.Text.Trim()),
                        new SqlParameter("@Prioridad",Convert.ToInt32(txPrioridad.Text)),
                        new SqlParameter("@EditorUser",Program.Usuario),
                        new SqlParameter("@Option",option),
                    });

                if (result > 0)
                {
                    base.Set();
                }
            }
        }


        private void frmCategoriaModal_Load(object sender, EventArgs e)
        {
            base.ConfigFormulary();
            //base.ConfigButton();
        }

        private void txPrioridad_KeyPress(object sender, KeyPressEventArgs e)
        {
            Autonomo.Class.Validating.OnlyNumber(e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChange();
        }
    }
}
