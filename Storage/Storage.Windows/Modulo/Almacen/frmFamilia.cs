﻿using Storage.Datos;
using Storage.Entidad;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmFamilia : Autonomo.Object.Registry
    {
        List<Familia> familias;
        public frmFamilia()
        {
            InitializeComponent();
        }
        public void LoadData()
        {
            using (UnitOfWork uow= new UnitOfWork())
            {
                familias = new List<Familia>();
                familias = uow.Familias.GetList().ToList();
                FilterData();
            }
        }
        public void FilterData()
        {
            if(familias!=null && familias.Count() > 0)
            {
                var newList = from o in familias
                              select new {
                                  IdFamilia = o.IdFamilia,
                                  Descripcion = o.Descripcion
                              };
                grdData.DataSource = newList
                    .Where(o => o.Descripcion.Contains(txFilter.Text))
                    .OrderBy(o => o.Descripcion)
                    .ToList();
            }
        }

        private void LoadModal(string option, string title)
        {
            var f = new frmFamiliaModal();
            f.Title.Text = title;
            if (option == "Update")
            {
                string id = grdData.CurrentRow.Cells["IdFamilia"].Value.ToString();
                var obj = familias.FirstOrDefault(o => o.IdFamilia.Equals(id));
                if(obj!=null)
                {
                    f.LoadData(
                        obj.IdFamilia,
                        obj.Descripcion
                        );
                }                
            }
            Autonomo.Class.Fomulary.ShowModal(f, option, false);
            if (f.Tag.ToString() == "Get") { LoadData(); }
        }

        private void frmFamilia_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void txFilter_TextBoxChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LoadModal("Insert", "Registrar nueva Familia");
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if(grdData.Rows.Count>0)
                LoadModal("Update", "Actualizar Familia");
        }
    }
}
