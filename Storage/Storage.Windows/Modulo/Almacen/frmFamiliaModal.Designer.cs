﻿namespace Storage.Windows.Modulo.Almacen
{
    partial class frmFamiliaModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmFamiliaModal));
            this.txDescripcion = new Autonomo.CustomControls.FlatTextBox();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Size = new System.Drawing.Size(319, 195);
            // 
            // Body
            // 
            this.Body.Controls.Add(this.txDescripcion);
            this.Body.Size = new System.Drawing.Size(319, 102);
            this.Body.TabIndex = 0;
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 143);
            this.Footer.Size = new System.Drawing.Size(319, 52);
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(319, 41);
            // 
            // Title
            // 
            this.Title.Size = new System.Drawing.Size(278, 41);
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txDescripcion
            // 
            this.txDescripcion.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txDescripcion.BackColor = System.Drawing.Color.White;
            this.txDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txDescripcion.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txDescripcion.ColorLine = System.Drawing.Color.Gray;
            this.txDescripcion.ColorText = System.Drawing.SystemColors.WindowText;
            this.txDescripcion.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txDescripcion.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txDescripcion.Error = "";
            this.txDescripcion.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txDescripcion.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txDescripcion.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txDescripcion.ImageIcon")));
            this.txDescripcion.Info = "";
            this.txDescripcion.Location = new System.Drawing.Point(12, 24);
            this.txDescripcion.MaterialStyle = true;
            this.txDescripcion.MaxLength = 100;
            this.txDescripcion.MultiLineText = false;
            this.txDescripcion.Name = "txDescripcion";
            this.txDescripcion.PasswordChar = '\0';
            this.txDescripcion.Placeholder = "Descripción de la Familia";
            this.txDescripcion.ReadOnly = false;
            this.txDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txDescripcion.Size = new System.Drawing.Size(278, 58);
            this.txDescripcion.SizeLine = 2;
            this.txDescripcion.TabIndex = 0;
            this.txDescripcion.Title = "Descripción de la Familia";
            this.txDescripcion.VisibleIcon = true;
            this.txDescripcion.VisibleTitle = false;
            // 
            // frmFamiliaModal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(319, 195);
            this.Name = "frmFamiliaModal";
            this.Text = "frmFamiliaModal";
            this.Load += new System.EventHandler(this.frmFamiliaModal_Load);
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Autonomo.CustomControls.FlatTextBox txDescripcion;
    }
}