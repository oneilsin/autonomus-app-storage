﻿using Storage.Datos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmFamiliaModal : Autonomo.Object.Modal
    {
        private string IdFamilia;
        public frmFamiliaModal()
        {
            InitializeComponent();
            this.IdFamilia = string.Empty;
        }

        public void LoadData(string idFamilia, string descripcion)
        {
            this.IdFamilia = idFamilia;
            txDescripcion.Text = descripcion;
        }
        private void SaveChange()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                string option = this.Tag.ToString();

                var result = uow.Familias.Crud(
                    new Entidad.Familia()
                    {
                        IdFamilia = this.IdFamilia,
                        Descripcion = txDescripcion.Text.Trim(),
                        EditorUser = Program.Usuario
                    }, option);
                if (result > 0) { base.Set(); }
            }
        }
        private void frmFamiliaModal_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChange();
        }
    }
}
