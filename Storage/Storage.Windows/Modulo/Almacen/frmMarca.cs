﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmMarca : Autonomo.Object.Registry
    {
        List<MarcaReport> marcas;
        public frmMarca()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                marcas = new List<MarcaReport>();
                marcas = uow.Marcas.GetList().ToList();
                LoadFilter();
            }
        }

        private void LoadFilter()
        {
            if(marcas!=null && marcas.Count() > 0)
            {
                var newList = from o in marcas
                              orderby o.NombreMarca
                              select new
                              {
                                  IdMarca = o.IdMarca,
                                  NombreMarca = o.NombreMarca,
                                  Descripcion = o.Descripcion,
                                  Prioridad = o.Prioridad,
                                  NombreProveedor = o.NombreProveedor
                              };
                grdData.DataSource = newList
                    .Where(o => o.NombreMarca.Contains(txFilter.Text))
                    .ToList();
            }
        }

        private void LoadModal(string option, string title)
        {
            var f = new frmMarcaModal();
            f.Title.Text = title;
            f.LoadCombo();
            if (option == "Update")
            {
                string id = grdData.CurrentRow.Cells["IdMarca"].Value.ToString();
                var obj = marcas.FirstOrDefault(o => o.IdMarca.Equals(id));
                if (obj != null)
                {
                    f.LoadData(obj.IdMarca, obj.IdProveedor, obj.NombreMarca
                        , obj.Descripcion, obj.Prioridad.ToString());
                }
            }
            Autonomo.Class.Fomulary.ShowModal(f, option, false);
            if (f.Tag.ToString() == "Get")
            {
                LoadData();
            }
        }
        private void frmMarca_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void txFilter_TextBoxChanged(object sender, EventArgs e)
        {
            LoadFilter();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LoadModal("Insert", "Registrar nueva Marca");
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if(grdData.Rows.Count>0)
                LoadModal("Update", "Actualizar nueva Marca");
        }
    }
}
