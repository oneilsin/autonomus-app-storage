﻿namespace Storage.Windows.Modulo.Almacen
{
    partial class frmMarcaModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMarcaModal));
            this.txPrioridad = new Autonomo.CustomControls.FlatTextBox();
            this.txDescripcion = new Autonomo.CustomControls.FlatTextBox();
            this.txNombre = new Autonomo.CustomControls.FlatTextBox();
            this.cbProveedor = new Autonomo.CustomControls.FlatComboBox();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Size = new System.Drawing.Size(319, 382);
            // 
            // Body
            // 
            this.Body.Controls.Add(this.cbProveedor);
            this.Body.Controls.Add(this.txPrioridad);
            this.Body.Controls.Add(this.txDescripcion);
            this.Body.Controls.Add(this.txNombre);
            this.Body.Size = new System.Drawing.Size(319, 289);
            this.Body.TabIndex = 0;
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 330);
            this.Footer.Size = new System.Drawing.Size(319, 52);
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(319, 41);
            // 
            // Title
            // 
            this.Title.Size = new System.Drawing.Size(278, 41);
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txPrioridad
            // 
            this.txPrioridad.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txPrioridad.BackColor = System.Drawing.Color.White;
            this.txPrioridad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txPrioridad.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txPrioridad.ColorLine = System.Drawing.Color.Gray;
            this.txPrioridad.ColorText = System.Drawing.SystemColors.WindowText;
            this.txPrioridad.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txPrioridad.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txPrioridad.Error = "";
            this.txPrioridad.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txPrioridad.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txPrioridad.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txPrioridad.ImageIcon")));
            this.txPrioridad.Info = "";
            this.txPrioridad.Location = new System.Drawing.Point(12, 212);
            this.txPrioridad.MaterialStyle = true;
            this.txPrioridad.MaxLength = 1;
            this.txPrioridad.MultiLineText = false;
            this.txPrioridad.Name = "txPrioridad";
            this.txPrioridad.PasswordChar = '\0';
            this.txPrioridad.Placeholder = "Prioridad";
            this.txPrioridad.ReadOnly = false;
            this.txPrioridad.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txPrioridad.Size = new System.Drawing.Size(278, 58);
            this.txPrioridad.SizeLine = 2;
            this.txPrioridad.TabIndex = 3;
            this.txPrioridad.Title = "Prioridad";
            this.txPrioridad.VisibleIcon = true;
            this.txPrioridad.VisibleTitle = false;
            this.txPrioridad.KeyPress += new System.EventHandler<System.Windows.Forms.KeyPressEventArgs>(this.txPrioridad_KeyPress);
            // 
            // txDescripcion
            // 
            this.txDescripcion.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txDescripcion.BackColor = System.Drawing.Color.White;
            this.txDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txDescripcion.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txDescripcion.ColorLine = System.Drawing.Color.Gray;
            this.txDescripcion.ColorText = System.Drawing.SystemColors.WindowText;
            this.txDescripcion.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txDescripcion.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txDescripcion.Error = "";
            this.txDescripcion.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txDescripcion.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txDescripcion.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txDescripcion.ImageIcon")));
            this.txDescripcion.Info = "";
            this.txDescripcion.Location = new System.Drawing.Point(12, 148);
            this.txDescripcion.MaterialStyle = true;
            this.txDescripcion.MaxLength = 100;
            this.txDescripcion.MultiLineText = false;
            this.txDescripcion.Name = "txDescripcion";
            this.txDescripcion.PasswordChar = '\0';
            this.txDescripcion.Placeholder = "Descripción de la Marca";
            this.txDescripcion.ReadOnly = false;
            this.txDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txDescripcion.Size = new System.Drawing.Size(278, 58);
            this.txDescripcion.SizeLine = 2;
            this.txDescripcion.TabIndex = 2;
            this.txDescripcion.Title = "Descripción de la Marca";
            this.txDescripcion.VisibleIcon = true;
            this.txDescripcion.VisibleTitle = false;
            // 
            // txNombre
            // 
            this.txNombre.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txNombre.BackColor = System.Drawing.Color.White;
            this.txNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txNombre.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txNombre.ColorLine = System.Drawing.Color.Gray;
            this.txNombre.ColorText = System.Drawing.SystemColors.WindowText;
            this.txNombre.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txNombre.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txNombre.Error = "";
            this.txNombre.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txNombre.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txNombre.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txNombre.ImageIcon")));
            this.txNombre.Info = "";
            this.txNombre.Location = new System.Drawing.Point(12, 84);
            this.txNombre.MaterialStyle = true;
            this.txNombre.MaxLength = 50;
            this.txNombre.MultiLineText = false;
            this.txNombre.Name = "txNombre";
            this.txNombre.PasswordChar = '\0';
            this.txNombre.Placeholder = "Nombre de la Marca";
            this.txNombre.ReadOnly = false;
            this.txNombre.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txNombre.Size = new System.Drawing.Size(278, 58);
            this.txNombre.SizeLine = 2;
            this.txNombre.TabIndex = 1;
            this.txNombre.Title = "Nombre de la Marca";
            this.txNombre.VisibleIcon = true;
            this.txNombre.VisibleTitle = false;
            // 
            // cbProveedor
            // 
            this.cbProveedor.BackColor = System.Drawing.Color.White;
            this.cbProveedor.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbProveedor.ColorLine = System.Drawing.Color.Gray;
            this.cbProveedor.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbProveedor.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbProveedor.DisplayMember = "";
            this.cbProveedor.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbProveedor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProveedor.Error = "";
            this.cbProveedor.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbProveedor.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbProveedor.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbProveedor.ImageIcon")));
            this.cbProveedor.Info = "";
            this.cbProveedor.Location = new System.Drawing.Point(12, 20);
            this.cbProveedor.MaterialStyle = true;
            this.cbProveedor.Name = "cbProveedor";
            this.cbProveedor.Placeholder = "Seleccione el Proveedor";
            this.cbProveedor.SelectedIndex = -1;
            this.cbProveedor.Size = new System.Drawing.Size(278, 58);
            this.cbProveedor.SizeLine = 2;
            this.cbProveedor.TabIndex = 0;
            this.cbProveedor.Title = "Seleccione el Proveedor";
            this.cbProveedor.ValueMember = "";
            this.cbProveedor.VisibleIcon = true;
            this.cbProveedor.VisibleTitle = false;
            // 
            // frmMarcaModal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(319, 382);
            this.Name = "frmMarcaModal";
            this.Text = "frmMarcaModal";
            this.Load += new System.EventHandler(this.frmMarcaModal_Load);
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Autonomo.CustomControls.FlatTextBox txPrioridad;
        private Autonomo.CustomControls.FlatTextBox txDescripcion;
        private Autonomo.CustomControls.FlatTextBox txNombre;
        private Autonomo.CustomControls.FlatComboBox cbProveedor;
    }
}