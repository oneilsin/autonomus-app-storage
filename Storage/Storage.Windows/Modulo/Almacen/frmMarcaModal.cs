﻿using Storage.Datos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmMarcaModal : Autonomo.Object.Modal
    {
        private string IdMarca;
        public frmMarcaModal()
        {
            InitializeComponent();
            this.IdMarca = string.Empty;
        }
        public void LoadCombo()
        {
            Tools.ComboBoxHelper.ComboBoxProveedor(cbProveedor);
        }
        public void LoadData(string id, string proveedor, string nombre,
            string descripcion, string prioridad)
        {
            this.IdMarca = id;
            cbProveedor.SelectedValue = proveedor;
            txNombre.Text = nombre;
            txDescripcion.Text = descripcion;
            txPrioridad.Text = prioridad;
        }

        private void SaveChange()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                string option = this.Tag.ToString();

                var result = uow.Marcas.Crud(
                    new Entidad.Marca()
                    {
                        IdMarca = this.IdMarca,
                        NombreMarca = txNombre.Text,
                        Descripcion = txDescripcion.Text,
                        Prioridad = int.Parse(txPrioridad.Text),
                        IdProveedor = cbProveedor.SelectedValue.ToString(),
                        EditorUser = Program.Usuario
                    }, option);
                if (result > 0)
                {
                    base.Set();
                }
            }
        }
        private void frmMarcaModal_Load(object sender, EventArgs e)
        {
            base.ConfigFormulary();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChange();
        }

        private void txPrioridad_KeyPress(object sender, KeyPressEventArgs e)
        {
            Autonomo.Class.Validating.OnlyNumber(e);
        }
    }
}
