﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmProducto : Autonomo.Object.Registry
    {
        List<ProductoReport> productos;

        public frmProducto()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                productos = new List<ProductoReport>();
                productos = uow.Productos.GetList().ToList();
                FilterData();
            }
        }

        private void FilterData()
        {
            if (productos != null && productos.Count > 0)
            {
                //LinQ
                var newList = from o in productos
                              select new
                              {
                                  IdProducto = o.IdProducto,
                                  NombreProducto = o.NombreProducto,
                                  StokMinimo = o.StokMinimo,
                                  NombreCategoria = o.NombreCategoria,
                                  NombreSubCategoria = o.NombreSubCategoria,
                                  NombreMarca = o.NombreMarca,
                                  NombreTalla = o.NombreTalla,
                                  NombreFamilia = o.NombreFamilia,
                                  Descripcion = o.Descripcion,
                                  Genero = o.Genero,
                                  EnlaceWeb = o.EnlaceWeb,
                                  Sku = o.Sku
                              };
                //Lambda
                grdData.DataSource = newList
                    .Where(o => o.NombreProducto.Contains(txFilter.Text))
                    .OrderBy(o => o.NombreProducto)
                    .ToList();
            }
        }

        private void LoadModal(string option, string title)
        {
            var f = new frmProductoModal();
            f.Title.Text = title;
            f.LoadCombo();
            if (option == "Update")
            {
                string id = grdData.CurrentRow.Cells["IdProducto"].Value.ToString();
                //Lambda: .FirstOrDefault(o => o.IdProducto == id)
                var obj = productos.FirstOrDefault(o => o.IdProducto == id);
                //Sql: select top(1) * from productos where IdProducto=id
                if (obj != null)
                    f.LoadData(obj);
            }

            Autonomo.Class.Fomulary.ShowModal(f, option, false);
            if (f.Tag.ToString() == "Get")
            {
                LoadData();
            }
            f.Dispose();
        }

        private void frmProducto_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void txFilter_TextBoxChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LoadModal("Insert", "Registro de Productos");
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (grdData.Rows.Count > 0)
                LoadModal("Update", "Actualización de Productos");
        }
    }
}
