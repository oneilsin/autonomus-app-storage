﻿
namespace Storage.Windows.Modulo.Almacen
{
    partial class frmProductoModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProductoModal));
            this.cbCategoria = new Autonomo.CustomControls.FlatComboBox();
            this.txNombre = new Autonomo.CustomControls.FlatTextBox();
            this.rbtMasculino = new Autonomo.CustomControls.CustomRadius();
            this.cbSubCategoria = new Autonomo.CustomControls.FlatComboBox();
            this.cbMarca = new Autonomo.CustomControls.FlatComboBox();
            this.cbTalla = new Autonomo.CustomControls.FlatComboBox();
            this.cbFamilia = new Autonomo.CustomControls.FlatComboBox();
            this.txDescripcion = new Autonomo.CustomControls.FlatTextBox();
            this.txEnlace = new Autonomo.CustomControls.FlatTextBox();
            this.txSku = new Autonomo.CustomControls.FlatTextBox();
            this.txStock = new Autonomo.CustomControls.FlatTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.rbtFemenino = new Autonomo.CustomControls.CustomRadius();
            this.rbtUnisex = new Autonomo.CustomControls.CustomRadius();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txPrecioBase = new Autonomo.CustomControls.FlatTextBox();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Size = new System.Drawing.Size(592, 518);
            // 
            // Body
            // 
            this.Body.Controls.Add(this.txPrecioBase);
            this.Body.Controls.Add(this.label3);
            this.Body.Controls.Add(this.label2);
            this.Body.Controls.Add(this.label1);
            this.Body.Controls.Add(this.rbtUnisex);
            this.Body.Controls.Add(this.rbtFemenino);
            this.Body.Controls.Add(this.rbtMasculino);
            this.Body.Controls.Add(this.txStock);
            this.Body.Controls.Add(this.txSku);
            this.Body.Controls.Add(this.txEnlace);
            this.Body.Controls.Add(this.txDescripcion);
            this.Body.Controls.Add(this.txNombre);
            this.Body.Controls.Add(this.cbFamilia);
            this.Body.Controls.Add(this.cbTalla);
            this.Body.Controls.Add(this.cbMarca);
            this.Body.Controls.Add(this.cbSubCategoria);
            this.Body.Controls.Add(this.cbCategoria);
            this.Body.Size = new System.Drawing.Size(592, 425);
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 466);
            this.Footer.Size = new System.Drawing.Size(592, 52);
            this.Footer.TabIndex = 13;
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(592, 41);
            // 
            // Title
            // 
            this.Title.Size = new System.Drawing.Size(551, 41);
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Location = new System.Drawing.Point(219, 8);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbCategoria
            // 
            this.cbCategoria.BackColor = System.Drawing.Color.White;
            this.cbCategoria.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbCategoria.ColorLine = System.Drawing.Color.Gray;
            this.cbCategoria.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbCategoria.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbCategoria.DisplayMember = "";
            this.cbCategoria.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCategoria.Error = "";
            this.cbCategoria.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbCategoria.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbCategoria.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbCategoria.ImageIcon")));
            this.cbCategoria.Info = "";
            this.cbCategoria.Location = new System.Drawing.Point(15, 46);
            this.cbCategoria.MaterialStyle = false;
            this.cbCategoria.Name = "cbCategoria";
            this.cbCategoria.Placeholder = "";
            this.cbCategoria.SelectedIndex = -1;
            this.cbCategoria.Size = new System.Drawing.Size(278, 58);
            this.cbCategoria.SizeLine = 2;
            this.cbCategoria.TabIndex = 0;
            this.cbCategoria.Title = "Seleccionar la Categoría";
            this.cbCategoria.ValueMember = "";
            this.cbCategoria.VisibleIcon = true;
            this.cbCategoria.VisibleTitle = true;
            this.cbCategoria.SelectedValueChanged += new System.EventHandler(this.cbCategoria_SelectedValueChanged);
            // 
            // txNombre
            // 
            this.txNombre.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txNombre.BackColor = System.Drawing.Color.White;
            this.txNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txNombre.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txNombre.ColorLine = System.Drawing.Color.Gray;
            this.txNombre.ColorText = System.Drawing.SystemColors.WindowText;
            this.txNombre.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txNombre.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txNombre.Error = "";
            this.txNombre.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txNombre.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txNombre.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txNombre.ImageIcon")));
            this.txNombre.Info = "";
            this.txNombre.Location = new System.Drawing.Point(299, 46);
            this.txNombre.MaterialStyle = true;
            this.txNombre.MaxLength = 32767;
            this.txNombre.MultiLineText = false;
            this.txNombre.Name = "txNombre";
            this.txNombre.PasswordChar = '\0';
            this.txNombre.Placeholder = "Nombre del Producto";
            this.txNombre.ReadOnly = false;
            this.txNombre.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txNombre.Size = new System.Drawing.Size(278, 58);
            this.txNombre.SizeLine = 2;
            this.txNombre.TabIndex = 5;
            this.txNombre.Title = "Nombre del Producto";
            this.txNombre.VisibleIcon = true;
            this.txNombre.VisibleTitle = false;
            // 
            // rbtMasculino
            // 
            this.rbtMasculino.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtMasculino.AutoSize = true;
            this.rbtMasculino.Checked = true;
            this.rbtMasculino.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtMasculino.FlatAppearance.BorderSize = 0;
            this.rbtMasculino.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.rbtMasculino.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.rbtMasculino.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtMasculino.Font = new System.Drawing.Font("Verdana", 10F);
            this.rbtMasculino.Image = ((System.Drawing.Image)(resources.GetObject("rbtMasculino.Image")));
            this.rbtMasculino.ImageChecking = ((System.Drawing.Image)(resources.GetObject("rbtMasculino.ImageChecking")));
            this.rbtMasculino.ImageUnChecking = ((System.Drawing.Image)(resources.GetObject("rbtMasculino.ImageUnChecking")));
            this.rbtMasculino.Location = new System.Drawing.Point(35, 373);
            this.rbtMasculino.Name = "rbtMasculino";
            this.rbtMasculino.Size = new System.Drawing.Size(105, 27);
            this.rbtMasculino.TabIndex = 10;
            this.rbtMasculino.TabStop = true;
            this.rbtMasculino.Text = "Masculino";
            this.rbtMasculino.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rbtMasculino.UseVisualStyleBackColor = true;
            // 
            // cbSubCategoria
            // 
            this.cbSubCategoria.BackColor = System.Drawing.Color.White;
            this.cbSubCategoria.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbSubCategoria.ColorLine = System.Drawing.Color.Gray;
            this.cbSubCategoria.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbSubCategoria.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbSubCategoria.DisplayMember = "";
            this.cbSubCategoria.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbSubCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSubCategoria.Error = "";
            this.cbSubCategoria.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbSubCategoria.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbSubCategoria.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbSubCategoria.ImageIcon")));
            this.cbSubCategoria.Info = "";
            this.cbSubCategoria.Location = new System.Drawing.Point(15, 110);
            this.cbSubCategoria.MaterialStyle = false;
            this.cbSubCategoria.Name = "cbSubCategoria";
            this.cbSubCategoria.Placeholder = "";
            this.cbSubCategoria.SelectedIndex = -1;
            this.cbSubCategoria.Size = new System.Drawing.Size(278, 58);
            this.cbSubCategoria.SizeLine = 2;
            this.cbSubCategoria.TabIndex = 1;
            this.cbSubCategoria.Title = "Seleccionar la Sub-Categoría";
            this.cbSubCategoria.ValueMember = "";
            this.cbSubCategoria.VisibleIcon = true;
            this.cbSubCategoria.VisibleTitle = true;
            // 
            // cbMarca
            // 
            this.cbMarca.BackColor = System.Drawing.Color.White;
            this.cbMarca.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbMarca.ColorLine = System.Drawing.Color.Gray;
            this.cbMarca.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbMarca.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbMarca.DisplayMember = "";
            this.cbMarca.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbMarca.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMarca.Error = "";
            this.cbMarca.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbMarca.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbMarca.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbMarca.ImageIcon")));
            this.cbMarca.Info = "";
            this.cbMarca.Location = new System.Drawing.Point(15, 174);
            this.cbMarca.MaterialStyle = false;
            this.cbMarca.Name = "cbMarca";
            this.cbMarca.Placeholder = "";
            this.cbMarca.SelectedIndex = -1;
            this.cbMarca.Size = new System.Drawing.Size(278, 58);
            this.cbMarca.SizeLine = 2;
            this.cbMarca.TabIndex = 2;
            this.cbMarca.Title = "Selecionar la Marca";
            this.cbMarca.ValueMember = "";
            this.cbMarca.VisibleIcon = true;
            this.cbMarca.VisibleTitle = true;
            // 
            // cbTalla
            // 
            this.cbTalla.BackColor = System.Drawing.Color.White;
            this.cbTalla.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbTalla.ColorLine = System.Drawing.Color.Gray;
            this.cbTalla.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbTalla.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbTalla.DisplayMember = "";
            this.cbTalla.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbTalla.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTalla.Error = "";
            this.cbTalla.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbTalla.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbTalla.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbTalla.ImageIcon")));
            this.cbTalla.Info = "";
            this.cbTalla.Location = new System.Drawing.Point(15, 238);
            this.cbTalla.MaterialStyle = false;
            this.cbTalla.Name = "cbTalla";
            this.cbTalla.Placeholder = "";
            this.cbTalla.SelectedIndex = -1;
            this.cbTalla.Size = new System.Drawing.Size(278, 58);
            this.cbTalla.SizeLine = 2;
            this.cbTalla.TabIndex = 3;
            this.cbTalla.Title = "Selecionar la Talla";
            this.cbTalla.ValueMember = "";
            this.cbTalla.VisibleIcon = true;
            this.cbTalla.VisibleTitle = true;
            // 
            // cbFamilia
            // 
            this.cbFamilia.BackColor = System.Drawing.Color.White;
            this.cbFamilia.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbFamilia.ColorLine = System.Drawing.Color.Gray;
            this.cbFamilia.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbFamilia.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbFamilia.DisplayMember = "";
            this.cbFamilia.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbFamilia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbFamilia.Error = "";
            this.cbFamilia.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbFamilia.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbFamilia.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbFamilia.ImageIcon")));
            this.cbFamilia.Info = "";
            this.cbFamilia.Location = new System.Drawing.Point(15, 293);
            this.cbFamilia.MaterialStyle = false;
            this.cbFamilia.Name = "cbFamilia";
            this.cbFamilia.Placeholder = "";
            this.cbFamilia.SelectedIndex = -1;
            this.cbFamilia.Size = new System.Drawing.Size(278, 58);
            this.cbFamilia.SizeLine = 2;
            this.cbFamilia.TabIndex = 4;
            this.cbFamilia.Title = "Selecionar la Familia";
            this.cbFamilia.ValueMember = "";
            this.cbFamilia.VisibleIcon = true;
            this.cbFamilia.VisibleTitle = true;
            // 
            // txDescripcion
            // 
            this.txDescripcion.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txDescripcion.BackColor = System.Drawing.Color.White;
            this.txDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txDescripcion.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txDescripcion.ColorLine = System.Drawing.Color.Gray;
            this.txDescripcion.ColorText = System.Drawing.SystemColors.WindowText;
            this.txDescripcion.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txDescripcion.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txDescripcion.Error = "";
            this.txDescripcion.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txDescripcion.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txDescripcion.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txDescripcion.ImageIcon")));
            this.txDescripcion.Info = "";
            this.txDescripcion.Location = new System.Drawing.Point(299, 110);
            this.txDescripcion.MaterialStyle = true;
            this.txDescripcion.MaxLength = 32767;
            this.txDescripcion.MultiLineText = true;
            this.txDescripcion.Name = "txDescripcion";
            this.txDescripcion.PasswordChar = '\0';
            this.txDescripcion.Placeholder = "Descipción del Producto";
            this.txDescripcion.ReadOnly = false;
            this.txDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txDescripcion.Size = new System.Drawing.Size(278, 122);
            this.txDescripcion.SizeLine = 2;
            this.txDescripcion.TabIndex = 6;
            this.txDescripcion.Title = "Descipción del Producto";
            this.txDescripcion.VisibleIcon = true;
            this.txDescripcion.VisibleTitle = false;
            // 
            // txEnlace
            // 
            this.txEnlace.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txEnlace.BackColor = System.Drawing.Color.White;
            this.txEnlace.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txEnlace.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txEnlace.ColorLine = System.Drawing.Color.Gray;
            this.txEnlace.ColorText = System.Drawing.SystemColors.WindowText;
            this.txEnlace.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txEnlace.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txEnlace.Error = "";
            this.txEnlace.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txEnlace.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txEnlace.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txEnlace.ImageIcon")));
            this.txEnlace.Info = "";
            this.txEnlace.Location = new System.Drawing.Point(299, 238);
            this.txEnlace.MaterialStyle = true;
            this.txEnlace.MaxLength = 32767;
            this.txEnlace.MultiLineText = false;
            this.txEnlace.Name = "txEnlace";
            this.txEnlace.PasswordChar = '\0';
            this.txEnlace.Placeholder = "Enlace para Web";
            this.txEnlace.ReadOnly = false;
            this.txEnlace.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txEnlace.Size = new System.Drawing.Size(278, 58);
            this.txEnlace.SizeLine = 2;
            this.txEnlace.TabIndex = 7;
            this.txEnlace.Title = "Enlace para Web";
            this.txEnlace.VisibleIcon = true;
            this.txEnlace.VisibleTitle = false;
            // 
            // txSku
            // 
            this.txSku.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txSku.BackColor = System.Drawing.Color.White;
            this.txSku.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txSku.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txSku.ColorLine = System.Drawing.Color.Gray;
            this.txSku.ColorText = System.Drawing.SystemColors.WindowText;
            this.txSku.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txSku.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txSku.Error = "";
            this.txSku.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txSku.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txSku.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txSku.ImageIcon")));
            this.txSku.Info = "";
            this.txSku.Location = new System.Drawing.Point(299, 293);
            this.txSku.MaterialStyle = true;
            this.txSku.MaxLength = 32767;
            this.txSku.MultiLineText = false;
            this.txSku.Name = "txSku";
            this.txSku.PasswordChar = '\0';
            this.txSku.Placeholder = "SKU";
            this.txSku.ReadOnly = false;
            this.txSku.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txSku.Size = new System.Drawing.Size(137, 58);
            this.txSku.SizeLine = 2;
            this.txSku.TabIndex = 8;
            this.txSku.Title = "SKU";
            this.txSku.VisibleIcon = true;
            this.txSku.VisibleTitle = false;
            // 
            // txStock
            // 
            this.txStock.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txStock.BackColor = System.Drawing.Color.White;
            this.txStock.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txStock.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txStock.ColorLine = System.Drawing.Color.Gray;
            this.txStock.ColorText = System.Drawing.SystemColors.WindowText;
            this.txStock.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txStock.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txStock.Error = "";
            this.txStock.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txStock.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txStock.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txStock.ImageIcon")));
            this.txStock.Info = "";
            this.txStock.Location = new System.Drawing.Point(442, 293);
            this.txStock.MaterialStyle = true;
            this.txStock.MaxLength = 32767;
            this.txStock.MultiLineText = false;
            this.txStock.Name = "txStock";
            this.txStock.PasswordChar = '\0';
            this.txStock.Placeholder = "Stock mínimo";
            this.txStock.ReadOnly = false;
            this.txStock.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txStock.Size = new System.Drawing.Size(135, 58);
            this.txStock.SizeLine = 2;
            this.txStock.TabIndex = 9;
            this.txStock.Text = "1";
            this.txStock.Title = "Stock mínimo";
            this.txStock.VisibleIcon = true;
            this.txStock.VisibleTitle = true;
            this.txStock.KeyPress += new System.EventHandler<System.Windows.Forms.KeyPressEventArgs>(this.txStock_KeyPress);
            this.txStock.Leave += new System.EventHandler(this.txStock_Leave);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(115, 354);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 16);
            this.label1.TabIndex = 3;
            this.label1.Text = "Genero del producto";
            // 
            // rbtFemenino
            // 
            this.rbtFemenino.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtFemenino.AutoSize = true;
            this.rbtFemenino.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtFemenino.FlatAppearance.BorderSize = 0;
            this.rbtFemenino.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.rbtFemenino.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.rbtFemenino.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtFemenino.Font = new System.Drawing.Font("Verdana", 10F);
            this.rbtFemenino.Image = ((System.Drawing.Image)(resources.GetObject("rbtFemenino.Image")));
            this.rbtFemenino.ImageChecking = ((System.Drawing.Image)(resources.GetObject("rbtFemenino.ImageChecking")));
            this.rbtFemenino.ImageUnChecking = ((System.Drawing.Image)(resources.GetObject("rbtFemenino.ImageUnChecking")));
            this.rbtFemenino.Location = new System.Drawing.Point(145, 373);
            this.rbtFemenino.Name = "rbtFemenino";
            this.rbtFemenino.Size = new System.Drawing.Size(103, 27);
            this.rbtFemenino.TabIndex = 11;
            this.rbtFemenino.TabStop = true;
            this.rbtFemenino.Text = "Femenino";
            this.rbtFemenino.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rbtFemenino.UseVisualStyleBackColor = true;
            // 
            // rbtUnisex
            // 
            this.rbtUnisex.Appearance = System.Windows.Forms.Appearance.Button;
            this.rbtUnisex.AutoSize = true;
            this.rbtUnisex.Cursor = System.Windows.Forms.Cursors.Hand;
            this.rbtUnisex.FlatAppearance.BorderSize = 0;
            this.rbtUnisex.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.rbtUnisex.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.rbtUnisex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.rbtUnisex.Font = new System.Drawing.Font("Verdana", 10F);
            this.rbtUnisex.Image = ((System.Drawing.Image)(resources.GetObject("rbtUnisex.Image")));
            this.rbtUnisex.ImageChecking = ((System.Drawing.Image)(resources.GetObject("rbtUnisex.ImageChecking")));
            this.rbtUnisex.ImageUnChecking = ((System.Drawing.Image)(resources.GetObject("rbtUnisex.ImageUnChecking")));
            this.rbtUnisex.Location = new System.Drawing.Point(254, 373);
            this.rbtUnisex.Name = "rbtUnisex";
            this.rbtUnisex.Size = new System.Drawing.Size(83, 27);
            this.rbtUnisex.TabIndex = 12;
            this.rbtUnisex.Text = "Unisex";
            this.rbtUnisex.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.rbtUnisex.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 11F);
            this.label2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label2.Location = new System.Drawing.Point(24, 17);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 18);
            this.label2.TabIndex = 3;
            this.label2.Text = "Relación de entidades";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 11F);
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label3.Location = new System.Drawing.Point(296, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(158, 18);
            this.label3.TabIndex = 3;
            this.label3.Text = "Datos del productos";
            // 
            // txPrecioBase
            // 
            this.txPrecioBase.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txPrecioBase.BackColor = System.Drawing.Color.White;
            this.txPrecioBase.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txPrecioBase.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txPrecioBase.ColorLine = System.Drawing.Color.Gray;
            this.txPrecioBase.ColorText = System.Drawing.SystemColors.WindowText;
            this.txPrecioBase.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txPrecioBase.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txPrecioBase.Error = "";
            this.txPrecioBase.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txPrecioBase.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txPrecioBase.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txPrecioBase.ImageIcon")));
            this.txPrecioBase.Info = "";
            this.txPrecioBase.Location = new System.Drawing.Point(445, 354);
            this.txPrecioBase.MaterialStyle = true;
            this.txPrecioBase.MaxLength = 32767;
            this.txPrecioBase.MultiLineText = false;
            this.txPrecioBase.Name = "txPrecioBase";
            this.txPrecioBase.PasswordChar = '\0';
            this.txPrecioBase.Placeholder = "Precio base";
            this.txPrecioBase.ReadOnly = false;
            this.txPrecioBase.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txPrecioBase.Size = new System.Drawing.Size(135, 58);
            this.txPrecioBase.SizeLine = 2;
            this.txPrecioBase.TabIndex = 13;
            this.txPrecioBase.Text = "0.00";
            this.txPrecioBase.Title = "Precio base";
            this.txPrecioBase.VisibleIcon = true;
            this.txPrecioBase.VisibleTitle = true;
            this.txPrecioBase.Leave += new System.EventHandler(this.txPrecioBase_Leave);
            // 
            // frmProductoModal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(592, 518);
            this.Name = "frmProductoModal";
            this.Text = "frmProductoModal";
            this.Load += new System.EventHandler(this.frmProductoModal_Load);
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.Body.PerformLayout();
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Autonomo.CustomControls.FlatComboBox cbCategoria;
        private System.Windows.Forms.Label label1;
        private Autonomo.CustomControls.CustomRadius rbtUnisex;
        private Autonomo.CustomControls.CustomRadius rbtFemenino;
        private Autonomo.CustomControls.CustomRadius rbtMasculino;
        private Autonomo.CustomControls.FlatTextBox txStock;
        private Autonomo.CustomControls.FlatTextBox txSku;
        private Autonomo.CustomControls.FlatTextBox txEnlace;
        private Autonomo.CustomControls.FlatTextBox txDescripcion;
        private Autonomo.CustomControls.FlatTextBox txNombre;
        private Autonomo.CustomControls.FlatComboBox cbFamilia;
        private Autonomo.CustomControls.FlatComboBox cbTalla;
        private Autonomo.CustomControls.FlatComboBox cbMarca;
        private Autonomo.CustomControls.FlatComboBox cbSubCategoria;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Autonomo.CustomControls.FlatTextBox txPrecioBase;
    }
}