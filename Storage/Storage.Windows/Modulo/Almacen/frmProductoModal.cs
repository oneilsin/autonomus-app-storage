﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmProductoModal : Autonomo.Object.Modal
    {
        private string IdProducto;
        public frmProductoModal()
        {
            InitializeComponent();
            this.IdProducto = string.Empty;
        }
        
        public void LoadData(ProductoReport productos)
        {
            this.IdProducto = productos.IdProducto;
            cbCategoria.SelectedValue = productos.IdCategoria;
            cbSubCategoria.SelectedValue = productos.IdSubCategoria;
            cbMarca.SelectedValue = productos.IdMarca;
            cbTalla.SelectedValue = productos.IdTalla;
            cbFamilia.SelectedValue = productos.IdFamilia;
            txNombre.Text = productos.NombreProducto;
            txDescripcion.Text = productos.Descripcion;
            txEnlace.Text = productos.EnlaceWeb;
            txSku.Text = productos.Sku;
            txStock.Text = productos.StokMinimo.ToString();
            txPrecioBase.Text = productos.PrecioBase.ToString();

            string g = productos.Genero;
            switch (g)
            {
                case "M":
                    {
                        rbtMasculino.Checked = true;
                        break;
                    }
                case "F":
                    {
                        rbtFemenino.Checked = true;
                        break;
                    }
                case "U":
                    {
                        rbtUnisex.Checked = true;
                        break;
                    }
            }
        }

        public void LoadCombo()
        {
            Tools.ComboBoxHelper.ComboBoxCategoria(cbCategoria);
            Tools.ComboBoxHelper.ComboBoxMarca(cbMarca);
            Tools.ComboBoxHelper.ComboBoxTalla(cbTalla);
            Tools.ComboBoxHelper.ComboBoxFamilia(cbFamilia);
        }
        private void SaveChange()
        {
            try
            {
                using (UnitOfWork uow = new UnitOfWork())
                {
                    string option = this.Tag.ToString();
                    var result = uow.Productos.Crud(
                        new Entidad.Producto()
                        {
                            IdProducto = this.IdProducto,
                            IdCategoria = cbCategoria.SelectedValue.ToString(),
                            IdSubCategoria = cbSubCategoria.SelectedValue.ToString(),
                            IdMarca = cbMarca.SelectedValue.ToString(),
                            IdTalla = cbTalla.SelectedValue.ToString(),
                            IdFamilia = cbFamilia.SelectedValue.ToString(),
                            NombreProducto = txNombre.Text,
                            Descripcion = txDescripcion.Text,
                            Genero = GetGenero(),
                            EnlaceWeb = txEnlace.Text,
                            Sku = txSku.Text,
                            StokMinimo = Convert.ToDecimal(txStock.Text),
                            PrecioBase = Convert.ToDecimal(txPrecioBase.Text),
                            EditorUser = Program.Usuario
                        }, option);
                    if (result > 0) { base.Set(); }
                }
            }
            catch (Exception ex)
            {
                //Pronto mensaje personalizado.
                throw ex;
            }
        }

        string GetGenero()
        {
            return rbtMasculino.Checked ? "M"
                : rbtFemenino.Checked ? "F"
                : "U";
        }
        private void frmProductoModal_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            // OBS::: Question: Yes/not.
            SaveChange();
        }

        private void cbCategoria_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbCategoria.Items.Count > 0)
            {
                string idCategoria = cbCategoria.SelectedValue.ToString();
                Tools.ComboBoxHelper.ComboBoxSubCategoria(cbSubCategoria, idCategoria);
            }
        }

        private void txStock_KeyPress(object sender, KeyPressEventArgs e)
        {
            Autonomo.Class.Validating.OnlyNumber(e);
        }

        private void txStock_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txStock.Text))
                txStock.Text = "1";
        }

        private void txPrecioBase_Leave(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txPrecioBase.Text))
                txPrecioBase.Text = "0.00";
        }
    }
}
