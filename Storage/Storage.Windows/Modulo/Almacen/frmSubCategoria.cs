﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmSubCategoria : Autonomo.Object.Registry
    {
        List<SubCategoriaReport> subCategorias;
        public frmSubCategoria()
        {
            InitializeComponent();
        }
        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                subCategorias = new List<SubCategoriaReport>();
                subCategorias = uow.SubCategorias.GetList().ToList();
                LoadFilter();
            }
        }

        private void LoadFilter()
        {
            if (subCategorias != null && subCategorias.Count() > 0)
            {
                //LinQ
                var newList = from o in subCategorias
                              select new
                              {
                                  IdSubCategoria = o.IdSubCategoria,
                                  NombreCategoria = o.NombreCategoria,
                                  NombreSubCategoria=o.NombreSubCategoria,
                                  Descripcion = o.Descripcion
                              };

                grdData.DataSource = newList
                    .Where(o => o.NombreCategoria.Contains(txFilter.Text))
                    .ToList();
                //  select * from Categoria Where NombreCtg like'%'+@filtro+'%'
            }
        }
        private void LoadModal(string opcion, string title)
        {
            var f = new frmSubCategoriaModal();
            f.Title.Text = title;
            f.LoadCombo();
            if (opcion == "Update")
            {
                string id = grdData.CurrentRow.Cells["IdSubCategoria"].Value.ToString();
                var ctg = subCategorias.FirstOrDefault(o => o.IdSubCategoria.Equals(id));
                //SQL: select * from SubCategoria where IdSubCategoria='id'
                if (ctg != null)
                {
                    f.LoadData(ctg.IdSubCategoria,
                        ctg.IdCategoria,
                        ctg.NombreSubCategoria,
                        ctg.Descripcion);
                }
            }
            Autonomo.Class.Fomulary.ShowModal(f, opcion, false);
            if (f.Tag.ToString() == "Get")
            {
                LoadData();
            }
        }
        private void frmSubCategoria_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void txFilter_TextBoxChanged(object sender, EventArgs e)
        {
            LoadFilter();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LoadModal("Insert", "Registro de Sub-Categoría");
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            LoadModal("Update", "Actualizar Sub-Categoría");
        }
    }
}
