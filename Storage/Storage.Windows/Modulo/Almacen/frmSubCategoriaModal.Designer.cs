﻿namespace Storage.Windows.Modulo.Almacen
{
    partial class frmSubCategoriaModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSubCategoriaModal));
            this.cbCategoria = new Autonomo.CustomControls.FlatComboBox();
            this.txDescripcion = new Autonomo.CustomControls.FlatTextBox();
            this.txNombre = new Autonomo.CustomControls.FlatTextBox();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Size = new System.Drawing.Size(316, 320);
            // 
            // Body
            // 
            this.Body.Controls.Add(this.txDescripcion);
            this.Body.Controls.Add(this.txNombre);
            this.Body.Controls.Add(this.cbCategoria);
            this.Body.Size = new System.Drawing.Size(316, 227);
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 268);
            this.Footer.Size = new System.Drawing.Size(316, 52);
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(316, 41);
            // 
            // Title
            // 
            this.Title.Size = new System.Drawing.Size(275, 41);
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbCategoria
            // 
            this.cbCategoria.BackColor = System.Drawing.Color.White;
            this.cbCategoria.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbCategoria.ColorLine = System.Drawing.Color.Gray;
            this.cbCategoria.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbCategoria.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbCategoria.DisplayMember = "";
            this.cbCategoria.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbCategoria.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCategoria.Error = "";
            this.cbCategoria.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbCategoria.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbCategoria.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbCategoria.ImageIcon")));
            this.cbCategoria.Info = "";
            this.cbCategoria.Location = new System.Drawing.Point(12, 25);
            this.cbCategoria.MaterialStyle = true;
            this.cbCategoria.Name = "cbCategoria";
            this.cbCategoria.Placeholder = "";
            this.cbCategoria.SelectedIndex = -1;
            this.cbCategoria.Size = new System.Drawing.Size(278, 58);
            this.cbCategoria.SizeLine = 2;
            this.cbCategoria.TabIndex = 0;
            this.cbCategoria.Title = "Seleccionar la Categoría";
            this.cbCategoria.ValueMember = "";
            this.cbCategoria.VisibleIcon = true;
            this.cbCategoria.VisibleTitle = true;
            // 
            // txDescripcion
            // 
            this.txDescripcion.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txDescripcion.BackColor = System.Drawing.Color.White;
            this.txDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txDescripcion.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txDescripcion.ColorLine = System.Drawing.Color.Gray;
            this.txDescripcion.ColorText = System.Drawing.SystemColors.WindowText;
            this.txDescripcion.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txDescripcion.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txDescripcion.Error = "";
            this.txDescripcion.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txDescripcion.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txDescripcion.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txDescripcion.ImageIcon")));
            this.txDescripcion.Info = "";
            this.txDescripcion.Location = new System.Drawing.Point(12, 144);
            this.txDescripcion.MaterialStyle = true;
            this.txDescripcion.MaxLength = 100;
            this.txDescripcion.MultiLineText = false;
            this.txDescripcion.Name = "txDescripcion";
            this.txDescripcion.PasswordChar = '\0';
            this.txDescripcion.Placeholder = "Descripción de la Sub-Categoría";
            this.txDescripcion.ReadOnly = false;
            this.txDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txDescripcion.Size = new System.Drawing.Size(278, 58);
            this.txDescripcion.SizeLine = 2;
            this.txDescripcion.TabIndex = 2;
            this.txDescripcion.Title = "Descripción de la Sub-Categoría";
            this.txDescripcion.VisibleIcon = true;
            this.txDescripcion.VisibleTitle = false;
            // 
            // txNombre
            // 
            this.txNombre.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txNombre.BackColor = System.Drawing.Color.White;
            this.txNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txNombre.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txNombre.ColorLine = System.Drawing.Color.Gray;
            this.txNombre.ColorText = System.Drawing.SystemColors.WindowText;
            this.txNombre.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txNombre.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txNombre.Error = "";
            this.txNombre.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txNombre.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txNombre.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txNombre.ImageIcon")));
            this.txNombre.Info = "";
            this.txNombre.Location = new System.Drawing.Point(12, 80);
            this.txNombre.MaterialStyle = true;
            this.txNombre.MaxLength = 50;
            this.txNombre.MultiLineText = false;
            this.txNombre.Name = "txNombre";
            this.txNombre.PasswordChar = '\0';
            this.txNombre.Placeholder = "Nombre de la Sub-Categoría";
            this.txNombre.ReadOnly = false;
            this.txNombre.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txNombre.Size = new System.Drawing.Size(278, 58);
            this.txNombre.SizeLine = 2;
            this.txNombre.TabIndex = 1;
            this.txNombre.Title = "Nombre de la Sub-Categoría";
            this.txNombre.VisibleIcon = true;
            this.txNombre.VisibleTitle = false;
            // 
            // frmSubCategoriaModal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(316, 320);
            this.Name = "frmSubCategoriaModal";
            this.Text = "frmSubCategoriaModal";
            this.Load += new System.EventHandler(this.frmSubCategoriaModal_Load);
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Autonomo.CustomControls.FlatComboBox cbCategoria;
        private Autonomo.CustomControls.FlatTextBox txDescripcion;
        private Autonomo.CustomControls.FlatTextBox txNombre;
    }
}