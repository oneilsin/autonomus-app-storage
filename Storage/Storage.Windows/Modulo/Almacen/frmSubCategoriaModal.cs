﻿using Storage.Datos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmSubCategoriaModal : Autonomo.Object.Modal
    {
        private string IdSubCategoria;
        public frmSubCategoriaModal()
        {
            InitializeComponent();
            this.IdSubCategoria = string.Empty;
        }
        public void LoadCombo()
        {
            Tools.ComboBoxHelper.ComboBoxCategoria(cbCategoria);
        }
        public void LoadData(string idSubCategoria, string idCategoria,
           string nombre, string descripcion)
        {
            this.IdSubCategoria = idSubCategoria;
            txNombre.Text = nombre;
            txDescripcion.Text = descripcion;
            cbCategoria.SelectedValue = idCategoria;
        }
        private void SaveChange()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                string option = this.Tag.ToString(); //Insert  Update

                var result = uow.SubCategorias.Crud(
                    new Entidad.SubCategoria()
                    {
                        IdSubCategoria = IdSubCategoria,
                        NombreSubCategoria = txNombre.Text,
                        Descripcion = txDescripcion.Text,
                        IdCategoria = cbCategoria.SelectedValue.ToString(),
                        EditorUser = Program.Usuario
                    }, option);

              
                if (result > 0)
                {
                    // MessageBox:::  Después vamos a personalizar.
                    base.Set(); // Si se ha realziado el proceso de manera correcta,
                    //este retornará "Get" y cerrará el formulario.
                }
            }
        }
        private void frmSubCategoriaModal_Load(object sender, EventArgs e)
        {
            base.ConfigFormulary();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChange();
        }
    }
}
