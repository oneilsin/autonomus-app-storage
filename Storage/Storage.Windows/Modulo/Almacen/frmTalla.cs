﻿using Storage.Datos;
using Storage.Entidad;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmTalla : Autonomo.Object.Registry
    {
        List<Talla> tallas;
        public frmTalla()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                tallas = new List<Talla>();
                tallas = uow.Tallas.GetList().ToList();
                LoadFilter();
            }
        }

        private void LoadFilter()
        {
            if(tallas!=null && tallas.Count() > 0)
            {
                var newList = from o in tallas
                              select new
                              {
                                  IdTalla = o.IdTalla,
                                  NombreTalla = o.NombreTalla,
                                  Descripcion = o.Descripcion
                              };
                grdData.DataSource = newList
                    .Where(o => o.NombreTalla.Contains(txFilter.Text))
                    .OrderBy(o => o.NombreTalla)
                    .ToList();
            }
        }

        private void LoadModal(string option, string title)
        {
            var f = new frmTallaModal();
            f.Title.Text = title;
            if (option == "Update")
            {
                string id = grdData.CurrentRow.Cells["IdTalla"].Value.ToString();
                var obj = tallas.FirstOrDefault(o => o.IdTalla.Equals(id));
                if (obj != null)
                {
                    f.LoadData(
                        obj.IdTalla, obj.NombreTalla, obj.Descripcion
                        );
                }
            }
            Autonomo.Class.Fomulary.ShowModal(f, option, false);
            if (f.Tag.ToString() == "Get") { LoadData(); }
        }
        private void frmTalla_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void txFilter_TextBoxChanged(object sender, EventArgs e)
        {
            LoadFilter();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LoadModal("Insert", "Registrar nueva Talla");
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            LoadModal("Update", "Actualizar Talla");
        }
    }
}
