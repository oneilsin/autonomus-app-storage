﻿using Storage.Datos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmTallaModal : Autonomo.Object.Modal
    {
        private string IdTalla;
        public frmTallaModal()
        {
            InitializeComponent();
            this.IdTalla = string.Empty;
        }
        public void LoadData(string idTalla, string nombre, string descripcion)
        {
            this.IdTalla = idTalla;
            txNombre.Text = nombre;
            txDescripcion.Text = descripcion;
        }
        private void SaveChange()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                string option = this.Tag.ToString();

                var result = uow.Tallas.Crud(
                    new Entidad.Talla()
                    {
                        IdTalla = this.IdTalla,
                        NombreTalla = txNombre.Text.Trim(),
                        Descripcion = txDescripcion.Text.Trim(),
                        EditorUser = Program.Usuario
                    }, option);
                if (result > 0) { base.Set(); }
            }
        }

        private void frmTallaModal_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChange();
        }

        
    }
}
