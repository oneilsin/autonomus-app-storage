﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmTienda : Autonomo.Object.Registry
    {
        List<TiendaReport> tiendas;
        public frmTienda()
        {
            InitializeComponent();
        }
        /*
            1. Iniciar Listado desde SQL y almancenar el memoria List
            2. Filtramos Lista de datos
            3. Inciamos el modal para CRUD
        */
        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                tiendas = new List<TiendaReport>();
                tiendas = uow.Tiendas.GetList().OrderBy(o=>o.NombreTienda).ToList();
                FilterData();
            }
        }
        private void FilterData()
        {
            if(tiendas!=null && tiendas.Count > 0)
            {
                var newList = tiendas.Where(o => o.NombreTienda.Contains(txFilter.Text)
                    || o.Descripcion.Contains(txFilter.Text))
                    .Select(x => new
                    {
                        Id = x.IdTienda,
                        Consigna = x.Consigna,
                        Responsable = x.RazonSocial,
                        Nombre = x.NombreTienda,
                        Descripcion = x.Descripcion
                    });
                grdData.DataSource = newList.ToList();
            }
        }
        private void LoadModal(string title, string option)
        {
            var f = new frmTiendaModal();
            f.Title.Text = title;
            f.LoadCombo();
            if (option == "Update")
            {
                string id = grdData.CurrentRow.Cells["Id"].Value.ToString();
                var obj = tiendas.FirstOrDefault(o => o.IdTienda.Equals(id));
                f.LoadData(obj);
            }

            Autonomo.Class.Fomulary.ShowModal(f, option);
            if (f.Tag.ToString() == "Get")
                LoadData();
            f.Dispose();// Asegurarse que se ha eliminado de la menoria esta instancia.

        }
        private void frmTienda_Load(object sender, EventArgs e)
        {
            ThemeStyle(Theme.White);
            LoadData();
        }

        private void txFilter_TextBoxChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LoadModal("Registrar nueva Tienda", "Insert");
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (grdData.Rows.Count > 0)
                LoadModal("Actualizar datos de la Tienda", "Update");
        }
    }
}
