﻿
namespace Storage.Windows.Modulo.Almacen
{
    partial class frmTiendaModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmTiendaModal));
            this.cbResponsable = new Autonomo.CustomControls.FlatComboBox();
            this.txDescripcion = new Autonomo.CustomControls.FlatTextBox();
            this.txNombre = new Autonomo.CustomControls.FlatTextBox();
            this.cbxConsignacion = new Autonomo.CustomControls.CustomCheck();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Size = new System.Drawing.Size(311, 359);
            // 
            // Body
            // 
            this.Body.Controls.Add(this.cbxConsignacion);
            this.Body.Controls.Add(this.cbResponsable);
            this.Body.Controls.Add(this.txDescripcion);
            this.Body.Controls.Add(this.txNombre);
            this.Body.Size = new System.Drawing.Size(311, 266);
            this.Body.TabIndex = 0;
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 307);
            this.Footer.Size = new System.Drawing.Size(311, 52);
            this.Footer.TabIndex = 4;
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(311, 41);
            // 
            // Title
            // 
            this.Title.Size = new System.Drawing.Size(270, 41);
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Location = new System.Drawing.Point(78, 8);
            this.btnSave.TabIndex = 0;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbResponsable
            // 
            this.cbResponsable.BackColor = System.Drawing.Color.White;
            this.cbResponsable.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbResponsable.ColorLine = System.Drawing.Color.Gray;
            this.cbResponsable.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbResponsable.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbResponsable.DisplayMember = "";
            this.cbResponsable.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbResponsable.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbResponsable.Error = "";
            this.cbResponsable.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbResponsable.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbResponsable.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbResponsable.ImageIcon")));
            this.cbResponsable.Info = "";
            this.cbResponsable.Location = new System.Drawing.Point(12, 156);
            this.cbResponsable.MaterialStyle = true;
            this.cbResponsable.Name = "cbResponsable";
            this.cbResponsable.Placeholder = "Seleccione el Responsable";
            this.cbResponsable.SelectedIndex = -1;
            this.cbResponsable.Size = new System.Drawing.Size(278, 58);
            this.cbResponsable.SizeLine = 2;
            this.cbResponsable.TabIndex = 2;
            this.cbResponsable.Title = "Seleccione el Responsable";
            this.cbResponsable.ValueMember = "";
            this.cbResponsable.VisibleIcon = true;
            this.cbResponsable.VisibleTitle = false;
            // 
            // txDescripcion
            // 
            this.txDescripcion.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txDescripcion.BackColor = System.Drawing.Color.White;
            this.txDescripcion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txDescripcion.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txDescripcion.ColorLine = System.Drawing.Color.Gray;
            this.txDescripcion.ColorText = System.Drawing.SystemColors.WindowText;
            this.txDescripcion.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txDescripcion.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txDescripcion.Error = "";
            this.txDescripcion.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txDescripcion.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txDescripcion.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txDescripcion.ImageIcon")));
            this.txDescripcion.Info = "";
            this.txDescripcion.Location = new System.Drawing.Point(12, 92);
            this.txDescripcion.MaterialStyle = true;
            this.txDescripcion.MaxLength = 100;
            this.txDescripcion.MultiLineText = false;
            this.txDescripcion.Name = "txDescripcion";
            this.txDescripcion.PasswordChar = '\0';
            this.txDescripcion.Placeholder = "Descripción de la Tienda";
            this.txDescripcion.ReadOnly = false;
            this.txDescripcion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txDescripcion.Size = new System.Drawing.Size(278, 58);
            this.txDescripcion.SizeLine = 2;
            this.txDescripcion.TabIndex = 1;
            this.txDescripcion.Title = "Descripción de la Tienda";
            this.txDescripcion.VisibleIcon = true;
            this.txDescripcion.VisibleTitle = false;
            // 
            // txNombre
            // 
            this.txNombre.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txNombre.BackColor = System.Drawing.Color.White;
            this.txNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txNombre.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txNombre.ColorLine = System.Drawing.Color.Gray;
            this.txNombre.ColorText = System.Drawing.SystemColors.WindowText;
            this.txNombre.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txNombre.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txNombre.Error = "";
            this.txNombre.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txNombre.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txNombre.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txNombre.ImageIcon")));
            this.txNombre.Info = "";
            this.txNombre.Location = new System.Drawing.Point(12, 28);
            this.txNombre.MaterialStyle = true;
            this.txNombre.MaxLength = 50;
            this.txNombre.MultiLineText = false;
            this.txNombre.Name = "txNombre";
            this.txNombre.PasswordChar = '\0';
            this.txNombre.Placeholder = "Nombre de la tienda";
            this.txNombre.ReadOnly = false;
            this.txNombre.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txNombre.Size = new System.Drawing.Size(278, 58);
            this.txNombre.SizeLine = 2;
            this.txNombre.TabIndex = 0;
            this.txNombre.Title = "Nombre de la tienda";
            this.txNombre.VisibleIcon = true;
            this.txNombre.VisibleTitle = false;
            // 
            // cbxConsignacion
            // 
            this.cbxConsignacion.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbxConsignacion.AutoSize = true;
            this.cbxConsignacion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxConsignacion.FlatAppearance.BorderSize = 0;
            this.cbxConsignacion.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cbxConsignacion.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cbxConsignacion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxConsignacion.Font = new System.Drawing.Font("Verdana", 10F);
            this.cbxConsignacion.Image = ((System.Drawing.Image)(resources.GetObject("cbxConsignacion.Image")));
            this.cbxConsignacion.ImageChecking = ((System.Drawing.Image)(resources.GetObject("cbxConsignacion.ImageChecking")));
            this.cbxConsignacion.ImageUnChecking = ((System.Drawing.Image)(resources.GetObject("cbxConsignacion.ImageUnChecking")));
            this.cbxConsignacion.Location = new System.Drawing.Point(12, 222);
            this.cbxConsignacion.Name = "cbxConsignacion";
            this.cbxConsignacion.Size = new System.Drawing.Size(231, 27);
            this.cbxConsignacion.TabIndex = 3;
            this.cbxConsignacion.Text = " NO permite a Consignación";
            this.cbxConsignacion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cbxConsignacion.UseVisualStyleBackColor = true;
            this.cbxConsignacion.CheckedChanged += new System.EventHandler(this.cbxConsignacion_CheckedChanged);
            // 
            // frmTiendaModal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(311, 359);
            this.Name = "frmTiendaModal";
            this.Text = "frmTiendaModal";
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.Body.PerformLayout();
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Autonomo.CustomControls.CustomCheck cbxConsignacion;
        private Autonomo.CustomControls.FlatComboBox cbResponsable;
        private Autonomo.CustomControls.FlatTextBox txDescripcion;
        private Autonomo.CustomControls.FlatTextBox txNombre;
    }
}