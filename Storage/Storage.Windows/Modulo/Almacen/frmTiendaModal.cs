﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Almacen
{
    public partial class frmTiendaModal : Autonomo.Object.Modal
    {
        private string IdTienda;
        public frmTiendaModal()
        {
            InitializeComponent();
            this.IdTienda = string.Empty;
        }

        public void LoadData(TiendaReport obj)
        {
            this.IdTienda = obj.IdTienda;
            txNombre.Text = obj.NombreTienda;
            txDescripcion.Text = obj.Descripcion;
            cbResponsable.SelectedValue = obj.IdResponsable;
            cbxConsignacion.Checked = obj.Consigna;
        }
        public void LoadCombo()
        {
            Tools.ComboBoxHelper.ComboBoxTrabajador(cbResponsable);
        }

        private void SaveChanges()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                string option = this.Tag.ToString();
                var result = uow.Tiendas.Crud(
                    new Entidad.Tienda()
                    {
                        IdTienda = this.IdTienda,
                        NombreTienda = txNombre.Text,
                        Descripcion = txDescripcion.Text,
                        IdResponsable = int.Parse(cbResponsable.SelectedValue.ToString()),
                        Consigna = cbxConsignacion.Checked,
                        EditorUser = Program.Usuario
                    }, option);
                if (result > 0) { base.Set(); }
            }
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges();
        }

        private void cbxConsignacion_CheckedChanged(object sender, EventArgs e)
        {
            var value = cbxConsignacion.Checked;
            cbxConsignacion.Text = value ? " SI permite a Consignación" : " NO permite a Consignación";
        }
    }
}
