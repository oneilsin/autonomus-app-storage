﻿using Storage.Datos;
using Storage.Entidad;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Storage.Windows.Modulo.Clientes
{
    public partial class frmCliente : Autonomo.Object.Registry
    {
        List<ClienteReport> clientes;
        public string Rol; // Proveedor
        public frmCliente()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                clientes = new List<ClienteReport>();
                clientes = uow.Clientes.GetList()
                    .Where(o => o.Rol.ToLower().Contains(Rol.ToLower()))
                    .ToList();
                FilterData();
            }
        }

        private void FilterData()
        {
            if (clientes != null && clientes.Count > 0)
            {
                //LinQ
                var newList = from o in clientes
                              select new
                              {
                                  IdCliente = o.IdCliente,
                                  TipoCliente = o.TipoCliente,
                                  CodigoCliente = o.CodigoCliente,
                                  TipoDocumento = o.TipoDocumento,
                                  NumeroDocumento = o.NumeroDocumento,
                                  Sexo = o.Sexo,
                                  FechaNacimiento = o.FechaNacimiento.ToShortDateString(),
                                  RazonSocial = o.RazonSocial
                              };

                // select * from Clientes where RazonSocial Like'param1%' or NumeDoc like'param2%' order by RazonSocial;
                string filter = txFilter.Text.ToLower();
                grdData.DataSource = newList.Where(o => o.RazonSocial.ToLower().StartsWith(filter)
                    || o.NumeroDocumento.ToLower().StartsWith(filter))
                    .OrderBy(o => o.RazonSocial)
                    .ToList();
            }
        }

        private void LoadModal(string title, string option)
        {
            var f = new frmClienteModal();
            f.Title.Text = title;
            f.LoadCombo();
            if (option == "Update")
            {
                string id = grdData.CurrentRow.Cells["IdCliente"].Value.ToString();
                var obj = clientes.FirstOrDefault(o => o.IdCliente.Equals(id));
                f.LoadData(obj);
            }
            Autonomo.Class.Fomulary.ShowModal(f, option, false);
            if (f.Tag.ToString() == "Get") { LoadData(); }
            f.Dispose();
        }

        private void frmCliente_Load(object sender, EventArgs e)
        {
            ThemeStyle(Theme.White);
            LoadData();
        }

        private void txFilter_TextBoxChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LoadModal($"Registrar nuevo {Rol}", "Insert");
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            if (grdData.Rows.Count > 0)
                LoadModal($"Actualizar datos del {Rol}", "Update");
        }
    }
}
