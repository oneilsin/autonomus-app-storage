﻿
namespace Storage.Windows.Modulo.Clientes
{
    partial class frmClienteModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmClienteModal));
            this.txNumeroDocumento = new Autonomo.CustomControls.FlatTextBox();
            this.cbTipoCliente = new Autonomo.CustomControls.FlatComboBox();
            this.dtFechaNacimiento = new Autonomo.CustomControls.FlatDateTime();
            this.cbxCliente = new Autonomo.CustomControls.CustomCheck();
            this.cbTipoDocumento = new Autonomo.CustomControls.FlatComboBox();
            this.txNombres = new Autonomo.CustomControls.FlatTextBox();
            this.txApellidos = new Autonomo.CustomControls.FlatTextBox();
            this.cbxProveedor = new Autonomo.CustomControls.CustomCheck();
            this.cbxEmpleado = new Autonomo.CustomControls.CustomCheck();
            this.label2 = new System.Windows.Forms.Label();
            this.cbSexo = new Autonomo.CustomControls.FlatComboBox();
            this.cbNivelPrecio = new Autonomo.CustomControls.FlatComboBox();
            this.cbxCredito = new Autonomo.CustomControls.CustomCheck();
            this.label1 = new System.Windows.Forms.Label();
            this.txLineaCrredito = new Autonomo.CustomControls.FlatTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Size = new System.Drawing.Size(500, 498);
            // 
            // Body
            // 
            this.Body.Controls.Add(this.label3);
            this.Body.Controls.Add(this.txLineaCrredito);
            this.Body.Controls.Add(this.label1);
            this.Body.Controls.Add(this.cbxCredito);
            this.Body.Controls.Add(this.cbNivelPrecio);
            this.Body.Controls.Add(this.cbSexo);
            this.Body.Controls.Add(this.label2);
            this.Body.Controls.Add(this.cbxEmpleado);
            this.Body.Controls.Add(this.cbxProveedor);
            this.Body.Controls.Add(this.cbxCliente);
            this.Body.Controls.Add(this.dtFechaNacimiento);
            this.Body.Controls.Add(this.cbTipoDocumento);
            this.Body.Controls.Add(this.cbTipoCliente);
            this.Body.Controls.Add(this.txApellidos);
            this.Body.Controls.Add(this.txNombres);
            this.Body.Controls.Add(this.txNumeroDocumento);
            this.Body.Size = new System.Drawing.Size(500, 405);
            this.Body.TabIndex = 0;
            this.Body.Paint += new System.Windows.Forms.PaintEventHandler(this.Body_Paint);
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 446);
            this.Footer.Size = new System.Drawing.Size(500, 52);
            this.Footer.TabIndex = 1;
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(500, 41);
            // 
            // Title
            // 
            this.Title.Size = new System.Drawing.Size(459, 41);
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Location = new System.Drawing.Point(244, 8);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txNumeroDocumento
            // 
            this.txNumeroDocumento.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txNumeroDocumento.BackColor = System.Drawing.Color.White;
            this.txNumeroDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txNumeroDocumento.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txNumeroDocumento.ColorLine = System.Drawing.Color.Gray;
            this.txNumeroDocumento.ColorText = System.Drawing.SystemColors.WindowText;
            this.txNumeroDocumento.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txNumeroDocumento.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txNumeroDocumento.Error = "";
            this.txNumeroDocumento.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txNumeroDocumento.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txNumeroDocumento.FormatLogin = false;
            this.txNumeroDocumento.ImageIcon = null;
            this.txNumeroDocumento.Info = "";
            this.txNumeroDocumento.Location = new System.Drawing.Point(159, 209);
            this.txNumeroDocumento.MaterialStyle = false;
            this.txNumeroDocumento.MaxLength = 32767;
            this.txNumeroDocumento.MultiLineText = false;
            this.txNumeroDocumento.Name = "txNumeroDocumento";
            this.txNumeroDocumento.PasswordChar = '\0';
            this.txNumeroDocumento.Placeholder = "";
            this.txNumeroDocumento.ReadOnly = false;
            this.txNumeroDocumento.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txNumeroDocumento.Size = new System.Drawing.Size(152, 58);
            this.txNumeroDocumento.SizeLine = 2;
            this.txNumeroDocumento.TabIndex = 2;
            this.txNumeroDocumento.Title = "Num. Documento";
            this.txNumeroDocumento.VisibleIcon = false;
            this.txNumeroDocumento.VisibleTitle = true;
            this.txNumeroDocumento.KeyPress += new System.EventHandler<System.Windows.Forms.KeyPressEventArgs>(this.txNumeroDocumento_KeyPress);
            // 
            // cbTipoCliente
            // 
            this.cbTipoCliente.BackColor = System.Drawing.Color.White;
            this.cbTipoCliente.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbTipoCliente.ColorLine = System.Drawing.Color.Gray;
            this.cbTipoCliente.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbTipoCliente.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbTipoCliente.DisplayMember = "";
            this.cbTipoCliente.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbTipoCliente.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTipoCliente.Error = "";
            this.cbTipoCliente.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbTipoCliente.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbTipoCliente.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbTipoCliente.ImageIcon")));
            this.cbTipoCliente.Info = "";
            this.cbTipoCliente.Location = new System.Drawing.Point(13, 17);
            this.cbTipoCliente.MaterialStyle = false;
            this.cbTipoCliente.Name = "cbTipoCliente";
            this.cbTipoCliente.Placeholder = "";
            this.cbTipoCliente.SelectedIndex = -1;
            this.cbTipoCliente.Size = new System.Drawing.Size(298, 58);
            this.cbTipoCliente.SizeLine = 2;
            this.cbTipoCliente.TabIndex = 0;
            this.cbTipoCliente.Title = "Estalece Tipo de Cliente";
            this.cbTipoCliente.ValueMember = "";
            this.cbTipoCliente.VisibleIcon = true;
            this.cbTipoCliente.VisibleTitle = true;
            // 
            // dtFechaNacimiento
            // 
            this.dtFechaNacimiento.BackColor = System.Drawing.Color.White;
            this.dtFechaNacimiento.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.dtFechaNacimiento.ColorLine = System.Drawing.Color.Gray;
            this.dtFechaNacimiento.ColorText = System.Drawing.SystemColors.WindowText;
            this.dtFechaNacimiento.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.dtFechaNacimiento.DateSeparatorFormat = '/';
            this.dtFechaNacimiento.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.dtFechaNacimiento.Error = "";
            this.dtFechaNacimiento.FontText = new System.Drawing.Font("Verdana", 10F);
            this.dtFechaNacimiento.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.dtFechaNacimiento.Format = Autonomo.CustomControls.HelperControl.FormatDate.Short;
            this.dtFechaNacimiento.ImageIcon = ((System.Drawing.Image)(resources.GetObject("dtFechaNacimiento.ImageIcon")));
            this.dtFechaNacimiento.Info = "";
            this.dtFechaNacimiento.Location = new System.Drawing.Point(12, 273);
            this.dtFechaNacimiento.MaterialStyle = false;
            this.dtFechaNacimiento.Name = "dtFechaNacimiento";
            this.dtFechaNacimiento.Placeholder = "";
            this.dtFechaNacimiento.Size = new System.Drawing.Size(140, 58);
            this.dtFechaNacimiento.SizeLine = 2;
            this.dtFechaNacimiento.TabIndex = 3;
            this.dtFechaNacimiento.Text = "22/03/2021";
            this.dtFechaNacimiento.Title = "F. Nacimiento";
            this.dtFechaNacimiento.Value = new System.DateTime(2021, 3, 22, 12, 10, 9, 617);
            this.dtFechaNacimiento.VisibleIcon = true;
            this.dtFechaNacimiento.VisibleTitle = true;
            // 
            // cbxCliente
            // 
            this.cbxCliente.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbxCliente.AutoSize = true;
            this.cbxCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxCliente.FlatAppearance.BorderSize = 0;
            this.cbxCliente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cbxCliente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cbxCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxCliente.Font = new System.Drawing.Font("Verdana", 10F);
            this.cbxCliente.Image = ((System.Drawing.Image)(resources.GetObject("cbxCliente.Image")));
            this.cbxCliente.ImageChecking = ((System.Drawing.Image)(resources.GetObject("cbxCliente.ImageChecking")));
            this.cbxCliente.ImageUnChecking = ((System.Drawing.Image)(resources.GetObject("cbxCliente.ImageUnChecking")));
            this.cbxCliente.Location = new System.Drawing.Point(322, 182);
            this.cbxCliente.Name = "cbxCliente";
            this.cbxCliente.Size = new System.Drawing.Size(83, 27);
            this.cbxCliente.TabIndex = 9;
            this.cbxCliente.Text = "Cliente";
            this.cbxCliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cbxCliente.UseVisualStyleBackColor = true;
            // 
            // cbTipoDocumento
            // 
            this.cbTipoDocumento.BackColor = System.Drawing.Color.White;
            this.cbTipoDocumento.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbTipoDocumento.ColorLine = System.Drawing.Color.Gray;
            this.cbTipoDocumento.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbTipoDocumento.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbTipoDocumento.DisplayMember = "";
            this.cbTipoDocumento.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbTipoDocumento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTipoDocumento.Error = "";
            this.cbTipoDocumento.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbTipoDocumento.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbTipoDocumento.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbTipoDocumento.ImageIcon")));
            this.cbTipoDocumento.Info = "";
            this.cbTipoDocumento.Location = new System.Drawing.Point(12, 209);
            this.cbTipoDocumento.MaterialStyle = false;
            this.cbTipoDocumento.Name = "cbTipoDocumento";
            this.cbTipoDocumento.Placeholder = "";
            this.cbTipoDocumento.SelectedIndex = -1;
            this.cbTipoDocumento.Size = new System.Drawing.Size(140, 58);
            this.cbTipoDocumento.SizeLine = 2;
            this.cbTipoDocumento.TabIndex = 1;
            this.cbTipoDocumento.Title = "T/Docum.";
            this.cbTipoDocumento.ValueMember = "";
            this.cbTipoDocumento.VisibleIcon = true;
            this.cbTipoDocumento.VisibleTitle = true;
            this.cbTipoDocumento.SelectedValueChanged += new System.EventHandler(this.cbTipoDocumento_SelectedValueChanged);
            // 
            // txNombres
            // 
            this.txNombres.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txNombres.BackColor = System.Drawing.Color.White;
            this.txNombres.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txNombres.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txNombres.ColorLine = System.Drawing.Color.Gray;
            this.txNombres.ColorText = System.Drawing.SystemColors.WindowText;
            this.txNombres.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txNombres.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txNombres.Error = "";
            this.txNombres.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txNombres.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txNombres.FormatLogin = false;
            this.txNombres.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txNombres.ImageIcon")));
            this.txNombres.Info = "";
            this.txNombres.Location = new System.Drawing.Point(12, 81);
            this.txNombres.MaterialStyle = false;
            this.txNombres.MaxLength = 32767;
            this.txNombres.MultiLineText = false;
            this.txNombres.Name = "txNombres";
            this.txNombres.PasswordChar = '\0';
            this.txNombres.Placeholder = "";
            this.txNombres.ReadOnly = false;
            this.txNombres.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txNombres.Size = new System.Drawing.Size(299, 58);
            this.txNombres.SizeLine = 2;
            this.txNombres.TabIndex = 4;
            this.txNombres.Title = "Nombres";
            this.txNombres.VisibleIcon = true;
            this.txNombres.VisibleTitle = true;
            // 
            // txApellidos
            // 
            this.txApellidos.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txApellidos.BackColor = System.Drawing.Color.White;
            this.txApellidos.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txApellidos.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txApellidos.ColorLine = System.Drawing.Color.Gray;
            this.txApellidos.ColorText = System.Drawing.SystemColors.WindowText;
            this.txApellidos.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txApellidos.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txApellidos.Error = "";
            this.txApellidos.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txApellidos.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txApellidos.FormatLogin = false;
            this.txApellidos.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txApellidos.ImageIcon")));
            this.txApellidos.Info = "";
            this.txApellidos.Location = new System.Drawing.Point(12, 145);
            this.txApellidos.MaterialStyle = false;
            this.txApellidos.MaxLength = 32767;
            this.txApellidos.MultiLineText = false;
            this.txApellidos.Name = "txApellidos";
            this.txApellidos.PasswordChar = '\0';
            this.txApellidos.Placeholder = "";
            this.txApellidos.ReadOnly = false;
            this.txApellidos.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txApellidos.Size = new System.Drawing.Size(299, 58);
            this.txApellidos.SizeLine = 2;
            this.txApellidos.TabIndex = 5;
            this.txApellidos.Title = "Apellidos";
            this.txApellidos.VisibleIcon = true;
            this.txApellidos.VisibleTitle = true;
            // 
            // cbxProveedor
            // 
            this.cbxProveedor.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbxProveedor.AutoSize = true;
            this.cbxProveedor.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxProveedor.FlatAppearance.BorderSize = 0;
            this.cbxProveedor.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cbxProveedor.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cbxProveedor.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxProveedor.Font = new System.Drawing.Font("Verdana", 10F);
            this.cbxProveedor.Image = ((System.Drawing.Image)(resources.GetObject("cbxProveedor.Image")));
            this.cbxProveedor.ImageChecking = ((System.Drawing.Image)(resources.GetObject("cbxProveedor.ImageChecking")));
            this.cbxProveedor.ImageUnChecking = ((System.Drawing.Image)(resources.GetObject("cbxProveedor.ImageUnChecking")));
            this.cbxProveedor.Location = new System.Drawing.Point(322, 215);
            this.cbxProveedor.Name = "cbxProveedor";
            this.cbxProveedor.Size = new System.Drawing.Size(107, 27);
            this.cbxProveedor.TabIndex = 10;
            this.cbxProveedor.Text = "Proveedor";
            this.cbxProveedor.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cbxProveedor.UseVisualStyleBackColor = true;
            // 
            // cbxEmpleado
            // 
            this.cbxEmpleado.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbxEmpleado.AutoSize = true;
            this.cbxEmpleado.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxEmpleado.FlatAppearance.BorderSize = 0;
            this.cbxEmpleado.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cbxEmpleado.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cbxEmpleado.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxEmpleado.Font = new System.Drawing.Font("Verdana", 10F);
            this.cbxEmpleado.Image = ((System.Drawing.Image)(resources.GetObject("cbxEmpleado.Image")));
            this.cbxEmpleado.ImageChecking = ((System.Drawing.Image)(resources.GetObject("cbxEmpleado.ImageChecking")));
            this.cbxEmpleado.ImageUnChecking = ((System.Drawing.Image)(resources.GetObject("cbxEmpleado.ImageUnChecking")));
            this.cbxEmpleado.Location = new System.Drawing.Point(322, 248);
            this.cbxEmpleado.Name = "cbxEmpleado";
            this.cbxEmpleado.Size = new System.Drawing.Size(104, 27);
            this.cbxEmpleado.TabIndex = 11;
            this.cbxEmpleado.Text = "Empleado";
            this.cbxEmpleado.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cbxEmpleado.UseVisualStyleBackColor = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(322, 163);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(126, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Establecer \"Roles\"";
            // 
            // cbSexo
            // 
            this.cbSexo.BackColor = System.Drawing.Color.White;
            this.cbSexo.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbSexo.ColorLine = System.Drawing.Color.Gray;
            this.cbSexo.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbSexo.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbSexo.DisplayMember = "";
            this.cbSexo.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbSexo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSexo.Error = "";
            this.cbSexo.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbSexo.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbSexo.ImageIcon = null;
            this.cbSexo.Info = "";
            this.cbSexo.Location = new System.Drawing.Point(158, 273);
            this.cbSexo.MaterialStyle = false;
            this.cbSexo.Name = "cbSexo";
            this.cbSexo.Placeholder = "";
            this.cbSexo.SelectedIndex = -1;
            this.cbSexo.Size = new System.Drawing.Size(153, 58);
            this.cbSexo.SizeLine = 2;
            this.cbSexo.TabIndex = 12;
            this.cbSexo.Title = "-Select- Sexo";
            this.cbSexo.ValueMember = "";
            this.cbSexo.VisibleIcon = false;
            this.cbSexo.VisibleTitle = true;
            // 
            // cbNivelPrecio
            // 
            this.cbNivelPrecio.BackColor = System.Drawing.Color.White;
            this.cbNivelPrecio.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbNivelPrecio.ColorLine = System.Drawing.Color.Gray;
            this.cbNivelPrecio.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbNivelPrecio.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbNivelPrecio.DisplayMember = "";
            this.cbNivelPrecio.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbNivelPrecio.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbNivelPrecio.Error = "";
            this.cbNivelPrecio.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbNivelPrecio.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbNivelPrecio.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbNivelPrecio.ImageIcon")));
            this.cbNivelPrecio.Info = "";
            this.cbNivelPrecio.Location = new System.Drawing.Point(13, 337);
            this.cbNivelPrecio.MaterialStyle = false;
            this.cbNivelPrecio.Name = "cbNivelPrecio";
            this.cbNivelPrecio.Placeholder = "";
            this.cbNivelPrecio.SelectedIndex = -1;
            this.cbNivelPrecio.Size = new System.Drawing.Size(298, 58);
            this.cbNivelPrecio.SizeLine = 2;
            this.cbNivelPrecio.TabIndex = 13;
            this.cbNivelPrecio.Title = "Establecer el nivel de Precio";
            this.cbNivelPrecio.ValueMember = "";
            this.cbNivelPrecio.VisibleIcon = true;
            this.cbNivelPrecio.VisibleTitle = true;
            // 
            // cbxCredito
            // 
            this.cbxCredito.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbxCredito.AutoSize = true;
            this.cbxCredito.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxCredito.FlatAppearance.BorderSize = 0;
            this.cbxCredito.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cbxCredito.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cbxCredito.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxCredito.Font = new System.Drawing.Font("Verdana", 10F);
            this.cbxCredito.Image = ((System.Drawing.Image)(resources.GetObject("cbxCredito.Image")));
            this.cbxCredito.ImageChecking = ((System.Drawing.Image)(resources.GetObject("cbxCredito.ImageChecking")));
            this.cbxCredito.ImageUnChecking = ((System.Drawing.Image)(resources.GetObject("cbxCredito.ImageUnChecking")));
            this.cbxCredito.Location = new System.Drawing.Point(322, 48);
            this.cbxCredito.Name = "cbxCredito";
            this.cbxCredito.Size = new System.Drawing.Size(167, 27);
            this.cbxCredito.TabIndex = 14;
            this.cbxCredito.Text = "No aplica a Crédito";
            this.cbxCredito.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cbxCredito.UseVisualStyleBackColor = true;
            this.cbxCredito.CheckedChanged += new System.EventHandler(this.cbxCredito_CheckedChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(322, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 16);
            this.label1.TabIndex = 15;
            this.label1.Text = "Estado crediticio";
            // 
            // txLineaCrredito
            // 
            this.txLineaCrredito.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txLineaCrredito.BackColor = System.Drawing.Color.White;
            this.txLineaCrredito.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txLineaCrredito.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txLineaCrredito.ColorLine = System.Drawing.Color.Gray;
            this.txLineaCrredito.ColorText = System.Drawing.SystemColors.WindowText;
            this.txLineaCrredito.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txLineaCrredito.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txLineaCrredito.Enabled = false;
            this.txLineaCrredito.Error = "";
            this.txLineaCrredito.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txLineaCrredito.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txLineaCrredito.FormatLogin = false;
            this.txLineaCrredito.ImageIcon = null;
            this.txLineaCrredito.Info = "";
            this.txLineaCrredito.Location = new System.Drawing.Point(322, 86);
            this.txLineaCrredito.MaterialStyle = false;
            this.txLineaCrredito.MaxLength = 32767;
            this.txLineaCrredito.MultiLineText = false;
            this.txLineaCrredito.Name = "txLineaCrredito";
            this.txLineaCrredito.PasswordChar = '\0';
            this.txLineaCrredito.Placeholder = "";
            this.txLineaCrredito.ReadOnly = false;
            this.txLineaCrredito.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txLineaCrredito.Size = new System.Drawing.Size(167, 58);
            this.txLineaCrredito.SizeLine = 2;
            this.txLineaCrredito.TabIndex = 16;
            this.txLineaCrredito.Text = "0.00";
            this.txLineaCrredito.Title = "Crédito";
            this.txLineaCrredito.VisibleIcon = false;
            this.txLineaCrredito.VisibleTitle = true;
            this.txLineaCrredito.KeyPress += new System.EventHandler<System.Windows.Forms.KeyPressEventArgs>(this.txLineaCrredito_KeyPress);
            this.txLineaCrredito.Leave += new System.EventHandler(this.txLineaCrredito_Leave);
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.label3.Location = new System.Drawing.Point(319, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(2, 365);
            this.label3.TabIndex = 17;
            // 
            // frmClienteModal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(500, 498);
            this.Name = "frmClienteModal";
            this.Text = "frmClienteModal";
            this.Load += new System.EventHandler(this.frmClienteModal_Load);
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.Body.PerformLayout();
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label2;
        private Autonomo.CustomControls.CustomCheck cbxEmpleado;
        private Autonomo.CustomControls.CustomCheck cbxProveedor;
        private Autonomo.CustomControls.CustomCheck cbxCliente;
        private Autonomo.CustomControls.FlatDateTime dtFechaNacimiento;
        private Autonomo.CustomControls.FlatComboBox cbTipoDocumento;
        private Autonomo.CustomControls.FlatComboBox cbTipoCliente;
        private Autonomo.CustomControls.FlatTextBox txApellidos;
        private Autonomo.CustomControls.FlatTextBox txNombres;
        private Autonomo.CustomControls.FlatTextBox txNumeroDocumento;
        private Autonomo.CustomControls.FlatComboBox cbSexo;
        private System.Windows.Forms.Label label1;
        private Autonomo.CustomControls.CustomCheck cbxCredito;
        private Autonomo.CustomControls.FlatComboBox cbNivelPrecio;
        private Autonomo.CustomControls.FlatTextBox txLineaCrredito;
        private System.Windows.Forms.Label label3;
    }
}