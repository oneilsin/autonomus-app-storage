﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Clientes
{
    public partial class frmClienteModal : Autonomo.Object.Modal
    {
        private string IdCliente;
        public frmClienteModal()
        {
            InitializeComponent();
            this.IdCliente = "";
        }

        public void LoadData(ClienteReport obj)
        {
            this.IdCliente = obj.IdCliente;
            cbTipoCliente.SelectedValue = obj.IdTipoCliente;
            cbTipoDocumento.SelectedValue = obj.IdTipoDocumento;
            txNumeroDocumento.Text = obj.NumeroDocumento;
            dtFechaNacimiento.Value = obj.FechaNacimiento;
            txNombres.Text = obj.Nombres;
            txApellidos.Text = obj.Apellidos;
            cbSexo.SelectedValue = obj.Sexo;
            cbNivelPrecio.SelectedValue = obj.NivelPrecio.Split(':')[0]; // 00: Descripcion --> [00] [Descripcion]
            cbxCredito.Checked = obj.FlagCredito;
            txLineaCrredito.Text = obj.LineaCredito.ToString();
            string[] rol = obj.Rol.Split(',');
            SetRol(rol);
        }
        public void LoadCombo()
        {
            Tools.ComboBoxHelper.ComboBoxGeneral(cbTipoCliente, "01", "Descripcion");
            Tools.ComboBoxHelper.ComboBoxGeneral(cbTipoDocumento, "02", "Abreviatura");
            Tools.ComboBoxHelper.ComboBoxSexo(cbSexo);
            Tools.ComboBoxHelper.ComboBoxNivelPrecio(cbNivelPrecio);
        }
        private void SaveChanges()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                string option = this.Tag.ToString();
                var result = uow.Clientes.Crud(
                    new Entidad.Cliente()
                    {
                        IdCliente = this.IdCliente,
                        IdTipoCliente = cbTipoCliente.SelectedValue.ToString(),
                        IdTipoDocumento = cbTipoDocumento.SelectedValue.ToString(),
                        NumeroDocumento = txNumeroDocumento.Text,
                        Nombres = txNombres.Text,
                        Apellidos = txApellidos.Text,
                        Sexo = cbSexo.SelectedValue.ToString(),
                        FechaNacimiento = dtFechaNacimiento.Value,
                        Rol = GetRol(),
                        EditorUser = Program.Usuario,
                        IdNivelPrecio=cbNivelPrecio.SelectedValue.ToString(),
                        LineaCredito=decimal.Parse(txLineaCrredito.Text),
                        FlagCredito=cbxCredito.Checked
                    }, option);
                if (result > 0) { base.Set(); }
            }
        }
        private string GetRol()
        {
            string value = string.Empty;
            if (cbxCliente.Checked)
                value += "Cliente,";
            if (cbxEmpleado.Checked)
                value += "Empleado,";
            if (cbxProveedor.Checked)
                value += "Proveedor,";
            return value;
        }
       
        private void SetRol(string[] rol)
        {
            foreach (var item in rol)
            {
                if (item == "Cliente")
                    cbxCliente.Checked = true;
                if (item == "Proveedor")
                    cbxProveedor.Checked = true;
                if (item == "Empleado")
                    cbxEmpleado.Checked = true;
            }
        }
        private GeneralReport GetGeneral(string id)
        {
            GeneralReport obj = new GeneralReport();
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Generals.GetGeneralList("02");
                if (element != null)
                {
                    obj = element.FirstOrDefault(o => o.IdTabla.Equals(id));
                }
            }
            return obj;
        }
        private void frmClienteModal_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges();
        }

        private void txNumeroDocumento_KeyPress(object sender, KeyPressEventArgs e)
        {
            Autonomo.Class.Validating.OnlyNumber(e);
        }

        private void cbTipoDocumento_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbTipoDocumento.Items.Count > 0)
            {
                txNumeroDocumento.Focus();//
                string id = cbTipoDocumento.SelectedValue.ToString();
                var obj = GetGeneral(id);
                if (obj != null)
                {
                    int dimension = obj.Dimension;
                    txNumeroDocumento.MaxLength = dimension;
                }               
            }
        }

        private void Body_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cbxCredito_CheckedChanged(object sender, EventArgs e)
        {
            var value = cbxCredito.Checked;
            cbxCredito.Text = value ? "SI aplica a Crédito" : "NO aplica a Crédito";
            txLineaCrredito.Enabled = value;
        }

        private void txLineaCrredito_KeyPress(object sender, KeyPressEventArgs e)
        {
            Autonomo.Class.Validating.OnlyDecimal(e);
        }

        private void txLineaCrredito_Leave(object sender, EventArgs e)
        {
            if (txLineaCrredito.Text.Length > 0) return;
            txLineaCrredito.Text = "0.00";
        }
    }
}
