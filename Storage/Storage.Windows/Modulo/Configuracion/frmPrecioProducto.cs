﻿using Storage.Datos;
using Storage.Entidad.Report;
using Storage.Windows.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Configuracion
{
    public partial class frmPrecioProducto : Autonomo.CustomTemplate.RegistryDouble
    {
        List<ProductoReport> _productos;
        public frmPrecioProducto()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                _productos = new List<ProductoReport>();
                _productos = uow.Productos.GetList().ToList();

                if (_productos != null && _productos.Count > 0)
                {
                    FilterData();
                }
            }
        }

        private void FilterData()
        {
            var newList = _productos.Where((o) =>
            o.NombreProducto.Contains(txFilter.Text) || o.Descripcion.Contains(txFilter.Text))
                .Select((obj) => new
                {
                    IdProducto = obj.IdProducto,
                    NombreProducto = obj.NombreProducto,
                    PrecioBase = obj.PrecioBase,
                    Descripcion = obj.Descripcion
                }).OrderBy((p) => p.NombreProducto).ToList();

            grdData.DataSource = newList;
            grdData.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            grdData.Columns["Descripcion"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
        }
        private void LoadDetail(string idProducto)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var _precio = uow.Generals.GetPrecioProductoDetails(idProducto).ToList();

                if (_precio != null && _precio.Count > 0)
                {
                    grdDetalle.DataSource = _precio;
                    grdDetalle.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    grdDetalle.Columns["NivelPrecio"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                }
            }
        }

        private void ModalUpdatePrecio()
        {
            // Validar si  hay registros en los detalles
            if (!DataGridHelper.ValidateEmptyGrid(grdDetalle)) return;
            // mensaje ....
            DataGridViewRow row = grdDetalle.CurrentRow;
            var f = new frmPrecioProductoModal();
            f.Title.Text = "Actualizar precios";
            f.LoadData(
                row.Cells["IdPrecio"].Value.ToString(),
                grdData.CurrentRow.Cells["NombreProducto"].Value.ToString(),
                row.Cells["NivelPrecio"].Value.ToString(),
                row.Cells["Precio"].Value.ToString()
                );

            Autonomo.Class.Fomulary.ShowModal(f, "", false);
            if (f.StateFormulary)
                LoadData();
        }
        private void frmPrecioProducto_Load(object sender, EventArgs e)
        {
            base.ThemeStyle(Autonomo.CustomControls.HelperControl.Theme.White);
            LoadData();
        }

        private void txFilter_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                FilterData();
        }

        private void grdData_SelectionChanged(object sender, EventArgs e)
        {
            if (grdData.SelectedRows.Count > 0)
            {
                string idProducto = grdData.CurrentRow.Cells["IdProducto"].Value.ToString();
                LoadDetail(idProducto);
            }
            else { grdDetalle.DataSource = null; }
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            ModalUpdatePrecio();
        }
    }
}
