﻿
namespace Storage.Windows.Modulo.Configuracion
{
    partial class frmPrecioProductoModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmPrecioProductoModal));
            this.label1 = new System.Windows.Forms.Label();
            this.txProducto = new Autonomo.CustomControls.FlatTextBox();
            this.txNivel = new Autonomo.CustomControls.FlatTextBox();
            this.txPrecio = new Autonomo.CustomControls.FlatTextBox();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Size = new System.Drawing.Size(356, 287);
            // 
            // Body
            // 
            this.Body.Controls.Add(this.txPrecio);
            this.Body.Controls.Add(this.txNivel);
            this.Body.Controls.Add(this.txProducto);
            this.Body.Controls.Add(this.label1);
            this.Body.Size = new System.Drawing.Size(356, 194);
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 235);
            this.Footer.Size = new System.Drawing.Size(356, 52);
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(356, 41);
            // 
            // Title
            // 
            this.Title.Size = new System.Drawing.Size(315, 41);
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Location = new System.Drawing.Point(101, 8);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.DimGray;
            this.label1.Location = new System.Drawing.Point(12, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(289, 51);
            this.label1.TabIndex = 0;
            this.label1.Text = "Actualizar costos de productos según el nivel de precio";
            // 
            // txProducto
            // 
            this.txProducto.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txProducto.BackColor = System.Drawing.Color.White;
            this.txProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txProducto.ColorFocus = System.Drawing.Color.Gray;
            this.txProducto.ColorLine = System.Drawing.Color.Gray;
            this.txProducto.ColorText = System.Drawing.Color.Gray;
            this.txProducto.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txProducto.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txProducto.Enabled = false;
            this.txProducto.Error = "";
            this.txProducto.FontText = new System.Drawing.Font("Verdana", 10.25F, System.Drawing.FontStyle.Bold);
            this.txProducto.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txProducto.FormatLogin = false;
            this.txProducto.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txProducto.ImageIcon")));
            this.txProducto.Info = "";
            this.txProducto.Location = new System.Drawing.Point(15, 61);
            this.txProducto.MaterialStyle = false;
            this.txProducto.MaxLength = 32767;
            this.txProducto.MultiLineText = false;
            this.txProducto.Name = "txProducto";
            this.txProducto.PasswordChar = '\0';
            this.txProducto.Placeholder = "";
            this.txProducto.ReadOnly = true;
            this.txProducto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txProducto.Size = new System.Drawing.Size(320, 58);
            this.txProducto.SizeLine = 2;
            this.txProducto.TabIndex = 5;
            this.txProducto.Title = "Producto seleccionado";
            this.txProducto.VisibleIcon = true;
            this.txProducto.VisibleTitle = true;
            // 
            // txNivel
            // 
            this.txNivel.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txNivel.BackColor = System.Drawing.Color.White;
            this.txNivel.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txNivel.ColorFocus = System.Drawing.Color.Gray;
            this.txNivel.ColorLine = System.Drawing.Color.Gray;
            this.txNivel.ColorText = System.Drawing.Color.Gray;
            this.txNivel.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txNivel.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txNivel.Enabled = false;
            this.txNivel.Error = "";
            this.txNivel.FontText = new System.Drawing.Font("Verdana", 10.25F, System.Drawing.FontStyle.Bold);
            this.txNivel.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txNivel.FormatLogin = false;
            this.txNivel.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txNivel.ImageIcon")));
            this.txNivel.Info = "";
            this.txNivel.Location = new System.Drawing.Point(15, 114);
            this.txNivel.MaterialStyle = false;
            this.txNivel.MaxLength = 32767;
            this.txNivel.MultiLineText = false;
            this.txNivel.Name = "txNivel";
            this.txNivel.PasswordChar = '\0';
            this.txNivel.Placeholder = "";
            this.txNivel.ReadOnly = true;
            this.txNivel.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txNivel.Size = new System.Drawing.Size(196, 58);
            this.txNivel.SizeLine = 2;
            this.txNivel.TabIndex = 6;
            this.txNivel.Title = "Nivel de precio";
            this.txNivel.VisibleIcon = true;
            this.txNivel.VisibleTitle = true;
            // 
            // txPrecio
            // 
            this.txPrecio.AlignText = System.Windows.Forms.HorizontalAlignment.Center;
            this.txPrecio.BackColor = System.Drawing.Color.White;
            this.txPrecio.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txPrecio.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txPrecio.ColorLine = System.Drawing.Color.Gray;
            this.txPrecio.ColorText = System.Drawing.SystemColors.WindowText;
            this.txPrecio.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txPrecio.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txPrecio.Error = "";
            this.txPrecio.FontText = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txPrecio.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txPrecio.FormatLogin = false;
            this.txPrecio.ImageIcon = null;
            this.txPrecio.Info = "";
            this.txPrecio.Location = new System.Drawing.Point(228, 114);
            this.txPrecio.MaterialStyle = false;
            this.txPrecio.MaxLength = 32767;
            this.txPrecio.MultiLineText = false;
            this.txPrecio.Name = "txPrecio";
            this.txPrecio.PasswordChar = '\0';
            this.txPrecio.Placeholder = "";
            this.txPrecio.ReadOnly = false;
            this.txPrecio.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txPrecio.Size = new System.Drawing.Size(107, 58);
            this.txPrecio.SizeLine = 2;
            this.txPrecio.TabIndex = 7;
            this.txPrecio.Text = "0.00";
            this.txPrecio.Title = "Precio (#.00)";
            this.txPrecio.VisibleIcon = false;
            this.txPrecio.VisibleTitle = true;
            this.txPrecio.KeyPress += new System.EventHandler<System.Windows.Forms.KeyPressEventArgs>(this.txPrecio_KeyPress);
            // 
            // frmPrecioProductoModal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(356, 287);
            this.Name = "frmPrecioProductoModal";
            this.Text = "frmPrecioProductoModal";
            this.Load += new System.EventHandler(this.frmPrecioProductoModal_Load);
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Autonomo.CustomControls.FlatTextBox txNivel;
        private Autonomo.CustomControls.FlatTextBox txProducto;
        private Autonomo.CustomControls.FlatTextBox txPrecio;
    }
}