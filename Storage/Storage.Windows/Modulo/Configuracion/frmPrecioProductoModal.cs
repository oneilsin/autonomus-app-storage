﻿using Storage.Datos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Configuracion
{
    public partial class frmPrecioProductoModal : Autonomo.Object.Modal
    {
        string idPrecio;
        public frmPrecioProductoModal()
        {
            InitializeComponent();
        }

        public void LoadData(string id, string producto, 
            string nivel, string precio)
        {
            idPrecio = id;
            txProducto.Text = producto;
            txNivel.Text = nivel;
            txPrecio.Text = precio;
        }

        private void SaveChanges()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var response = uow.Generals.UpdatePrecios(
                    idPrecio, decimal.Parse(txPrecio.Text));

                if (response > 0)
                    base.Set();
            }
        }

        private void frmPrecioProductoModal_Load(object sender, EventArgs e)
        {

        }

        private void txPrecio_KeyPress(object sender, KeyPressEventArgs e)
        {
            Autonomo.Class.Validating.OnlyDecimal(e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges();
        }
    }
}
