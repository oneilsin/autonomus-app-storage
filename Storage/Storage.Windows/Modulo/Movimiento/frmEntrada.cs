﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Storage.Windows.Modulo.Movimiento
{
    public partial class frmEntrada : Autonomo.Object.Registry
    {
        List<EntradaReport> _entradaList;
        public frmEntrada()
        {
            InitializeComponent();
        }
        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                _entradaList = new List<EntradaReport>();
                _entradaList = uow.Entrada.GetList(dtDesde.Value, dtHasta.Value)
                    .OrderByDescending(o => o.FechaEntrada)
                    .ToList();

                grdData.DataSource = null;
                grdData.Refresh();
                FilterData();
            }
        }
        private void FilterData()
        {
            if (_entradaList != null && _entradaList.Count > 0)
            {              
                var data = _entradaList
                    .Where(o => o.Documento.ToLower()
                    .Contains(txFilter.Text.ToLower()))
                    .ToList();

                grdData.DataSource = data.ToList();
                grdData.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
        }
        private void LoadModal(string title, string option)
        {
            var f = new frmEntradaModal();
            f.Title.Text = title;
            f.LoadCombo();
            Autonomo.Class.Fomulary.ShowModal(f, option, false);
            // StateFormulary nos difica el estado del Formualrio,
            // está en la nueva version de Autonomo.dll
            if (f.StateFormulary)
                LoadData();

            //if (f.Tag.ToString() == "Get") { LoadData(); }
            f.Dispose();
        }

        private void GetDetail(string documento)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Inventario.GetMovimientos(documento, "E").ToList();
                grdDetalle.DataSource = element;
                grdDetalle.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
        }
        private void frmEntrada_Load(object sender, EventArgs e)
        {
            dtDesde.Value = DateTime.Now;
            dtHasta.Value = DateTime.Now;
            ThemeStyle(Theme.White);
            LoadData();
        }
        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LoadModal($"Registrar nueva Entrada", "Insert");
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
           
        }

        private void txFilter_TextBoxChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        private void btnProcesar_Click(object sender, EventArgs e)
        {
         LoadData();
        }

        private void grdData_SelectionChanged(object sender, EventArgs e)
        {
            if (grdData.SelectedRows.Count > 0)
            {
                // Considerar utilizar el nombre la columna.... ["NombreColumna"]
                string _idEntrada = grdData.CurrentRow.Cells[0].Value.ToString();
                GetDetail(_idEntrada);
            }
        }
    }
}
