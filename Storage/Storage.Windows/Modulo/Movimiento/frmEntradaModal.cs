﻿using Storage.Datos;
using Storage.Windows.Tools;
using Storage.Windows.Tools.Formulary;
using Storage.Windows.Tools.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Movimiento
{
    public partial class frmEntradaModal : Autonomo.Object.Modal
    {
        private string IdEntrada;
        public frmEntradaModal()
        {
            InitializeComponent();
            IdEntrada = string.Empty;
        }
        //public void LoadData(ClienteReport obj)
        //{
        //    this.IdCliente = obj.IdCliente;
        //    cbTipoCliente.SelectedValue = obj.IdTipoCliente;
        //    cbTipoDocumento.SelectedValue = obj.IdTipoDocumento;
        //    txNumeroDocumento.Text = obj.NumeroDocumento;
        //    dtFechaNacimiento.Value = obj.FechaNacimiento;
        //    txNombres.Text = obj.Nombres;
        //    txApellidos.Text = obj.Apellidos;
        //    string sexo = obj.Sexo;
        //    SetSexo(sexo);
        //    string[] rol = obj.Rol.Split(',');
        //    SetRol(rol);
        //}

        public void LoadCombo()
        {
            Tools.ComboBoxHelper.ComboBoxTienda(cbTienda);
            Tools.ComboBoxHelper.ComboBoxProveedor(cbProveedor);
            Tools.ComboBoxHelper.ComboBoxTipoMovimiento(cbTMovimiento, "E");           
        }


        private void GetProduct()
        {
            var f = new frmConsultaProducto();
            Autonomo.Class.Fomulary.ShowModal(f, "", false);
            if (f.StateFormulary)
            {
                txProducto.Text = f.Producto.NombreProducto;
                txProducto.TextId = f.Producto.IdProducto;
                // Tag de txProducto: Almancena la Categoría;
                txProducto.Tag = f.Producto.Categoria;
            }
        }

        private void SaveChanges()
        {
            if (!ValidateControls())
            {
                // mensaje de  campos invalidos:: MessageBox:Personalizado
                return;
            }

            try
            {
                using (UnitOfWork uow = new UnitOfWork())
                {
                    string GetCorrelative = uow.Entrada.GetLastId();
                    int response = 0;

                    foreach (DataGridViewRow row in grdCanastilla.Rows)
                    {
                        string _idProducto = row.Cells["IdProduct"].Value.ToString();
                        decimal _cantidad = decimal.Parse(row.Cells["Cantidad"].Value.ToString());
                        decimal _precio = decimal.Parse(row.Cells["Precio"].Value.ToString());

                        response += uow.Entrada.Crud(
                          new Entidad.Entrada()
                          {
                              IdEntrada = GetCorrelative,
                              IdTienda = cbTienda.SelectedValue.ToString(),
                              IdProveedor = cbProveedor.SelectedValue.ToString(),
                              Documento = txDocumento.Text,
                              EditorUser = Program.Usuario
                          }, new Entidad.Inventario()
                          {
                              IdTipoMov = cbTMovimiento.SelectedValue.ToString(),
                              IdProducto = _idProducto,
                              Cantidad = _cantidad,
                              PrecioUnitario = _precio
                          });
                    }

                    if (response > 0)
                    {
                        //MessageBox Personalizado... pronto.
                        MessageBox.Show($"Se ha insertado {response} registros correctamente.");
                        base.Set();
                    }
                }
            }
            catch (Exception ex)
            {
                //MessageBox Personalizado... pronto.
                MessageBox.Show(ex.Message);
            }
        }

        private bool ValidateControls()
        {
            if (string.IsNullOrEmpty(cbTienda.SelectedValue.ToString()))
            {
                cbTienda.Error = "Este campo es requerido";
                return false;
            }
            cbTienda.Error = "";

            if (string.IsNullOrEmpty(cbProveedor.SelectedValue.ToString()))
            {
                cbProveedor.Error = "Este campo es requerido";
                return false;
            }
            cbProveedor.Error = "";

            if (string.IsNullOrEmpty(txDocumento.Text))
            {
                txDocumento.Error = "Este campo es requerido";
                return false;
            }
            txDocumento.Error = "";

            if (string.IsNullOrEmpty(cbTMovimiento.SelectedValue.ToString()))
            {
                cbTMovimiento.Error = "Este campo es requerido";
                return false;
            }
            cbTMovimiento.Error = "";

            if (!DataGridHelper.ValidateEmptyGrid(grdCanastilla))
            {
                lblMessageGrid.Text = "No se ha encontrado detalles, seleccionar al menos uno.";
                lblMessageGrid.BackColor = Color.Coral;
                return false;
            }

            lblMessageGrid.Text = "";
            lblMessageGrid.BackColor = Color.SeaGreen;

            return true;
        }

        private void frmEntradaModal_Load(object sender, EventArgs e)
        {
            if (new Validation().EsVendedor())
            {
                cbTienda.SelectedValue = Program.Local;
                cbTienda.Enabled = false;
            }
        }

        private void txProducto_ButtonClick(object sender, EventArgs e)
        {
            GetProduct();
        }

        private void txProducto_KeyDown(object sender, KeyEventArgs e)
        {
            //Ejecutar cuando se presiona Enter
            if (e.KeyCode == Keys.Enter)
            {
                GetProduct();
            }
        }

        private void txPUnitario_KeyPress(object sender, KeyPressEventArgs e)
        {
            Autonomo.Class.Validating.OnlyDecimal(e);
        }

        private void txPUnitario_Leave(object sender, EventArgs e)
        {
            if (txPUnitario.Text.Length > 0) return;
            txPUnitario.Text = "0.00";
        }

        private void txCantidad_Leave(object sender, EventArgs e)
        {
            if (txCantidad.Text.Length > 0) return;
            txCantidad.Text = "1.00";
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            //Validamos si se ha seleccionado algun producto
            if (string.IsNullOrEmpty(txProducto.TextId))
            {
                lblMessageGrid.Text = "No se ha seleccionado ningún producto, vuelva a intentarlo.";
                lblMessageGrid.BackColor = Color.Coral;
                return;
            }

            var canastilla = new CanastillaModels()
            {
                IdItem = txProducto.TextId,
                NameItem = txProducto.Text,
                CategoryItem = txProducto.Tag.ToString(),
                PriceItem = decimal.Parse(txPUnitario.Text),
                QuantityItem = decimal.Parse(txCantidad.Text)
            };

            var response = DataGridHelper.PopulateGrid(grdCanastilla, Enumerables.LlenarGrilla.Entrada, canastilla);

            // Validamos si se ha insertado o no::: Insertado=1, No Insertado=0.
            MostrarTotales();
            LimpiarAgregarCanastilla();

            lblMessageGrid.Text = response.Mensaje;
            if (response.Estado)
            {
                lblMessageGrid.BackColor = Color.SeaGreen;
                return;
            }
            lblMessageGrid.BackColor = Color.Coral;
        }

        private void MostrarTotales()
        {
            lblTotals.Text = $"{grdCanastilla.Rows.Count}, Items en canastilla";

            var canasta = DataGridHelper.GetTotals(grdCanastilla, Enumerables.LlenarGrilla.Entrada);
            lblPrecio.Text = canasta.PriceItem.ToString();
            lblCantidad.Text = canasta.QuantityItem.ToString();
        }

        private void LimpiarAgregarCanastilla()
        {
            txProducto.Text = "";
            txProducto.Tag = "";
            txProducto.TextId = "";
            txCantidad.Text = "1.00";
            txPUnitario.Text = "0.00";
        }
        private void grdCanastilla_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (grdCanastilla.Rows.Count > 0)
            {
                MostrarTotales();
            }
        }

        private void grdCanastilla_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (grdCanastilla.SelectedRows.Count > 0)
            {
                //Verificamos en que columna se hace Click.
                var column = grdCanastilla.Columns[e.ColumnIndex];
                if (column.Index == 2) return;

                int rowIndex = grdCanastilla.CurrentCell.RowIndex;
                DataGridHelper.RemoveRows(grdCanastilla, rowIndex);
                MostrarTotales();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges();
        }
    }
}
