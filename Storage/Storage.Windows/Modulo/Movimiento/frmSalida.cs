﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Storage.Windows.Modulo.Movimiento
{
    public partial class frmSalida : Autonomo.CustomTemplate.RegistryDouble
    {
        List<SalidaReport> _salidaList;
        public frmSalida()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                _salidaList = new List<SalidaReport>();
                _salidaList = uow.Salida.GetList(dtDesde.Value, dtHasta.Value)
                    .OrderByDescending(o => o.FechaSalida)
                    .ToList();

                grdData.DataSource = null;
                grdData.Refresh();

                FilterData();
            }
        }

        private void FilterData()
        {
            if (_salidaList != null && _salidaList.Count > 0)
            {
                var data = _salidaList
                    .Where(o => o.Documento.ToLower()
                    .Contains(txFilter.Text.ToLower()));

                grdData.DataSource = data.ToList();
                grdData.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
        }

        private void LoadModal(string title, string option)
        {
            var f = new frmSalidaModal();
            f.Title.Text = title;
            f.LoadCombo();
            Autonomo.Class.Fomulary.ShowModal(f, option, false);
            // StateFormulary nos difica el estado del Formualrio,
            // está en la nueva version de Autonomo.dll
            if (f.StateFormulary)
                LoadData();

            //if (f.Tag.ToString() == "Get") { LoadData(); }
            f.Dispose();
        }

        /// <summary>
        /// Metodo para obtener el detalle de las saldias, según documento que es el ID de la salida.
        /// </summary>
        /// <param name="documento">es el ID de la salida.</param>
        private void GetDetail(string documento)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Inventario.GetMovimientos(documento, "S").ToList();
                grdDetalle.DataSource = element;
                grdDetalle.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
        }

        private void frmSalida_Load(object sender, EventArgs e)
        {
            dtDesde.Value = DateTime.Now;
            dtHasta.Value = DateTime.Now;
            ThemeStyle(Theme.White);
            LoadData();
        }

        private void grdData_SelectionChanged(object sender, EventArgs e)
        {
            if (grdData.SelectedRows.Count > 0)
            {
                // Considerar utilizar el nombre la columna.... ["NombreColumna"]
                string _idSalida = grdData.CurrentRow.Cells["IdSalida"].Value.ToString();
                GetDetail(_idSalida);
            }
        }

        private void txFilter_TextBoxChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        private void btnProcesar_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LoadModal($"Registrar nueva Salida", "Insert");
        }
    }
}
