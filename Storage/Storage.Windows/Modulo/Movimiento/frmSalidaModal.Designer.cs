﻿
namespace Storage.Windows.Modulo.Movimiento
{
    partial class frmSalidaModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSalidaModal));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.cbTMovimiento = new Autonomo.CustomControls.FlatComboBox();
            this.txDocumento = new Autonomo.CustomControls.FlatTextBox();
            this.cbTienda = new Autonomo.CustomControls.FlatComboBox();
            this.txProducto = new Autonomo.CustomControls.FlatFindText();
            this.btnAgregar = new Autonomo.CustomControls.CustomButton();
            this.txCantidad = new Autonomo.CustomControls.FlatTextBox();
            this.grdCanastilla = new Autonomo.CustomControls.CustomGrid();
            this.IdProduct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Producto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblCantidad = new System.Windows.Forms.Label();
            this.lblTotals = new System.Windows.Forms.Label();
            this.lblMessageGrid = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCanastilla)).BeginInit();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Size = new System.Drawing.Size(847, 397);
            // 
            // Body
            // 
            this.Body.Controls.Add(this.lblMessageGrid);
            this.Body.Controls.Add(this.label2);
            this.Body.Controls.Add(this.lblCantidad);
            this.Body.Controls.Add(this.lblTotals);
            this.Body.Controls.Add(this.grdCanastilla);
            this.Body.Controls.Add(this.btnAgregar);
            this.Body.Controls.Add(this.txCantidad);
            this.Body.Controls.Add(this.txProducto);
            this.Body.Controls.Add(this.cbTMovimiento);
            this.Body.Controls.Add(this.txDocumento);
            this.Body.Controls.Add(this.cbTienda);
            this.Body.Size = new System.Drawing.Size(847, 304);
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 345);
            this.Footer.Size = new System.Drawing.Size(847, 52);
            this.Footer.TabIndex = 7;
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(847, 41);
            // 
            // Title
            // 
            this.Title.Size = new System.Drawing.Size(806, 41);
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Location = new System.Drawing.Point(680, 8);
            this.btnSave.TabIndex = 0;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbTMovimiento
            // 
            this.cbTMovimiento.BackColor = System.Drawing.Color.White;
            this.cbTMovimiento.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbTMovimiento.ColorLine = System.Drawing.Color.Gray;
            this.cbTMovimiento.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbTMovimiento.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbTMovimiento.DisplayMember = "";
            this.cbTMovimiento.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbTMovimiento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTMovimiento.Error = "";
            this.cbTMovimiento.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbTMovimiento.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbTMovimiento.ImageIcon = null;
            this.cbTMovimiento.Info = "Campo requerido";
            this.cbTMovimiento.Location = new System.Drawing.Point(12, 143);
            this.cbTMovimiento.MaterialStyle = false;
            this.cbTMovimiento.Name = "cbTMovimiento";
            this.cbTMovimiento.Placeholder = "";
            this.cbTMovimiento.SelectedIndex = -1;
            this.cbTMovimiento.Size = new System.Drawing.Size(264, 58);
            this.cbTMovimiento.SizeLine = 2;
            this.cbTMovimiento.TabIndex = 2;
            this.cbTMovimiento.Title = "Seleccionar Tipo de Movimiento";
            this.cbTMovimiento.ValueMember = "";
            this.cbTMovimiento.VisibleIcon = true;
            this.cbTMovimiento.VisibleTitle = true;
            // 
            // txDocumento
            // 
            this.txDocumento.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txDocumento.BackColor = System.Drawing.Color.White;
            this.txDocumento.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txDocumento.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txDocumento.ColorLine = System.Drawing.Color.Gray;
            this.txDocumento.ColorText = System.Drawing.SystemColors.WindowText;
            this.txDocumento.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txDocumento.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txDocumento.Error = "";
            this.txDocumento.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txDocumento.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txDocumento.ImageIcon = null;
            this.txDocumento.Info = "Campo requerido";
            this.txDocumento.Location = new System.Drawing.Point(12, 79);
            this.txDocumento.MaterialStyle = true;
            this.txDocumento.MaxLength = 32767;
            this.txDocumento.MultiLineText = false;
            this.txDocumento.Name = "txDocumento";
            this.txDocumento.PasswordChar = '\0';
            this.txDocumento.Placeholder = "Número de documento";
            this.txDocumento.ReadOnly = false;
            this.txDocumento.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txDocumento.Size = new System.Drawing.Size(264, 58);
            this.txDocumento.SizeLine = 2;
            this.txDocumento.TabIndex = 1;
            this.txDocumento.Title = "Número de documento";
            this.txDocumento.VisibleIcon = true;
            this.txDocumento.VisibleTitle = false;
            // 
            // cbTienda
            // 
            this.cbTienda.BackColor = System.Drawing.Color.White;
            this.cbTienda.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbTienda.ColorLine = System.Drawing.Color.Gray;
            this.cbTienda.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbTienda.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbTienda.DisplayMember = "";
            this.cbTienda.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbTienda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTienda.Error = "";
            this.cbTienda.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbTienda.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbTienda.ImageIcon = null;
            this.cbTienda.Info = "Campo requerido";
            this.cbTienda.Location = new System.Drawing.Point(12, 15);
            this.cbTienda.MaterialStyle = false;
            this.cbTienda.Name = "cbTienda";
            this.cbTienda.Placeholder = "";
            this.cbTienda.SelectedIndex = -1;
            this.cbTienda.Size = new System.Drawing.Size(264, 58);
            this.cbTienda.SizeLine = 2;
            this.cbTienda.TabIndex = 0;
            this.cbTienda.Title = "Establecer la Tienda";
            this.cbTienda.ValueMember = "";
            this.cbTienda.VisibleIcon = true;
            this.cbTienda.VisibleTitle = true;
            // 
            // txProducto
            // 
            this.txProducto.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txProducto.BackColor = System.Drawing.Color.White;
            this.txProducto.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txProducto.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txProducto.ColorLine = System.Drawing.Color.Gray;
            this.txProducto.ColorText = System.Drawing.SystemColors.WindowText;
            this.txProducto.ColorTitle = System.Drawing.Color.Gray;
            this.txProducto.DockIcon = System.Windows.Forms.DockStyle.Right;
            this.txProducto.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txProducto.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txProducto.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txProducto.ImageIcon")));
            this.txProducto.Location = new System.Drawing.Point(282, 15);
            this.txProducto.MaterialStyle = true;
            this.txProducto.MaxLength = 32767;
            this.txProducto.MultiLineText = false;
            this.txProducto.Name = "txProducto";
            this.txProducto.ObjectArray = null;
            this.txProducto.PasswordChar = '\0';
            this.txProducto.Placeholder = "Buscar y seleccionar Producto";
            this.txProducto.PlaceHolderHeight = 18;
            this.txProducto.ReadOnly = false;
            this.txProducto.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txProducto.Size = new System.Drawing.Size(348, 44);
            this.txProducto.SizeLine = 2;
            this.txProducto.StringArray = null;
            this.txProducto.TabIndex = 3;
            this.txProducto.TextId = "";
            this.txProducto.Title = "Buscar y seleccionar Producto";
            this.txProducto.VisibleIcon = true;
            this.txProducto.VisibleTitle = false;
            this.txProducto.KeyDown += new System.EventHandler<System.Windows.Forms.KeyEventArgs>(this.txProducto_KeyDown);
            this.txProducto.ButtonClick += new System.EventHandler(this.txProducto_ButtonClick);
            // 
            // btnAgregar
            // 
            this.btnAgregar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregar.FlatAppearance.BorderSize = 0;
            this.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregar.Font = new System.Drawing.Font("Verdana", 10F);
            this.btnAgregar.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregar.Image")));
            this.btnAgregar.Location = new System.Drawing.Point(714, 15);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(121, 44);
            this.btnAgregar.TabIndex = 5;
            this.btnAgregar.Text = "Agregar a Canastilla";
            this.btnAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAgregar.UseVisualStyleBackColor = true;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txCantidad
            // 
            this.txCantidad.AlignText = System.Windows.Forms.HorizontalAlignment.Center;
            this.txCantidad.BackColor = System.Drawing.Color.White;
            this.txCantidad.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txCantidad.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txCantidad.ColorLine = System.Drawing.Color.Gray;
            this.txCantidad.ColorText = System.Drawing.SystemColors.WindowText;
            this.txCantidad.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txCantidad.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txCantidad.Error = "";
            this.txCantidad.FontText = new System.Drawing.Font("Verdana", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txCantidad.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txCantidad.ImageIcon = null;
            this.txCantidad.Info = "";
            this.txCantidad.Location = new System.Drawing.Point(636, 15);
            this.txCantidad.MaterialStyle = true;
            this.txCantidad.MaxLength = 32767;
            this.txCantidad.MultiLineText = false;
            this.txCantidad.Name = "txCantidad";
            this.txCantidad.PasswordChar = '\0';
            this.txCantidad.Placeholder = "Cantidad";
            this.txCantidad.ReadOnly = false;
            this.txCantidad.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txCantidad.Size = new System.Drawing.Size(72, 58);
            this.txCantidad.SizeLine = 2;
            this.txCantidad.TabIndex = 4;
            this.txCantidad.Text = "1.00";
            this.txCantidad.Title = "Cantidad";
            this.txCantidad.VisibleIcon = false;
            this.txCantidad.VisibleTitle = true;
            // 
            // grdCanastilla
            // 
            this.grdCanastilla.AllowUserToAddRows = false;
            this.grdCanastilla.AllowUserToDeleteRows = false;
            this.grdCanastilla.AllowUserToResizeRows = false;
            this.grdCanastilla.BackgroundColor = System.Drawing.Color.White;
            this.grdCanastilla.BodyFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdCanastilla.BodyForeColor = System.Drawing.SystemColors.ControlText;
            this.grdCanastilla.BodySelectColor = System.Drawing.SystemColors.Highlight;
            this.grdCanastilla.BodySelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.grdCanastilla.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdCanastilla.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdCanastilla.CellStyleBackColor = System.Drawing.SystemColors.Window;
            this.grdCanastilla.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdCanastilla.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdCanastilla.ColumnHeadersHeight = 34;
            this.grdCanastilla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdCanastilla.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdProduct,
            this.Producto,
            this.Cantidad});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdCanastilla.DefaultCellStyle = dataGridViewCellStyle3;
            this.grdCanastilla.EnableBottomDown = false;
            this.grdCanastilla.EnableBottomLeft = false;
            this.grdCanastilla.EnableBottomRight = false;
            this.grdCanastilla.EnableBottomUp = false;
            this.grdCanastilla.EnableHeadersVisualStyles = false;
            this.grdCanastilla.HeaderColor = System.Drawing.SystemColors.Control;
            this.grdCanastilla.HeaderFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdCanastilla.HeaderForeColor = System.Drawing.SystemColors.WindowText;
            this.grdCanastilla.Location = new System.Drawing.Point(293, 65);
            this.grdCanastilla.MultiSelect = false;
            this.grdCanastilla.Name = "grdCanastilla";
            this.grdCanastilla.RowHeadersVisible = false;
            this.grdCanastilla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdCanastilla.Size = new System.Drawing.Size(542, 169);
            this.grdCanastilla.TabIndex = 6;
            this.grdCanastilla.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCanastilla_CellContentClick);
            // 
            // IdProduct
            // 
            this.IdProduct.DataPropertyName = "IdProduct";
            this.IdProduct.HeaderText = "Id";
            this.IdProduct.Name = "IdProduct";
            this.IdProduct.ReadOnly = true;
            this.IdProduct.Visible = false;
            // 
            // Producto
            // 
            this.Producto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Producto.DataPropertyName = "Producto";
            this.Producto.HeaderText = "Producto";
            this.Producto.Name = "Producto";
            this.Producto.ReadOnly = true;
            // 
            // Cantidad
            // 
            this.Cantidad.DataPropertyName = "Cantidad";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Info;
            this.Cantidad.DefaultCellStyle = dataGridViewCellStyle2;
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.Width = 70;
            // 
            // lblCantidad
            // 
            this.lblCantidad.Font = new System.Drawing.Font("Verdana", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCantidad.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblCantidad.Location = new System.Drawing.Point(765, 237);
            this.lblCantidad.Name = "lblCantidad";
            this.lblCantidad.Size = new System.Drawing.Size(70, 16);
            this.lblCantidad.TabIndex = 13;
            this.lblCantidad.Text = "0.00";
            this.lblCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTotals
            // 
            this.lblTotals.AutoSize = true;
            this.lblTotals.Font = new System.Drawing.Font("Verdana", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotals.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTotals.Location = new System.Drawing.Point(290, 237);
            this.lblTotals.Name = "lblTotals";
            this.lblTotals.Size = new System.Drawing.Size(164, 16);
            this.lblTotals.TabIndex = 12;
            this.lblTotals.Text = "0, Items en canastilla";
            // 
            // lblMessageGrid
            // 
            this.lblMessageGrid.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.lblMessageGrid.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessageGrid.ForeColor = System.Drawing.Color.White;
            this.lblMessageGrid.Location = new System.Drawing.Point(290, 258);
            this.lblMessageGrid.Name = "lblMessageGrid";
            this.lblMessageGrid.Size = new System.Drawing.Size(545, 19);
            this.lblMessageGrid.TabIndex = 15;
            this.lblMessageGrid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Font = new System.Drawing.Font("Verdana", 8.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(663, 278);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(172, 19);
            this.label2.TabIndex = 16;
            this.label2.Text = "Doble Click para Quitar Item";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // frmSalidaModal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(847, 397);
            this.Name = "frmSalidaModal";
            this.Text = "frmSalidaModal";
            this.Load += new System.EventHandler(this.frmSalidaModal_Load);
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.Body.PerformLayout();
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCanastilla)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Autonomo.CustomControls.FlatComboBox cbTMovimiento;
        private Autonomo.CustomControls.FlatTextBox txDocumento;
        private Autonomo.CustomControls.FlatComboBox cbTienda;
        private Autonomo.CustomControls.FlatFindText txProducto;
        private Autonomo.CustomControls.CustomButton btnAgregar;
        private Autonomo.CustomControls.FlatTextBox txCantidad;
        private Autonomo.CustomControls.CustomGrid grdCanastilla;
        private System.Windows.Forms.Label lblCantidad;
        private System.Windows.Forms.Label lblTotals;
        private System.Windows.Forms.Label lblMessageGrid;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdProduct;
        private System.Windows.Forms.DataGridViewTextBoxColumn Producto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
    }
}