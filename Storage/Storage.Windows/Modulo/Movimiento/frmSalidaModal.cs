﻿using Storage.Datos;
using Storage.Entidad.Report;
using Storage.Windows.Tools;
using Storage.Windows.Tools.Formulary;
using Storage.Windows.Tools.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Movimiento
{
    public partial class frmSalidaModal : Autonomo.Object.Modal
    {
        private string IdSalida;
        StockStore _itemsSeleccionados;
        public frmSalidaModal()
        {
            InitializeComponent();
            IdSalida = string.Empty;
        }
        public void LoadCombo()
        {
            Tools.ComboBoxHelper.ComboBoxTienda(cbTienda);
            Tools.ComboBoxHelper.ComboBoxTipoMovimiento(cbTMovimiento, "S");

           
        }

        private void GetProduct()
        {
            string idTienda = cbTienda.SelectedValue.ToString();
            var f = new frmConsultaStockStore(idTienda);
            f.Title.Text = "Consulta de items del Inventario";
            Autonomo.Class.Fomulary.ShowModal(f, "", false);
            if (f.StateFormulary)
            {
                _itemsSeleccionados = new StockStore();
                txProducto.Text = $"{f.Items.NombreProducto}, Stock({f.Items.StokFinal})";  //string.Concat(f.Items.NombreProducto, " Stock(", f.Items.StokFinal, ")");
                //txProducto.TextId = f.Producto.IdProducto;
                //// Tag de txProducto: Almancena la Categoría;
                //txProducto.Tag = f.Producto.Categoria;
                _itemsSeleccionados = f.Items;
            }
        }

        private void PopulateGrid()
        {
            //Validamos si se ha seleccionado algun producto
            if (_itemsSeleccionados == null)
            {
                lblMessageGrid.Text = "No se ha seleccionado ningún producto, vuelva a intentarlo.";
                lblMessageGrid.BackColor = Color.Coral;
                return;
            }

           // Validar si la cantidad de items supera el stock
            if (CantidadSuperaStock(decimal.Parse(txCantidad.Text), _itemsSeleccionados.StokFinal))
            {
                lblMessageGrid.Text = $"La cantidad supera al Stock permitido, Stock {_itemsSeleccionados.StokFinal}/cantidad {txCantidad.Text}";
                lblMessageGrid.BackColor = Color.Coral;
                return;
            }

            var canastilla = new CanastillaModels()
            {
                IdItem = _itemsSeleccionados.IdProducto,
                NameItem = _itemsSeleccionados.NombreProducto,
                QuantityItem = decimal.Parse(txCantidad.Text)
            };

            var response = DataGridHelper.PopulateGrid(grdCanastilla, Enumerables.LlenarGrilla.Salida, canastilla);
            // Validamos si se ha insertado o no::: Insertado=1, No Insertado=0.
            MostrarTotales();
            LimpiarAgregarCanastilla();

            lblMessageGrid.Text = response.Mensaje;
            if (response.Estado)
            {
                lblMessageGrid.BackColor = Color.SeaGreen;
                return;
            }
            lblMessageGrid.BackColor = Color.Coral;
        }

        bool CantidadSuperaStock(decimal cantidad, decimal stock)
        {
            return cantidad > stock ? true : false;
        }

        private void MostrarTotales()
        {
            lblTotals.Text = $"{grdCanastilla.Rows.Count}, Items en canastilla";

            var canasta = DataGridHelper.GetTotals(grdCanastilla, Enumerables.LlenarGrilla.Salida);
            lblCantidad.Text = canasta.QuantityItem.ToString();
        }

        private void LimpiarAgregarCanastilla()
        {
            txProducto.Clear();
            txCantidad.Text = "1.00";
            _itemsSeleccionados = null;
        }

        private void RemoverItem(DataGridViewCellEventArgs e)
        {
            if (grdCanastilla.SelectedRows.Count > 0)
            {
                //Verificamos en que columna se hace Click.
                var column = grdCanastilla.Columns[e.ColumnIndex];
                if (column.Name == "Cantidad") return;

                int rowIndex = grdCanastilla.CurrentCell.RowIndex;
                DataGridHelper.RemoveRows(grdCanastilla, rowIndex);
                MostrarTotales();
            }
        }

        private bool ValidateControls(out string controlsEmpty)
        {
            controlsEmpty = string.Empty;
            if (string.IsNullOrEmpty(cbTienda.SelectedValue.ToString()))
            {
                controlsEmpty = "Id de la tienda es nulo.";
                cbTienda.Error = "Este campo es requerido";
                return false;
            }
            cbTienda.Error = "";

            if (string.IsNullOrEmpty(txDocumento.Text))
            {
                controlsEmpty = "Documento es nulo.";
                txDocumento.Error = "Este campo es requerido";
                return false;
            }
            txDocumento.Error = "";

            if (string.IsNullOrEmpty(cbTMovimiento.SelectedValue.ToString()))
            {
                controlsEmpty = "Id tipo de movimiento es nulo";
                cbTMovimiento.Error = "Este campo es requerido";
                return false;
            }
            cbTMovimiento.Error = "";

            if (!DataGridHelper.ValidateEmptyGrid(grdCanastilla))
            {
                controlsEmpty = "La canastilla no tiene valores";
                lblMessageGrid.Text = "No se ha encontrado detalles, seleccionar al menos uno.";
                lblMessageGrid.BackColor = Color.Coral;
                return false;
            }

            lblMessageGrid.Text = "";
            lblMessageGrid.BackColor = Color.SeaGreen;

            return true;
        }

        private void SaveChange()
        {
            if (!ValidateControls(out string controlsEmpty))
            {
                Tools.Mensaje.MessageBox(Enumerables.Mensajeria.Warning,
                    $"Campos inválidos: {controlsEmpty}");
                // mensaje de  campos invalidos:: MessageBox:Personalizado
                return;
            }

            try
            {
                using (UnitOfWork uow = new UnitOfWork())
                {
                    string GetCorrelative = uow.Salida.GetLastId();
                    int response = -1;

                    foreach (DataGridViewRow row in grdCanastilla.Rows)
                    {
                        string _idProducto = row.Cells["IdProduct"].Value.ToString();
                        decimal _cantidad = decimal.Parse(row.Cells["Cantidad"].Value.ToString());

                        response += uow.Salida.Crud(
                          new Entidad.Salida()
                          {
                              IdSalida = GetCorrelative,
                              IdTienda = cbTienda.SelectedValue.ToString(),
                              Documento = txDocumento.Text,
                              EditorUser = Program.Usuario
                          }, new Entidad.Inventario()
                          {
                              IdTipoMov = cbTMovimiento.SelectedValue.ToString(),
                              IdProducto = _idProducto,
                              Cantidad = _cantidad
                          });
                    }

                    if (response > 0)
                    {
                        Tools.Mensaje.MessageBox(
                            Enumerables.Mensajeria.Succesful,
                            $"El proceso ha retornado {response} registros de manera correcta,");
                        base.Set();
                    }
                }
            }
            catch (Exception ex)
            {
                Tools.Mensaje.MessageBox(Enumerables.Mensajeria.Error, ex);
            }
        }

        private void frmSalidaModal_Load(object sender, EventArgs e)
        {
            if (new Validation().EsVendedor())
            {
                cbTienda.SelectedValue = Program.Local;
                cbTienda.Enabled = false;
            }
        }

        private void txProducto_ButtonClick(object sender, EventArgs e)
        {
            GetProduct();
        }

        private void txProducto_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                GetProduct();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            PopulateGrid();
        }

        private void grdCanastilla_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            RemoverItem(e);
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChange();
        }
    }
}
