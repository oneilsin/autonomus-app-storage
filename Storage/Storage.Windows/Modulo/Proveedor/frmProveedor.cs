﻿using Storage.Datos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Proveedor
{
    public partial class frmProveedor : Autonomo.Object.Registry
    {
        List<Entidad.Proveedor> proveedores;
        public frmProveedor()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                proveedores = new List<Entidad.Proveedor>();
                proveedores = uow.Proveedors.GetList().ToList();
                LoadFilter();
            }
        }

        private void LoadFilter()
        {
            if(proveedores!=null && proveedores.Count() > 0)
            {
                var newList = from o in proveedores
                              select new
                              {
                                  IdProveedor = o.IdProveedor,
                                  NombreProveedor = o.NombreProveedor,
                                  Telefonos = $"{o.TelefFijo}  {o.TelefMovil}",
                                  Email = o.Email,
                                  Ubigeo = o.Ubigeo,
                                  Urbanizacion = o.Urbanizacion,
                                  Direccion = o.Direccion
                              };
                grdData.DataSource = newList
                    .Where(o => o.NombreProveedor.Contains(txFilter.Text))
                    .OrderBy(o => o.NombreProveedor)
                    .ToList();
            }
        }

        private void LoadModal(string option, string title)
        {
            var f = new frmProveedorModal();
            f.Title.Text = title;
            if (option == "Update")
            {
                string id = grdData.CurrentRow.Cells["IdProveedor"].Value.ToString();
                var obj = proveedores.FirstOrDefault(o => o.IdProveedor.Equals(id));
                if (obj != null)
                {
                    f.LoadData(obj.IdProveedor, obj.NombreProveedor, obj.TelefFijo,
                        obj.TelefMovil, obj.Email, obj.Ubigeo, obj.Direccion, obj.Urbanizacion
                        );
                }
            }
            Autonomo.Class.Fomulary.ShowModal(f, option, false);
            if (f.Tag.ToString() == "Get") { LoadData(); }
        }

        private void frmProveedor_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void txFilter_TextBoxChanged(object sender, EventArgs e)
        {
            LoadFilter();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LoadModal("Insert", "Registrar nuevo Proveedor");
        }

        private void btnEditar_Click(object sender, EventArgs e)
        {
            LoadModal("Update", "Actualizar Proveedor");
        }

        private void btnExportar_Click(object sender, EventArgs e)
        {
            grdData.DefaultCellStyle.BackColor = Color.FromArgb(35, 45, 54);
        }
    }
}
