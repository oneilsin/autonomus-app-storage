﻿namespace Storage.Windows.Modulo.Proveedor
{
    partial class frmProveedorModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmProveedorModal));
            this.txNombre = new Autonomo.CustomControls.FlatTextBox();
            this.txFijo = new Autonomo.CustomControls.FlatTextBox();
            this.txMovil = new Autonomo.CustomControls.FlatTextBox();
            this.txEmail = new Autonomo.CustomControls.FlatTextBox();
            this.txDireccion = new Autonomo.CustomControls.FlatTextBox();
            this.txUrbanizacion = new Autonomo.CustomControls.FlatTextBox();
            this.txUbigeo = new Autonomo.CustomControls.FlatFindText();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Size = new System.Drawing.Size(620, 369);
            // 
            // Body
            // 
            this.Body.Controls.Add(this.txUbigeo);
            this.Body.Controls.Add(this.txUrbanizacion);
            this.Body.Controls.Add(this.txDireccion);
            this.Body.Controls.Add(this.txEmail);
            this.Body.Controls.Add(this.txMovil);
            this.Body.Controls.Add(this.txFijo);
            this.Body.Controls.Add(this.txNombre);
            this.Body.Size = new System.Drawing.Size(620, 276);
            this.Body.TabIndex = 0;
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 317);
            this.Footer.Size = new System.Drawing.Size(620, 52);
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(620, 41);
            // 
            // Title
            // 
            this.Title.Size = new System.Drawing.Size(579, 41);
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Location = new System.Drawing.Point(233, 8);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txNombre
            // 
            this.txNombre.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txNombre.BackColor = System.Drawing.Color.White;
            this.txNombre.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txNombre.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txNombre.ColorLine = System.Drawing.Color.Gray;
            this.txNombre.ColorText = System.Drawing.SystemColors.WindowText;
            this.txNombre.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txNombre.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txNombre.Error = "";
            this.txNombre.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txNombre.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txNombre.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txNombre.ImageIcon")));
            this.txNombre.Info = "";
            this.txNombre.Location = new System.Drawing.Point(12, 19);
            this.txNombre.MaterialStyle = true;
            this.txNombre.MaxLength = 60;
            this.txNombre.MultiLineText = false;
            this.txNombre.Name = "txNombre";
            this.txNombre.PasswordChar = '\0';
            this.txNombre.Placeholder = "Nombre del Proveedor";
            this.txNombre.ReadOnly = false;
            this.txNombre.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txNombre.Size = new System.Drawing.Size(278, 58);
            this.txNombre.SizeLine = 2;
            this.txNombre.TabIndex = 0;
            this.txNombre.Title = "Nombre del Proveedor";
            this.txNombre.VisibleIcon = true;
            this.txNombre.VisibleTitle = false;
            // 
            // txFijo
            // 
            this.txFijo.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txFijo.BackColor = System.Drawing.Color.White;
            this.txFijo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txFijo.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txFijo.ColorLine = System.Drawing.Color.Gray;
            this.txFijo.ColorText = System.Drawing.SystemColors.WindowText;
            this.txFijo.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txFijo.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txFijo.Error = "";
            this.txFijo.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txFijo.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txFijo.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txFijo.ImageIcon")));
            this.txFijo.Info = "";
            this.txFijo.Location = new System.Drawing.Point(12, 83);
            this.txFijo.MaterialStyle = true;
            this.txFijo.MaxLength = 10;
            this.txFijo.MultiLineText = false;
            this.txFijo.Name = "txFijo";
            this.txFijo.PasswordChar = '\0';
            this.txFijo.Placeholder = "Teléfono (fijo)";
            this.txFijo.ReadOnly = false;
            this.txFijo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txFijo.Size = new System.Drawing.Size(278, 58);
            this.txFijo.SizeLine = 2;
            this.txFijo.TabIndex = 1;
            this.txFijo.Title = "Teléfono (fijo)";
            this.txFijo.VisibleIcon = true;
            this.txFijo.VisibleTitle = false;
            this.txFijo.KeyPress += new System.EventHandler<System.Windows.Forms.KeyPressEventArgs>(this.txFijo_KeyPress);
            // 
            // txMovil
            // 
            this.txMovil.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txMovil.BackColor = System.Drawing.Color.White;
            this.txMovil.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txMovil.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txMovil.ColorLine = System.Drawing.Color.Gray;
            this.txMovil.ColorText = System.Drawing.SystemColors.WindowText;
            this.txMovil.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txMovil.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txMovil.Error = "";
            this.txMovil.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txMovil.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txMovil.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txMovil.ImageIcon")));
            this.txMovil.Info = "";
            this.txMovil.Location = new System.Drawing.Point(12, 147);
            this.txMovil.MaterialStyle = true;
            this.txMovil.MaxLength = 13;
            this.txMovil.MultiLineText = false;
            this.txMovil.Name = "txMovil";
            this.txMovil.PasswordChar = '\0';
            this.txMovil.Placeholder = "Teléfono (movil)";
            this.txMovil.ReadOnly = false;
            this.txMovil.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txMovil.Size = new System.Drawing.Size(278, 58);
            this.txMovil.SizeLine = 2;
            this.txMovil.TabIndex = 2;
            this.txMovil.Title = "Teléfono (movil)";
            this.txMovil.VisibleIcon = true;
            this.txMovil.VisibleTitle = false;
            this.txMovil.KeyPress += new System.EventHandler<System.Windows.Forms.KeyPressEventArgs>(this.txFijo_KeyPress);
            // 
            // txEmail
            // 
            this.txEmail.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txEmail.BackColor = System.Drawing.Color.White;
            this.txEmail.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
            this.txEmail.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txEmail.ColorLine = System.Drawing.Color.Gray;
            this.txEmail.ColorText = System.Drawing.SystemColors.WindowText;
            this.txEmail.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txEmail.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txEmail.Error = "";
            this.txEmail.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txEmail.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txEmail.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txEmail.ImageIcon")));
            this.txEmail.Info = "";
            this.txEmail.Location = new System.Drawing.Point(12, 211);
            this.txEmail.MaterialStyle = true;
            this.txEmail.MaxLength = 100;
            this.txEmail.MultiLineText = false;
            this.txEmail.Name = "txEmail";
            this.txEmail.PasswordChar = '\0';
            this.txEmail.Placeholder = "Email";
            this.txEmail.ReadOnly = false;
            this.txEmail.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txEmail.Size = new System.Drawing.Size(278, 58);
            this.txEmail.SizeLine = 2;
            this.txEmail.TabIndex = 3;
            this.txEmail.Title = "Email";
            this.txEmail.VisibleIcon = true;
            this.txEmail.VisibleTitle = false;
            this.txEmail.KeyPress += new System.EventHandler<System.Windows.Forms.KeyPressEventArgs>(this.txEmail_KeyPress);
            // 
            // txDireccion
            // 
            this.txDireccion.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txDireccion.BackColor = System.Drawing.Color.White;
            this.txDireccion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txDireccion.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txDireccion.ColorLine = System.Drawing.Color.Gray;
            this.txDireccion.ColorText = System.Drawing.SystemColors.WindowText;
            this.txDireccion.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txDireccion.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txDireccion.Error = "";
            this.txDireccion.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txDireccion.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txDireccion.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txDireccion.ImageIcon")));
            this.txDireccion.Info = "";
            this.txDireccion.Location = new System.Drawing.Point(321, 83);
            this.txDireccion.MaterialStyle = true;
            this.txDireccion.MaxLength = 250;
            this.txDireccion.MultiLineText = true;
            this.txDireccion.Name = "txDireccion";
            this.txDireccion.PasswordChar = '\0';
            this.txDireccion.Placeholder = "Dirección";
            this.txDireccion.ReadOnly = false;
            this.txDireccion.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txDireccion.Size = new System.Drawing.Size(278, 122);
            this.txDireccion.SizeLine = 2;
            this.txDireccion.TabIndex = 5;
            this.txDireccion.Title = "Dirección";
            this.txDireccion.VisibleIcon = true;
            this.txDireccion.VisibleTitle = false;
            // 
            // txUrbanizacion
            // 
            this.txUrbanizacion.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txUrbanizacion.BackColor = System.Drawing.Color.White;
            this.txUrbanizacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txUrbanizacion.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txUrbanizacion.ColorLine = System.Drawing.Color.Gray;
            this.txUrbanizacion.ColorText = System.Drawing.SystemColors.WindowText;
            this.txUrbanizacion.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txUrbanizacion.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txUrbanizacion.Error = "";
            this.txUrbanizacion.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txUrbanizacion.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txUrbanizacion.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txUrbanizacion.ImageIcon")));
            this.txUrbanizacion.Info = "";
            this.txUrbanizacion.Location = new System.Drawing.Point(321, 211);
            this.txUrbanizacion.MaterialStyle = true;
            this.txUrbanizacion.MaxLength = 100;
            this.txUrbanizacion.MultiLineText = false;
            this.txUrbanizacion.Name = "txUrbanizacion";
            this.txUrbanizacion.PasswordChar = '\0';
            this.txUrbanizacion.Placeholder = "Urbanización ";
            this.txUrbanizacion.ReadOnly = false;
            this.txUrbanizacion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txUrbanizacion.Size = new System.Drawing.Size(278, 58);
            this.txUrbanizacion.SizeLine = 2;
            this.txUrbanizacion.TabIndex = 6;
            this.txUrbanizacion.Title = "Urbanización ";
            this.txUrbanizacion.VisibleIcon = true;
            this.txUrbanizacion.VisibleTitle = false;
            // 
            // txUbigeo
            // 
            this.txUbigeo.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txUbigeo.BackColor = System.Drawing.Color.White;
            this.txUbigeo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txUbigeo.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txUbigeo.ColorLine = System.Drawing.Color.Gray;
            this.txUbigeo.ColorText = System.Drawing.SystemColors.WindowText;
            this.txUbigeo.ColorTitle = System.Drawing.Color.Gray;
            this.txUbigeo.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txUbigeo.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txUbigeo.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txUbigeo.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txUbigeo.ImageIcon")));
            this.txUbigeo.Location = new System.Drawing.Point(321, 6);
            this.txUbigeo.MaterialStyle = true;
            this.txUbigeo.MaxLength = 6;
            this.txUbigeo.MultiLineText = false;
            this.txUbigeo.Name = "txUbigeo";
            this.txUbigeo.PasswordChar = '\0';
            this.txUbigeo.Placeholder = "Seleccionar Ubigeo (press enter)";
            this.txUbigeo.ReadOnly = true;
            this.txUbigeo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txUbigeo.Size = new System.Drawing.Size(278, 44);
            this.txUbigeo.SizeLine = 2;
            this.txUbigeo.TabIndex = 4;
            this.txUbigeo.TextId = "";
            this.txUbigeo.Title = "Seleccionar Ubigeo (press enter)";
            this.txUbigeo.VisibleIcon = true;
            this.txUbigeo.VisibleTitle = false;
            this.txUbigeo.KeyDown += new System.EventHandler<System.Windows.Forms.KeyEventArgs>(this.txUbigeo_KeyDown);
            this.txUbigeo.ButtonClick += new System.EventHandler(this.txUbigeo_ButtonClick);
            // 
            // frmProveedorModal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(620, 369);
            this.Name = "frmProveedorModal";
            this.Text = "frmProveedorModal";
            this.Load += new System.EventHandler(this.frmProveedorModal_Load);
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Autonomo.CustomControls.FlatTextBox txUrbanizacion;
        private Autonomo.CustomControls.FlatTextBox txDireccion;
        private Autonomo.CustomControls.FlatTextBox txEmail;
        private Autonomo.CustomControls.FlatTextBox txMovil;
        private Autonomo.CustomControls.FlatTextBox txFijo;
        private Autonomo.CustomControls.FlatTextBox txNombre;
        private Autonomo.CustomControls.FlatFindText txUbigeo;
    }
}