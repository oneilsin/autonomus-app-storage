﻿using Storage.Datos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Proveedor
{
    public partial class frmProveedorModal : Autonomo.Object.Modal
    {
        private string IdProveedor;
        public frmProveedorModal()
        {
            InitializeComponent();
            this.IdProveedor = string.Empty;
        }
        public void LoadData(string idProveedor, string nombre, string fijo,
            string movil, string email, string ubigeo, string direccion,
            string urbanizacion)
        {
            this.IdProveedor = idProveedor;
            txNombre.Text = nombre;
            txFijo.Text = fijo;
            txMovil.Text = movil;
            txEmail.Text = email;
            txUbigeo.Text = ubigeo;
            txDireccion.Text = direccion;
            txUrbanizacion.Text = urbanizacion;
        }

        private void GetUbigeo()
        {
            txUbigeo.Clear();
            var f = new Tools.Formulary.frmUbigeo();
            // Editar : LoadData : not implemented
            Autonomo.Class.Fomulary.ShowModal(f, "");
            if (f.Tag.ToString() == "Get")
            {
                txUbigeo.Text = f.lbUbigeo.Text;
            }
        }

        private void SaveChange()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                string option = this.Tag.ToString();

                var result = uow.Proveedors.Crud(
                    new Entidad.Proveedor()
                    {
                        IdProveedor = this.IdProveedor,
                        NombreProveedor = txNombre.Text.Trim(),
                        Ubigeo = txUbigeo.Text.Trim(),
                        Direccion = txDireccion.Text.Trim(),
                        Urbanizacion = txUrbanizacion.Text.Trim(),
                        TelefFijo = txFijo.Text.Trim(),
                        TelefMovil = txMovil.Text.Trim(),
                        Email = txEmail.Text.Trim(),
                        EditorUser = Program.Usuario
                    }, option);
                if (result > 0) { base.Set(); }
            }
        }
        private void frmProveedorModal_Load(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChange();
        }

        private void txFijo_KeyPress(object sender, KeyPressEventArgs e)
        {
            Autonomo.Class.Validating.OnlyNumber(e);
        }

        private void txEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            Autonomo.Class.Validating.OnlyEmail(e);
        }

        private void txUbigeo_ButtonClick(object sender, EventArgs e)
        {
            GetUbigeo();
        }

        private void txUbigeo_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                GetUbigeo();
        }
    }
}
