﻿using Storage.Datos;
using Storage.Entidad.Report;
using Storage.Windows.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Storage.Windows.Modulo.Ventas.Gestion
{
    public partial class frmVenta : Autonomo.CustomTemplate.RegistryDouble
    {
        public new Form ParentForm;
       //ist<OrdenVentaReport> _ordenList;
        public frmVenta()
        {
            InitializeComponent();
        }
        private void Tasking(bool value)
        {
            var f = (Home.frmMenu)this.ParentForm;
            f.ShowInTaskbar = value;
        }
        private void LoadCombo()
        {
            Tools.ComboBoxHelper.ComboBoxTiendaTodos(cbTienda);
            //Validar el Rol del Usuario
            if (new Validation().EsVendedor())
            {
                cbTienda.SelectedValue = Program.Local;
                cbTienda.Enabled = false;
            }
        }

        public void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                string idTienda = cbTienda.SelectedValue.ToString();
                var _ordenList = new List<OrdenVentaReport>();
                _ordenList = uow.OrdenVenta.GetList(dtDesde.Value, dtHasta.Value,
                    new Entidad.OrdenVenta() {IdTienda=idTienda,Situacion="O" }
                    ).OrderByDescending(o => o.FechaRegistro).ToList();

                grdData.DataSource = _ordenList.ToList();
                grdData.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
        }

        private void GetDetail(string documento)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Inventario.GetDetalleOrden(documento, "S").ToList();
                grdDetalle.DataSource = element;
                grdDetalle.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
        }

        private void SetTotals(bool vacio)
        {
            decimal importe = vacio?0m: decimal.Parse(grdData.CurrentRow.Cells["ImporteParcial"].Value.ToString());
            
            decimal sunat = Program.TCSunat;
            decimal igv = importe * Program.Igv;
            decimal importeGenerado = (importe - igv);

            lbGenerado.Text = String.Format(": {0}", importeGenerado.ToString("#,##0.00"));
            lbSunat.Text = String.Format(": {0}", sunat);
            lbIgv.Text = String.Format(": {0}", igv.ToString("#,##0.00"));
            lbImporte.Text = String.Format(": {0}", importe.ToString("#,##0.00"));
            lbIGVtext.Text = $"Impuesto IGV({Program.Igv * 100}%";
        }

        private void frmVenta_Load(object sender, EventArgs e)
        {
            Autonomo.Class.RoundObject.RoundButton(btnPagos, 8, 8);
            dtDesde.Value = Convert.ToDateTime("01/04/2021");//Autonomo.Class.Obtaining.GetFirstDayMonth();
            dtHasta.Value = DateTime.Now;
            ThemeStyle(Theme.White);
            LoadCombo();
            LoadData();
        }

        private void GotToPayment(string title)
        {
            var f = new frmVentaModal();
            f.ParentForm = this;
            f.Title.Text = title;

            DataGridViewRow row = grdData.CurrentRow;
            // string idCliente = row.Cells["IdCliente"].Value.ToString();
            //string vendedor = row.Cells["Vendedor"].Value.ToString();
            string idOrden = row.Cells["IdOrden"].Value.ToString();
            string fecha = row.Cells["FechaRegistro"].Value.ToString();
            f.LoadData(idOrden, fecha);
            Autonomo.Class.Fomulary.ShowFormInPanel(f, pnlContenedor, DockStyle.None, this);


            //  Program.f.ShowInTaskbar = false;
           // Tasking(false);
         //   Autonomo.Class.Fomulary.ShowModal(f);
           // Tasking(true);
        }

        private void btnProcesar_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void grdData_SelectionChanged(object sender, EventArgs e)
        {
            if (grdData.SelectedRows.Count > 0)
            {
                string _idOrden = grdData.CurrentRow.Cells["IdOrden"].Value.ToString();
                GetDetail(_idOrden);
                SetTotals(false);
            }
            else
            {
                grdDetalle.DataSource = null;
                grdDetalle.Refresh();
                SetTotals(true);
            }
        }

        private void btnPagos_Click(object sender, EventArgs e)
        {
            if (grdData.Rows.Count > 0)
            {
                 GotToPayment("tsdfsdfdsf");                
            }
        }
    }
}
