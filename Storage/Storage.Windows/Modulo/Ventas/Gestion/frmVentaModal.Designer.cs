﻿
namespace Storage.Windows.Modulo.Ventas.Gestion
{
    partial class frmVentaModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmVentaModal));
            this.pnlOrden = new System.Windows.Forms.Panel();
            this.lbLineaCredito = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lbSunat = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lbImporte = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lbFecha = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.lbDocumento = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.panelFormaPago = new System.Windows.Forms.Panel();
            this.panelEfectivo = new System.Windows.Forms.Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.btnAgregar = new Autonomo.CustomControls.CustomButton();
            this.txImporte = new Autonomo.CustomControls.FlatTextBox();
            this.txOperacion = new Autonomo.CustomControls.FlatTextBox();
            this.cbMoneda = new Autonomo.CustomControls.FlatComboBox();
            this.cbBanco = new Autonomo.CustomControls.FlatComboBox();
            this.label13 = new System.Windows.Forms.Label();
            this.lbForma = new System.Windows.Forms.Label();
            this.pnlCanasta = new System.Windows.Forms.Panel();
            this.pnlcanastilla = new System.Windows.Forms.Panel();
            this.grdCanastilla = new Autonomo.CustomControls.CustomGrid();
            this.IdUnico = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.IdMetodo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GMetodo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NumOperacion = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TipoMoneda = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GSoles = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.GDolares = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DeleteRow = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pnlResumen = new System.Windows.Forms.Panel();
            this.lbMensaje = new System.Windows.Forms.Label();
            this.cbxFactura = new Autonomo.CustomControls.CustomRadius();
            this.cbxBoleta = new Autonomo.CustomControls.CustomRadius();
            this.cbxInterno = new Autonomo.CustomControls.CustomRadius();
            this.label9 = new System.Windows.Forms.Label();
            this.txTotalCanastilla = new Autonomo.CustomControls.FlatFindText();
            this.txVueltoSoles = new Autonomo.CustomControls.FlatFindText();
            this.txSaldo = new Autonomo.CustomControls.FlatFindText();
            this.txVueltoDolares = new Autonomo.CustomControls.FlatFindText();
            this.txImporteGenerado = new Autonomo.CustomControls.FlatFindText();
            this.label8 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.pnlCliente = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.lbVendedor = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lbNivelPrecio = new System.Windows.Forms.Label();
            this.lbIdentificacion = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbCliente = new System.Windows.Forms.Label();
            this.btnCliente = new Autonomo.CustomControls.CustomButton();
            this.pnlMetodos = new System.Windows.Forms.Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.btnTransferencia = new Autonomo.CustomControls.CustomButton();
            this.label4 = new System.Windows.Forms.Label();
            this.btnCupon = new Autonomo.CustomControls.CustomButton();
            this.btnEfectivo = new Autonomo.CustomControls.CustomButton();
            this.btnCheque = new Autonomo.CustomControls.CustomButton();
            this.btnCredito = new Autonomo.CustomControls.CustomButton();
            this.btnTarjeta = new Autonomo.CustomControls.CustomButton();
            this.btnPrevisualizar = new Autonomo.CustomControls.CustomButton();
            this.lbCapComprobante = new System.Windows.Forms.Label();
            this.lbNumComprobante = new System.Windows.Forms.Label();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            this.pnlOrden.SuspendLayout();
            this.panelFormaPago.SuspendLayout();
            this.panelEfectivo.SuspendLayout();
            this.pnlCanasta.SuspendLayout();
            this.pnlcanastilla.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCanastilla)).BeginInit();
            this.pnlResumen.SuspendLayout();
            this.pnlCliente.SuspendLayout();
            this.pnlMetodos.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Contenedor.Size = new System.Drawing.Size(960, 530);
            // 
            // Body
            // 
            this.Body.Controls.Add(this.pnlCanasta);
            this.Body.Controls.Add(this.panelFormaPago);
            this.Body.Controls.Add(this.pnlOrden);
            this.Body.Controls.Add(this.pnlMetodos);
            this.Body.Controls.Add(this.pnlCliente);
            this.Body.Size = new System.Drawing.Size(958, 431);
            // 
            // Footer
            // 
            this.Footer.Controls.Add(this.lbCapComprobante);
            this.Footer.Controls.Add(this.lbNumComprobante);
            this.Footer.Controls.Add(this.btnPrevisualizar);
            this.Footer.Location = new System.Drawing.Point(0, 472);
            this.Footer.Size = new System.Drawing.Size(958, 56);
            this.Footer.Controls.SetChildIndex(this.btnSave, 0);
            this.Footer.Controls.SetChildIndex(this.btnPrevisualizar, 0);
            this.Footer.Controls.SetChildIndex(this.lbNumComprobante, 0);
            this.Footer.Controls.SetChildIndex(this.lbCapComprobante, 0);
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(958, 41);
            // 
            // Title
            // 
            this.Title.Size = new System.Drawing.Size(917, 41);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(212)))));
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Font = new System.Drawing.Font("Verdana", 10F);
            this.btnSave.Location = new System.Drawing.Point(780, 6);
            this.btnSave.Size = new System.Drawing.Size(168, 42);
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // pnlOrden
            // 
            this.pnlOrden.Controls.Add(this.lbLineaCredito);
            this.pnlOrden.Controls.Add(this.label17);
            this.pnlOrden.Controls.Add(this.label15);
            this.pnlOrden.Controls.Add(this.label11);
            this.pnlOrden.Controls.Add(this.lbSunat);
            this.pnlOrden.Controls.Add(this.label14);
            this.pnlOrden.Controls.Add(this.lbImporte);
            this.pnlOrden.Controls.Add(this.label12);
            this.pnlOrden.Controls.Add(this.lbFecha);
            this.pnlOrden.Controls.Add(this.label10);
            this.pnlOrden.Controls.Add(this.lbDocumento);
            this.pnlOrden.Controls.Add(this.label7);
            this.pnlOrden.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnlOrden.Location = new System.Drawing.Point(0, 137);
            this.pnlOrden.Name = "pnlOrden";
            this.pnlOrden.Size = new System.Drawing.Size(150, 294);
            this.pnlOrden.TabIndex = 0;
            // 
            // lbLineaCredito
            // 
            this.lbLineaCredito.Font = new System.Drawing.Font("Verdana", 18F, System.Drawing.FontStyle.Bold);
            this.lbLineaCredito.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lbLineaCredito.Location = new System.Drawing.Point(7, 238);
            this.lbLineaCredito.Name = "lbLineaCredito";
            this.lbLineaCredito.Size = new System.Drawing.Size(136, 30);
            this.lbLineaCredito.TabIndex = 25;
            this.lbLineaCredito.Text = "0.00";
            this.lbLineaCredito.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(18, 223);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(114, 16);
            this.label17.TabIndex = 24;
            this.label17.Text = "Línea de crédito";
            // 
            // label15
            // 
            this.label15.BackColor = System.Drawing.Color.Gainsboro;
            this.label15.Dock = System.Windows.Forms.DockStyle.Right;
            this.label15.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(149, 34);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(1, 260);
            this.label15.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.label11.Dock = System.Windows.Forms.DockStyle.Top;
            this.label11.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(0, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(150, 34);
            this.label11.TabIndex = 22;
            this.label11.Text = "Resumen";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbSunat
            // 
            this.lbSunat.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbSunat.Location = new System.Drawing.Point(7, 186);
            this.lbSunat.Name = "lbSunat";
            this.lbSunat.Size = new System.Drawing.Size(136, 25);
            this.lbSunat.TabIndex = 21;
            this.lbSunat.Text = "--";
            this.lbSunat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(21, 170);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(108, 16);
            this.label14.TabIndex = 20;
            this.label14.Text = "Tipo de cambio";
            // 
            // lbImporte
            // 
            this.lbImporte.Font = new System.Drawing.Font("Verdana", 15.25F, System.Drawing.FontStyle.Bold);
            this.lbImporte.ForeColor = System.Drawing.Color.Red;
            this.lbImporte.Location = new System.Drawing.Point(7, 142);
            this.lbImporte.Name = "lbImporte";
            this.lbImporte.Size = new System.Drawing.Size(136, 25);
            this.lbImporte.TabIndex = 19;
            this.lbImporte.Text = "0.00";
            this.lbImporte.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(46, 126);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(59, 16);
            this.label12.TabIndex = 18;
            this.label12.Text = "Importe";
            // 
            // lbFecha
            // 
            this.lbFecha.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFecha.Location = new System.Drawing.Point(7, 105);
            this.lbFecha.Name = "lbFecha";
            this.lbFecha.Size = new System.Drawing.Size(136, 25);
            this.lbFecha.TabIndex = 17;
            this.lbFecha.Text = "--";
            this.lbFecha.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(51, 89);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 16);
            this.label10.TabIndex = 16;
            this.label10.Text = "Fecha";
            // 
            // lbDocumento
            // 
            this.lbDocumento.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDocumento.Location = new System.Drawing.Point(7, 68);
            this.lbDocumento.Name = "lbDocumento";
            this.lbDocumento.Size = new System.Drawing.Size(136, 25);
            this.lbDocumento.TabIndex = 15;
            this.lbDocumento.Text = "--";
            this.lbDocumento.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(135, 16);
            this.label7.TabIndex = 14;
            this.label7.Text = "Documento de O.V.";
            // 
            // panelFormaPago
            // 
            this.panelFormaPago.Controls.Add(this.panelEfectivo);
            this.panelFormaPago.Controls.Add(this.lbForma);
            this.panelFormaPago.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelFormaPago.Location = new System.Drawing.Point(150, 137);
            this.panelFormaPago.Name = "panelFormaPago";
            this.panelFormaPago.Size = new System.Drawing.Size(231, 294);
            this.panelFormaPago.TabIndex = 1;
            // 
            // panelEfectivo
            // 
            this.panelEfectivo.Controls.Add(this.label16);
            this.panelEfectivo.Controls.Add(this.btnAgregar);
            this.panelEfectivo.Controls.Add(this.txImporte);
            this.panelEfectivo.Controls.Add(this.txOperacion);
            this.panelEfectivo.Controls.Add(this.cbMoneda);
            this.panelEfectivo.Controls.Add(this.cbBanco);
            this.panelEfectivo.Controls.Add(this.label13);
            this.panelEfectivo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelEfectivo.Location = new System.Drawing.Point(0, 34);
            this.panelEfectivo.Name = "panelEfectivo";
            this.panelEfectivo.Size = new System.Drawing.Size(231, 260);
            this.panelEfectivo.TabIndex = 21;
            // 
            // label16
            // 
            this.label16.BackColor = System.Drawing.Color.Gainsboro;
            this.label16.Dock = System.Windows.Forms.DockStyle.Right;
            this.label16.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(230, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(1, 209);
            this.label16.TabIndex = 24;
            // 
            // btnAgregar
            // 
            this.btnAgregar.BackColor = System.Drawing.Color.SeaGreen;
            this.btnAgregar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAgregar.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnAgregar.FlatAppearance.BorderSize = 0;
            this.btnAgregar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAgregar.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAgregar.ForeColor = System.Drawing.Color.White;
            this.btnAgregar.Image = ((System.Drawing.Image)(resources.GetObject("btnAgregar.Image")));
            this.btnAgregar.Location = new System.Drawing.Point(0, 209);
            this.btnAgregar.Name = "btnAgregar";
            this.btnAgregar.Size = new System.Drawing.Size(231, 46);
            this.btnAgregar.TabIndex = 12;
            this.btnAgregar.Text = "Agregar a canastilla de formas de pago";
            this.btnAgregar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAgregar.UseVisualStyleBackColor = false;
            this.btnAgregar.Click += new System.EventHandler(this.btnAgregar_Click);
            // 
            // txImporte
            // 
            this.txImporte.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txImporte.BackColor = System.Drawing.Color.White;
            this.txImporte.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txImporte.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txImporte.ColorLine = System.Drawing.Color.Gray;
            this.txImporte.ColorText = System.Drawing.SystemColors.WindowText;
            this.txImporte.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txImporte.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txImporte.Error = "";
            this.txImporte.FontText = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txImporte.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txImporte.FormatLogin = false;
            this.txImporte.ImageIcon = null;
            this.txImporte.Info = "";
            this.txImporte.Location = new System.Drawing.Point(12, 151);
            this.txImporte.MaterialStyle = true;
            this.txImporte.MaxLength = 32767;
            this.txImporte.MultiLineText = false;
            this.txImporte.Name = "txImporte";
            this.txImporte.PasswordChar = '\0';
            this.txImporte.Placeholder = "Digitar el Importe/Monto";
            this.txImporte.ReadOnly = false;
            this.txImporte.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txImporte.Size = new System.Drawing.Size(207, 58);
            this.txImporte.SizeLine = 2;
            this.txImporte.TabIndex = 1;
            this.txImporte.Title = "Digitar el Importe/Monto";
            this.txImporte.VisibleIcon = false;
            this.txImporte.VisibleTitle = false;
            this.txImporte.KeyPress += new System.EventHandler<System.Windows.Forms.KeyPressEventArgs>(this.txImporte_KeyPress);
            // 
            // txOperacion
            // 
            this.txOperacion.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txOperacion.BackColor = System.Drawing.Color.White;
            this.txOperacion.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txOperacion.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txOperacion.ColorLine = System.Drawing.Color.Gray;
            this.txOperacion.ColorText = System.Drawing.SystemColors.WindowText;
            this.txOperacion.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txOperacion.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txOperacion.Enabled = false;
            this.txOperacion.Error = "";
            this.txOperacion.FontText = new System.Drawing.Font("Verdana", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txOperacion.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txOperacion.FormatLogin = false;
            this.txOperacion.ImageIcon = null;
            this.txOperacion.Info = "";
            this.txOperacion.Location = new System.Drawing.Point(12, 102);
            this.txOperacion.MaterialStyle = true;
            this.txOperacion.MaxLength = 32767;
            this.txOperacion.MultiLineText = false;
            this.txOperacion.Name = "txOperacion";
            this.txOperacion.PasswordChar = '\0';
            this.txOperacion.Placeholder = "Operación";
            this.txOperacion.ReadOnly = false;
            this.txOperacion.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txOperacion.Size = new System.Drawing.Size(207, 58);
            this.txOperacion.SizeLine = 2;
            this.txOperacion.TabIndex = 6;
            this.txOperacion.Title = "Operación";
            this.txOperacion.VisibleIcon = false;
            this.txOperacion.VisibleTitle = false;
            this.txOperacion.KeyPress += new System.EventHandler<System.Windows.Forms.KeyPressEventArgs>(this.txOperacion_KeyPress);
            // 
            // cbMoneda
            // 
            this.cbMoneda.BackColor = System.Drawing.Color.White;
            this.cbMoneda.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbMoneda.ColorLine = System.Drawing.Color.Gray;
            this.cbMoneda.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbMoneda.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbMoneda.DisplayMember = "";
            this.cbMoneda.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbMoneda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbMoneda.Error = "";
            this.cbMoneda.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbMoneda.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbMoneda.ImageIcon = null;
            this.cbMoneda.Info = "";
            this.cbMoneda.Location = new System.Drawing.Point(12, 52);
            this.cbMoneda.MaterialStyle = true;
            this.cbMoneda.Name = "cbMoneda";
            this.cbMoneda.Placeholder = "-Select- Tipo de moneda";
            this.cbMoneda.SelectedIndex = -1;
            this.cbMoneda.Size = new System.Drawing.Size(207, 58);
            this.cbMoneda.SizeLine = 2;
            this.cbMoneda.TabIndex = 0;
            this.cbMoneda.Title = "-Select- Tipo de moneda";
            this.cbMoneda.ValueMember = "";
            this.cbMoneda.VisibleIcon = false;
            this.cbMoneda.VisibleTitle = false;
            // 
            // cbBanco
            // 
            this.cbBanco.BackColor = System.Drawing.Color.White;
            this.cbBanco.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbBanco.ColorLine = System.Drawing.Color.Gray;
            this.cbBanco.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbBanco.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbBanco.DisplayMember = "";
            this.cbBanco.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbBanco.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbBanco.Enabled = false;
            this.cbBanco.Error = "";
            this.cbBanco.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbBanco.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbBanco.ImageIcon = null;
            this.cbBanco.Info = "";
            this.cbBanco.Location = new System.Drawing.Point(12, 3);
            this.cbBanco.MaterialStyle = true;
            this.cbBanco.Name = "cbBanco";
            this.cbBanco.Placeholder = "-Select- Entidad bancaria";
            this.cbBanco.SelectedIndex = -1;
            this.cbBanco.Size = new System.Drawing.Size(207, 58);
            this.cbBanco.SizeLine = 2;
            this.cbBanco.TabIndex = 3;
            this.cbBanco.Title = "-Select- Entidad bancaria";
            this.cbBanco.ValueMember = "";
            this.cbBanco.VisibleIcon = false;
            this.cbBanco.VisibleTitle = false;
            // 
            // label13
            // 
            this.label13.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label13.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(0, 255);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(231, 5);
            this.label13.TabIndex = 21;
            // 
            // lbForma
            // 
            this.lbForma.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(241)))), ((int)(((byte)(241)))), ((int)(((byte)(241)))));
            this.lbForma.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbForma.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbForma.Location = new System.Drawing.Point(0, 0);
            this.lbForma.Name = "lbForma";
            this.lbForma.Size = new System.Drawing.Size(231, 34);
            this.lbForma.TabIndex = 20;
            this.lbForma.Text = "Pago - Tarjeta de Crédito";
            this.lbForma.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pnlCanasta
            // 
            this.pnlCanasta.Controls.Add(this.pnlcanastilla);
            this.pnlCanasta.Controls.Add(this.pnlResumen);
            this.pnlCanasta.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCanasta.Location = new System.Drawing.Point(381, 137);
            this.pnlCanasta.Name = "pnlCanasta";
            this.pnlCanasta.Size = new System.Drawing.Size(577, 294);
            this.pnlCanasta.TabIndex = 2;
            // 
            // pnlcanastilla
            // 
            this.pnlcanastilla.Controls.Add(this.grdCanastilla);
            this.pnlcanastilla.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlcanastilla.Location = new System.Drawing.Point(0, 0);
            this.pnlcanastilla.Name = "pnlcanastilla";
            this.pnlcanastilla.Size = new System.Drawing.Size(577, 170);
            this.pnlcanastilla.TabIndex = 0;
            // 
            // grdCanastilla
            // 
            this.grdCanastilla.AllowUserToAddRows = false;
            this.grdCanastilla.AllowUserToDeleteRows = false;
            this.grdCanastilla.AllowUserToResizeRows = false;
            this.grdCanastilla.BackgroundColor = System.Drawing.Color.White;
            this.grdCanastilla.BodyFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdCanastilla.BodyForeColor = System.Drawing.SystemColors.ControlText;
            this.grdCanastilla.BodySelectColor = System.Drawing.SystemColors.Highlight;
            this.grdCanastilla.BodySelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.grdCanastilla.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdCanastilla.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdCanastilla.CellStyleBackColor = System.Drawing.SystemColors.Window;
            this.grdCanastilla.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdCanastilla.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdCanastilla.ColumnHeadersHeight = 34;
            this.grdCanastilla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdCanastilla.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdUnico,
            this.IdMetodo,
            this.GMetodo,
            this.NumOperacion,
            this.TipoMoneda,
            this.GSoles,
            this.GDolares,
            this.Total,
            this.DeleteRow});
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdCanastilla.DefaultCellStyle = dataGridViewCellStyle6;
            this.grdCanastilla.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCanastilla.EnableBottomDown = false;
            this.grdCanastilla.EnableBottomLeft = false;
            this.grdCanastilla.EnableBottomRight = false;
            this.grdCanastilla.EnableBottomUp = false;
            this.grdCanastilla.EnableHeadersVisualStyles = false;
            this.grdCanastilla.HeaderColor = System.Drawing.SystemColors.Control;
            this.grdCanastilla.HeaderFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdCanastilla.HeaderForeColor = System.Drawing.SystemColors.WindowText;
            this.grdCanastilla.Location = new System.Drawing.Point(0, 0);
            this.grdCanastilla.MultiSelect = false;
            this.grdCanastilla.Name = "grdCanastilla";
            this.grdCanastilla.ReadOnly = true;
            this.grdCanastilla.RowHeadersVisible = false;
            this.grdCanastilla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdCanastilla.Size = new System.Drawing.Size(577, 170);
            this.grdCanastilla.TabIndex = 8;
            this.grdCanastilla.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCanastilla_CellContentDoubleClick);
            // 
            // IdUnico
            // 
            this.IdUnico.DataPropertyName = "IdUnico";
            this.IdUnico.HeaderText = "IdUnico";
            this.IdUnico.Name = "IdUnico";
            this.IdUnico.ReadOnly = true;
            this.IdUnico.Visible = false;
            // 
            // IdMetodo
            // 
            this.IdMetodo.DataPropertyName = "IdMetodo";
            this.IdMetodo.HeaderText = "IdMetodo";
            this.IdMetodo.Name = "IdMetodo";
            this.IdMetodo.ReadOnly = true;
            this.IdMetodo.Visible = false;
            // 
            // GMetodo
            // 
            this.GMetodo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.GMetodo.DataPropertyName = "GMetodo";
            this.GMetodo.HeaderText = "Método de pago";
            this.GMetodo.Name = "GMetodo";
            this.GMetodo.ReadOnly = true;
            // 
            // NumOperacion
            // 
            this.NumOperacion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells;
            this.NumOperacion.DataPropertyName = "NumOperacion";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.NumOperacion.DefaultCellStyle = dataGridViewCellStyle2;
            this.NumOperacion.HeaderText = "#op.";
            this.NumOperacion.Name = "NumOperacion";
            this.NumOperacion.ReadOnly = true;
            this.NumOperacion.Width = 60;
            // 
            // TipoMoneda
            // 
            this.TipoMoneda.DataPropertyName = "TipoMoneda";
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.TipoMoneda.DefaultCellStyle = dataGridViewCellStyle3;
            this.TipoMoneda.HeaderText = "t/m";
            this.TipoMoneda.Name = "TipoMoneda";
            this.TipoMoneda.ReadOnly = true;
            this.TipoMoneda.Width = 36;
            // 
            // GSoles
            // 
            this.GSoles.DataPropertyName = "GSoles";
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Info;
            this.GSoles.DefaultCellStyle = dataGridViewCellStyle4;
            this.GSoles.HeaderText = "Cuenta S/.";
            this.GSoles.Name = "GSoles";
            this.GSoles.ReadOnly = true;
            this.GSoles.Width = 70;
            // 
            // GDolares
            // 
            this.GDolares.DataPropertyName = "GDolares";
            this.GDolares.HeaderText = "Cuenta $/.";
            this.GDolares.Name = "GDolares";
            this.GDolares.ReadOnly = true;
            this.GDolares.Width = 70;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            this.Total.HeaderText = "Importe Total S/.";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Width = 70;
            // 
            // DeleteRow
            // 
            this.DeleteRow.DataPropertyName = "DeleteRow";
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.Red;
            dataGridViewCellStyle5.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.Red;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.White;
            this.DeleteRow.DefaultCellStyle = dataGridViewCellStyle5;
            this.DeleteRow.HeaderText = "";
            this.DeleteRow.Name = "DeleteRow";
            this.DeleteRow.ReadOnly = true;
            this.DeleteRow.Width = 26;
            // 
            // pnlResumen
            // 
            this.pnlResumen.Controls.Add(this.lbMensaje);
            this.pnlResumen.Controls.Add(this.cbxFactura);
            this.pnlResumen.Controls.Add(this.cbxBoleta);
            this.pnlResumen.Controls.Add(this.cbxInterno);
            this.pnlResumen.Controls.Add(this.label9);
            this.pnlResumen.Controls.Add(this.txTotalCanastilla);
            this.pnlResumen.Controls.Add(this.txVueltoSoles);
            this.pnlResumen.Controls.Add(this.txSaldo);
            this.pnlResumen.Controls.Add(this.txVueltoDolares);
            this.pnlResumen.Controls.Add(this.txImporteGenerado);
            this.pnlResumen.Controls.Add(this.label8);
            this.pnlResumen.Controls.Add(this.label18);
            this.pnlResumen.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.pnlResumen.Location = new System.Drawing.Point(0, 170);
            this.pnlResumen.Name = "pnlResumen";
            this.pnlResumen.Size = new System.Drawing.Size(577, 124);
            this.pnlResumen.TabIndex = 0;
            // 
            // lbMensaje
            // 
            this.lbMensaje.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lbMensaje.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMensaje.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.lbMensaje.Location = new System.Drawing.Point(0, 99);
            this.lbMensaje.Name = "lbMensaje";
            this.lbMensaje.Size = new System.Drawing.Size(577, 20);
            this.lbMensaje.TabIndex = 30;
            this.lbMensaje.Text = "Mensaje";
            this.lbMensaje.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cbxFactura
            // 
            this.cbxFactura.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbxFactura.AutoSize = true;
            this.cbxFactura.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxFactura.Enabled = false;
            this.cbxFactura.FlatAppearance.BorderSize = 0;
            this.cbxFactura.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cbxFactura.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cbxFactura.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxFactura.Font = new System.Drawing.Font("Verdana", 10F);
            this.cbxFactura.Image = ((System.Drawing.Image)(resources.GetObject("cbxFactura.Image")));
            this.cbxFactura.ImageChecking = ((System.Drawing.Image)(resources.GetObject("cbxFactura.ImageChecking")));
            this.cbxFactura.ImageUnChecking = ((System.Drawing.Image)(resources.GetObject("cbxFactura.ImageUnChecking")));
            this.cbxFactura.Location = new System.Drawing.Point(466, 67);
            this.cbxFactura.Name = "cbxFactura";
            this.cbxFactura.Size = new System.Drawing.Size(104, 27);
            this.cbxFactura.TabIndex = 29;
            this.cbxFactura.Text = "FACTURA";
            this.cbxFactura.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cbxFactura.UseVisualStyleBackColor = true;
            this.cbxFactura.CheckedChanged += new System.EventHandler(this.cbxFactura_CheckedChanged);
            // 
            // cbxBoleta
            // 
            this.cbxBoleta.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbxBoleta.AutoSize = true;
            this.cbxBoleta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxBoleta.Enabled = false;
            this.cbxBoleta.FlatAppearance.BorderSize = 0;
            this.cbxBoleta.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cbxBoleta.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cbxBoleta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxBoleta.Font = new System.Drawing.Font("Verdana", 10F);
            this.cbxBoleta.Image = ((System.Drawing.Image)(resources.GetObject("cbxBoleta.Image")));
            this.cbxBoleta.ImageChecking = ((System.Drawing.Image)(resources.GetObject("cbxBoleta.ImageChecking")));
            this.cbxBoleta.ImageUnChecking = ((System.Drawing.Image)(resources.GetObject("cbxBoleta.ImageUnChecking")));
            this.cbxBoleta.Location = new System.Drawing.Point(316, 67);
            this.cbxBoleta.Name = "cbxBoleta";
            this.cbxBoleta.Size = new System.Drawing.Size(148, 27);
            this.cbxBoleta.TabIndex = 28;
            this.cbxBoleta.Text = "BOLETA-VENTA";
            this.cbxBoleta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cbxBoleta.UseVisualStyleBackColor = true;
            this.cbxBoleta.CheckedChanged += new System.EventHandler(this.cbxBoleta_CheckedChanged);
            // 
            // cbxInterno
            // 
            this.cbxInterno.Appearance = System.Windows.Forms.Appearance.Button;
            this.cbxInterno.AutoSize = true;
            this.cbxInterno.Checked = true;
            this.cbxInterno.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cbxInterno.FlatAppearance.BorderSize = 0;
            this.cbxInterno.FlatAppearance.MouseDownBackColor = System.Drawing.Color.Transparent;
            this.cbxInterno.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent;
            this.cbxInterno.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cbxInterno.Font = new System.Drawing.Font("Verdana", 10F);
            this.cbxInterno.Image = ((System.Drawing.Image)(resources.GetObject("cbxInterno.Image")));
            this.cbxInterno.ImageChecking = ((System.Drawing.Image)(resources.GetObject("cbxInterno.ImageChecking")));
            this.cbxInterno.ImageUnChecking = ((System.Drawing.Image)(resources.GetObject("cbxInterno.ImageUnChecking")));
            this.cbxInterno.Location = new System.Drawing.Point(153, 67);
            this.cbxInterno.Name = "cbxInterno";
            this.cbxInterno.Size = new System.Drawing.Size(160, 27);
            this.cbxInterno.TabIndex = 27;
            this.cbxInterno.TabStop = true;
            this.cbxInterno.Text = "TICKET-INTERNO";
            this.cbxInterno.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cbxInterno.UseVisualStyleBackColor = true;
            this.cbxInterno.CheckedChanged += new System.EventHandler(this.cbxInterno_CheckedChanged);
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label9.Location = new System.Drawing.Point(6, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(144, 41);
            this.label9.TabIndex = 26;
            this.label9.Text = "Establecer el Tipo de Comprobante";
            // 
            // txTotalCanastilla
            // 
            this.txTotalCanastilla.AlignText = System.Windows.Forms.HorizontalAlignment.Center;
            this.txTotalCanastilla.BackColor = System.Drawing.Color.White;
            this.txTotalCanastilla.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txTotalCanastilla.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txTotalCanastilla.ColorLine = System.Drawing.Color.Gray;
            this.txTotalCanastilla.ColorText = System.Drawing.SystemColors.WindowText;
            this.txTotalCanastilla.ColorTitle = System.Drawing.Color.Gray;
            this.txTotalCanastilla.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txTotalCanastilla.FontText = new System.Drawing.Font("Verdana", 13F, System.Drawing.FontStyle.Bold);
            this.txTotalCanastilla.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txTotalCanastilla.ImageIcon = null;
            this.txTotalCanastilla.Location = new System.Drawing.Point(459, 6);
            this.txTotalCanastilla.MaterialStyle = false;
            this.txTotalCanastilla.MaxLength = 32767;
            this.txTotalCanastilla.MultiLineText = false;
            this.txTotalCanastilla.Name = "txTotalCanastilla";
            this.txTotalCanastilla.ObjectArray = null;
            this.txTotalCanastilla.PasswordChar = '\0';
            this.txTotalCanastilla.Placeholder = "";
            this.txTotalCanastilla.PlaceHolderHeight = 18;
            this.txTotalCanastilla.ReadOnly = true;
            this.txTotalCanastilla.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txTotalCanastilla.Size = new System.Drawing.Size(108, 44);
            this.txTotalCanastilla.SizeLine = 2;
            this.txTotalCanastilla.StringArray = null;
            this.txTotalCanastilla.TabIndex = 24;
            this.txTotalCanastilla.Text = "0.00";
            this.txTotalCanastilla.TextId = "";
            this.txTotalCanastilla.Title = "Suma Canastilla";
            this.txTotalCanastilla.VisibleIcon = false;
            this.txTotalCanastilla.VisibleTitle = true;
            // 
            // txVueltoSoles
            // 
            this.txVueltoSoles.AlignText = System.Windows.Forms.HorizontalAlignment.Center;
            this.txVueltoSoles.BackColor = System.Drawing.Color.White;
            this.txVueltoSoles.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txVueltoSoles.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txVueltoSoles.ColorLine = System.Drawing.Color.Gray;
            this.txVueltoSoles.ColorText = System.Drawing.Color.SeaGreen;
            this.txVueltoSoles.ColorTitle = System.Drawing.Color.Gray;
            this.txVueltoSoles.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txVueltoSoles.FontText = new System.Drawing.Font("Verdana", 13F, System.Drawing.FontStyle.Bold);
            this.txVueltoSoles.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txVueltoSoles.ImageIcon = null;
            this.txVueltoSoles.Location = new System.Drawing.Point(353, 6);
            this.txVueltoSoles.MaterialStyle = false;
            this.txVueltoSoles.MaxLength = 32767;
            this.txVueltoSoles.MultiLineText = false;
            this.txVueltoSoles.Name = "txVueltoSoles";
            this.txVueltoSoles.ObjectArray = null;
            this.txVueltoSoles.PasswordChar = '\0';
            this.txVueltoSoles.Placeholder = "";
            this.txVueltoSoles.PlaceHolderHeight = 18;
            this.txVueltoSoles.ReadOnly = true;
            this.txVueltoSoles.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txVueltoSoles.Size = new System.Drawing.Size(100, 44);
            this.txVueltoSoles.SizeLine = 2;
            this.txVueltoSoles.StringArray = null;
            this.txVueltoSoles.TabIndex = 23;
            this.txVueltoSoles.Text = "0.00";
            this.txVueltoSoles.TextId = "";
            this.txVueltoSoles.Title = "Vuelto S/.";
            this.txVueltoSoles.VisibleIcon = false;
            this.txVueltoSoles.VisibleTitle = true;
            // 
            // txSaldo
            // 
            this.txSaldo.AlignText = System.Windows.Forms.HorizontalAlignment.Center;
            this.txSaldo.BackColor = System.Drawing.Color.White;
            this.txSaldo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txSaldo.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txSaldo.ColorLine = System.Drawing.Color.Gray;
            this.txSaldo.ColorText = System.Drawing.Color.Tomato;
            this.txSaldo.ColorTitle = System.Drawing.Color.Gray;
            this.txSaldo.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txSaldo.FontText = new System.Drawing.Font("Verdana", 13F, System.Drawing.FontStyle.Bold);
            this.txSaldo.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txSaldo.ImageIcon = null;
            this.txSaldo.Location = new System.Drawing.Point(156, 6);
            this.txSaldo.MaterialStyle = false;
            this.txSaldo.MaxLength = 32767;
            this.txSaldo.MultiLineText = false;
            this.txSaldo.Name = "txSaldo";
            this.txSaldo.ObjectArray = null;
            this.txSaldo.PasswordChar = '\0';
            this.txSaldo.Placeholder = "";
            this.txSaldo.PlaceHolderHeight = 18;
            this.txSaldo.ReadOnly = true;
            this.txSaldo.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txSaldo.Size = new System.Drawing.Size(85, 44);
            this.txSaldo.SizeLine = 2;
            this.txSaldo.StringArray = null;
            this.txSaldo.TabIndex = 23;
            this.txSaldo.Text = "0.00";
            this.txSaldo.TextId = "";
            this.txSaldo.Title = "Saldo";
            this.txSaldo.VisibleIcon = false;
            this.txSaldo.VisibleTitle = true;
            // 
            // txVueltoDolares
            // 
            this.txVueltoDolares.AlignText = System.Windows.Forms.HorizontalAlignment.Center;
            this.txVueltoDolares.BackColor = System.Drawing.Color.White;
            this.txVueltoDolares.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txVueltoDolares.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txVueltoDolares.ColorLine = System.Drawing.Color.Gray;
            this.txVueltoDolares.ColorText = System.Drawing.Color.SeaGreen;
            this.txVueltoDolares.ColorTitle = System.Drawing.Color.Gray;
            this.txVueltoDolares.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txVueltoDolares.FontText = new System.Drawing.Font("Verdana", 13F, System.Drawing.FontStyle.Bold);
            this.txVueltoDolares.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txVueltoDolares.ImageIcon = null;
            this.txVueltoDolares.Location = new System.Drawing.Point(247, 6);
            this.txVueltoDolares.MaterialStyle = false;
            this.txVueltoDolares.MaxLength = 32767;
            this.txVueltoDolares.MultiLineText = false;
            this.txVueltoDolares.Name = "txVueltoDolares";
            this.txVueltoDolares.ObjectArray = null;
            this.txVueltoDolares.PasswordChar = '\0';
            this.txVueltoDolares.Placeholder = "";
            this.txVueltoDolares.PlaceHolderHeight = 18;
            this.txVueltoDolares.ReadOnly = true;
            this.txVueltoDolares.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txVueltoDolares.Size = new System.Drawing.Size(100, 44);
            this.txVueltoDolares.SizeLine = 2;
            this.txVueltoDolares.StringArray = null;
            this.txVueltoDolares.TabIndex = 22;
            this.txVueltoDolares.Text = "0.00";
            this.txVueltoDolares.TextId = "";
            this.txVueltoDolares.Title = "vuelto $/.";
            this.txVueltoDolares.VisibleIcon = false;
            this.txVueltoDolares.VisibleTitle = true;
            // 
            // txImporteGenerado
            // 
            this.txImporteGenerado.AlignText = System.Windows.Forms.HorizontalAlignment.Center;
            this.txImporteGenerado.BackColor = System.Drawing.Color.White;
            this.txImporteGenerado.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txImporteGenerado.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txImporteGenerado.ColorLine = System.Drawing.Color.Gray;
            this.txImporteGenerado.ColorText = System.Drawing.SystemColors.WindowText;
            this.txImporteGenerado.ColorTitle = System.Drawing.Color.Gray;
            this.txImporteGenerado.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txImporteGenerado.FontText = new System.Drawing.Font("Verdana", 13F, System.Drawing.FontStyle.Bold);
            this.txImporteGenerado.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txImporteGenerado.ImageIcon = null;
            this.txImporteGenerado.Location = new System.Drawing.Point(8, 6);
            this.txImporteGenerado.MaterialStyle = false;
            this.txImporteGenerado.MaxLength = 32767;
            this.txImporteGenerado.MultiLineText = false;
            this.txImporteGenerado.Name = "txImporteGenerado";
            this.txImporteGenerado.ObjectArray = null;
            this.txImporteGenerado.PasswordChar = '\0';
            this.txImporteGenerado.Placeholder = "";
            this.txImporteGenerado.PlaceHolderHeight = 18;
            this.txImporteGenerado.ReadOnly = true;
            this.txImporteGenerado.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txImporteGenerado.Size = new System.Drawing.Size(142, 44);
            this.txImporteGenerado.SizeLine = 2;
            this.txImporteGenerado.StringArray = null;
            this.txImporteGenerado.TabIndex = 22;
            this.txImporteGenerado.Text = "0.00";
            this.txImporteGenerado.TextId = "";
            this.txImporteGenerado.Title = "Importe generado S/.";
            this.txImporteGenerado.VisibleIcon = false;
            this.txImporteGenerado.VisibleTitle = true;
            // 
            // label8
            // 
            this.label8.BackColor = System.Drawing.Color.Gainsboro;
            this.label8.Dock = System.Windows.Forms.DockStyle.Top;
            this.label8.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(0, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(577, 1);
            this.label8.TabIndex = 21;
            // 
            // label18
            // 
            this.label18.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label18.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(0, 119);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(577, 5);
            this.label18.TabIndex = 31;
            // 
            // pnlCliente
            // 
            this.pnlCliente.Controls.Add(this.label1);
            this.pnlCliente.Controls.Add(this.lbVendedor);
            this.pnlCliente.Controls.Add(this.label3);
            this.pnlCliente.Controls.Add(this.lbNivelPrecio);
            this.pnlCliente.Controls.Add(this.lbIdentificacion);
            this.pnlCliente.Controls.Add(this.label5);
            this.pnlCliente.Controls.Add(this.label2);
            this.pnlCliente.Controls.Add(this.lbCliente);
            this.pnlCliente.Controls.Add(this.btnCliente);
            this.pnlCliente.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlCliente.Location = new System.Drawing.Point(0, 0);
            this.pnlCliente.Name = "pnlCliente";
            this.pnlCliente.Size = new System.Drawing.Size(958, 61);
            this.pnlCliente.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.YellowGreen;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(0, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(958, 1);
            this.label1.TabIndex = 19;
            // 
            // lbVendedor
            // 
            this.lbVendedor.AutoSize = true;
            this.lbVendedor.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVendedor.Location = new System.Drawing.Point(702, 31);
            this.lbVendedor.Name = "lbVendedor";
            this.lbVendedor.Size = new System.Drawing.Size(71, 18);
            this.lbVendedor.TabIndex = 18;
            this.lbVendedor.Text = "Regular";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(626, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(75, 16);
            this.label3.TabIndex = 17;
            this.label3.Text = "Vendedor:";
            // 
            // lbNivelPrecio
            // 
            this.lbNivelPrecio.AutoSize = true;
            this.lbNivelPrecio.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNivelPrecio.Location = new System.Drawing.Point(483, 34);
            this.lbNivelPrecio.Name = "lbNivelPrecio";
            this.lbNivelPrecio.Size = new System.Drawing.Size(71, 18);
            this.lbNivelPrecio.TabIndex = 16;
            this.lbNivelPrecio.Text = "Regular";
            // 
            // lbIdentificacion
            // 
            this.lbIdentificacion.AutoSize = true;
            this.lbIdentificacion.Font = new System.Drawing.Font("Verdana", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbIdentificacion.Location = new System.Drawing.Point(229, 32);
            this.lbIdentificacion.Name = "lbIdentificacion";
            this.lbIdentificacion.Size = new System.Drawing.Size(140, 18);
            this.lbIdentificacion.TabIndex = 14;
            this.lbIdentificacion.Text = "012345678912";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(375, 33);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 16);
            this.label5.TabIndex = 15;
            this.label5.Text = "Nivel de precio:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(129, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(103, 16);
            this.label2.TabIndex = 13;
            this.label2.Text = "Identificación:";
            // 
            // lbCliente
            // 
            this.lbCliente.AutoSize = true;
            this.lbCliente.Font = new System.Drawing.Font("Verdana", 13F);
            this.lbCliente.Location = new System.Drawing.Point(129, 6);
            this.lbCliente.Name = "lbCliente";
            this.lbCliente.Size = new System.Drawing.Size(184, 22);
            this.lbCliente.TabIndex = 12;
            this.lbCliente.Text = "Nombre del Cliente";
            // 
            // btnCliente
            // 
            this.btnCliente.BackColor = System.Drawing.Color.SeaGreen;
            this.btnCliente.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCliente.FlatAppearance.BorderSize = 0;
            this.btnCliente.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCliente.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCliente.ForeColor = System.Drawing.Color.White;
            this.btnCliente.Image = ((System.Drawing.Image)(resources.GetObject("btnCliente.Image")));
            this.btnCliente.Location = new System.Drawing.Point(12, 6);
            this.btnCliente.Name = "btnCliente";
            this.btnCliente.Size = new System.Drawing.Size(111, 46);
            this.btnCliente.TabIndex = 11;
            this.btnCliente.Text = "Reasignar Cliente";
            this.btnCliente.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCliente.UseVisualStyleBackColor = false;
            this.btnCliente.Click += new System.EventHandler(this.btnCliente_Click);
            // 
            // pnlMetodos
            // 
            this.pnlMetodos.Controls.Add(this.label6);
            this.pnlMetodos.Controls.Add(this.btnTransferencia);
            this.pnlMetodos.Controls.Add(this.label4);
            this.pnlMetodos.Controls.Add(this.btnCupon);
            this.pnlMetodos.Controls.Add(this.btnEfectivo);
            this.pnlMetodos.Controls.Add(this.btnCheque);
            this.pnlMetodos.Controls.Add(this.btnCredito);
            this.pnlMetodos.Controls.Add(this.btnTarjeta);
            this.pnlMetodos.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnlMetodos.Location = new System.Drawing.Point(0, 61);
            this.pnlMetodos.Name = "pnlMetodos";
            this.pnlMetodos.Size = new System.Drawing.Size(958, 76);
            this.pnlMetodos.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.BackColor = System.Drawing.Color.YellowGreen;
            this.label6.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label6.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 75);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(958, 1);
            this.label6.TabIndex = 20;
            // 
            // btnTransferencia
            // 
            this.btnTransferencia.BackColor = System.Drawing.Color.Transparent;
            this.btnTransferencia.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTransferencia.FlatAppearance.BorderSize = 0;
            this.btnTransferencia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTransferencia.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTransferencia.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnTransferencia.Image = ((System.Drawing.Image)(resources.GetObject("btnTransferencia.Image")));
            this.btnTransferencia.Location = new System.Drawing.Point(704, 23);
            this.btnTransferencia.Name = "btnTransferencia";
            this.btnTransferencia.Size = new System.Drawing.Size(160, 46);
            this.btnTransferencia.TabIndex = 19;
            this.btnTransferencia.Text = "Transferencia";
            this.btnTransferencia.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTransferencia.UseVisualStyleBackColor = false;
            this.btnTransferencia.Click += new System.EventHandler(this.btnTransferencia_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Verdana", 11F);
            this.label4.Location = new System.Drawing.Point(12, 3);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(138, 18);
            this.label4.TabIndex = 13;
            this.label4.Text = "Métodos de pago";
            // 
            // btnCupon
            // 
            this.btnCupon.BackColor = System.Drawing.Color.Transparent;
            this.btnCupon.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCupon.Enabled = false;
            this.btnCupon.FlatAppearance.BorderSize = 0;
            this.btnCupon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCupon.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCupon.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCupon.Image = ((System.Drawing.Image)(resources.GetObject("btnCupon.Image")));
            this.btnCupon.Location = new System.Drawing.Point(574, 23);
            this.btnCupon.Name = "btnCupon";
            this.btnCupon.Size = new System.Drawing.Size(124, 46);
            this.btnCupon.TabIndex = 18;
            this.btnCupon.Text = "Cupón";
            this.btnCupon.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCupon.UseVisualStyleBackColor = false;
            this.btnCupon.Click += new System.EventHandler(this.btnCupon_Click);
            // 
            // btnEfectivo
            // 
            this.btnEfectivo.BackColor = System.Drawing.Color.Transparent;
            this.btnEfectivo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnEfectivo.FlatAppearance.BorderSize = 0;
            this.btnEfectivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEfectivo.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnEfectivo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnEfectivo.Image = ((System.Drawing.Image)(resources.GetObject("btnEfectivo.Image")));
            this.btnEfectivo.Location = new System.Drawing.Point(15, 24);
            this.btnEfectivo.Name = "btnEfectivo";
            this.btnEfectivo.Size = new System.Drawing.Size(124, 46);
            this.btnEfectivo.TabIndex = 14;
            this.btnEfectivo.Text = "Efectivo";
            this.btnEfectivo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnEfectivo.UseVisualStyleBackColor = false;
            this.btnEfectivo.Click += new System.EventHandler(this.btnEfectivo_Click);
            // 
            // btnCheque
            // 
            this.btnCheque.BackColor = System.Drawing.Color.Transparent;
            this.btnCheque.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCheque.FlatAppearance.BorderSize = 0;
            this.btnCheque.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCheque.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCheque.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCheque.Image = ((System.Drawing.Image)(resources.GetObject("btnCheque.Image")));
            this.btnCheque.Location = new System.Drawing.Point(145, 23);
            this.btnCheque.Name = "btnCheque";
            this.btnCheque.Size = new System.Drawing.Size(124, 46);
            this.btnCheque.TabIndex = 15;
            this.btnCheque.Text = "Cheque";
            this.btnCheque.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCheque.UseVisualStyleBackColor = false;
            this.btnCheque.Click += new System.EventHandler(this.btnCheque_Click);
            // 
            // btnCredito
            // 
            this.btnCredito.BackColor = System.Drawing.Color.Transparent;
            this.btnCredito.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCredito.Enabled = false;
            this.btnCredito.FlatAppearance.BorderSize = 0;
            this.btnCredito.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCredito.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnCredito.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnCredito.Image = ((System.Drawing.Image)(resources.GetObject("btnCredito.Image")));
            this.btnCredito.Location = new System.Drawing.Point(444, 23);
            this.btnCredito.Name = "btnCredito";
            this.btnCredito.Size = new System.Drawing.Size(124, 46);
            this.btnCredito.TabIndex = 17;
            this.btnCredito.Text = "Línea de Crédito";
            this.btnCredito.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCredito.UseVisualStyleBackColor = false;
            this.btnCredito.Click += new System.EventHandler(this.btnCredito_Click);
            // 
            // btnTarjeta
            // 
            this.btnTarjeta.BackColor = System.Drawing.Color.Transparent;
            this.btnTarjeta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnTarjeta.FlatAppearance.BorderSize = 0;
            this.btnTarjeta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTarjeta.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTarjeta.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.btnTarjeta.Image = ((System.Drawing.Image)(resources.GetObject("btnTarjeta.Image")));
            this.btnTarjeta.Location = new System.Drawing.Point(275, 24);
            this.btnTarjeta.Name = "btnTarjeta";
            this.btnTarjeta.Size = new System.Drawing.Size(163, 46);
            this.btnTarjeta.TabIndex = 16;
            this.btnTarjeta.Text = "Tarjeta Débito/Credito";
            this.btnTarjeta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTarjeta.UseVisualStyleBackColor = false;
            this.btnTarjeta.Click += new System.EventHandler(this.btnTarjeta_Click);
            // 
            // btnPrevisualizar
            // 
            this.btnPrevisualizar.BackColor = System.Drawing.Color.SeaGreen;
            this.btnPrevisualizar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPrevisualizar.FlatAppearance.BorderSize = 0;
            this.btnPrevisualizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPrevisualizar.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPrevisualizar.ForeColor = System.Drawing.Color.White;
            this.btnPrevisualizar.Image = ((System.Drawing.Image)(resources.GetObject("btnPrevisualizar.Image")));
            this.btnPrevisualizar.Location = new System.Drawing.Point(574, 6);
            this.btnPrevisualizar.Name = "btnPrevisualizar";
            this.btnPrevisualizar.Size = new System.Drawing.Size(200, 42);
            this.btnPrevisualizar.TabIndex = 13;
            this.btnPrevisualizar.Text = "Previsualizar pagos";
            this.btnPrevisualizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPrevisualizar.UseVisualStyleBackColor = false;
            // 
            // lbCapComprobante
            // 
            this.lbCapComprobante.Font = new System.Drawing.Font("Verdana", 8.75F);
            this.lbCapComprobante.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbCapComprobante.Location = new System.Drawing.Point(17, 5);
            this.lbCapComprobante.Name = "lbCapComprobante";
            this.lbCapComprobante.Size = new System.Drawing.Size(537, 13);
            this.lbCapComprobante.TabIndex = 27;
            this.lbCapComprobante.Text = "P  O  S  I  B  L  E      N  °      D  E      C  O  M  P  R  O  B  A  N  T  E";
            this.lbCapComprobante.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            // 
            // lbNumComprobante
            // 
            this.lbNumComprobante.Font = new System.Drawing.Font("Verdana", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbNumComprobante.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbNumComprobante.Location = new System.Drawing.Point(12, 18);
            this.lbNumComprobante.Name = "lbNumComprobante";
            this.lbNumComprobante.Size = new System.Drawing.Size(542, 33);
            this.lbNumComprobante.TabIndex = 27;
            this.lbNumComprobante.Text = "INT-2021-000001";
            this.lbNumComprobante.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // frmVentaModal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(960, 530);
            this.Name = "frmVentaModal";
            this.Text = "frmVentaModal";
            this.Load += new System.EventHandler(this.frmVentaModal_Load);
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.pnlOrden.ResumeLayout(false);
            this.pnlOrden.PerformLayout();
            this.panelFormaPago.ResumeLayout(false);
            this.panelEfectivo.ResumeLayout(false);
            this.pnlCanasta.ResumeLayout(false);
            this.pnlcanastilla.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.grdCanastilla)).EndInit();
            this.pnlResumen.ResumeLayout(false);
            this.pnlResumen.PerformLayout();
            this.pnlCliente.ResumeLayout(false);
            this.pnlCliente.PerformLayout();
            this.pnlMetodos.ResumeLayout(false);
            this.pnlMetodos.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlMetodos;
        private System.Windows.Forms.Panel pnlCliente;
        private System.Windows.Forms.Panel pnlCanasta;
        private System.Windows.Forms.Panel panelFormaPago;
        private System.Windows.Forms.Panel pnlOrden;
        private Autonomo.CustomControls.CustomButton btnCliente;
        private System.Windows.Forms.Label lbNivelPrecio;
        private System.Windows.Forms.Label lbIdentificacion;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lbCliente;
        private System.Windows.Forms.Label lbVendedor;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label1;
        private Autonomo.CustomControls.CustomButton btnTransferencia;
        private Autonomo.CustomControls.CustomButton btnCupon;
        private Autonomo.CustomControls.CustomButton btnCredito;
        private Autonomo.CustomControls.CustomButton btnTarjeta;
        private Autonomo.CustomControls.CustomButton btnCheque;
        private Autonomo.CustomControls.CustomButton btnEfectivo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbSunat;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lbImporte;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lbFecha;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label lbDocumento;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbForma;
        private System.Windows.Forms.Panel panelEfectivo;
        private Autonomo.CustomControls.FlatComboBox cbBanco;
        private Autonomo.CustomControls.FlatTextBox txImporte;
        private Autonomo.CustomControls.FlatComboBox cbMoneda;
        private Autonomo.CustomControls.FlatTextBox txOperacion;
        private Autonomo.CustomControls.CustomButton btnAgregar;
        private System.Windows.Forms.Panel pnlResumen;
        private System.Windows.Forms.Panel pnlcanastilla;
        private Autonomo.CustomControls.CustomGrid grdCanastilla;
        private Autonomo.CustomControls.FlatFindText txTotalCanastilla;
        private Autonomo.CustomControls.FlatFindText txVueltoSoles;
        private Autonomo.CustomControls.FlatFindText txSaldo;
        private Autonomo.CustomControls.FlatFindText txVueltoDolares;
        private Autonomo.CustomControls.FlatFindText txImporteGenerado;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private Autonomo.CustomControls.CustomButton btnPrevisualizar;
        private System.Windows.Forms.Label lbCapComprobante;
        private System.Windows.Forms.Label lbNumComprobante;
        private Autonomo.CustomControls.CustomRadius cbxFactura;
        private Autonomo.CustomControls.CustomRadius cbxBoleta;
        private Autonomo.CustomControls.CustomRadius cbxInterno;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdUnico;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdMetodo;
        private System.Windows.Forms.DataGridViewTextBoxColumn GMetodo;
        private System.Windows.Forms.DataGridViewTextBoxColumn NumOperacion;
        private System.Windows.Forms.DataGridViewTextBoxColumn TipoMoneda;
        private System.Windows.Forms.DataGridViewTextBoxColumn GSoles;
        private System.Windows.Forms.DataGridViewTextBoxColumn GDolares;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.DataGridViewTextBoxColumn DeleteRow;
        private System.Windows.Forms.Label lbLineaCredito;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label lbMensaje;
        private System.Windows.Forms.Label label18;
    }
}