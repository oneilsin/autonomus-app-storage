﻿using Storage.Datos;
using Storage.Entidad;
using Storage.Entidad.Report;
using Storage.Windows.Tools;
using Storage.Windows.Tools.Formulary;
using Storage.Windows.Tools.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Storage.Windows.Tools.Enumerables;

namespace Storage.Windows.Modulo.Ventas.Gestion
{
    public partial class frmVentaModal : Autonomo.Object.Modal
    {
        public new Form ParentForm;

        OrdenVentaReport _orden;
        MetodoPago metodo;
        Comprobante comprobante;
        public frmVentaModal()
        {
            InitializeComponent();
            metodo = MetodoPago.Efectivo;
            comprobante = Comprobante.Ticket;
        }


        private void PopulateGrid()
        {
            // Validar si el saldo es <=0
            if (decimal.Parse(txSaldo.Text) <= 0m)
            {
                MensajeEmergente(EtiquetaMsg.Warning, "El saldo es 0, ya no se puede registrar más formas de pago.");
                return;
            }

            //Validar si el abono excede la línea de crédito.
            if ((decimal.Parse(txImporte.Text) > decimal.Parse(lbLineaCredito.Text))
                && metodo.ToString().ToLower().Contains("credito")
                )
            {
                MensajeEmergente(EtiquetaMsg.Warning, $"El crédito del cliente es {lbLineaCredito.Text}, esto no es suficiente para cubir el saldo.");
                return;
            }

            var id = new GetData().GetGeneralIdByName("05", metodo).IdTabla;
            var pago = new CanastillaPagoModels()
            {
                IdUnico = getUnico(),
                IdMetodo = id,
                Metodo = getUnico().ToUpper(),
                Operacion = txOperacion.Text,
                Moneda = getMoneda(),
                Soles = getImporte(txImporte.Text, "S"),
                Dolares = getImporte(txImporte.Text, "D"),
                Total = getTotal(txImporte.Text)
            };
            var response = DataGridHelper.PopulateGrid(grdCanastilla, LlenarGrilla.Venta, pago);

            MostrarTotales();
            if (response.Estado)
            {
                MensajeEmergente(EtiquetaMsg.Success, response.Mensaje);
                txImporte.Clear();
                return;
            }
            MensajeEmergente(EtiquetaMsg.Warning, response.Mensaje);
        }

        #region Metodos - Insert to Grid
        string getUnico()
        {
            string banco = (metodo.ToString().ToLower().Contains("cheque")
                || metodo.ToString().ToLower().Contains("tarjeta"))
                ? cbBanco.Text : "";


            return string.Concat(metodo.ToString(), " ", banco, " (", getMoneda(), ")");
        }
        string getMoneda()
        {
            string moneda = cbMoneda.Text.ToLower();
            return moneda.Contains("sole") ? "S" : "D";
        }
        decimal getImporte(string monto, string moneda)
        {
            decimal _monto = decimal.Parse(monto);
            decimal tCambio = Program.TCSunat;
            string _moneda = getMoneda();

            if (_moneda == moneda)
                return _monto;
            else
            {
                return (
                    moneda.Equals("S") ? (_monto * tCambio)
                    : _monto / tCambio
                    );
            }

        }
        decimal getTotal(string monto)
        {
            decimal _monto = getImporte(monto, "S");
            decimal saldo = decimal.Parse(txSaldo.Text);
            decimal tCambio = Program.TCSunat;

            if (_monto <= saldo)
                return _monto;
            else
            {
                txVueltoSoles.Text = (_monto - saldo).ToString("#,##0.00");
                txVueltoDolares.Text = ((_monto - saldo) / tCambio).ToString("#,##0.00");
                return saldo;
            }
        }
        #endregion


        private void MostrarTotales()
        {

            var canasta = DataGridHelper.GetTotals(grdCanastilla, LlenarGrilla.Venta);
            // Saldo y Sumatoria
            txSaldo.Text = (decimal.Parse(txImporteGenerado.Text) - canasta.TotalPrice).ToString("#,##0.00");
            txTotalCanastilla.Text = canasta.TotalPrice.ToString("#,##0.00");
            // Cambio Soles/Dolares
            decimal cambio = canasta.PriceItem - decimal.Parse(txImporteGenerado.Text);
            txVueltoSoles.Text = (cambio >= 0 ? cambio.ToString("#,##0.00") : "0.00");
            txVueltoDolares.Text = (
                txVueltoSoles.Text == "0.00" ? "0.00"
                : (decimal.Parse(txVueltoSoles.Text) / Program.TCSunat).ToString("#,##0.00")
                );
        }
        public void LoadData(string idOrden, string fecha)
        {
            Tools.ComboBoxHelper.ComboBoxGeneral(cbBanco, "03", "Descripcion");
            Tools.ComboBoxHelper.ComboBoxGeneral(cbMoneda, "04", "Descripcion");

            using (UnitOfWork uow = new UnitOfWork())
            {
                DateTime fechas = DateTime.Parse(fecha);

                _orden = new OrdenVentaReport();
                _orden = uow.OrdenVenta.GetList(fechas, fechas
                    , new Entidad.OrdenVenta() { IdOrden = idOrden })
                    .ToList().First();

                var cl = uow.Clientes.GetById(_orden.IdCliente);
                lbCliente.Text = Autonomo.Class.Conversion.ConvertToTitleCase(cl.RazonSocial);
                lbIdentificacion.Text = cl.NumeroDocumento;

                var np = uow.Generals.GetPrecioNivels()
                    .FirstOrDefault(o => o.IdNivel.Equals(cl.IdNivelPrecio));

                lbNivelPrecio.Text = Autonomo.Class.Conversion.ConvertToTitleCase(np.Descripcion);
                lbVendedor.Text = Autonomo.Class.Conversion.ConvertToTitleCase(_orden.Vendedor);
                lbLineaCredito.Text = cl.LineaCredito.ToString("#,##0.00");

                lbDocumento.Text = _orden.IdOrden;
                lbFecha.Text = _orden.FechaRegistro.ToShortDateString();
                lbImporte.Text = _orden.ImporteParcial.ToString("#,##0.00");
                lbSunat.Text = Program.TCSunat.ToString();

                txImporteGenerado.Text = _orden.ImporteParcial.ToString("#,##0.00");
                txSaldo.Text = _orden.ImporteParcial.ToString("#,##0.00");


                lbNumComprobante.Text = uow.Venta.GetCorrelativoComprobante(new GetData().GetIdComprobante(comprobante));

            }
        }




        private void IniciarCampo(string formaText, bool bBanco,
            bool bMoneda, bool bOperacion, string operacionText)
        {
            txImporte.Clear();
            txOperacion.Clear();

            lbForma.Text = formaText;
            txImporte.Enabled = true;

            cbBanco.Enabled = bBanco;
            cbMoneda.Enabled = bMoneda;
            txOperacion.Enabled = bOperacion;
            txOperacion.Title = operacionText;
        }


        private void ManageMetodos()
        {
            switch (metodo)
            {
                case MetodoPago.Efectivo:
                    {
                        IniciarCampo("Pago - Efectivo", false, true, false, "Operación");
                        break;
                    }
                case MetodoPago.Cheque:
                    {
                        IniciarCampo("Pago - Cheque", true, true, true, "Ingrese el número de Cheque");
                        break;
                    }
                case MetodoPago.Tarjeta:
                    {
                        IniciarCampo("Pago - Tarjeta bancaria", true, true, false, "Operación");
                        break;
                    }
                case MetodoPago.Credito:
                    {
                        IniciarCampo("Pago - Línea de crédito", false, false, true, "Vigencia del crédito def. 30 días");
                        break;
                    }
                case MetodoPago.Cupon:
                    {
                        IniciarCampo("Pago - Cupón", false, false, true, "Ingrese el Código del Cupón");
                        break;
                    }
                case MetodoPago.Transferencia:
                    {
                        IniciarCampo("Pago - Transferencia", false, false, true, "ngrese el númeo de operación");
                        break;
                    }
            }
        }

        private void ManageComprobante()
        {
            //  Traer de SQL  el correlativo...
            string _title = "P  O  S  I  B  L  E      N  °      D  E      C  O  M  P  R  O  B  A  N  T  E";
            switch (comprobante)
            {
                case Comprobante.Ticket:
                    {
                        lbCapComprobante.Text = _title;
                        lbNumComprobante.Text = ""; // Desde SQL
                        break;
                    }
                case Comprobante.Boleta:
                    {
                        lbCapComprobante.Text = _title;
                        lbNumComprobante.Text = ""; // Desde SQL
                        break;
                    }
                case Comprobante.Factura:
                    {
                        lbCapComprobante.Text = _title;
                        lbNumComprobante.Text = ""; // Desde SQL
                        break;
                    }
            }
        }

        private void OpenModalCliente()
        {
            var f = new frmConsultaClienteNivel();
            f.Title.Text = "Listado de Clientes";
            Autonomo.Class.Fomulary.ShowModal(f, "", false);
            if (f.StateFormulary)
            {
                var clienteSelect = f.Cliente;
                // Actualizar precios // y refrescar cambios
                if (UpdatePrecios(clienteSelect.NivelPrecio, clienteSelect.IdCliente))
                    LoadData(lbDocumento.Text, lbFecha.Text);
            }
        }
        private bool UpdatePrecios(string idNivel, string idCliente)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                //(01: PRECIO GENERAL) --> [01][PRECIO GENERAL]
                string _idNivel = idNivel.Split(':')[0];

                string idOrden = lbDocumento.Text;
                var response = uow.Inventario.UpdateProductoPrecioporNivel(_idNivel, idOrden, idCliente);
                if (response > 0)
                    return true;
            }
            return false;
        }

        private void SaveChanges()
        {
            //validar los saldos...
            if (!ValidarImportePagado())
            {
                MensajeEmergente(EtiquetaMsg.Warning, "El importe pagado no coincide con el Importe total a pagar, verificar que los datos sean los correctos.");
                return;
            }
            using (UnitOfWork uow = new UnitOfWork())
            {
                string idVenta = uow.Venta.GetLastId();
                
                int response = 0;
                var gDat = new GetData();
                string _idComprobante = gDat.GetIdComprobante(comprobante);

                string correlativoNumComprobante = uow.Venta.GetCorrelativoComprobante(_idComprobante);
                foreach (DataGridViewRow row in grdCanastilla.Rows)
                {
                    response += uow.Venta.Crud(new Venta()
                    {
                        IdTienda=Program.Local,
                        IdVenta = idVenta,
                        IdOrden = lbDocumento.Text,
                        IdComprobante = _idComprobante,
                        NumComprobante = correlativoNumComprobante,
                        NivelPrecio = lbNivelPrecio.Text.ToUpper(),
                        ImporteTotal = decimal.Parse(txTotalCanastilla.Text),
                        Observacion = "opcional",
                        EditorUser = Program.Usuario
                    }, new VentaPago()
                    {
                        IdFormaPago = row.Cells["IdMetodo"].Value.ToString(),
                        MetodoPago = row.Cells["GMetodo"].Value.ToString(),
                        NumOperacion = row.Cells["NumOperacion"].Value.ToString(),
                        Moneda = row.Cells["TipoMoneda"].Value.ToString(),
                        Cambio = Program.TCSunat,
                        Importe = decimal.Parse(row.Cells["Total"].Value.ToString())
                    });
                }
                if (response > 0)
                {
                    frmVenta f = (frmVenta)this.ParentForm;
                    f.LoadData();
                    this.Close();
                }
            }
        }

        bool ValidarImportePagado()
        {
            decimal importeGenerado = decimal.Parse(txImporteGenerado.Text);
            decimal importePagado = decimal.Parse(txTotalCanastilla.Text);

            return importePagado == importeGenerado ? true : false;
        }

        void MensajeEmergente(EtiquetaMsg etiqueta, string mensaje)
        {
            lbMensaje.BackColor = etiqueta == EtiquetaMsg.Success ? Color.LightSeaGreen
                : Color.LightCoral;

            lbMensaje.Text = mensaje;
        }

        private void frmVentaModal_Load(object sender, EventArgs e)
        {
            ManageMetodos();
        }

        private void btnTransferencia_Click(object sender, EventArgs e)
        {
            metodo = MetodoPago.Transferencia;
            ManageMetodos();
        }

        private void btnCupon_Click(object sender, EventArgs e)
        {
            metodo = MetodoPago.Cupon;
            ManageMetodos();
        }

        private void btnCredito_Click(object sender, EventArgs e)
        {
            metodo = MetodoPago.Credito;
            ManageMetodos();
        }

        private void btnTarjeta_Click(object sender, EventArgs e)
        {
            metodo = MetodoPago.Tarjeta;
            ManageMetodos();
        }

        private void btnCheque_Click(object sender, EventArgs e)
        {
            metodo = MetodoPago.Cheque;
            ManageMetodos();
        }

        private void btnEfectivo_Click(object sender, EventArgs e)
        {
            metodo = MetodoPago.Efectivo;
            ManageMetodos();
        }

        private void txImporte_KeyPress(object sender, KeyPressEventArgs e)
        {
            Autonomo.Class.Validating.OnlyDecimal(e);
        }

        private void txOperacion_KeyPress(object sender, KeyPressEventArgs e)
        {

            switch (metodo)
            {
                case MetodoPago.Credito:
                    Autonomo.Class.Validating.OnlyNumber(e);
                    break;
                default:
                    Autonomo.Class.Validating.NoSpaces(e);
                    break;
            }
        }

        private void cbxInterno_CheckedChanged(object sender, EventArgs e)
        {
            comprobante = Comprobante.Ticket;
            ManageComprobante();
        }

        private void cbxFactura_CheckedChanged(object sender, EventArgs e)
        {
            comprobante = Comprobante.Factura;
            ManageComprobante();
        }

        private void cbxBoleta_CheckedChanged(object sender, EventArgs e)
        {
            comprobante = Comprobante.Boleta;
            ManageComprobante();
        }

        private void btnAgregar_Click(object sender, EventArgs e)
        {
            PopulateGrid();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveChanges();
        }

        private void grdCanastilla_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (grdCanastilla.SelectedRows.Count > 0)
            {
                //Verificamos en que columna se hace Click.
                var column = grdCanastilla.Columns[e.ColumnIndex];
                if (column.Name != "DeleteRow") return;

                int rowIndex = grdCanastilla.CurrentCell.RowIndex;
                DataGridHelper.RemoveRows(grdCanastilla, rowIndex);

                MostrarTotales();
            }
        }

        private void btnCliente_Click(object sender, EventArgs e)
        {
            OpenModalCliente();
        }
    }
}
