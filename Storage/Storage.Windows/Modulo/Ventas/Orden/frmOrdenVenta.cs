﻿using Storage.Datos;
using Storage.Entidad.Report;
using Storage.Windows.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Autonomo.CustomControls.HelperControl;

namespace Storage.Windows.Modulo.Ventas.Orden
{
    public partial class frmOrdenVenta : Autonomo.CustomTemplate.RegistryDouble
    {
        List<OrdenVentaReport> _ordenList;

        public frmOrdenVenta()
        {
            InitializeComponent();
        }

        private void LoadCombo()
        {
            Tools.ComboBoxHelper.ComboBoxTiendaTodos(cbTienda);
            //Validar el Rol del Usuario
            if (new Validation().EsVendedor())
            {
                cbTienda.SelectedValue = Program.Local;
                cbTienda.Enabled = false;
            }
        }

        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                string idTienda = cbTienda.SelectedValue.ToString();
                _ordenList = new List<OrdenVentaReport>();
                _ordenList = uow.OrdenVenta.GetList(dtDesde.Value, dtHasta.Value,
                    new Entidad.OrdenVenta() { IdTienda=idTienda}
                    ).OrderByDescending(o => o.FechaRegistro).ToList();


                grdData.DataSource = null;
                grdData.Refresh();

                FilterData();
            }
        }


        private void FilterData()
        {
            if (_ordenList != null && _ordenList.Count > 0)
            {
                var data = _ordenList
                    .Where(o => o.Cliente.ToLower().Contains(txFilter.Text.ToLower()));

                grdData.DataSource = data.ToList();
                grdData.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
        }

        private void GetDetail(string documento)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Inventario.GetDetalleOrden(documento, "S").ToList();
                grdDetalle.DataSource = element;
                grdDetalle.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
        }

        private void LoadModal(string title)
        {
            var f = new frmOrdenVentaModal();
            f.Title.Text = title;
            Autonomo.Class.Fomulary.ShowModal(f, "", false);
            if (f.StateFormulary)
            {
                LoadData();
            }
        }

        private void frmOrdenVenta_Load(object sender, EventArgs e)
        {
            dtDesde.Value = Autonomo.Class.Obtaining.GetFirstDayMonth();
            dtHasta.Value = DateTime.Now;
            ThemeStyle(Theme.White);
            LoadCombo();
            LoadData();
        }

        private void grdData_SelectionChanged(object sender, EventArgs e)
        {
            if (grdData.SelectedRows.Count > 0)
            {
                string _idOrden = grdData.CurrentRow.Cells["IdOrden"].Value.ToString();
                GetDetail(_idOrden);
            }
            else
            {
                grdDetalle.DataSource = null;
                grdDetalle.Refresh();
            }
        }

        private void txFilter_TextBoxChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        private void btnProcesar_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            LoadModal("Generar Nueva Orden de Venta");
        }
    }
}
