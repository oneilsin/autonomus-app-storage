﻿
namespace Storage.Windows.Modulo.Ventas.Orden
{
    partial class frmOrdenVentaModal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmOrdenVentaModal));
            this.panelItems = new System.Windows.Forms.Panel();
            this.flowCart = new System.Windows.Forms.FlowLayoutPanel();
            this.pnelBuscador = new System.Windows.Forms.Panel();
            this.cbTienda = new Autonomo.CustomControls.FlatComboBox();
            this.txFiltro = new Autonomo.CustomControls.FlatFindText();
            this.panelCanastilla = new System.Windows.Forms.Panel();
            this.panlTotals = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.lblMessageGrid = new System.Windows.Forms.Label();
            this.lbCantidad = new System.Windows.Forms.Label();
            this.lbUnitario = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbTotal = new System.Windows.Forms.Label();
            this.lblTotals = new System.Windows.Forms.Label();
            this.grdCanastilla = new Autonomo.CustomControls.CustomGrid();
            this.IdProduct = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Producto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.PrecioUnitario = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panelControles = new System.Windows.Forms.Panel();
            this.btnAlgo = new Autonomo.CustomControls.CustomButton();
            this.btnVenta = new Autonomo.CustomControls.CustomButton();
            this.btnGuardar = new Autonomo.CustomControls.CustomButton();
            this.txObservaciones = new Autonomo.CustomControls.FlatTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.txCliente = new Autonomo.CustomControls.FlatFindText();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            this.panelItems.SuspendLayout();
            this.pnelBuscador.SuspendLayout();
            this.panelCanastilla.SuspendLayout();
            this.panlTotals.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCanastilla)).BeginInit();
            this.panelControles.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Size = new System.Drawing.Size(905, 490);
            // 
            // Body
            // 
            this.Body.Controls.Add(this.panelCanastilla);
            this.Body.Controls.Add(this.panel1);
            this.Body.Controls.Add(this.panelControles);
            this.Body.Controls.Add(this.panelItems);
            this.Body.Size = new System.Drawing.Size(905, 439);
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 480);
            this.Footer.Size = new System.Drawing.Size(905, 10);
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(905, 41);
            // 
            // Title
            // 
            this.Title.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.Title.Size = new System.Drawing.Size(864, 41);
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Location = new System.Drawing.Point(84, 4);
            this.btnSave.Visible = false;
            // 
            // panelItems
            // 
            this.panelItems.Controls.Add(this.flowCart);
            this.panelItems.Controls.Add(this.pnelBuscador);
            this.panelItems.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelItems.Location = new System.Drawing.Point(0, 0);
            this.panelItems.Name = "panelItems";
            this.panelItems.Size = new System.Drawing.Size(385, 439);
            this.panelItems.TabIndex = 0;
            // 
            // flowCart
            // 
            this.flowCart.AutoScroll = true;
            this.flowCart.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowCart.Location = new System.Drawing.Point(0, 63);
            this.flowCart.Name = "flowCart";
            this.flowCart.Size = new System.Drawing.Size(385, 376);
            this.flowCart.TabIndex = 2;
            // 
            // pnelBuscador
            // 
            this.pnelBuscador.Controls.Add(this.cbTienda);
            this.pnelBuscador.Controls.Add(this.txFiltro);
            this.pnelBuscador.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnelBuscador.Location = new System.Drawing.Point(0, 0);
            this.pnelBuscador.Name = "pnelBuscador";
            this.pnelBuscador.Size = new System.Drawing.Size(385, 63);
            this.pnelBuscador.TabIndex = 1;
            // 
            // cbTienda
            // 
            this.cbTienda.BackColor = System.Drawing.Color.White;
            this.cbTienda.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbTienda.ColorLine = System.Drawing.Color.Gray;
            this.cbTienda.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbTienda.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbTienda.DisplayMember = "";
            this.cbTienda.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbTienda.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbTienda.Error = "";
            this.cbTienda.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbTienda.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbTienda.ImageIcon = null;
            this.cbTienda.Info = "";
            this.cbTienda.Location = new System.Drawing.Point(12, 6);
            this.cbTienda.MaterialStyle = false;
            this.cbTienda.Name = "cbTienda";
            this.cbTienda.Placeholder = "";
            this.cbTienda.SelectedIndex = -1;
            this.cbTienda.Size = new System.Drawing.Size(129, 58);
            this.cbTienda.SizeLine = 2;
            this.cbTienda.TabIndex = 1;
            this.cbTienda.Title = "-Select- Tienda";
            this.cbTienda.ValueMember = "";
            this.cbTienda.VisibleIcon = false;
            this.cbTienda.VisibleTitle = true;
            // 
            // txFiltro
            // 
            this.txFiltro.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txFiltro.BackColor = System.Drawing.Color.White;
            this.txFiltro.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txFiltro.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txFiltro.ColorLine = System.Drawing.Color.Gray;
            this.txFiltro.ColorText = System.Drawing.SystemColors.WindowText;
            this.txFiltro.ColorTitle = System.Drawing.Color.Gray;
            this.txFiltro.DockIcon = System.Windows.Forms.DockStyle.Right;
            this.txFiltro.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txFiltro.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txFiltro.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txFiltro.ImageIcon")));
            this.txFiltro.Location = new System.Drawing.Point(147, 6);
            this.txFiltro.MaterialStyle = true;
            this.txFiltro.MaxLength = 32767;
            this.txFiltro.MultiLineText = false;
            this.txFiltro.Name = "txFiltro";
            this.txFiltro.ObjectArray = null;
            this.txFiltro.PasswordChar = '\0';
            this.txFiltro.Placeholder = "Buscar por producto...";
            this.txFiltro.PlaceHolderHeight = 18;
            this.txFiltro.ReadOnly = false;
            this.txFiltro.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txFiltro.Size = new System.Drawing.Size(235, 44);
            this.txFiltro.SizeLine = 2;
            this.txFiltro.StringArray = null;
            this.txFiltro.TabIndex = 0;
            this.txFiltro.TextId = "";
            this.txFiltro.Title = "Buscar por producto...";
            this.txFiltro.VisibleIcon = true;
            this.txFiltro.VisibleTitle = false;
            this.txFiltro.KeyDown += new System.EventHandler<System.Windows.Forms.KeyEventArgs>(this.txFiltro_KeyDown);
            // 
            // panelCanastilla
            // 
            this.panelCanastilla.Controls.Add(this.panlTotals);
            this.panelCanastilla.Controls.Add(this.grdCanastilla);
            this.panelCanastilla.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelCanastilla.Location = new System.Drawing.Point(385, 64);
            this.panelCanastilla.Name = "panelCanastilla";
            this.panelCanastilla.Size = new System.Drawing.Size(520, 238);
            this.panelCanastilla.TabIndex = 0;
            // 
            // panlTotals
            // 
            this.panlTotals.Controls.Add(this.label2);
            this.panlTotals.Controls.Add(this.lblMessageGrid);
            this.panlTotals.Controls.Add(this.lbCantidad);
            this.panlTotals.Controls.Add(this.lbUnitario);
            this.panlTotals.Controls.Add(this.label1);
            this.panlTotals.Controls.Add(this.lbTotal);
            this.panlTotals.Controls.Add(this.lblTotals);
            this.panlTotals.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panlTotals.Location = new System.Drawing.Point(0, 178);
            this.panlTotals.Name = "panlTotals";
            this.panlTotals.Size = new System.Drawing.Size(520, 60);
            this.panlTotals.TabIndex = 8;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.Gray;
            this.label2.Location = new System.Drawing.Point(342, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(178, 14);
            this.label2.TabIndex = 19;
            this.label2.Text = "Doble Click para Retirar Item";
            // 
            // lblMessageGrid
            // 
            this.lblMessageGrid.BackColor = System.Drawing.Color.MediumSeaGreen;
            this.lblMessageGrid.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblMessageGrid.Font = new System.Drawing.Font("Verdana", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMessageGrid.ForeColor = System.Drawing.Color.White;
            this.lblMessageGrid.Location = new System.Drawing.Point(0, 41);
            this.lblMessageGrid.Name = "lblMessageGrid";
            this.lblMessageGrid.Size = new System.Drawing.Size(520, 19);
            this.lblMessageGrid.TabIndex = 18;
            this.lblMessageGrid.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbCantidad
            // 
            this.lbCantidad.Font = new System.Drawing.Font("Verdana", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCantidad.ForeColor = System.Drawing.Color.DarkGray;
            this.lbCantidad.Location = new System.Drawing.Point(299, 7);
            this.lbCantidad.Name = "lbCantidad";
            this.lbCantidad.Size = new System.Drawing.Size(70, 16);
            this.lbCantidad.TabIndex = 17;
            this.lbCantidad.Text = "0.00";
            this.lbCantidad.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lbUnitario
            // 
            this.lbUnitario.Font = new System.Drawing.Font("Verdana", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUnitario.ForeColor = System.Drawing.Color.DarkGray;
            this.lbUnitario.Location = new System.Drawing.Point(373, 7);
            this.lbUnitario.Name = "lbUnitario";
            this.lbUnitario.Size = new System.Drawing.Size(70, 16);
            this.lbUnitario.TabIndex = 17;
            this.lbUnitario.Text = "0.00";
            this.lbUnitario.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.Color.LightGray;
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Verdana", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.label1.Location = new System.Drawing.Point(0, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(520, 1);
            this.label1.TabIndex = 16;
            // 
            // lbTotal
            // 
            this.lbTotal.Font = new System.Drawing.Font("Verdana", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTotal.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lbTotal.Location = new System.Drawing.Point(447, 7);
            this.lbTotal.Name = "lbTotal";
            this.lbTotal.Size = new System.Drawing.Size(70, 16);
            this.lbTotal.TabIndex = 15;
            this.lbTotal.Text = "0.00";
            this.lbTotal.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblTotals
            // 
            this.lblTotals.AutoSize = true;
            this.lblTotals.Font = new System.Drawing.Font("Verdana", 9.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotals.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.lblTotals.Location = new System.Drawing.Point(6, 7);
            this.lblTotals.Name = "lblTotals";
            this.lblTotals.Size = new System.Drawing.Size(164, 16);
            this.lblTotals.TabIndex = 14;
            this.lblTotals.Text = "0, Items en canastilla";
            // 
            // grdCanastilla
            // 
            this.grdCanastilla.AllowUserToAddRows = false;
            this.grdCanastilla.AllowUserToDeleteRows = false;
            this.grdCanastilla.AllowUserToResizeRows = false;
            this.grdCanastilla.BackgroundColor = System.Drawing.Color.White;
            this.grdCanastilla.BodyFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdCanastilla.BodyForeColor = System.Drawing.SystemColors.ControlText;
            this.grdCanastilla.BodySelectColor = System.Drawing.SystemColors.Highlight;
            this.grdCanastilla.BodySelectForeColor = System.Drawing.SystemColors.HighlightText;
            this.grdCanastilla.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.grdCanastilla.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.None;
            this.grdCanastilla.CellStyleBackColor = System.Drawing.SystemColors.Window;
            this.grdCanastilla.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.grdCanastilla.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.grdCanastilla.ColumnHeadersHeight = 34;
            this.grdCanastilla.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.grdCanastilla.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.IdProduct,
            this.Producto,
            this.Cantidad,
            this.PrecioUnitario,
            this.Total});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.grdCanastilla.DefaultCellStyle = dataGridViewCellStyle3;
            this.grdCanastilla.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grdCanastilla.EnableBottomDown = false;
            this.grdCanastilla.EnableBottomLeft = false;
            this.grdCanastilla.EnableBottomRight = false;
            this.grdCanastilla.EnableBottomUp = false;
            this.grdCanastilla.EnableHeadersVisualStyles = false;
            this.grdCanastilla.HeaderColor = System.Drawing.SystemColors.Control;
            this.grdCanastilla.HeaderFont = new System.Drawing.Font("Verdana", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.grdCanastilla.HeaderForeColor = System.Drawing.SystemColors.WindowText;
            this.grdCanastilla.Location = new System.Drawing.Point(0, 0);
            this.grdCanastilla.MultiSelect = false;
            this.grdCanastilla.Name = "grdCanastilla";
            this.grdCanastilla.ReadOnly = true;
            this.grdCanastilla.RowHeadersVisible = false;
            this.grdCanastilla.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.grdCanastilla.Size = new System.Drawing.Size(520, 238);
            this.grdCanastilla.TabIndex = 7;
            this.grdCanastilla.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.grdCanastilla_CellContentDoubleClick);
            // 
            // IdProduct
            // 
            this.IdProduct.DataPropertyName = "IdProduct";
            this.IdProduct.HeaderText = "Id";
            this.IdProduct.Name = "IdProduct";
            this.IdProduct.ReadOnly = true;
            this.IdProduct.Visible = false;
            // 
            // Producto
            // 
            this.Producto.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Producto.DataPropertyName = "Producto";
            this.Producto.HeaderText = "Producto";
            this.Producto.Name = "Producto";
            this.Producto.ReadOnly = true;
            // 
            // Cantidad
            // 
            this.Cantidad.DataPropertyName = "Cantidad";
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Info;
            this.Cantidad.DefaultCellStyle = dataGridViewCellStyle2;
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            this.Cantidad.Width = 70;
            // 
            // PrecioUnitario
            // 
            this.PrecioUnitario.DataPropertyName = "PrecioUnitario";
            this.PrecioUnitario.HeaderText = "P.Unit.";
            this.PrecioUnitario.Name = "PrecioUnitario";
            this.PrecioUnitario.ReadOnly = true;
            this.PrecioUnitario.Width = 70;
            // 
            // Total
            // 
            this.Total.DataPropertyName = "Total";
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            this.Total.Width = 70;
            // 
            // panelControles
            // 
            this.panelControles.Controls.Add(this.btnAlgo);
            this.panelControles.Controls.Add(this.btnVenta);
            this.panelControles.Controls.Add(this.btnGuardar);
            this.panelControles.Controls.Add(this.txObservaciones);
            this.panelControles.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panelControles.Location = new System.Drawing.Point(385, 302);
            this.panelControles.Name = "panelControles";
            this.panelControles.Size = new System.Drawing.Size(520, 137);
            this.panelControles.TabIndex = 0;
            // 
            // btnAlgo
            // 
            this.btnAlgo.BackColor = System.Drawing.Color.SeaGreen;
            this.btnAlgo.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAlgo.FlatAppearance.BorderSize = 0;
            this.btnAlgo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAlgo.Font = new System.Drawing.Font("Verdana", 10F);
            this.btnAlgo.ForeColor = System.Drawing.Color.White;
            this.btnAlgo.Image = ((System.Drawing.Image)(resources.GetObject("btnAlgo.Image")));
            this.btnAlgo.Location = new System.Drawing.Point(23, 79);
            this.btnAlgo.Name = "btnAlgo";
            this.btnAlgo.Size = new System.Drawing.Size(137, 42);
            this.btnAlgo.TabIndex = 9;
            this.btnAlgo.Text = "Algo más";
            this.btnAlgo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAlgo.UseVisualStyleBackColor = false;
            // 
            // btnVenta
            // 
            this.btnVenta.BackColor = System.Drawing.Color.SeaGreen;
            this.btnVenta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVenta.FlatAppearance.BorderSize = 0;
            this.btnVenta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVenta.Font = new System.Drawing.Font("Verdana", 10F);
            this.btnVenta.ForeColor = System.Drawing.Color.White;
            this.btnVenta.Image = ((System.Drawing.Image)(resources.GetObject("btnVenta.Image")));
            this.btnVenta.Location = new System.Drawing.Point(166, 79);
            this.btnVenta.Name = "btnVenta";
            this.btnVenta.Size = new System.Drawing.Size(173, 42);
            this.btnVenta.TabIndex = 8;
            this.btnVenta.Text = "Generar Venta";
            this.btnVenta.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnVenta.UseVisualStyleBackColor = false;
            // 
            // btnGuardar
            // 
            this.btnGuardar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(120)))), ((int)(((byte)(212)))));
            this.btnGuardar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnGuardar.FlatAppearance.BorderSize = 0;
            this.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGuardar.Font = new System.Drawing.Font("Verdana", 10F);
            this.btnGuardar.ForeColor = System.Drawing.Color.White;
            this.btnGuardar.Image = ((System.Drawing.Image)(resources.GetObject("btnGuardar.Image")));
            this.btnGuardar.Location = new System.Drawing.Point(345, 79);
            this.btnGuardar.Name = "btnGuardar";
            this.btnGuardar.Size = new System.Drawing.Size(168, 42);
            this.btnGuardar.TabIndex = 7;
            this.btnGuardar.Text = "Guardar cambios";
            this.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnGuardar.UseVisualStyleBackColor = false;
            this.btnGuardar.Click += new System.EventHandler(this.btnGuardar_Click);
            // 
            // txObservaciones
            // 
            this.txObservaciones.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txObservaciones.BackColor = System.Drawing.Color.White;
            this.txObservaciones.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txObservaciones.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txObservaciones.ColorLine = System.Drawing.Color.Gray;
            this.txObservaciones.ColorText = System.Drawing.SystemColors.WindowText;
            this.txObservaciones.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.txObservaciones.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.txObservaciones.Error = "";
            this.txObservaciones.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txObservaciones.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txObservaciones.FormatLogin = false;
            this.txObservaciones.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txObservaciones.ImageIcon")));
            this.txObservaciones.Info = "";
            this.txObservaciones.Location = new System.Drawing.Point(6, 6);
            this.txObservaciones.MaterialStyle = false;
            this.txObservaciones.MaxLength = 32767;
            this.txObservaciones.MultiLineText = false;
            this.txObservaciones.Name = "txObservaciones";
            this.txObservaciones.PasswordChar = '\0';
            this.txObservaciones.Placeholder = "";
            this.txObservaciones.ReadOnly = false;
            this.txObservaciones.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txObservaciones.Size = new System.Drawing.Size(502, 58);
            this.txObservaciones.SizeLine = 2;
            this.txObservaciones.TabIndex = 6;
            this.txObservaciones.Title = "Observaciones";
            this.txObservaciones.VisibleIcon = true;
            this.txObservaciones.VisibleTitle = true;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.txCliente);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(385, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(520, 64);
            this.panel1.TabIndex = 1;
            // 
            // txCliente
            // 
            this.txCliente.AlignText = System.Windows.Forms.HorizontalAlignment.Left;
            this.txCliente.BackColor = System.Drawing.Color.White;
            this.txCliente.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            this.txCliente.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.txCliente.ColorLine = System.Drawing.Color.Gray;
            this.txCliente.ColorText = System.Drawing.SystemColors.WindowText;
            this.txCliente.ColorTitle = System.Drawing.Color.Gray;
            this.txCliente.DockIcon = System.Windows.Forms.DockStyle.Right;
            this.txCliente.FontText = new System.Drawing.Font("Verdana", 10F);
            this.txCliente.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.txCliente.ImageIcon = ((System.Drawing.Image)(resources.GetObject("txCliente.ImageIcon")));
            this.txCliente.Location = new System.Drawing.Point(6, 6);
            this.txCliente.MaterialStyle = true;
            this.txCliente.MaxLength = 32767;
            this.txCliente.MultiLineText = false;
            this.txCliente.Name = "txCliente";
            this.txCliente.ObjectArray = null;
            this.txCliente.PasswordChar = '\0';
            this.txCliente.Placeholder = "Click en el Icono o Enter para establecer el Cliente";
            this.txCliente.PlaceHolderHeight = 18;
            this.txCliente.ReadOnly = true;
            this.txCliente.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txCliente.Size = new System.Drawing.Size(502, 44);
            this.txCliente.SizeLine = 2;
            this.txCliente.StringArray = null;
            this.txCliente.TabIndex = 1;
            this.txCliente.TextId = "";
            this.txCliente.Title = "Click en el Icono o Enter para establecer el Cliente";
            this.txCliente.VisibleIcon = true;
            this.txCliente.VisibleTitle = false;
            this.txCliente.KeyDown += new System.EventHandler<System.Windows.Forms.KeyEventArgs>(this.txCliente_KeyDown);
            this.txCliente.ButtonClick += new System.EventHandler(this.txCliente_ButtonClick);
            // 
            // frmOrdenVentaModal
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(905, 490);
            this.Name = "frmOrdenVentaModal";
            this.Text = "frmOrdenVentaModal";
            this.Load += new System.EventHandler(this.frmOrdenVentaModal_Load);
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.panelItems.ResumeLayout(false);
            this.pnelBuscador.ResumeLayout(false);
            this.panelCanastilla.ResumeLayout(false);
            this.panlTotals.ResumeLayout(false);
            this.panlTotals.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.grdCanastilla)).EndInit();
            this.panelControles.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelItems;
        private System.Windows.Forms.Panel panelControles;
        private System.Windows.Forms.Panel panelCanastilla;
        private System.Windows.Forms.Panel pnelBuscador;
        private Autonomo.CustomControls.FlatFindText txFiltro;
        private System.Windows.Forms.FlowLayoutPanel flowCart;
        private Autonomo.CustomControls.FlatComboBox cbTienda;
        private Autonomo.CustomControls.CustomGrid grdCanastilla;
        private System.Windows.Forms.DataGridViewTextBoxColumn IdProduct;
        private System.Windows.Forms.DataGridViewTextBoxColumn Producto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn PrecioUnitario;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.Panel panlTotals;
        private System.Windows.Forms.Label lbCantidad;
        private System.Windows.Forms.Label lbUnitario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbTotal;
        private System.Windows.Forms.Label lblTotals;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblMessageGrid;
        private System.Windows.Forms.Panel panel1;
        private Autonomo.CustomControls.FlatFindText txCliente;
        private Autonomo.CustomControls.FlatTextBox txObservaciones;
        private Autonomo.CustomControls.CustomButton btnAlgo;
        private Autonomo.CustomControls.CustomButton btnVenta;
        private Autonomo.CustomControls.CustomButton btnGuardar;
    }
}