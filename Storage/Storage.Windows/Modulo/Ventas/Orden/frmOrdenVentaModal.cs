﻿using Autonomo.CustomControls;
using Storage.Datos;
using Storage.Entidad.Report;
using Storage.Windows.Tools;
using Storage.Windows.Tools.Formulary;
using Storage.Windows.Tools.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Ventas.Orden
{
    public partial class frmOrdenVentaModal : Autonomo.Object.Modal
    {
        ClienteNivelprecioReport _cliente;
        public frmOrdenVentaModal()
        {
            InitializeComponent();
        }

        private void LoadCombo()
        {
            Tools.ComboBoxHelper.ComboBoxTiendaTodos(cbTienda);
            //Validar el Rol del Usuario
            if (new Validation().EsVendedor())
            {
                cbTienda.SelectedValue = Program.Local;
                cbTienda.Enabled = false;
            }
        }

        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                string idTienda = cbTienda.SelectedValue.ToString();
                var stock = uow.Inventario.GetStockDetails(idTienda)
                    .Where(o => o.Producto.ToLower().Contains(txFiltro.Text.ToLower()))
                    .ToList();
                // SQL: stock --> Select * from stock where producto like'%VALOR A BUSCAR%';

                PopulateCart(stock);
            }
        }

        private void PopulateCart(List<StockDetail> stocks)
        {
            if (stocks != null && stocks.Count > 0)
            {
                flowCart.Controls.Clear();
                foreach (var item in stocks)
                {
                    CustomList cart = new CustomList()
                    {
                        IdProducto = item.IdProducto,
                        Titulo = item.Producto,
                        Category = item.Categoria,
                        Stock = item.StokFinal.ToString()
                    };

                    cart.Agregar.Click += (sender, args)
                    => PopulateGrid(
                        new CanastillaModels()
                        {
                            IdItem = item.IdProducto,
                            NameItem = item.Producto,
                            QuantityItem = cart.Cantidad,
                            PriceItem = item.PrecioBase,
                            TotalPrice = (cart.Cantidad * item.PrecioBase)
                        }, item.StokFinal);

                    flowCart.Controls.Add(cart);
                }
            }
        }

        private void PopulateGrid(CanastillaModels items, decimal stock)
        {
            if (items.QuantityItem > 0 && stock >= items.QuantityItem)
            {
                if (_cliente == null)
                {
                    lblMessageGrid.Text = "No se ha seleccionado los datos del Cliente....";
                    lblMessageGrid.BackColor = Color.Coral;
                    return;
                }


                var response = DataGridHelper.PopulateGrid(grdCanastilla,
                                Enumerables.LlenarGrilla.Orden, items);

                MostrarTotales();
                lblMessageGrid.Text = response.Mensaje;
                if (response.Estado)
                {
                    lblMessageGrid.BackColor = Color.SeaGreen;
                    return;
                }
                lblMessageGrid.BackColor = Color.Coral;
            }
        }

        private void MostrarTotales()
        {
            lblTotals.Text = $"{grdCanastilla.Rows.Count}, Items en canastilla";

            var canasta = DataGridHelper.GetTotals(grdCanastilla, Enumerables.LlenarGrilla.Orden);
            lbCantidad.Text = canasta.QuantityItem.ToString();
            lbUnitario.Text = canasta.PriceItem.ToString();
            lbTotal.Text = canasta.TotalPrice.ToString();
        }

        private void RemoverItem(DataGridViewCellEventArgs e)
        {
            if (grdCanastilla.SelectedRows.Count > 0)
            {
                //Verificamos en que columna se hace Click.
                // var column = grdCanastilla.Columns[e.ColumnIndex];
                //  if (column.Name == "Cantidad") return;

                int rowIndex = grdCanastilla.CurrentCell.RowIndex;
                DataGridHelper.RemoveRows(grdCanastilla, rowIndex);
                MostrarTotales();
            }
        }

        private void GetCliente()
        {
            var f = new frmConsultaClienteNivel();
            f.Title.Text = "Listado de Clientes";
            Autonomo.Class.Fomulary.ShowModal(f, "", false);
            if (f.StateFormulary)
            {
                _cliente = new ClienteNivelprecioReport();
                _cliente.IdCliente = f.Cliente.IdCliente;
                _cliente.RazonSocial = f.Cliente.RazonSocial;
                txCliente.Text = _cliente.RazonSocial;
            }
        }

        private void SaveChange()
        {
            if (!ValidateControls(out string controlsEmpty))
            {
                Tools.Mensaje.MessageBox(Enumerables.Mensajeria.Warning,
                    $"Campos inválidos: {controlsEmpty}");
                // mensaje de  campos invalidos:: MessageBox:Personalizado
                return;
            }

            try
            {
                using (UnitOfWork uow = new UnitOfWork())
                {
                    string GetCorrelative = uow.OrdenVenta.GetLastId();
                    int response = -1;

                    foreach (DataGridViewRow row in grdCanastilla.Rows)
                    {
                        string _idProducto = row.Cells["IdProduct"].Value.ToString();
                        decimal _cantidad = decimal.Parse(row.Cells["Cantidad"].Value.ToString());
                        decimal _unitario = decimal.Parse(row.Cells["PrecioUnitario"].Value.ToString());

                        response += uow.OrdenVenta.Crud(
                          new Entidad.OrdenVenta()
                          {
                              IdOrden = GetCorrelative,
                              IdTienda = cbTienda.SelectedValue.ToString(),
                              IdVendedor = Program.IdUsuario,
                              IdCliente = _cliente.IdCliente,
                              Observacion = txObservaciones.Text,
                              EditorUser = Program.Usuario
                          }, new Entidad.Inventario()
                          {
                              IdProducto = _idProducto,
                              Cantidad = _cantidad,
                              PrecioUnitario = _unitario
                          });
                    }

                    if (response > 0)
                    {
                        Tools.Mensaje.MessageBox(
                            Enumerables.Mensajeria.Succesful,
                            $"El proceso ha retornado {response} registros de manera correcta,");
                        base.Set();
                    }
                }
            }
            catch (Exception ex)
            {
                Tools.Mensaje.MessageBox(Enumerables.Mensajeria.Error, ex);
            }
        }

        private bool ValidateControls(out string controlsEmpty)
        {
            controlsEmpty = string.Empty;
            if (string.IsNullOrEmpty(cbTienda.SelectedValue.ToString()))
            {
                controlsEmpty = "Id de la tienda es nulo.";
                cbTienda.Error = "Este campo es requerido";
                return false;
            }
            cbTienda.Error = "";

            if (string.IsNullOrEmpty(txCliente.Text))
            {
                controlsEmpty = "Cliente no seleccionado.";
                return false;
            }

            if (!DataGridHelper.ValidateEmptyGrid(grdCanastilla))
            {
                controlsEmpty = "La canastilla no tiene valores";
                lblMessageGrid.Text = "No se ha encontrado detalles, seleccionar al menos uno.";
                lblMessageGrid.BackColor = Color.Coral;
                return false;
            }

            lblMessageGrid.Text = "";
            lblMessageGrid.BackColor = Color.SeaGreen;
            return true;
        }

        private void frmOrdenVentaModal_Load(object sender, EventArgs e)
        {
            Autonomo.Class.RoundObject.RoundButton(btnAlgo, 7, 7);
            Autonomo.Class.RoundObject.RoundButton(btnVenta, 7, 7);
            Autonomo.Class.RoundObject.RoundButton(btnGuardar, 7, 7);

            LoadCombo();
            LoadData();
        }

        private void txFiltro_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                LoadData();
        }

        private void grdCanastilla_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            RemoverItem(e);
        }

        private void txCliente_ButtonClick(object sender, EventArgs e)
        {
            GetCliente();
        }

        private void txCliente_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                GetCliente();
        }

        private void btnGuardar_Click(object sender, EventArgs e)
        {
            SaveChange();
        }
    }
}
