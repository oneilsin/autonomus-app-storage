﻿using Storage.Datos;
using Storage.Windows.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Modulo.Ventas.Stock
{
    public partial class frmStockDetalle : Autonomo.Object.Registry
    {
        public frmStockDetalle()
        {
            InitializeComponent();
        }

        void LoadCombo()
        {
            Tools.ComboBoxHelper.ComboBoxTiendaTodos(cbTienda);
            //Validar el Rol del Usuario
            if(new Validation().EsVendedor())
            {
                cbTienda.SelectedValue = Program.Local;
                cbTienda.Enabled = false;
            }
    
        }

        void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                string idTienda = cbTienda.SelectedValue.ToString();
                var element = uow.Inventario.GetStockDetails(idTienda);
               
                grdData.DataSource = element.ToList();
            }
        }


        private void frmStockDetalle_Load(object sender, EventArgs e)
        {
            LoadCombo();
            LoadData();           
        }

        private void btnProcesar_Click(object sender, EventArgs e)
        {
            LoadData();
        }

        private void grdData_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            DataGridHelper.ValidateLowerStock(grdData);
        }
    }
}
