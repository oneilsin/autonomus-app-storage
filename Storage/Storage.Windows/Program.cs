﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows
{
    static class Program
    {
        // PARA DESPUES_____ --> COOKIE
        public static string Usuario = "Administrador";
        public static string IdUsuario = "00005";
        public static string Local = "A0002";// Por mientras
        public static decimal TCSunat = 3.98m;
        public static decimal Igv = 0.18m;//18 %;
        public static string[] Role = new string[] { "vendedor", "algomas" };

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            var f = new Home.frmMenu();

            //   f.ShowInTaskbar=
            // f.TopMost = true;
            Application.Run(f);
        }
    }
}
