﻿using Storage.Datos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Windows.Tools
{
    public class ComboBoxHelper
    {
        public static void ComboBoxCategoria(Autonomo.CustomControls.FlatComboBox box)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Categorias.GetList()
                    .OrderBy(o => o.NombreCategoria);
                if (element != null && element.Count() > 0)
                {
                    box.DataSource = element.ToList();
                    box.DisplayMember = "NombreCategoria";
                    box.ValueMember = "IdCategoria";
                }
            }
        }
        public static void ComboBoxSubCategoria(Autonomo.CustomControls.FlatComboBox box, string idCategoria)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.SubCategorias.GetList()
                    .OrderBy(o => o.NombreSubCategoria);
                if (element != null && element.Count() > 0)
                {
                    //Lambda: element.Where(o=>o.IdCategoria.Equals(idCategoria)).ToList()
                    //SQL: select * from subCategoria where IdCategoria='@idCategoria'
                    box.DataSource = element.Where(o => o.IdCategoria.Equals(idCategoria)).ToList();
                    box.DisplayMember = "NombreSubCategoria";
                    box.ValueMember = "IdSubCategoria";
                }
            }
        }
        public static void ComboBoxMarca(Autonomo.CustomControls.FlatComboBox box)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Marcas.GetList()
                    .OrderBy(o => o.NombreMarca);
                if (element != null && element.Count() > 0)
                {
                    box.DataSource = element.ToList();
                    box.DisplayMember = "NombreMarca";
                    box.ValueMember = "IdMarca";
                }
            }
        }
        public static void ComboBoxTalla(Autonomo.CustomControls.FlatComboBox box)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Tallas.GetList()
                    .OrderBy(o => o.NombreTalla);
                if (element != null && element.Count() > 0)
                {
                    box.DataSource = element.ToList();
                    box.DisplayMember = "NombreTalla";
                    box.ValueMember = "IdTalla";
                }
            }
        }
        public static void ComboBoxFamilia(Autonomo.CustomControls.FlatComboBox box)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Familias.GetList()
                    .OrderBy(o => o.Descripcion);
                if (element != null && element.Count() > 0)
                {
                    box.DataSource = element.ToList();
                    box.DisplayMember = "Descripcion";
                    box.ValueMember = "IdFamilia";
                }
            }
        }

        public static void ComboBoxDepartamento(Autonomo.CustomControls.FlatComboBox box)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Generals.GetUbigeoList();
                var newList = (from o in element
                               orderby o.Departamento
                               select new
                               {
                                   IdDepartamento = o.IdDepartamento,
                                   Departamento = o.Departamento
                               }).Distinct();

                if (newList != null && newList.Count() > 0)
                {
                    box.DataSource = newList.ToList();
                    box.DisplayMember = "Departamento";
                    box.ValueMember = "IdDepartamento";
                }
            }
        }
        public static void ComboBoxProvincia(Autonomo.CustomControls.FlatComboBox box, string departamento)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Generals.GetUbigeoList();
                var newList = (from o in element
                               where o.IdDepartamento.Equals(departamento)
                               orderby o.Provincia
                               select new
                               {
                                   IdProvincia = o.IdProvincia,
                                   Provincia = o.Provincia
                               }).Distinct();

                if (newList != null && newList.Count() > 0)
                {
                    box.DataSource = newList.ToList();
                    box.DisplayMember = "Provincia";
                    box.ValueMember = "IdProvincia";
                }
            }
        }
        public static void ComboBoxDistrito(Autonomo.CustomControls.FlatComboBox box
            , string departamento, string provincia)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Generals.GetUbigeoList();
                var newList = (from o in element
                               where o.IdDepartamento.Equals(departamento)
                               && o.IdProvincia.Equals(provincia)
                               orderby o.Distrito
                               select new
                               {
                                   Ubigeo = o.Ubigeo,
                                   Distrito = o.Distrito
                               }).Distinct();

                if (newList != null && newList.Count() > 0)
                {
                    box.DataSource = newList.ToList();
                    box.DisplayMember = "Distrito";
                    box.ValueMember = "Ubigeo";
                }
            }
        }
        public static void ComboBoxProveedor(Autonomo.CustomControls.FlatComboBox box)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Proveedors.GetList()
                    .OrderBy(o => o.NombreProveedor);
                if (element != null && element.Count() > 0)
                {
                    box.DataSource = element.ToList();
                    box.DisplayMember = "NombreProveedor";
                    box.ValueMember = "IdProveedor";
                }
            }
        }

        public static void ComboBoxGeneral(Autonomo.CustomControls.FlatComboBox box, string numeroTabla, string columna)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Generals.GetGeneralList(numeroTabla)
                    .OrderBy(o => columna == "Descripcion" ? o.Descripcion : o.Abreviatura);
                if (element != null && element.Count() > 0)
                {
                    box.DataSource = element.ToList();
                    box.DisplayMember = columna;
                    box.ValueMember = "IdTabla";
                }
            }
        }
        public static void ComboBoxTipoMovimiento(Autonomo.CustomControls.FlatComboBox box)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Generals.GetTipoMovimientoList()
                    .OrderBy(o => o.Descripcion);
                if (element != null && element.Count() > 0)
                {
                    box.DataSource = element.ToList();
                    box.DisplayMember = "Descripcion";
                    box.ValueMember = "IdTipoMov";
                }
            }
        }
        public static void ComboBoxTipoMovimiento(Autonomo.CustomControls.FlatComboBox box, string flag)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Generals.GetTipoMovimientoList()
                    .Where(o=>o.Movimiento==flag) //Flag: es E/S  Abreviado
                    .OrderBy(o => o.Descripcion);
                if (element != null && element.Count() > 0)
                {
                    box.DataSource = element.ToList();
                    box.DisplayMember = "Descripcion";
                    box.ValueMember = "IdTipoMov";
                }
            }
        }
        public static void ComboBoxTrabajador(Autonomo.CustomControls.FlatComboBox box)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Clientes.GetList()
                    .Where(o => o.Rol.ToLower().Contains("empleado"))
                    .OrderBy(o => o.RazonSocial);
                if (element != null && element.Count() > 0)
                {
                    box.DataSource = element.ToList();
                    box.DisplayMember = "RazonSocial";
                    box.ValueMember = "IdCliente";
                }
            }
        }
        public static void ComboBoxTienda(Autonomo.CustomControls.FlatComboBox box)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Tiendas.GetList()
                    .OrderBy(o => o.NombreTienda).ToList();               
                if (element != null && element.Count() > 0)
                {
                    box.DataSource = element.ToList();
                    box.DisplayMember = "NombreTienda";
                    box.ValueMember = "IdTienda";
                }
            }
        }
        public static void ComboBoxTiendaTodos(Autonomo.CustomControls.FlatComboBox box)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Tiendas.GetList().ToList();
                element.Add(new Entidad.Report.TiendaReport() { IdTienda = "", NombreTienda = "--Mostrar todos--" });
                    
                if (element != null && element.Count() > 0)
                {
                    box.DataSource = element.OrderBy(o => o.IdTienda).ThenBy(o=>o.NombreTienda).ToList();
                    box.DisplayMember = "NombreTienda";
                    box.ValueMember = "IdTienda";
                }
            }
        }

        public static void ComboBoxNivelPrecio(Autonomo.CustomControls.FlatComboBox box)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Generals.GetPrecioNivels().ToList();

                if (element != null && element.Count() > 0)
                {
                    box.DataSource = element.OrderBy(o => o.Descripcion).ToList();
                    box.DisplayMember = "Descripcion";
                    box.ValueMember = "IdNivel";
                }
            }
        }


        public static void ComboBoxSexo(Autonomo.CustomControls.FlatComboBox box)
        {
            var s = new List<Sexo>();
            s.Add(new Sexo { Id = "H", Descripcion = "HOMBRE" });
            s.Add(new Sexo { Id = "M", Descripcion = "MUJER" });
            s.Add(new Sexo { Id = "E", Descripcion = "EMPRESA" });

            box.DataSource = s.OrderBy(o => o.Descripcion).ToList();
            box.DisplayMember = "Descripcion";
            box.ValueMember = "Id";
        }

        class Sexo
        {
            public string Id { get; set; } //--HME
            public string Descripcion { get; set; }
        }
    }
}
