﻿using Autonomo.CustomControls;
using Storage.Windows.Tools.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Storage.Windows.Tools.Enumerables;

namespace Storage.Windows.Tools
{
    public static class DataGridHelper
    {

        /// <summary>
        /// Método para validar si el item ya está en la canastilla
        /// </summary>
        /// <param name="idItem">Corresponde al ID del item que se está insertando a la canastilla.</param>
        /// <param name="grid">Corresponde a la Grilla "Canastilla".</param>
        /// <returns></returns>
        private static bool ValidateGrid(string idItem, CustomGrid grid)
        {
            bool returnValue = false;
            foreach (DataGridViewRow row in grid.Rows)
            {
                var _idItem = row.Cells[0].Value.ToString();
                if (_idItem == idItem)
                {
                    returnValue = true;
                    break;
                }
            }
            return returnValue;
        }

       
        /// <summary>
        /// Método para agregar items a la Grilla.
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="tipoModulo"></param>
        /// <param name="canastilla"></param>
        /// <returns></returns>
        public static CanastillaReturn PopulateGrid<T>(CustomGrid grid, LlenarGrilla tipoModulo, T canasta)
        {
            var returnValue = new CanastillaReturn();
            var pagos = new CanastillaPagoModels();
            var productos = new CanastillaModels();

            string unico = string.Empty;
            string nombre = string.Empty;
            if(tipoModulo== LlenarGrilla.Venta)
            {
                pagos = (CanastillaPagoModels)Convert.ChangeType(canasta, typeof(T));
                unico = pagos.IdUnico;
                nombre = pagos.Metodo;
            }
            else
            {
                productos = (CanastillaModels)Convert.ChangeType(canasta, typeof(T));
                unico = productos.IdItem;
                nombre = productos.NameItem;
            }

            if (!ValidateGrid(unico, grid))
            {
                switch (tipoModulo)
                {
                    case LlenarGrilla.Entrada:
                        {
                            grid.Rows.Add(productos.IdItem,
                                productos.NameItem,
                                productos.CategoryItem,
                                productos.PriceItem,
                                productos.QuantityItem
                                );
                            break;
                        }
                    case LlenarGrilla.Salida:
                        {

                            grid.Rows.Add(productos.IdItem,
                               productos.NameItem,
                               productos.QuantityItem
                               );
                            break;
                        }
                    case LlenarGrilla.Orden:
                        {
                            grid.Rows.Add(productos.IdItem,
                                productos.NameItem,
                                productos.QuantityItem,
                                productos.PriceItem,
                                productos.TotalPrice
                                );
                            break;
                        }
                    case LlenarGrilla.Venta:
                        {
                            grid.Rows.Add(pagos.IdUnico,
                                pagos.IdMetodo,
                                pagos.Metodo,
                                pagos.Operacion,
                                pagos.Moneda,
                                pagos.Soles.ToString("#,##0.00"),
                                pagos.Dolares.ToString("#,##0.00"),
                                pagos.Total.ToString("#,##0.00"),
                                "x"
                                );
                            break;
                        }
                }
                returnValue.Estado = true;
                returnValue.Mensaje = $"Item {nombre}, se ha adicionado correctamente.";
            }
            else
            {
                returnValue.Estado = false;
                returnValue.Mensaje = $"El Item {nombre}, ya existe en la canastilla.";
            }
            return returnValue;
        }

        /// <summary>
        /// Método para obtener los totales de Precios y Cantidades || sumatoria de pagos.
        /// </summary>
        /// <param name="grid">Corresponde a la grilla "Canastilla".</param>
        /// <param name="priceItem">Variable que almacena el total de los Precios.</param>
        /// <param name="quantityItem">Variable que almacena el total de las Cantidades.</param>
        public static CanastillaModels GetTotals(CustomGrid grid, LlenarGrilla tipoModulo)
        {
            var canasta = new CanastillaModels();
            decimal priceItem = 0m;
            decimal quantityItem = 0m;
            decimal totalPrice = 0m;
            switch (tipoModulo)
            {
                case LlenarGrilla.Entrada:
                    {
                        foreach (DataGridViewRow row in grid.Rows)
                        {
                            priceItem += decimal.Parse(row.Cells[3].Value.ToString());
                            quantityItem += decimal.Parse(row.Cells[4].Value.ToString());
                        }
                        break;
                    }
                case LlenarGrilla.Salida:
                    {
                        foreach (DataGridViewRow row in grid.Rows)
                        {
                            quantityItem += decimal.Parse(row.Cells["Cantidad"].Value.ToString());
                        }
                        break;
                    }
                case LlenarGrilla.Orden:
                    {
                        foreach (DataGridViewRow row in grid.Rows)
                        {
                            quantityItem += decimal.Parse(row.Cells["Cantidad"].Value.ToString());
                            priceItem += decimal.Parse(row.Cells["PrecioUnitario"].Value.ToString());
                            totalPrice += decimal.Parse(row.Cells["Total"].Value.ToString());
                        }
                        break;
                    }
                case LlenarGrilla.Venta:
                    {
                        foreach (DataGridViewRow row in grid.Rows)
                        {
                            priceItem += decimal.Parse(row.Cells["GSoles"].Value.ToString()); // Reff ->ImporteSoles
                            totalPrice += decimal.Parse(row.Cells["Total"].Value.ToString()); // Reff -> TotalSoles
                        }
                        break;
                    }
            }
            canasta.PriceItem = priceItem;
            canasta.QuantityItem = quantityItem;
            canasta.TotalPrice = totalPrice;
            return canasta;
        }

        /// <summary>
        /// Método para Eliminar un Item de la Grilla.
        /// </summary>
        /// <param name="grid"></param>
        /// <param name="index"></param>
        public static void RemoveRows(CustomGrid grid, int rowIndex)
        {
            grid.Rows.RemoveAt(rowIndex);
        }

        /// <summary>
        /// Método para validar si la grilla está vacia.
        /// </summary>
        /// <param name="grid"></param>
        /// <returns>Retorna True si la grilla contiene valores, caso contrario retorna False.</returns>
        public static bool ValidateEmptyGrid(CustomGrid grid)
        {
            return grid.Rows.Count > 0 ? true : false;
        }



        public static void ValidateLowerStock(CustomGrid grid)
        {
            if (grid.Rows.Count > 0)
            {
                foreach (DataGridViewRow row in grid.Rows)
                {
                    decimal _minimo = decimal.Parse(row.Cells["StokMinimo"].Value.ToString());
                    decimal _stock = decimal.Parse(row.Cells["StokFinal"].Value.ToString());
                    if (_stock <= _minimo && _stock > 0)
                    {
                        row.DefaultCellStyle.ForeColor = Color.Orange;
                        row.DefaultCellStyle.Font = new Font("Verdana", 10f, FontStyle.Bold);
                    }
                    if (_stock < 1)
                    {
                        row.DefaultCellStyle.ForeColor = Color.Red;
                        row.DefaultCellStyle.Font = new Font("Verdana", 10f, FontStyle.Bold);
                    }
                }
            }
        }
    }
}
