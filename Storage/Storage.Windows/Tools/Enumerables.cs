﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Windows.Tools
{
    public class Enumerables
    {
        /// <summary>
        /// Opciones de Entidades para las grillas.
        /// </summary>
        public enum LlenarGrilla
        {
            Entrada=0,
            Salida=1,
            Orden=2,
            Venta=3
        }

        public enum Mensajeria
        {
            Succesful,
            Warning,
            Error,
            YesNot
        }

        public enum MetodoPago
        {
            Efectivo,//
            Cheque,
            Tarjeta,//
            Credito,
            Cupon,
            Transferencia//
        }

        public enum Comprobante
        {
            Ticket,
            Boleta,
            Factura
        }
        public enum EtiquetaMsg
        {
            Success, Warning, Nothing
        }
    }
}
