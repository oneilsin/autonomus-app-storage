﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Tools.Formulary
{
    public partial class frmConsultaClienteNivel : Autonomo.Object.Query
    {
        // OJO:: Más adelante incorporar el Nivel de precio...
        public ClienteNivelprecioReport Cliente;
        List<ClienteNivelprecioReport> _cliente;
        public frmConsultaClienteNivel()
        {
            InitializeComponent();
        }

        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                _cliente = new List<ClienteNivelprecioReport>();
                _cliente = uow.Clientes.GetClienteNivelprecios().ToList();
                FilterData();
                ConfigGridView();
            }
        }

        private void FilterData()
        {
            if (_cliente != null && _cliente.Count > 0)
            {
                grdData.DataSource =  // Contains() ===>  Like'%%' SQL
                    _cliente.Where(o => o.RazonSocial.Contains(txConsulta.Text)).ToList();
            }
        }

        private void ConfigGridView()
        {
            if (grdData.Rows.Count > 0)
            {
                grdData.Columns["IdCliente"].Visible = false;         
                grdData.Columns["NumeroDocumento"].Visible = false;

                grdData.Columns["CodigoCliente"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                grdData.Columns["RazonSocial"].HeaderText = "Código";

                grdData.Columns["RazonSocial"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                grdData.Columns["RazonSocial"].HeaderText = "Nombre del cliente";
                
                // Pronto Nivel de precio
            }
        }

        private void SelectedData()
        {
            if (grdData.Rows.Count > 0)
            {
                DataGridViewRow row = grdData.CurrentRow;
                Cliente = new ClienteNivelprecioReport()
                {
                    IdCliente = row.Cells["IdCliente"].Value.ToString(),
                    CodigoCliente = row.Cells["CodigoCliente"].Value.ToString(),
                    RazonSocial = row.Cells["RazonSocial"].Value.ToString(),
                    NivelPrecio = row.Cells["NivelPrecio"].Value.ToString()
                };
            }
        }

        private void frmConsultaClienteNivel_Load(object sender, EventArgs e)
        {
            base.ConfigFormulary();
            LoadData();
            txConsulta.Focus();
        }

        private void frmConsultaClienteNivel_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectedData();
        }

        private void frmConsultaClienteNivel_CellContentKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SelectedData();
        }

        private void txConsulta_TextBoxChanged(object sender, EventArgs e)
        {
            FilterData();
        }
    }
}
