﻿
namespace Storage.Windows.Tools.Formulary
{
    partial class frmConsultaProducto
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Contenedor.SuspendLayout();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Size = new System.Drawing.Size(667, 343);
            // 
            // Body
            // 
            this.Body.Size = new System.Drawing.Size(667, 271);
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 312);
            this.Footer.Size = new System.Drawing.Size(667, 31);
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(667, 41);
            // 
            // Title
            // 
            this.Title.Size = new System.Drawing.Size(626, 41);
            // 
            // txConsulta
            // 
            this.txConsulta.Size = new System.Drawing.Size(667, 44);
            this.txConsulta.VisibleIcon = true;
            this.txConsulta.VisibleTitle = true;
            this.txConsulta.TextBoxChanged += new System.EventHandler(this.txConsulta_TextBoxChanged);
            // 
            // frmConsultaProducto
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(667, 343);
            this.Name = "frmConsultaProducto";
            this.Tag = "";
            this.Text = "frmConsultaProducto";
            this.CellContentDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.frmConsultaProducto_CellContentDoubleClick);
            this.CellContentKeyDown += new System.EventHandler<System.Windows.Forms.KeyEventArgs>(this.frmConsultaProducto_CellContentKeyDown);
            this.Load += new System.EventHandler(this.frmConsultaProducto_Load);
            this.Contenedor.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
    }
}