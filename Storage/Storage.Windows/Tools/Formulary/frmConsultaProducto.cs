﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Tools.Formulary
{
    public partial class frmConsultaProducto : Autonomo.Object.Query
    {
        public class ProductoView
        {
            public string IdProducto { get; set; }
            public string NombreProducto { get; set; }
            public string Categoria { get; set; }
            public string Marca { get; set; }
        }

        /// <summary>
        /// Este listado va a ser útil para los filtros en la grilla.
        /// </summary>
        List<ProductoView> _productoList;

        /// <summary>
        /// Este Objeto será el que almacenará los datos seleccionados de la grilla
        /// </summary>
        public ProductoView Producto;
        public frmConsultaProducto()
        {
            InitializeComponent();
        }
        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                var list = uow.Productos.GetList()
                    .OrderBy(o => o.NombreProducto);
                              
                _productoList = new List<ProductoView>();
                _productoList = list
                    .Select(obj => new ProductoView()
                    {
                        IdProducto = obj.IdProducto,
                        NombreProducto = obj.NombreProducto,
                        Categoria = obj.NombreCategoria,
                        Marca = obj.NombreMarca
                    }).ToList();

                FilterData();
            }
        }
        private void FilterData()
        {
            if (_productoList.Count > 0 && _productoList != null)
            {
                var data = _productoList.Where(o => o.NombreProducto.Contains(txConsulta.Text))
                    .ToList();
                grdData.DataSource = data;
                ConfigGridView();
            }
        }

        /// <summary>
        /// Método para configurar las columnas de la grilla
        /// </summary>
        private void ConfigGridView()
        {            
            grdData.Columns[0].Visible = false;
            grdData.Columns[1].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            grdData.Columns[1].HeaderText = "Nombre del producto";
            grdData.Columns[2].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
            grdData.Columns[3].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
        }

        /// <summary>
        /// Método para seleccionar los datos de la Grilla (DobleClick / Enter)
        /// </summary>
        private void SelectedData()
        {
            DataGridViewRow row = grdData.CurrentRow;
            Producto = new ProductoView()
            {
                IdProducto = row.Cells[0].Value.ToString(),
                NombreProducto = row.Cells[1].Value.ToString(),
                Categoria= row.Cells[2].Value.ToString()
            };
        }


        private void frmConsultaProducto_Load(object sender, EventArgs e)
        {
            base.ConfigFormulary();
            LoadData();
        }

        private void txConsulta_TextBoxChanged(object sender, EventArgs e)
        {
            FilterData();
        }

        private void frmConsultaProducto_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectedData();
        }

        private void frmConsultaProducto_CellContentKeyDown(object sender, KeyEventArgs e)
        {
            if(e.KeyCode==Keys.Enter)
                SelectedData();
        }
    }
}
