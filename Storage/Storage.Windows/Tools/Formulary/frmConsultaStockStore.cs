﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Tools.Formulary
{
    public partial class frmConsultaStockStore : Autonomo.Object.Query
    {
        public StockStore Items;
        List<StockStore> _stockList;
        string _idTienda;

        public frmConsultaStockStore(string idTienda)
        {
            InitializeComponent();
            _idTienda = idTienda;
        }

        private void LoadData()
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                _stockList = new List<StockStore>();
                _stockList = uow.Inventario.GetStockByStore(_idTienda).ToList();
                FilterData();
                ConfigGridView();

              //  var aaa = _stockList.Count();
            }
        }


        private void FilterData()
        {
            if(_stockList!=null && _stockList.Count > 0)
            {
                grdData.DataSource =  // Contains() ===>  Like'%%' SQL
                    _stockList.Where(o => o.NombreProducto.Contains(txConsulta.Text)).ToList();
            }
        }


        private void ConfigGridView()
        {
            if (grdData.Rows.Count > 0)
            {
                grdData.Columns["IdProducto"].Visible = false;
                grdData.Columns["NombreProducto"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
                grdData.Columns["NombreProducto"].HeaderText = "Nombre del producto";
                grdData.Columns["StokFinal"].AutoSizeMode = DataGridViewAutoSizeColumnMode.DisplayedCells;
                grdData.Columns["StokFinal"].HeaderText = "Stock";
            }           
        }

        private void SelectedData()
        {
            if (grdData.Rows.Count > 0)
            {
                DataGridViewRow row = grdData.CurrentRow;
                Items = new StockStore()
                {
                    IdProducto = row.Cells["IdProducto"].Value.ToString(),
                    NombreProducto = row.Cells["NombreProducto"].Value.ToString(),
                    StokFinal = decimal.Parse(row.Cells["StokFinal"].Value.ToString())
                };
            }
        }
        private void frmConsultaStockStore_Load(object sender, EventArgs e)
        {
            base.ConfigFormulary();
            LoadData();
            txConsulta.Focus();
        }

        private void frmConsultaStockStore_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            SelectedData();
        }

        private void frmConsultaStockStore_CellContentKeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                SelectedData();
        }

        private void txConsulta_TextBoxChanged(object sender, EventArgs e)
        {
            FilterData();
        }
    }
}
