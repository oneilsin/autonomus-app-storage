﻿namespace Storage.Windows.Tools.Formulary
{
    partial class frmUbigeo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmUbigeo));
            this.cbDepartamento = new Autonomo.CustomControls.FlatComboBox();
            this.cbProvincia = new Autonomo.CustomControls.FlatComboBox();
            this.cbDistrito = new Autonomo.CustomControls.FlatComboBox();
            this.lbUbigeo = new System.Windows.Forms.Label();
            this.Contenedor.SuspendLayout();
            this.Body.SuspendLayout();
            this.Footer.SuspendLayout();
            this.Header.SuspendLayout();
            this.SuspendLayout();
            // 
            // Contenedor
            // 
            this.Contenedor.Size = new System.Drawing.Size(278, 373);
            // 
            // Body
            // 
            this.Body.Controls.Add(this.lbUbigeo);
            this.Body.Controls.Add(this.cbDistrito);
            this.Body.Controls.Add(this.cbProvincia);
            this.Body.Controls.Add(this.cbDepartamento);
            this.Body.Size = new System.Drawing.Size(278, 280);
            this.Body.TabIndex = 0;
            this.Body.Paint += new System.Windows.Forms.PaintEventHandler(this.Body_Paint);
            // 
            // Footer
            // 
            this.Footer.Location = new System.Drawing.Point(0, 321);
            this.Footer.Size = new System.Drawing.Size(278, 52);
            // 
            // Header
            // 
            this.Header.Size = new System.Drawing.Size(278, 41);
            // 
            // Title
            // 
            this.Title.Size = new System.Drawing.Size(237, 41);
            this.Title.Text = "      Establecer Ubigeo";
            // 
            // btnSave
            // 
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.Size = new System.Drawing.Size(124, 36);
            this.btnSave.Text = "Establecer";
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // cbDepartamento
            // 
            this.cbDepartamento.BackColor = System.Drawing.Color.White;
            this.cbDepartamento.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbDepartamento.ColorLine = System.Drawing.Color.Gray;
            this.cbDepartamento.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbDepartamento.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbDepartamento.DisplayMember = "";
            this.cbDepartamento.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbDepartamento.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDepartamento.Error = "";
            this.cbDepartamento.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbDepartamento.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbDepartamento.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbDepartamento.ImageIcon")));
            this.cbDepartamento.Info = "";
            this.cbDepartamento.Location = new System.Drawing.Point(12, 15);
            this.cbDepartamento.MaterialStyle = true;
            this.cbDepartamento.Name = "cbDepartamento";
            this.cbDepartamento.Placeholder = "Seleccionar Departamento";
            this.cbDepartamento.SelectedIndex = -1;
            this.cbDepartamento.Size = new System.Drawing.Size(240, 58);
            this.cbDepartamento.SizeLine = 2;
            this.cbDepartamento.TabIndex = 0;
            this.cbDepartamento.Title = "Seleccionar Departamento";
            this.cbDepartamento.ValueMember = "";
            this.cbDepartamento.VisibleIcon = true;
            this.cbDepartamento.VisibleTitle = false;
            this.cbDepartamento.SelectedValueChanged += new System.EventHandler(this.cbDepartamento_SelectedValueChanged);
            // 
            // cbProvincia
            // 
            this.cbProvincia.BackColor = System.Drawing.Color.White;
            this.cbProvincia.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbProvincia.ColorLine = System.Drawing.Color.Gray;
            this.cbProvincia.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbProvincia.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbProvincia.DisplayMember = "";
            this.cbProvincia.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbProvincia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbProvincia.Error = "";
            this.cbProvincia.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbProvincia.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbProvincia.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbProvincia.ImageIcon")));
            this.cbProvincia.Info = "";
            this.cbProvincia.Location = new System.Drawing.Point(12, 79);
            this.cbProvincia.MaterialStyle = false;
            this.cbProvincia.Name = "cbProvincia";
            this.cbProvincia.Placeholder = "";
            this.cbProvincia.SelectedIndex = -1;
            this.cbProvincia.Size = new System.Drawing.Size(240, 58);
            this.cbProvincia.SizeLine = 2;
            this.cbProvincia.TabIndex = 1;
            this.cbProvincia.Title = "Seleccionar Provincia";
            this.cbProvincia.ValueMember = "";
            this.cbProvincia.VisibleIcon = true;
            this.cbProvincia.VisibleTitle = true;
            this.cbProvincia.SelectedValueChanged += new System.EventHandler(this.cbProvincia_SelectedValueChanged);
            // 
            // cbDistrito
            // 
            this.cbDistrito.BackColor = System.Drawing.Color.White;
            this.cbDistrito.ColorFocus = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.cbDistrito.ColorLine = System.Drawing.Color.Gray;
            this.cbDistrito.ColorText = System.Drawing.SystemColors.WindowText;
            this.cbDistrito.ColorTitle = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.cbDistrito.DisplayMember = "";
            this.cbDistrito.DockIcon = System.Windows.Forms.DockStyle.Left;
            this.cbDistrito.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDistrito.Error = "";
            this.cbDistrito.FontText = new System.Drawing.Font("Verdana", 10F);
            this.cbDistrito.FontTitle = new System.Drawing.Font("Verdana", 9F);
            this.cbDistrito.ImageIcon = ((System.Drawing.Image)(resources.GetObject("cbDistrito.ImageIcon")));
            this.cbDistrito.Info = "";
            this.cbDistrito.Location = new System.Drawing.Point(12, 160);
            this.cbDistrito.MaterialStyle = false;
            this.cbDistrito.Name = "cbDistrito";
            this.cbDistrito.Placeholder = "";
            this.cbDistrito.SelectedIndex = -1;
            this.cbDistrito.Size = new System.Drawing.Size(240, 58);
            this.cbDistrito.SizeLine = 2;
            this.cbDistrito.TabIndex = 2;
            this.cbDistrito.Title = "Seleccionar Distrito";
            this.cbDistrito.ValueMember = "";
            this.cbDistrito.VisibleIcon = true;
            this.cbDistrito.VisibleTitle = true;
            this.cbDistrito.SelectedValueChanged += new System.EventHandler(this.cbDistrito_SelectedValueChanged);
            // 
            // lbUbigeo
            // 
            this.lbUbigeo.Font = new System.Drawing.Font("Verdana", 20F);
            this.lbUbigeo.Location = new System.Drawing.Point(19, 221);
            this.lbUbigeo.Name = "lbUbigeo";
            this.lbUbigeo.Size = new System.Drawing.Size(240, 42);
            this.lbUbigeo.TabIndex = 3;
            this.lbUbigeo.Text = "...";
            this.lbUbigeo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmUbigeo
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(278, 373);
            this.Name = "frmUbigeo";
            this.Text = "frmUbigeo";
            this.Load += new System.EventHandler(this.frmUbigeo_Load);
            this.Contenedor.ResumeLayout(false);
            this.Body.ResumeLayout(false);
            this.Footer.ResumeLayout(false);
            this.Header.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Label lbUbigeo;
        private Autonomo.CustomControls.FlatComboBox cbDistrito;
        private Autonomo.CustomControls.FlatComboBox cbProvincia;
        private Autonomo.CustomControls.FlatComboBox cbDepartamento;
    }
}