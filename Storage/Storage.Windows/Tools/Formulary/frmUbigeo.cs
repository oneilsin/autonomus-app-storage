﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Storage.Windows.Tools.Formulary
{
    public partial class frmUbigeo : Autonomo.Object.Modal
    {
        public frmUbigeo()
        {
            InitializeComponent();
        }

        private void frmUbigeo_Load(object sender, EventArgs e)
        {
            base.ConfigFormulary();
            Tools.ComboBoxHelper.ComboBoxDepartamento(cbDepartamento);
        }

        private void cbDepartamento_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbDepartamento.Items.Count > 0)
            {
                string id = cbDepartamento.SelectedValue.ToString();
                Tools.ComboBoxHelper.ComboBoxProvincia(cbProvincia, id);
            }
        }

        private void cbProvincia_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbProvincia.Items.Count > 0)
            {
                string dpto = cbDepartamento.SelectedValue.ToString();
                string prov = cbProvincia.SelectedValue.ToString();
                Tools.ComboBoxHelper.ComboBoxDistrito(cbDistrito, dpto, prov);
            }
        }

        private void cbDistrito_SelectedValueChanged(object sender, EventArgs e)
        {
            if (cbDistrito.Items.Count > 0) 
            {
                string ubigeo = cbDistrito.SelectedValue.ToString();
                lbUbigeo.Text = ubigeo;
            }
        }

        private void Body_Paint(object sender, PaintEventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            base.Set();
        }
    }
}
