﻿using Storage.Datos;
using Storage.Entidad.Report;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Storage.Windows.Tools.Enumerables;

namespace Storage.Windows.Tools
{
    public class GetData
    {
        public GeneralReport GetGeneral(string id)
        {
            GeneralReport obj = new GeneralReport();
            using (UnitOfWork uow = new UnitOfWork())
            {
                var element = uow.Generals.GetGeneralList("02");
                if (element != null)
                {
                    obj = element.FirstOrDefault(o => o.IdTabla.Equals(id));
                }
            }
            return obj;
        }
       
        public List<GeneralReport> GetGeneralList(string idTabla)
        {
            using (UnitOfWork uow = new UnitOfWork())
            {
                return uow.Generals.GetGeneralList(idTabla).ToList();
            }
        }

       public GeneralReport GetGeneralIdByName(string idTabla, MetodoPago metodo)
        {
            var general = GetGeneralList(idTabla);
            string valor = metodo.ToString().ToLower();

            var resultado = from obj in general
                            where obj.Descripcion.ToLower().Contains(valor)
                            select obj;

            return resultado.FirstOrDefault();
        }

        public string GetIdMetodoPago(MetodoPago metodo)
        {
            string r = string.Empty;
            switch (metodo)
            {
                case MetodoPago.Efectivo:
                    r = "05001";
                    break;
                case MetodoPago.Cheque:
                    r = "05002";
                    break;
                case MetodoPago.Tarjeta:
                    r = "05003";
                    break;
                case MetodoPago.Credito:
                    r = "05004";
                    break;
                case MetodoPago.Cupon:
                    r = "05005";
                    break;
                case MetodoPago.Transferencia:
                    r = "05006";
                    break;
            }

            return r;
        }
        
        public string GetIdComprobante(Comprobante comprobante)
        {
            string r = string.Empty;
            switch (comprobante)
            {
                case Comprobante.Ticket:
                    r = "06001";
                    break;
                case Comprobante.Boleta:
                    r = "06002";
                    break;
                case Comprobante.Factura:
                    r = "06003";
                    break;
            }
            return r;
        }
    }
}
