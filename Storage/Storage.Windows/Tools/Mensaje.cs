﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Storage.Windows.Tools.Enumerables;

namespace Storage.Windows.Tools
{
    public class Mensaje
    {
        // eerror
        public static void MessageBox(Mensajeria typeMessage, Exception ex)
        {
            System.Windows.Forms.MessageBox.Show(ex.Message);
        }
        // Yes Not

        public static void MessageBox(Mensajeria typeMessage, string warning)
        {
            System.Windows.Forms.MessageBox.Show(warning);
        }
    }
}
