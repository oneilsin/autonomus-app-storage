﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Windows.Tools.Models
{
    public class CanastillaModels
    { 
        string idItem;
        string nameItem;
        string categoryItem;
        decimal priceItem;
        decimal quantityItem;
        decimal totalPrice;

        

        public string IdItem { get => idItem; set => idItem = value; }
        public string NameItem { get => nameItem; set => nameItem = value; }
        public string CategoryItem { get => categoryItem; set => categoryItem = value; }
        public decimal PriceItem { get => priceItem; set => priceItem = value; }
        public decimal QuantityItem { get => quantityItem; set => quantityItem = value; }
        public decimal TotalPrice { get => totalPrice; set => totalPrice = value; }
        
    }
}
