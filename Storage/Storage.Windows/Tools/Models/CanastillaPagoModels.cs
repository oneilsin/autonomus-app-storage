﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Windows.Tools.Models
{
    public class CanastillaPagoModels
    {
        private string idUnico;
        private string idMetodo;
        private string metodo;
        private string operacion;
        private string moneda;
        private decimal soles;
        private decimal dolares;
        private decimal total;

        public string IdUnico { get => idUnico; set => idUnico = value; }
        public string IdMetodo { get => idMetodo; set => idMetodo = value; }
        public string Metodo { get => metodo; set => metodo = value; }
        public string Operacion { get => operacion; set => operacion = value; }
        public string Moneda { get => moneda; set => moneda = value; }
        public decimal Soles { get => soles; set => soles = value; }
        public decimal Dolares { get => dolares; set => dolares = value; }
        public decimal Total { get => total; set => total = value; }
    }
}
