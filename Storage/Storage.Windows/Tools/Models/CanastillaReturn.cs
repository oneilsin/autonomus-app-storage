﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Windows.Tools.Models
{
    public class CanastillaReturn
    {
        public bool Estado { get; set; }
        public string Mensaje { get; set; }
    }
}
