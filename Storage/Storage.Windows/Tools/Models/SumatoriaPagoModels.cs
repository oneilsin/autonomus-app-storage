﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Windows.Tools.Models
{
    public class SumatoriaPagoModels
    {
        private decimal saldo;
        private decimal total;
        private decimal cambioSoles;
        private decimal cambioDolares;

        public decimal Saldo { get => saldo; set => saldo = value; }
        public decimal Total { get => total; set => total = value; }
        public decimal CambioSoles { get => cambioSoles; set => cambioSoles = value; }
        public decimal CambioDolares { get => cambioDolares; set => cambioDolares = value; }
    }
}
