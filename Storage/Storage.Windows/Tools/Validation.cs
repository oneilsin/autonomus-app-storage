﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Storage.Windows.Tools
{
    public class Validation
    {
        public bool EsVendedor()
        {
           return Program.Role.ToList().Exists(o => o.ToLower() == "vendedor");
        }

    }
}
