/*
	Si aún no han creado la base de datos, deberán crearla.
	CREATE DATABASE Storage
*/

USE [Storage]
GO
/****** Object:  Schema [Admin]    Script Date: 31/01/2022 8:41:24 PM ******/
CREATE SCHEMA [Admin]
GO
/****** Object:  Schema [General]    Script Date: 31/01/2022 8:41:24 PM ******/
CREATE SCHEMA [General]
GO
/****** Object:  Schema [Sunat]    Script Date: 31/01/2022 8:41:24 PM ******/
CREATE SCHEMA [Sunat]
GO
/****** Object:  UserDefinedFunction [dbo].[GetNumbers]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE FUNCTION [dbo].[GetNumbers] (@string varchar(max))
RETURNS varchar(max)
AS
BEGIN
    WHILE  @String like '%[^0-9]%'
    SET    @String = REPLACE(@String, SUBSTRING(@String, PATINDEX('%[^0-9]%', @String), 1), '')
    RETURN @String
END
GO
/****** Object:  Table [dbo].[aaa]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[aaa](
	[id] [varchar](5) NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bitacora]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bitacora](
	[IdBitacora] [bigint] IDENTITY(1,1) NOT NULL,
	[NombreTabla] [varchar](70) NOT NULL,
	[Accion] [varchar](20) NULL,
	[EditorUser] [varchar](20) NOT NULL,
	[FechaRegistro] [datetime] NOT NULL,
	[Evento] [varchar](max) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdBitacora] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[IdCliente] [varchar](5) NOT NULL,
	[IdTipoCliente] [varchar](5) NOT NULL,
	[IdTipoDocumento] [varchar](5) NOT NULL,
	[NumeroDocumento] [nvarchar](11) NOT NULL,
	[CodigoCliente] [nvarchar](15) NOT NULL,
	[Nombres] [varchar](80) NOT NULL,
	[Apellidos] [varchar](80) NOT NULL,
	[RazonSocial] [varchar](200) NOT NULL,
	[Sexo] [char](1) NOT NULL,
	[FechaNacimiento] [date] NOT NULL,
	[Rol] [varchar](max) NOT NULL,
	[Estado] [bit] NOT NULL,
	[EditorUser] [varchar](20) NOT NULL,
	[IdNivelPrecio] [varchar](2) NOT NULL,
	[LineaCredito] [decimal](10, 2) NOT NULL,
	[FlagCredito] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Entrada]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Entrada](
	[IdEntrada] [varchar](10) NOT NULL,
	[IdTienda] [varchar](5) NOT NULL,
	[FechaEntrada] [date] NOT NULL,
	[IdProveedor] [varchar](6) NOT NULL,
	[Documento] [varchar](16) NOT NULL,
	[EditorUser] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdEntrada] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Inventario]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Inventario](
	[IdInventario] [varchar](10) NOT NULL,
	[IdTienda] [varchar](5) NOT NULL,
	[FechaMovimiento] [date] NOT NULL,
	[IdTipoMov] [varchar](5) NOT NULL,
	[FlagMov] [char](1) NOT NULL,
	[IdProducto] [nvarchar](6) NOT NULL,
	[Cantidad] [decimal](10, 3) NOT NULL,
	[PrecioUnitario] [decimal](10, 2) NOT NULL,
	[DocMovimiento] [varchar](16) NOT NULL,
	[Condicion] [char](1) NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdInventario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrdenVenta]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrdenVenta](
	[IdOrden] [varchar](10) NOT NULL,
	[IdTienda] [varchar](5) NOT NULL,
	[FechaRegistro] [date] NOT NULL,
	[IdVendedor] [varchar](5) NOT NULL,
	[IdCliente] [varchar](5) NOT NULL,
	[Situacion] [varchar](1) NOT NULL,
	[FechaDespacho] [date] NULL,
	[IdDespachador] [varchar](5) NULL,
	[ImporteParcial] [decimal](10, 2) NOT NULL,
	[Observacion] [varchar](200) NOT NULL,
	[EditorUser] [varchar](20) NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdOrden] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Producto]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producto](
	[IdProducto] [nvarchar](6) NOT NULL,
	[IdCategoria] [nvarchar](5) NOT NULL,
	[IdSubCategoria] [nvarchar](5) NOT NULL,
	[IdMarca] [nvarchar](6) NOT NULL,
	[IdTalla] [nvarchar](4) NOT NULL,
	[IdFamilia] [nvarchar](6) NOT NULL,
	[NombreProducto] [varchar](100) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Genero] [char](1) NOT NULL,
	[EnlaceWeb] [varchar](max) NULL,
	[Sku] [varchar](100) NULL,
	[StokMinimo] [decimal](10, 3) NOT NULL,
	[PrecioBase] [decimal](10, 2) NOT NULL,
	[Estado] [bit] NOT NULL,
	[EditorUser] [varchar](20) NOT NULL,
	[CodigoBarra] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Salida]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salida](
	[IdSalida] [varchar](10) NOT NULL,
	[IdTienda] [varchar](5) NOT NULL,
	[FechaSalida] [date] NOT NULL,
	[Documento] [varchar](16) NOT NULL,
	[EditorUser] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdSalida] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Stock]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Stock](
	[IdTienda] [varchar](5) NOT NULL,
	[IdProducto] [nvarchar](6) NOT NULL,
	[NombreProducto] [varchar](100) NOT NULL,
	[PrecioUnitario] [decimal](10, 2) NOT NULL,
	[StokMinimo] [decimal](10, 3) NOT NULL,
	[StokFinal] [decimal](10, 3) NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Apellido] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Telefono] [varchar](20) NULL,
	[UserName] [varchar](20) NULL,
	[Password] [varchar](max) NULL,
PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Venta]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Venta](
	[IdVenta] [varchar](10) NOT NULL,
	[IdTienda] [varchar](5) NOT NULL,
	[IdOrden] [varchar](10) NOT NULL,
	[FechaRegistrro] [date] NOT NULL,
	[IdComprobante] [varchar](5) NOT NULL,
	[NumComprobante] [varchar](15) NOT NULL,
	[NivelPrecio] [varchar](50) NOT NULL,
	[ImporteTotal] [decimal](10, 2) NOT NULL,
	[Observacion] [varchar](200) NOT NULL,
	[Facturado] [bit] NOT NULL,
	[FechaFacturado] [date] NULL,
	[IdClienteFinal] [varchar](5) NULL,
	[CorrelativoVenta] [varchar](12) NOT NULL,
	[CorrelativoBoleta] [varchar](12) NOT NULL,
	[CorrelativoFactura] [varchar](12) NOT NULL,
	[ComprobanteInterno] [varchar](15) NOT NULL,
	[Anulado] [bit] NOT NULL,
	[MotivoAnulacion] [varchar](200) NULL,
	[Estado] [bit] NOT NULL,
	[EditorUser] [varchar](20) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdVenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VentaPago]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VentaPago](
	[IdPago] [varchar](10) NOT NULL,
	[IdVenta] [varchar](10) NOT NULL,
	[IdFormaPago] [varchar](5) NOT NULL,
	[MetodoPago] [varchar](100) NOT NULL,
	[NumOperacion] [varchar](20) NOT NULL,
	[Moneda] [varchar](1) NOT NULL,
	[Cambio] [decimal](10, 3) NOT NULL,
	[Importe] [decimal](10, 2) NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPago] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[Categoria]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[Categoria](
	[IdCategoria] [nvarchar](5) NOT NULL,
	[NombreCategoria] [varchar](50) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Prioridad] [int] NOT NULL,
	[Estado] [bit] NOT NULL,
	[EditorUser] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdCategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[Departamento]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[Departamento](
	[IdDepartamento] [nvarchar](2) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDepartamento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[Distrito]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[Distrito](
	[IdDistrito] [nvarchar](6) NOT NULL,
	[IdDepartamento] [nvarchar](2) NOT NULL,
	[IdProvincia] [nvarchar](4) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdDistrito] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[Familia]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[Familia](
	[IdFamilia] [nvarchar](6) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Estado] [bit] NOT NULL,
	[EditorUser] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdFamilia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[Marca]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[Marca](
	[IdMarca] [nvarchar](6) NOT NULL,
	[NombreMarca] [varchar](50) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Prioridad] [int] NOT NULL,
	[IdProveedor] [nvarchar](6) NOT NULL,
	[Estado] [bit] NOT NULL,
	[EditorUser] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdMarca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[PrecioNivel]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[PrecioNivel](
	[IdNivel] [varchar](2) NOT NULL,
	[Descripcion] [varchar](50) NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdNivel] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[PrecioProducto]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[PrecioProducto](
	[IdPrecio] [varchar](6) NOT NULL,
	[IdProducto] [nvarchar](6) NOT NULL,
	[IdNivel] [varchar](2) NOT NULL,
	[Precio] [decimal](10, 2) NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdPrecio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[Proveedor]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[Proveedor](
	[IdProveedor] [nvarchar](6) NOT NULL,
	[NombreProveedor] [varchar](60) NOT NULL,
	[Ubigeo] [nvarchar](6) NOT NULL,
	[Direccion] [varchar](250) NULL,
	[Urbanizacion] [varchar](100) NULL,
	[TelefFijo] [nvarchar](10) NULL,
	[TelefMovil] [nvarchar](13) NULL,
	[Email] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
	[EditorUser] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdProveedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[Provincia]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[Provincia](
	[IdProvincia] [nvarchar](4) NOT NULL,
	[IdDepartamento] [nvarchar](2) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdProvincia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[SubCategoria]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[SubCategoria](
	[IdSubCategoria] [nvarchar](5) NOT NULL,
	[NombreSubCategoria] [varchar](50) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IdCategoria] [nvarchar](5) NOT NULL,
	[Estado] [bit] NOT NULL,
	[EditorUser] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdSubCategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[Tabla]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[Tabla](
	[NumeroTabla] [varchar](2) NOT NULL,
	[Correlativo] [varchar](3) NOT NULL,
	[IdTabla] [varchar](5) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Abreviatura] [varchar](30) NOT NULL,
	[Valor] [int] NOT NULL,
	[Dimension] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdTabla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[Talla]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[Talla](
	[IdTalla] [nvarchar](4) NOT NULL,
	[NombreTalla] [varchar](10) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[Estado] [bit] NOT NULL,
	[EditorUser] [varchar](20) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdTalla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[Tienda]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[Tienda](
	[IdTienda] [varchar](5) NOT NULL,
	[NombreTienda] [varchar](80) NOT NULL,
	[Descripcion] [varchar](100) NULL,
	[IdResponsable] [bigint] NOT NULL,
	[Consigna] [bit] NOT NULL,
	[EditorUser] [varchar](20) NOT NULL,
	[Estado] [bit] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdTienda] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [General].[TipoMovimiento]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [General].[TipoMovimiento](
	[IdTipoMov] [varchar](5) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Movimiento] [char](1) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[IdTipoMov] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Bitacora] ON 

INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (1, N'General.Categoria', N'Insert', N'Prueba', CAST(N'2021-01-27T23:11:29.230' AS DateTime), N'<inserted IdCategoria="C0001" NombreCategoria="PRUEBA 001" Descripcion="DESCRIPCION PRUEBA 001" Prioridad="1" Estado="1" EditorUser="Prueba"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (2, N'General.Categoria', N'Insert', N'Prueba', CAST(N'2021-01-27T23:11:45.700' AS DateTime), N'<inserted IdCategoria="C0002" NombreCategoria="PRUEBA0002" Descripcion="DESC 002" Prioridad="2" Estado="1" EditorUser="Prueba"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (3, N'General.Categoria', N'Update', N'Administrador', CAST(N'2021-01-27T23:33:58.907' AS DateTime), N'<inserted IdCategoria="C0002" NombreCategoria="PRUEBA --- 002" Descripcion="DESC 002" Prioridad="2" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (4, N'General.Categoria', N'Insert', N'Administrador', CAST(N'2021-01-27T23:44:07.170' AS DateTime), N'<inserted IdCategoria="C0003" NombreCategoria="CATEGROIAS 003" Descripcion="DESCRIPCIN 003" Prioridad="5" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (5, N'General.SubCategoria', N'Insert', N'Administrador', CAST(N'2021-01-28T23:28:18.327' AS DateTime), N'<inserted IdSubCategoria="SC001" NombreSubCategoria="SUB PRUEBA 002" Descripcion="DESCERIPCION P002" IdCategoria="C0002" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (6, N'General.SubCategoria', N'Insert', N'Administrador', CAST(N'2021-01-28T23:28:30.443' AS DateTime), N'<inserted IdSubCategoria="SC002" NombreSubCategoria="SUB P 0001" Descripcion="DES 000" IdCategoria="C0001" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (7, N'General.SubCategoria', N'Update', N'Administrador', CAST(N'2021-01-28T23:29:07.150' AS DateTime), N'<inserted IdSubCategoria="SC002" NombreSubCategoria="SUB P 0001 - EDIT" Descripcion="DES 000 - EDIT" IdCategoria="C0001" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (8, N'General.SubCategoria', N'Update', N'Administrador', CAST(N'2021-01-28T23:29:31.190' AS DateTime), N'<inserted IdSubCategoria="SC001" NombreSubCategoria="SUB PRUEBA 002 -EDITAFDO" Descripcion="DESCERIPCION P002" IdCategoria="C0002" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (9, N'General.Familia', N'Insert', N'Administrador', CAST(N'2021-01-31T00:04:09.223' AS DateTime), N'<inserted IdFamilia="000001" Descripcion="NUEVA FAMILAI DE PRODUCTO" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (10, N'General.Familia', N'Insert', N'Administrador', CAST(N'2021-01-31T00:04:14.533' AS DateTime), N'<inserted IdFamilia="000002" Descripcion="ANTIGUA FAMILIA" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (11, N'General.Familia', N'Insert', N'Administrador', CAST(N'2021-01-31T00:04:21.440' AS DateTime), N'<inserted IdFamilia="000003" Descripcion="FAMILIAR 001" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (12, N'General.Familia', N'Update', N'Administrador', CAST(N'2021-01-31T00:05:29.587' AS DateTime), N'<inserted IdFamilia="000003" Descripcion="FAMILIAR  EDITADO" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (13, N'General.Talla', N'Insert', N'Administrador', CAST(N'2021-01-31T00:07:58.090' AS DateTime), N'<inserted IdTalla="T001" NombreTalla="XS" Descripcion="EXTRA SAMLL" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (14, N'General.Talla', N'Insert', N'Administrador', CAST(N'2021-01-31T00:08:03.263' AS DateTime), N'<inserted IdTalla="T002" NombreTalla="L" Descripcion="LARGE" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (15, N'General.Talla', N'Insert', N'Administrador', CAST(N'2021-01-31T00:08:08.470' AS DateTime), N'<inserted IdTalla="T003" NombreTalla="XL" Descripcion="EXTRA ALRGE" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (16, N'General.Talla', N'Insert', N'Administrador', CAST(N'2021-01-31T00:08:16.493' AS DateTime), N'<inserted IdTalla="T004" NombreTalla="M" Descripcion="MEDIUM" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (17, N'General.Talla', N'Insert', N'Administrador', CAST(N'2021-01-31T00:08:21.757' AS DateTime), N'<inserted IdTalla="T005" NombreTalla="SADAS" Descripcion="ASDASDASD" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (18, N'General.Talla', N'Update', N'Administrador', CAST(N'2021-01-31T00:08:33.450' AS DateTime), N'<inserted IdTalla="T005" NombreTalla="S" Descripcion="SMALL" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (19, N'General.Proveedor', N'Insert', N'Administrador', CAST(N'2021-01-31T00:10:02.620' AS DateTime), N'<inserted IdProveedor="000001" NombreProveedor="JUAN CARLOS PEREZ" Ubigeo="150107" Direcion="CALLE LOS PERDIDOS, 546" Urbanizacion="NONE" TelefFijo="988462222" TelefMovil="232322323" Email="JCAPEREZ@MAIL.COLM" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (20, N'General.Proveedor', N'Insert', N'Administrador', CAST(N'2021-01-31T00:11:09.050' AS DateTime), N'<inserted IdProveedor="000002" NombreProveedor="JUAN PERES FLORES" Ubigeo="140303" Direcion="ASDASD" Urbanizacion="ASD" TelefFijo="95656565" TelefMovil="2112121" Email="jperes@mail.com" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (21, N'General.Proveedor', N'Update', N'Administrador', CAST(N'2021-01-31T00:11:45.987' AS DateTime), N'<inserted IdProveedor="000002" NombreProveedor="RICARDO SANCHEZ" Ubigeo="140303" Direcion="AV. MIGUEL GRAUD 456" Urbanizacion="LAS LOMAS" TelefFijo="95656565" TelefMovil="2112121" Email="jperes@mail.com" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (22, N'General.Marca', N'Insert', N'Administrador', CAST(N'2021-01-31T00:19:37.037' AS DateTime), N'<inserted IdMarca="M00001" NombreMarca="SPORT CENTRE" Descripcion="MARCAD EPORTIVA" Prioridad="2" IdProveedor="000001" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (23, N'General.Marca', N'Insert', N'Administrador', CAST(N'2021-01-31T00:19:56.147' AS DateTime), N'<inserted IdMarca="M00002" NombreMarca="JJPK" Descripcion="MARCA DE  FUTBOL CCCC" Prioridad="6" IdProveedor="000002" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (24, N'General.Marca', N'Update', N'Administrador', CAST(N'2021-01-31T00:20:11.100' AS DateTime), N'<inserted IdMarca="M00002" NombreMarca="JJPK - EDITED" Descripcion="MARCA DE  FUTBOL CCCC" Prioridad="6" IdProveedor="000002" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (25, N'General.Categoria', N'Update', N'Administrador', CAST(N'2021-02-03T19:31:20.557' AS DateTime), N'<inserted IdCategoria="C0001" NombreCategoria="PRUEBA 001" Descripcion="DESCRIPCION PRUEBA 001" Prioridad="1" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (26, N'General.SubCategoria', N'Insert', N'Administrador', CAST(N'2021-03-21T02:36:07.357' AS DateTime), N'<inserted IdSubCategoria="SC003" NombreSubCategoria="3ERA SUB CATEG" Descripcion="DDDD" IdCategoria="C0003" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (27, N'General.SubCategoria', N'Insert', N'Administrador', CAST(N'2021-03-21T02:36:21.713' AS DateTime), N'<inserted IdSubCategoria="SC004" NombreSubCategoria="CATEG 3ERA PRUEBA" Descripcion="XDD" IdCategoria="C0003" Estado="1" EditorUser="Administrador"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (28, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T02:05:05.240' AS DateTime), N'<inserted IdInventario="2021000001" FechaMovimiento="2021-05-07" IdTipoMov="00008" FlagMov="E" IdProducto="P00001" Cantidad="100.000" PrecioUnitario="35.50" DocMovimiento="INIT-0001" EditorUser="Admin"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (29, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T02:06:40.617' AS DateTime), N'<inserted IdInventario="2021000002" FechaMovimiento="2021-05-07" IdTipoMov="00008" FlagMov="E" IdProducto="P00002" Cantidad="100.000" PrecioUnitario="22.50" DocMovimiento="INIT-0001" EditorUser="Admin"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (30, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T02:07:22.220' AS DateTime), N'<inserted IdInventario="2021000003" FechaMovimiento="2021-05-07" IdTipoMov="00001" FlagMov="S" IdProducto="P00002" Cantidad="2.000" PrecioUnitario="22.50" DocMovimiento="BV-0011111" EditorUser="Admin"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (31, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T02:07:58.710' AS DateTime), N'<inserted IdInventario="2021000004" FechaMovimiento="2021-05-07" IdTipoMov="00001" FlagMov="S" IdProducto="P00002" Cantidad="7.000" PrecioUnitario="22.50" DocMovimiento="BV-0011111" EditorUser="Admin"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (32, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T21:25:08.470' AS DateTime), N'<inserted IdInventario="2021000005" FechaMovimiento="2021-05-07" IdTipoMov="00001" FlagMov="S" IdProducto="P00002" Cantidad="1.000" PrecioUnitario="22.50" DocMovimiento="BV-0012222" EditorUser="Admin" Condicion="P"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (33, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T23:32:37.107' AS DateTime), N'<inserted IdInventario="2021000001" IdTienda="A0001" FechaMovimiento="2021-05-07" IdTipoMov="00008" FlagMov="E" IdProducto="P00002" Cantidad="100.000" PrecioUnitario="22.50" DocMovimiento="INIT-0001" EditorUser="Admin" Condicion="C"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (34, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T23:32:44.417' AS DateTime), N'<inserted IdInventario="2021000002" IdTienda="A0002" FechaMovimiento="2021-05-07" IdTipoMov="00008" FlagMov="E" IdProducto="P00002" Cantidad="100.000" PrecioUnitario="22.50" DocMovimiento="INIT-0001" EditorUser="Admin" Condicion="C"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (35, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T23:33:46.077' AS DateTime), N'<inserted IdInventario="2021000003" IdTienda="A0001" FechaMovimiento="2021-05-07" IdTipoMov="00008" FlagMov="E" IdProducto="P00001" Cantidad="100.000" PrecioUnitario="22.50" DocMovimiento="INIT-0001" EditorUser="Admin" Condicion="C"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (36, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T23:33:50.883' AS DateTime), N'<inserted IdInventario="2021000004" IdTienda="A0002" FechaMovimiento="2021-05-07" IdTipoMov="00008" FlagMov="E" IdProducto="P00001" Cantidad="100.000" PrecioUnitario="22.50" DocMovimiento="INIT-0001" EditorUser="Admin" Condicion="C"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (37, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T23:34:03.527' AS DateTime), N'<inserted IdInventario="2021000005" IdTienda="A0001" FechaMovimiento="2021-05-07" IdTipoMov="00001" FlagMov="S" IdProducto="P00002" Cantidad="1.000" PrecioUnitario="22.50" DocMovimiento="BV-0012222" EditorUser="Admin" Condicion="P"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (38, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T23:34:11.337' AS DateTime), N'<inserted IdInventario="2021000006" IdTienda="A0002" FechaMovimiento="2021-05-07" IdTipoMov="00001" FlagMov="S" IdProducto="P00002" Cantidad="3.000" PrecioUnitario="22.50" DocMovimiento="BV-0012222" EditorUser="Admin" Condicion="P"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (39, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T23:45:29.407' AS DateTime), N'<inserted IdInventario="2021000007" IdTienda="A0001" FechaMovimiento="2021-05-07" IdTipoMov="00008" FlagMov="E" IdProducto="P00001" Cantidad="20.000" PrecioUnitario="22.50" DocMovimiento="INIT-0001" EditorUser="Admin" Condicion="C"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (40, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T23:45:37.397' AS DateTime), N'<inserted IdInventario="2021000008" IdTienda="A0002" FechaMovimiento="2021-05-07" IdTipoMov="00008" FlagMov="E" IdProducto="P00001" Cantidad="30.000" PrecioUnitario="22.50" DocMovimiento="INIT-0001" EditorUser="Admin" Condicion="C"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (41, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T23:45:47.883' AS DateTime), N'<inserted IdInventario="2021000009" IdTienda="A0002" FechaMovimiento="2021-05-07" IdTipoMov="00008" FlagMov="E" IdProducto="P00002" Cantidad="10.000" PrecioUnitario="22.50" DocMovimiento="INIT-0001" EditorUser="Admin" Condicion="C"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (42, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-07T23:45:56.067' AS DateTime), N'<inserted IdInventario="2021000010" IdTienda="A0001" FechaMovimiento="2021-05-07" IdTipoMov="00008" FlagMov="E" IdProducto="P00002" Cantidad="35.000" PrecioUnitario="22.50" DocMovimiento="INIT-0001" EditorUser="Admin" Condicion="C"/>')
INSERT [dbo].[Bitacora] ([IdBitacora], [NombreTabla], [Accion], [EditorUser], [FechaRegistro], [Evento]) VALUES (43, N'Inventario', N'Insert', N'Admin', CAST(N'2021-05-08T00:19:54.680' AS DateTime), N'<inserted IdInventario="2021000011" IdTienda="A0002" FechaMovimiento="2021-05-08" IdTipoMov="00001" FlagMov="S" IdProducto="P00002" Cantidad="1.000" PrecioUnitario="22.50" DocMovimiento="BV-0012222" EditorUser="Admin" Condicion="P"/>')
SET IDENTITY_INSERT [dbo].[Bitacora] OFF
GO
INSERT [dbo].[Cliente] ([IdCliente], [IdTipoCliente], [IdTipoDocumento], [NumeroDocumento], [CodigoCliente], [Nombres], [Apellidos], [RazonSocial], [Sexo], [FechaNacimiento], [Rol], [Estado], [EditorUser], [IdNivelPrecio], [LineaCredito], [FlagCredito]) VALUES (N'00001', N'01002', N'02002', N'20122212121', N'CD0020122212121', N'LAS FLROES HNOS', N'', N'LAS FLROES HNOS ', N'E', CAST(N'2020-01-30' AS Date), N'Cliente,', 1, N'Administrador', N'01', CAST(500.00 AS Decimal(10, 2)), 1)
INSERT [dbo].[Cliente] ([IdCliente], [IdTipoCliente], [IdTipoDocumento], [NumeroDocumento], [CodigoCliente], [Nombres], [Apellidos], [RazonSocial], [Sexo], [FechaNacimiento], [Rol], [Estado], [EditorUser], [IdNivelPrecio], [LineaCredito], [FlagCredito]) VALUES (N'00002', N'01001', N'02001', N'12121212', N'CD0000012121212', N'MARIA ', N'FAJARDO', N'MARIA  FAJARDO', N'M', CAST(N'2021-03-22' AS Date), N'Cliente,Proveedor,', 1, N'Administrador', N'01', CAST(500.00 AS Decimal(10, 2)), 1)
INSERT [dbo].[Cliente] ([IdCliente], [IdTipoCliente], [IdTipoDocumento], [NumeroDocumento], [CodigoCliente], [Nombres], [Apellidos], [RazonSocial], [Sexo], [FechaNacimiento], [Rol], [Estado], [EditorUser], [IdNivelPrecio], [LineaCredito], [FlagCredito]) VALUES (N'00003', N'01001', N'02001', N'41212121', N'CD0000041212121', N'LUIS ALBERTO', N'MARTINEZ NEGRETE', N'LUIS ALBERTO MARTINEZ NEGRETE', N'H', CAST(N'2021-03-22' AS Date), N'Proveedor,', 1, N'Administrador', N'', CAST(0.00 AS Decimal(10, 2)), 0)
INSERT [dbo].[Cliente] ([IdCliente], [IdTipoCliente], [IdTipoDocumento], [NumeroDocumento], [CodigoCliente], [Nombres], [Apellidos], [RazonSocial], [Sexo], [FechaNacimiento], [Rol], [Estado], [EditorUser], [IdNivelPrecio], [LineaCredito], [FlagCredito]) VALUES (N'00004', N'01002', N'02003', N'121212422', N'CD0000121212422', N'ASD ASD DA', N'ASDASD', N'ASD ASD DA ASDASD', N'H', CAST(N'2021-03-22' AS Date), N'Cliente,', 1, N'Administrador', N'01', CAST(0.00 AS Decimal(10, 2)), 0)
INSERT [dbo].[Cliente] ([IdCliente], [IdTipoCliente], [IdTipoDocumento], [NumeroDocumento], [CodigoCliente], [Nombres], [Apellidos], [RazonSocial], [Sexo], [FechaNacimiento], [Rol], [Estado], [EditorUser], [IdNivelPrecio], [LineaCredito], [FlagCredito]) VALUES (N'00005', N'01001', N'02001', N'51231112', N'CD0000051231112', N'RICARDO', N'PEREZ', N'RICARDO PEREZ', N'H', CAST(N'2021-03-22' AS Date), N'Empleado,', 1, N'Administrador', N'', CAST(0.00 AS Decimal(10, 2)), 0)
INSERT [dbo].[Cliente] ([IdCliente], [IdTipoCliente], [IdTipoDocumento], [NumeroDocumento], [CodigoCliente], [Nombres], [Apellidos], [RazonSocial], [Sexo], [FechaNacimiento], [Rol], [Estado], [EditorUser], [IdNivelPrecio], [LineaCredito], [FlagCredito]) VALUES (N'00006', N'01001', N'02003', N'665656565', N'CD0000665656565', N'FERNANDA', N'LLANOS UPDATE', N'FERNANDA LLANOS UPDATE', N'M', CAST(N'2021-03-22' AS Date), N'Cliente,Empleado,', 1, N'Administrador', N'03', CAST(500.00 AS Decimal(10, 2)), 1)
INSERT [dbo].[Cliente] ([IdCliente], [IdTipoCliente], [IdTipoDocumento], [NumeroDocumento], [CodigoCliente], [Nombres], [Apellidos], [RazonSocial], [Sexo], [FechaNacimiento], [Rol], [Estado], [EditorUser], [IdNivelPrecio], [LineaCredito], [FlagCredito]) VALUES (N'00007', N'01001', N'02001', N'42515121', N'CD0000042515121', N'GUILLERMO', N'CORIA MENDEZ', N'GUILLERMO CORIA MENDEZ', N'H', CAST(N'2021-03-22' AS Date), N'Cliente,', 1, N'Administrador', N'01', CAST(500.00 AS Decimal(10, 2)), 1)
INSERT [dbo].[Cliente] ([IdCliente], [IdTipoCliente], [IdTipoDocumento], [NumeroDocumento], [CodigoCliente], [Nombres], [Apellidos], [RazonSocial], [Sexo], [FechaNacimiento], [Rol], [Estado], [EditorUser], [IdNivelPrecio], [LineaCredito], [FlagCredito]) VALUES (N'00008', N'01001', N'02001', N'01234567', N'CD0000001234567', N'CLIENTE GENERAL', N'.', N'CLIENTE GENERAL .', N'H', CAST(N'2021-03-22' AS Date), N'Cliente,', 1, N'Administrador', N'01', CAST(0.00 AS Decimal(10, 2)), 0)
GO
INSERT [dbo].[Entrada] ([IdEntrada], [IdTienda], [FechaEntrada], [IdProveedor], [Documento], [EditorUser]) VALUES (N'2021000001', N'A0002', CAST(N'2021-07-03' AS Date), N'000001', N'DOC-001', N'Administrador')
INSERT [dbo].[Entrada] ([IdEntrada], [IdTienda], [FechaEntrada], [IdProveedor], [Documento], [EditorUser]) VALUES (N'2021000002', N'A0002', CAST(N'2021-07-03' AS Date), N'000001', N'E', N'Administrador')
INSERT [dbo].[Entrada] ([IdEntrada], [IdTienda], [FechaEntrada], [IdProveedor], [Documento], [EditorUser]) VALUES (N'2021000003', N'A0002', CAST(N'2021-07-03' AS Date), N'000001', N'DOOC 2231', N'Administrador')
INSERT [dbo].[Entrada] ([IdEntrada], [IdTienda], [FechaEntrada], [IdProveedor], [Documento], [EditorUser]) VALUES (N'2021000004', N'A0001', CAST(N'2021-07-04' AS Date), N'000001', N'STOC-0001', N'Administrador')
INSERT [dbo].[Entrada] ([IdEntrada], [IdTienda], [FechaEntrada], [IdProveedor], [Documento], [EditorUser]) VALUES (N'2021000005', N'A0002', CAST(N'2021-10-19' AS Date), N'000001', N'321312312', N'Administrador')
GO
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000001', N'A0002', CAST(N'2021-07-03' AS Date), N'00008', N'E', N'P00002', CAST(100.000 AS Decimal(10, 3)), CAST(0.00 AS Decimal(10, 2)), N'2021000001', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000002', N'A0002', CAST(N'2021-07-03' AS Date), N'00008', N'E', N'P00001', CAST(100.000 AS Decimal(10, 3)), CAST(0.00 AS Decimal(10, 2)), N'2021000001', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000003', N'A0002', CAST(N'2021-07-03' AS Date), N'00008', N'E', N'P00004', CAST(100.000 AS Decimal(10, 3)), CAST(0.00 AS Decimal(10, 2)), N'2021000001', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000004', N'A0002', CAST(N'2021-07-03' AS Date), N'00008', N'E', N'P00003', CAST(100.000 AS Decimal(10, 3)), CAST(0.00 AS Decimal(10, 2)), N'2021000001', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000005', N'A0002', CAST(N'2021-07-03' AS Date), N'00006', N'S', N'P00001', CAST(5.000 AS Decimal(10, 3)), CAST(0.00 AS Decimal(10, 2)), N'2021000001', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000006', N'A0002', CAST(N'2021-07-03' AS Date), N'00004', N'E', N'P00004', CAST(15.000 AS Decimal(10, 3)), CAST(0.00 AS Decimal(10, 2)), N'2021000002', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000007', N'A0002', CAST(N'2021-07-03' AS Date), N'00006', N'S', N'P00001', CAST(90.000 AS Decimal(10, 3)), CAST(0.00 AS Decimal(10, 2)), N'2021000002', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000008', N'A0002', CAST(N'2021-07-03' AS Date), N'00006', N'S', N'P00001', CAST(5.000 AS Decimal(10, 3)), CAST(0.00 AS Decimal(10, 2)), N'2021000003', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000009', N'A0002', CAST(N'2021-07-03' AS Date), N'00003', N'E', N'P00001', CAST(50.000 AS Decimal(10, 3)), CAST(0.00 AS Decimal(10, 2)), N'2021000003', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000010', N'A0001', CAST(N'2021-07-04' AS Date), N'00008', N'E', N'P00001', CAST(100.000 AS Decimal(10, 3)), CAST(0.00 AS Decimal(10, 2)), N'2021000004', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000011', N'A0002', CAST(N'2021-07-04' AS Date), N'00006', N'S', N'P00001', CAST(45.000 AS Decimal(10, 3)), CAST(0.00 AS Decimal(10, 2)), N'2021000004', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000012', N'A0002', CAST(N'2021-07-23' AS Date), N'00001', N'S', N'P00003', CAST(1.000 AS Decimal(10, 3)), CAST(75.00 AS Decimal(10, 2)), N'2021000001', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000013', N'A0002', CAST(N'2021-07-23' AS Date), N'00001', N'S', N'P00002', CAST(3.000 AS Decimal(10, 3)), CAST(22.50 AS Decimal(10, 2)), N'2021000001', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000014', N'A0002', CAST(N'2021-07-23' AS Date), N'00001', N'S', N'P00004', CAST(3.000 AS Decimal(10, 3)), CAST(21.00 AS Decimal(10, 2)), N'2021000002', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000015', N'A0002', CAST(N'2021-07-23' AS Date), N'00001', N'S', N'P00002', CAST(1.000 AS Decimal(10, 3)), CAST(16.50 AS Decimal(10, 2)), N'2021000002', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000016', N'A0002', CAST(N'2021-07-23' AS Date), N'00001', N'S', N'P00003', CAST(1.000 AS Decimal(10, 3)), CAST(68.00 AS Decimal(10, 2)), N'2021000002', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000017', N'A0002', CAST(N'2021-07-23' AS Date), N'00001', N'S', N'P00001', CAST(1.000 AS Decimal(10, 3)), CAST(31.50 AS Decimal(10, 2)), N'2021000002', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000018', N'A0001', CAST(N'2021-07-23' AS Date), N'00001', N'S', N'P00001', CAST(4.000 AS Decimal(10, 3)), CAST(35.50 AS Decimal(10, 2)), N'2021000003', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000019', N'A0002', CAST(N'2021-07-25' AS Date), N'00001', N'S', N'P00004', CAST(30.000 AS Decimal(10, 3)), CAST(21.00 AS Decimal(10, 2)), N'2021000004', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000020', N'A0002', CAST(N'2021-07-25' AS Date), N'00001', N'S', N'P00003', CAST(20.000 AS Decimal(10, 3)), CAST(68.00 AS Decimal(10, 2)), N'2021000004', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000021', N'A0002', CAST(N'2021-07-25' AS Date), N'00001', N'S', N'P00002', CAST(15.000 AS Decimal(10, 3)), CAST(16.50 AS Decimal(10, 2)), N'2021000004', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000022', N'A0002', CAST(N'2021-08-30' AS Date), N'00001', N'S', N'P00002', CAST(3.000 AS Decimal(10, 3)), CAST(16.50 AS Decimal(10, 2)), N'2021000005', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000023', N'A0002', CAST(N'2021-08-30' AS Date), N'00001', N'S', N'P00003', CAST(2.000 AS Decimal(10, 3)), CAST(68.00 AS Decimal(10, 2)), N'2021000005', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000024', N'A0002', CAST(N'2021-08-30' AS Date), N'00001', N'S', N'P00004', CAST(5.000 AS Decimal(10, 3)), CAST(21.00 AS Decimal(10, 2)), N'2021000005', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000025', N'A0002', CAST(N'2021-09-07' AS Date), N'00001', N'S', N'P00002', CAST(3.000 AS Decimal(10, 3)), CAST(22.50 AS Decimal(10, 2)), N'2021000006', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000026', N'A0002', CAST(N'2021-09-07' AS Date), N'00001', N'S', N'P00003', CAST(3.000 AS Decimal(10, 3)), CAST(75.00 AS Decimal(10, 2)), N'2021000006', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000027', N'A0002', CAST(N'2021-09-07' AS Date), N'00001', N'S', N'P00003', CAST(1.000 AS Decimal(10, 3)), CAST(75.00 AS Decimal(10, 2)), N'2021000007', N'P', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000028', N'A0002', CAST(N'2021-09-07' AS Date), N'00001', N'S', N'P00004', CAST(1.000 AS Decimal(10, 3)), CAST(25.00 AS Decimal(10, 2)), N'2021000007', N'P', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2021000029', N'A0002', CAST(N'2021-10-19' AS Date), N'00020', N'E', N'P00005', CAST(1.000 AS Decimal(10, 3)), CAST(1.00 AS Decimal(10, 2)), N'2021000005', N'C', 1)
INSERT [dbo].[Inventario] ([IdInventario], [IdTienda], [FechaMovimiento], [IdTipoMov], [FlagMov], [IdProducto], [Cantidad], [PrecioUnitario], [DocMovimiento], [Condicion], [Estado]) VALUES (N'2022000001', N'A0002', CAST(N'2022-01-03' AS Date), N'00001', N'S', N'P00001', CAST(2.000 AS Decimal(10, 3)), CAST(35.50 AS Decimal(10, 2)), N'2022000001', N'P', 1)
GO
INSERT [dbo].[OrdenVenta] ([IdOrden], [IdTienda], [FechaRegistro], [IdVendedor], [IdCliente], [Situacion], [FechaDespacho], [IdDespachador], [ImporteParcial], [Observacion], [EditorUser], [Estado]) VALUES (N'2021000001', N'A0002', CAST(N'2021-07-23' AS Date), N'00005', N'00002', N'V', NULL, NULL, CAST(142.50 AS Decimal(10, 2)), N'PRIMERA VENTA. ', N'Administrador', 1)
INSERT [dbo].[OrdenVenta] ([IdOrden], [IdTienda], [FechaRegistro], [IdVendedor], [IdCliente], [Situacion], [FechaDespacho], [IdDespachador], [ImporteParcial], [Observacion], [EditorUser], [Estado]) VALUES (N'2021000002', N'A0002', CAST(N'2021-07-23' AS Date), N'00005', N'00006', N'V', NULL, NULL, CAST(179.00 AS Decimal(10, 2)), N'NADA', N'Administrador', 1)
INSERT [dbo].[OrdenVenta] ([IdOrden], [IdTienda], [FechaRegistro], [IdVendedor], [IdCliente], [Situacion], [FechaDespacho], [IdDespachador], [ImporteParcial], [Observacion], [EditorUser], [Estado]) VALUES (N'2021000003', N'A0001', CAST(N'2021-07-23' AS Date), N'00005', N'00005', N'O', NULL, NULL, CAST(142.00 AS Decimal(10, 2)), N'', N'Administrador', 1)
INSERT [dbo].[OrdenVenta] ([IdOrden], [IdTienda], [FechaRegistro], [IdVendedor], [IdCliente], [Situacion], [FechaDespacho], [IdDespachador], [ImporteParcial], [Observacion], [EditorUser], [Estado]) VALUES (N'2021000004', N'A0002', CAST(N'2021-07-25' AS Date), N'00005', N'00006', N'V', NULL, NULL, CAST(2237.50 AS Decimal(10, 2)), N'', N'Administrador', 1)
INSERT [dbo].[OrdenVenta] ([IdOrden], [IdTienda], [FechaRegistro], [IdVendedor], [IdCliente], [Situacion], [FechaDespacho], [IdDespachador], [ImporteParcial], [Observacion], [EditorUser], [Estado]) VALUES (N'2021000005', N'A0002', CAST(N'2021-08-30' AS Date), N'00005', N'00006', N'V', NULL, NULL, CAST(290.50 AS Decimal(10, 2)), N'', N'Administrador', 1)
INSERT [dbo].[OrdenVenta] ([IdOrden], [IdTienda], [FechaRegistro], [IdVendedor], [IdCliente], [Situacion], [FechaDespacho], [IdDespachador], [ImporteParcial], [Observacion], [EditorUser], [Estado]) VALUES (N'2021000006', N'A0002', CAST(N'2021-09-07' AS Date), N'00005', N'00007', N'V', NULL, NULL, CAST(292.50 AS Decimal(10, 2)), N'', N'Administrador', 1)
INSERT [dbo].[OrdenVenta] ([IdOrden], [IdTienda], [FechaRegistro], [IdVendedor], [IdCliente], [Situacion], [FechaDespacho], [IdDespachador], [ImporteParcial], [Observacion], [EditorUser], [Estado]) VALUES (N'2021000007', N'A0002', CAST(N'2021-09-07' AS Date), N'00005', N'00006', N'O', NULL, NULL, CAST(100.00 AS Decimal(10, 2)), N'', N'Administrador', 1)
INSERT [dbo].[OrdenVenta] ([IdOrden], [IdTienda], [FechaRegistro], [IdVendedor], [IdCliente], [Situacion], [FechaDespacho], [IdDespachador], [ImporteParcial], [Observacion], [EditorUser], [Estado]) VALUES (N'2022000001', N'A0002', CAST(N'2022-01-03' AS Date), N'00005', N'00006', N'O', NULL, NULL, CAST(71.00 AS Decimal(10, 2)), N'', N'Administrador', 1)
GO
INSERT [dbo].[Producto] ([IdProducto], [IdCategoria], [IdSubCategoria], [IdMarca], [IdTalla], [IdFamilia], [NombreProducto], [Descripcion], [Genero], [EnlaceWeb], [Sku], [StokMinimo], [PrecioBase], [Estado], [EditorUser], [CodigoBarra]) VALUES (N'P00001', N'C0003', N'SC003', N'M00002', N'T002', N'000002', N'PANTALON', N'ASDASD', N'M', N'', N'', CAST(10.000 AS Decimal(10, 3)), CAST(35.50 AS Decimal(10, 2)), 1, N'Administrador', N'')
INSERT [dbo].[Producto] ([IdProducto], [IdCategoria], [IdSubCategoria], [IdMarca], [IdTalla], [IdFamilia], [NombreProducto], [Descripcion], [Genero], [EnlaceWeb], [Sku], [StokMinimo], [PrecioBase], [Estado], [EditorUser], [CodigoBarra]) VALUES (N'P00002', N'C0003', N'SC003', N'M00002', N'T002', N'000002', N'CAMISETA', N'', N'M', N'', N'', CAST(10.000 AS Decimal(10, 3)), CAST(22.50 AS Decimal(10, 2)), 1, N'Administrador', N'')
INSERT [dbo].[Producto] ([IdProducto], [IdCategoria], [IdSubCategoria], [IdMarca], [IdTalla], [IdFamilia], [NombreProducto], [Descripcion], [Genero], [EnlaceWeb], [Sku], [StokMinimo], [PrecioBase], [Estado], [EditorUser], [CodigoBarra]) VALUES (N'P00003', N'C0003', N'SC003', N'M00002', N'T002', N'000002', N'ZAPATOS', N'SIN MARCA', N'M', N'', N'', CAST(10.000 AS Decimal(10, 3)), CAST(75.00 AS Decimal(10, 2)), 1, N'Administrador', N'')
INSERT [dbo].[Producto] ([IdProducto], [IdCategoria], [IdSubCategoria], [IdMarca], [IdTalla], [IdFamilia], [NombreProducto], [Descripcion], [Genero], [EnlaceWeb], [Sku], [StokMinimo], [PrecioBase], [Estado], [EditorUser], [CodigoBarra]) VALUES (N'P00004', N'C0003', N'SC003', N'M00002', N'T002', N'000002', N'SHORTS', N'SIN MARCA', N'U', N'', N'', CAST(10.000 AS Decimal(10, 3)), CAST(25.00 AS Decimal(10, 2)), 1, N'Administrador', N'')
INSERT [dbo].[Producto] ([IdProducto], [IdCategoria], [IdSubCategoria], [IdMarca], [IdTalla], [IdFamilia], [NombreProducto], [Descripcion], [Genero], [EnlaceWeb], [Sku], [StokMinimo], [PrecioBase], [Estado], [EditorUser], [CodigoBarra]) VALUES (N'P00005', N'C0001', N'SC002', N'M00002', N'T002', N'000002', N'POLERA ', N'POLERA CON CAPUCHA', N'U', N'', N'', CAST(10.000 AS Decimal(10, 3)), CAST(96.80 AS Decimal(10, 2)), 1, N'Administrador', N'P00005C0001M00002')
GO
INSERT [dbo].[Salida] ([IdSalida], [IdTienda], [FechaSalida], [Documento], [EditorUser]) VALUES (N'2021000001', N'A0002', CAST(N'2021-07-03' AS Date), N'DOC-032', N'Administrador')
INSERT [dbo].[Salida] ([IdSalida], [IdTienda], [FechaSalida], [Documento], [EditorUser]) VALUES (N'2021000002', N'A0002', CAST(N'2021-07-03' AS Date), N'SDASD', N'Administrador')
INSERT [dbo].[Salida] ([IdSalida], [IdTienda], [FechaSalida], [Documento], [EditorUser]) VALUES (N'2021000003', N'A0002', CAST(N'2021-07-03' AS Date), N'SALIA', N'Administrador')
INSERT [dbo].[Salida] ([IdSalida], [IdTienda], [FechaSalida], [Documento], [EditorUser]) VALUES (N'2021000004', N'A0002', CAST(N'2021-07-04' AS Date), N'FGHFGH', N'Administrador')
GO
INSERT [dbo].[Stock] ([IdTienda], [IdProducto], [NombreProducto], [PrecioUnitario], [StokMinimo], [StokFinal]) VALUES (N'A0001', N'P00001', N'PANTALON', CAST(35.50 AS Decimal(10, 2)), CAST(10.000 AS Decimal(10, 3)), CAST(96.000 AS Decimal(10, 3)))
INSERT [dbo].[Stock] ([IdTienda], [IdProducto], [NombreProducto], [PrecioUnitario], [StokMinimo], [StokFinal]) VALUES (N'A0002', N'P00001', N'PANTALON', CAST(35.50 AS Decimal(10, 2)), CAST(10.000 AS Decimal(10, 3)), CAST(2.000 AS Decimal(10, 3)))
INSERT [dbo].[Stock] ([IdTienda], [IdProducto], [NombreProducto], [PrecioUnitario], [StokMinimo], [StokFinal]) VALUES (N'A0002', N'P00002', N'CAMISETA', CAST(22.50 AS Decimal(10, 2)), CAST(10.000 AS Decimal(10, 3)), CAST(75.000 AS Decimal(10, 3)))
INSERT [dbo].[Stock] ([IdTienda], [IdProducto], [NombreProducto], [PrecioUnitario], [StokMinimo], [StokFinal]) VALUES (N'A0002', N'P00003', N'ZAPATOS', CAST(75.00 AS Decimal(10, 2)), CAST(10.000 AS Decimal(10, 3)), CAST(72.000 AS Decimal(10, 3)))
INSERT [dbo].[Stock] ([IdTienda], [IdProducto], [NombreProducto], [PrecioUnitario], [StokMinimo], [StokFinal]) VALUES (N'A0002', N'P00004', N'SHORTS', CAST(25.00 AS Decimal(10, 2)), CAST(10.000 AS Decimal(10, 3)), CAST(76.000 AS Decimal(10, 3)))
INSERT [dbo].[Stock] ([IdTienda], [IdProducto], [NombreProducto], [PrecioUnitario], [StokMinimo], [StokFinal]) VALUES (N'A0002', N'P00005', N'POLERA ', CAST(96.80 AS Decimal(10, 2)), CAST(10.000 AS Decimal(10, 3)), CAST(1.000 AS Decimal(10, 3)))
GO
SET IDENTITY_INSERT [dbo].[Usuario] ON 

INSERT [dbo].[Usuario] ([Id], [Nombre], [Apellido], [Email], [Telefono], [UserName], [Password]) VALUES (1, N'Napoleon', N'Bonaparte', N'nbonaparte@mail.com', N'+51 999 999 999', N'Napoleon', N'123456789')
SET IDENTITY_INSERT [dbo].[Usuario] OFF
GO
INSERT [dbo].[Venta] ([IdVenta], [IdTienda], [IdOrden], [FechaRegistrro], [IdComprobante], [NumComprobante], [NivelPrecio], [ImporteTotal], [Observacion], [Facturado], [FechaFacturado], [IdClienteFinal], [CorrelativoVenta], [CorrelativoBoleta], [CorrelativoFactura], [ComprobanteInterno], [Anulado], [MotivoAnulacion], [Estado], [EditorUser]) VALUES (N'2021000001', N'A0002', N'2021000005', CAST(N'2021-09-07' AS Date), N'06001', N'TIC-2021-000001', N'PRECIO ESPECIAL', CAST(290.50 AS Decimal(10, 2)), N'opcional', 0, CAST(N'1900-01-01' AS Date), N'', N'', N'', N'', N'TIC-2021-000001', 0, N'', 1, N'Administrador')
INSERT [dbo].[Venta] ([IdVenta], [IdTienda], [IdOrden], [FechaRegistrro], [IdComprobante], [NumComprobante], [NivelPrecio], [ImporteTotal], [Observacion], [Facturado], [FechaFacturado], [IdClienteFinal], [CorrelativoVenta], [CorrelativoBoleta], [CorrelativoFactura], [ComprobanteInterno], [Anulado], [MotivoAnulacion], [Estado], [EditorUser]) VALUES (N'2021000002', N'A0002', N'2021000001', CAST(N'2021-09-07' AS Date), N'06001', N'TIC-2021-000002', N'PRECIO GENERAL', CAST(142.50 AS Decimal(10, 2)), N'opcional', 0, CAST(N'1900-01-01' AS Date), N'', N'', N'', N'', N'TIC-2021-000002', 0, N'', 1, N'Administrador')
INSERT [dbo].[Venta] ([IdVenta], [IdTienda], [IdOrden], [FechaRegistrro], [IdComprobante], [NumComprobante], [NivelPrecio], [ImporteTotal], [Observacion], [Facturado], [FechaFacturado], [IdClienteFinal], [CorrelativoVenta], [CorrelativoBoleta], [CorrelativoFactura], [ComprobanteInterno], [Anulado], [MotivoAnulacion], [Estado], [EditorUser]) VALUES (N'2021000003', N'A0002', N'2021000004', CAST(N'2021-09-07' AS Date), N'06001', N'TIC-2021-000003', N'PRECIO ESPECIAL', CAST(2237.50 AS Decimal(10, 2)), N'opcional', 0, CAST(N'1900-01-01' AS Date), N'', N'', N'', N'', N'TIC-2021-000003', 0, N'', 1, N'Administrador')
INSERT [dbo].[Venta] ([IdVenta], [IdTienda], [IdOrden], [FechaRegistrro], [IdComprobante], [NumComprobante], [NivelPrecio], [ImporteTotal], [Observacion], [Facturado], [FechaFacturado], [IdClienteFinal], [CorrelativoVenta], [CorrelativoBoleta], [CorrelativoFactura], [ComprobanteInterno], [Anulado], [MotivoAnulacion], [Estado], [EditorUser]) VALUES (N'2021000004', N'A0002', N'2021000002', CAST(N'2021-09-07' AS Date), N'06001', N'TIC-2021-000004', N'PRECIO ESPECIAL', CAST(179.00 AS Decimal(10, 2)), N'opcional', 0, CAST(N'1900-01-01' AS Date), N'', N'', N'', N'', N'TIC-2021-000004', 0, N'', 1, N'Administrador')
INSERT [dbo].[Venta] ([IdVenta], [IdTienda], [IdOrden], [FechaRegistrro], [IdComprobante], [NumComprobante], [NivelPrecio], [ImporteTotal], [Observacion], [Facturado], [FechaFacturado], [IdClienteFinal], [CorrelativoVenta], [CorrelativoBoleta], [CorrelativoFactura], [ComprobanteInterno], [Anulado], [MotivoAnulacion], [Estado], [EditorUser]) VALUES (N'2021000005', N'A0002', N'2021000006', CAST(N'2021-09-07' AS Date), N'06001', N'TIC-2021-000005', N'PRECIO GENERAL', CAST(292.50 AS Decimal(10, 2)), N'opcional', 0, CAST(N'1900-01-01' AS Date), N'', N'', N'', N'', N'TIC-2021-000005', 0, N'', 1, N'Administrador')
GO
INSERT [dbo].[VentaPago] ([IdPago], [IdVenta], [IdFormaPago], [MetodoPago], [NumOperacion], [Moneda], [Cambio], [Importe], [Estado]) VALUES (N'2021090001', N'2021000001', N'05001', N'EFECTIVO  (D)', N'', N'D', CAST(3.980 AS Decimal(10, 3)), CAST(199.00 AS Decimal(10, 2)), 1)
INSERT [dbo].[VentaPago] ([IdPago], [IdVenta], [IdFormaPago], [MetodoPago], [NumOperacion], [Moneda], [Cambio], [Importe], [Estado]) VALUES (N'2021090002', N'2021000001', N'05001', N'EFECTIVO  (S)', N'', N'S', CAST(3.980 AS Decimal(10, 3)), CAST(91.50 AS Decimal(10, 2)), 1)
INSERT [dbo].[VentaPago] ([IdPago], [IdVenta], [IdFormaPago], [MetodoPago], [NumOperacion], [Moneda], [Cambio], [Importe], [Estado]) VALUES (N'2021090003', N'2021000002', N'05001', N'EFECTIVO  (S)', N'', N'S', CAST(3.980 AS Decimal(10, 3)), CAST(142.50 AS Decimal(10, 2)), 1)
INSERT [dbo].[VentaPago] ([IdPago], [IdVenta], [IdFormaPago], [MetodoPago], [NumOperacion], [Moneda], [Cambio], [Importe], [Estado]) VALUES (N'2021090004', N'2021000003', N'05001', N'EFECTIVO  (D)', N'', N'D', CAST(3.980 AS Decimal(10, 3)), CAST(1990.00 AS Decimal(10, 2)), 1)
INSERT [dbo].[VentaPago] ([IdPago], [IdVenta], [IdFormaPago], [MetodoPago], [NumOperacion], [Moneda], [Cambio], [Importe], [Estado]) VALUES (N'2021090005', N'2021000003', N'05003', N'TARJETA BANCO INTERBANK (S)', N'', N'S', CAST(3.980 AS Decimal(10, 3)), CAST(100.00 AS Decimal(10, 2)), 1)
INSERT [dbo].[VentaPago] ([IdPago], [IdVenta], [IdFormaPago], [MetodoPago], [NumOperacion], [Moneda], [Cambio], [Importe], [Estado]) VALUES (N'2021090006', N'2021000003', N'05003', N'TARJETA BANCO DE CREDITO (S)', N'', N'S', CAST(3.980 AS Decimal(10, 3)), CAST(100.00 AS Decimal(10, 2)), 1)
INSERT [dbo].[VentaPago] ([IdPago], [IdVenta], [IdFormaPago], [MetodoPago], [NumOperacion], [Moneda], [Cambio], [Importe], [Estado]) VALUES (N'2021090007', N'2021000003', N'05001', N'EFECTIVO  (S)', N'', N'S', CAST(3.980 AS Decimal(10, 3)), CAST(47.50 AS Decimal(10, 2)), 1)
INSERT [dbo].[VentaPago] ([IdPago], [IdVenta], [IdFormaPago], [MetodoPago], [NumOperacion], [Moneda], [Cambio], [Importe], [Estado]) VALUES (N'2021090008', N'2021000004', N'05001', N'EFECTIVO  (D)', N'', N'D', CAST(3.980 AS Decimal(10, 3)), CAST(179.00 AS Decimal(10, 2)), 1)
INSERT [dbo].[VentaPago] ([IdPago], [IdVenta], [IdFormaPago], [MetodoPago], [NumOperacion], [Moneda], [Cambio], [Importe], [Estado]) VALUES (N'2021090009', N'2021000005', N'05001', N'EFECTIVO  (D)', N'', N'D', CAST(3.980 AS Decimal(10, 3)), CAST(119.40 AS Decimal(10, 2)), 1)
INSERT [dbo].[VentaPago] ([IdPago], [IdVenta], [IdFormaPago], [MetodoPago], [NumOperacion], [Moneda], [Cambio], [Importe], [Estado]) VALUES (N'2021090010', N'2021000005', N'05001', N'EFECTIVO  (S)', N'', N'S', CAST(3.980 AS Decimal(10, 3)), CAST(100.00 AS Decimal(10, 2)), 1)
INSERT [dbo].[VentaPago] ([IdPago], [IdVenta], [IdFormaPago], [MetodoPago], [NumOperacion], [Moneda], [Cambio], [Importe], [Estado]) VALUES (N'2021090011', N'2021000005', N'05003', N'TARJETA BANCO CONTINENTAL (S)', N'', N'S', CAST(3.980 AS Decimal(10, 3)), CAST(73.10 AS Decimal(10, 2)), 1)
GO
INSERT [General].[Categoria] ([IdCategoria], [NombreCategoria], [Descripcion], [Prioridad], [Estado], [EditorUser]) VALUES (N'C0001', N'PRUEBA 001', N'DESCRIPCION PRUEBA 001', 1, 1, N'Administrador')
INSERT [General].[Categoria] ([IdCategoria], [NombreCategoria], [Descripcion], [Prioridad], [Estado], [EditorUser]) VALUES (N'C0002', N'PRUEBA --- 002', N'DESC 002', 2, 1, N'Administrador')
INSERT [General].[Categoria] ([IdCategoria], [NombreCategoria], [Descripcion], [Prioridad], [Estado], [EditorUser]) VALUES (N'C0003', N'CATEGROIAS 003', N'DESCRIPCIN 003', 5, 1, N'Administrador')
GO
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'01', N'AMAZONAS')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'02', N'ÁNCASH')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'03', N'APURÍMAC')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'04', N'AREQUIPA')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'05', N'AYACUCHO')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'06', N'CAJAMARCA')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'07', N'CALLAO')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'08', N'CUSCO')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'09', N'HUANCAVELICA')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'10', N'HUÁNUCO')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'11', N'ICA')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'12', N'JUNÍN')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'13', N'LA LIBERTAD')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'14', N'LAMBAYEQUE')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'15', N'LIMA')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'16', N'LORETO')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'17', N'MADRE DE DIOS')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'18', N'MOQUEGUA')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'19', N'PASCO')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'20', N'PIURA')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'21', N'PUNO')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'22', N'SAN MARTÍN')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'23', N'TACNA')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'24', N'TUMBES')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'25', N'UCAYALI')
INSERT [General].[Departamento] ([IdDepartamento], [Descripcion]) VALUES (N'N0', N'SIN DATOS')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010101', N'01', N'0101', N'CHACHAPOYAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010102', N'01', N'0101', N'ASUNCION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010103', N'01', N'0101', N'BALSAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010104', N'01', N'0101', N'CHETO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010105', N'01', N'0101', N'CHILIQUIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010106', N'01', N'0101', N'CHUQUIBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010107', N'01', N'0101', N'GRANADA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010108', N'01', N'0101', N'HUANCAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010109', N'01', N'0101', N'LA JALCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010110', N'01', N'0101', N'LEIMEBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010111', N'01', N'0101', N'LEVANTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010112', N'01', N'0101', N'MAGDALENA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010113', N'01', N'0101', N'MARISCAL CASTILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010114', N'01', N'0101', N'MOLINOPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010115', N'01', N'0101', N'MONTEVIDEO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010116', N'01', N'0101', N'OLLEROS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010117', N'01', N'0101', N'QUINJALCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010118', N'01', N'0101', N'SAN FRANCISCO DE DAGUAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010119', N'01', N'0101', N'SAN ISIDRO DE MAINO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010120', N'01', N'0101', N'SOLOCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010121', N'01', N'0101', N'SONCHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010201', N'01', N'0102', N'BAGUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010202', N'01', N'0102', N'ARAMANGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010203', N'01', N'0102', N'COPALLIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010204', N'01', N'0102', N'EL PARCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010205', N'01', N'0102', N'IMAZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010206', N'01', N'0102', N'LA PECA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010301', N'01', N'0103', N'JUMBILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010302', N'01', N'0103', N'CHISQUILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010303', N'01', N'0103', N'CHURUJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010304', N'01', N'0103', N'COROSHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010305', N'01', N'0103', N'CUISPES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010306', N'01', N'0103', N'FLORIDA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010307', N'01', N'0103', N'JAZAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010308', N'01', N'0103', N'RECTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010309', N'01', N'0103', N'SAN CARLOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010310', N'01', N'0103', N'SHIPASBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010311', N'01', N'0103', N'VALERA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010312', N'01', N'0103', N'YAMBRASBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010401', N'01', N'0104', N'NIEVA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010402', N'01', N'0104', N'EL CENEPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010403', N'01', N'0104', N'RIO SANTIAGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010501', N'01', N'0105', N'LAMUD')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010502', N'01', N'0105', N'CAMPORREDONDO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010503', N'01', N'0105', N'COCABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010504', N'01', N'0105', N'COLCAMAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010505', N'01', N'0105', N'CONILA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010506', N'01', N'0105', N'INGUILPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010507', N'01', N'0105', N'LONGUITA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010508', N'01', N'0105', N'LONYA CHICO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010509', N'01', N'0105', N'LUYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010510', N'01', N'0105', N'LUYA VIEJO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010511', N'01', N'0105', N'MARIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010512', N'01', N'0105', N'OCALLI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010513', N'01', N'0105', N'OCUMAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010514', N'01', N'0105', N'PISUQUIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010515', N'01', N'0105', N'PROVIDENCIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010516', N'01', N'0105', N'SAN CRISTOBAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010517', N'01', N'0105', N'SAN FRANCISCO DEL YESO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010518', N'01', N'0105', N'SAN JERONIMO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010519', N'01', N'0105', N'SAN JUAN DE LOPECANCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010520', N'01', N'0105', N'SANTA CATALINA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010521', N'01', N'0105', N'SANTO TOMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010522', N'01', N'0105', N'TINGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010523', N'01', N'0105', N'TRITA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010601', N'01', N'0106', N'SAN NICOLAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010602', N'01', N'0106', N'CHIRIMOTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010603', N'01', N'0106', N'COCHAMAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010604', N'01', N'0106', N'HUAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010605', N'01', N'0106', N'LIMABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010606', N'01', N'0106', N'LONGAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010607', N'01', N'0106', N'MARISCAL BENAVIDES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010608', N'01', N'0106', N'MILPUC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010609', N'01', N'0106', N'OMIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010610', N'01', N'0106', N'SANTA ROSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010611', N'01', N'0106', N'TOTORA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010612', N'01', N'0106', N'VISTA ALEGRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010701', N'01', N'0107', N'BAGUA GRANDE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010702', N'01', N'0107', N'CAJARURO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010703', N'01', N'0107', N'CUMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010704', N'01', N'0107', N'EL MILAGRO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010705', N'01', N'0107', N'JAMALCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010706', N'01', N'0107', N'LONYA GRANDE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'010707', N'01', N'0107', N'YAMON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020101', N'02', N'0201', N'HUARAZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020102', N'02', N'0201', N'COCHABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020103', N'02', N'0201', N'COLCABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020104', N'02', N'0201', N'HUANCHAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020105', N'02', N'0201', N'INDEPENDENCIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020106', N'02', N'0201', N'JANGAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020107', N'02', N'0201', N'LA LIBERTAD')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020108', N'02', N'0201', N'OLLEROS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020109', N'02', N'0201', N'PAMPAS GRANDE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020110', N'02', N'0201', N'PARIACOTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020111', N'02', N'0201', N'PIRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020112', N'02', N'0201', N'TARICA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020201', N'02', N'0202', N'AIJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020202', N'02', N'0202', N'CORIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020203', N'02', N'0202', N'HUACLLAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020204', N'02', N'0202', N'LA MERCED')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020205', N'02', N'0202', N'SUCCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020301', N'02', N'0203', N'LLAMELLIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020302', N'02', N'0203', N'ACZO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020303', N'02', N'0203', N'CHACCHO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020304', N'02', N'0203', N'CHINGAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020305', N'02', N'0203', N'MIRGAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020306', N'02', N'0203', N'SAN JUAN DE RONTOY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020401', N'02', N'0204', N'CHACAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020402', N'02', N'0204', N'ACOCHACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020501', N'02', N'0205', N'CHIQUIAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020502', N'02', N'0205', N'ABELARDO PARDO LEZAMETA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020503', N'02', N'0205', N'ANTONIO RAYMONDI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020504', N'02', N'0205', N'AQUIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020505', N'02', N'0205', N'CAJACAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020506', N'02', N'0205', N'CANIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020507', N'02', N'0205', N'COLQUIOC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020508', N'02', N'0205', N'HUALLANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020509', N'02', N'0205', N'HUASTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020510', N'02', N'0205', N'HUAYLLACAYAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020511', N'02', N'0205', N'LA PRIMAVERA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020512', N'02', N'0205', N'MANGAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020513', N'02', N'0205', N'PACLLON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020514', N'02', N'0205', N'SAN MIGUEL DE CORPANQUI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020515', N'02', N'0205', N'TICLLOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020601', N'02', N'0206', N'CARHUAZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020602', N'02', N'0206', N'ACOPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020603', N'02', N'0206', N'AMASHCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020604', N'02', N'0206', N'ANTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020605', N'02', N'0206', N'ATAQUERO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020606', N'02', N'0206', N'MARCARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020607', N'02', N'0206', N'PARIAHUANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020608', N'02', N'0206', N'SAN MIGUEL DE ACO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020609', N'02', N'0206', N'SHILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020610', N'02', N'0206', N'TINCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020611', N'02', N'0206', N'YUNGAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020701', N'02', N'0207', N'SAN LUIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020702', N'02', N'0207', N'SAN NICOLAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020703', N'02', N'0207', N'YAUYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020801', N'02', N'0208', N'CASMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020802', N'02', N'0208', N'BUENA VISTA ALTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020803', N'02', N'0208', N'COMANDANTE NOEL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020804', N'02', N'0208', N'YAUTAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020901', N'02', N'0209', N'CORONGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020902', N'02', N'0209', N'ACO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020903', N'02', N'0209', N'BAMBAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020904', N'02', N'0209', N'CUSCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020905', N'02', N'0209', N'LA PAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020906', N'02', N'0209', N'YANAC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'020907', N'02', N'0209', N'YUPAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021001', N'02', N'0210', N'HUARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021002', N'02', N'0210', N'ANRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021003', N'02', N'0210', N'CAJAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021004', N'02', N'0210', N'CHAVIN DE HUANTAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021005', N'02', N'0210', N'HUACACHI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021006', N'02', N'0210', N'HUACCHIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021007', N'02', N'0210', N'HUACHIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021008', N'02', N'0210', N'HUANTAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021009', N'02', N'0210', N'MASIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021010', N'02', N'0210', N'PAUCAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021011', N'02', N'0210', N'PONTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021012', N'02', N'0210', N'RAHUAPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021013', N'02', N'0210', N'RAPAYAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021014', N'02', N'0210', N'SAN MARCOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021015', N'02', N'0210', N'SAN PEDRO DE CHANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021016', N'02', N'0210', N'UCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021101', N'02', N'0211', N'HUARMEY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021102', N'02', N'0211', N'COCHAPETI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021103', N'02', N'0211', N'CULEBRAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021104', N'02', N'0211', N'HUAYAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021105', N'02', N'0211', N'MALVAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021201', N'02', N'0212', N'CARAZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021202', N'02', N'0212', N'HUALLANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021203', N'02', N'0212', N'HUATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021204', N'02', N'0212', N'HUAYLAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021205', N'02', N'0212', N'MATO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021206', N'02', N'0212', N'PAMPAROMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021207', N'02', N'0212', N'PUEBLO LIBRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021208', N'02', N'0212', N'SANTA CRUZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021209', N'02', N'0212', N'SANTO TORIBIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021210', N'02', N'0212', N'YURACMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021301', N'02', N'0213', N'PISCOBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021302', N'02', N'0213', N'CASCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021303', N'02', N'0213', N'ELEAZAR GUZMAN BARRON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021304', N'02', N'0213', N'FIDEL OLIVAS ESCUDERO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021305', N'02', N'0213', N'LLAMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021306', N'02', N'0213', N'LLUMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021307', N'02', N'0213', N'LUCMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021308', N'02', N'0213', N'MUSGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021401', N'02', N'0214', N'OCROS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021402', N'02', N'0214', N'ACAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021403', N'02', N'0214', N'CAJAMARQUILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021404', N'02', N'0214', N'CARHUAPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021405', N'02', N'0214', N'COCHAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021406', N'02', N'0214', N'CONGAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021407', N'02', N'0214', N'LLIPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021408', N'02', N'0214', N'SAN CRISTOBAL DE RAJAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021409', N'02', N'0214', N'SAN PEDRO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021410', N'02', N'0214', N'SANTIAGO DE CHILCAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021501', N'02', N'0215', N'CABANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021502', N'02', N'0215', N'BOLOGNESI')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021503', N'02', N'0215', N'CONCHUCOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021504', N'02', N'0215', N'HUACASCHUQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021505', N'02', N'0215', N'HUANDOVAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021506', N'02', N'0215', N'LACABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021507', N'02', N'0215', N'LLAPO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021508', N'02', N'0215', N'PALLASCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021509', N'02', N'0215', N'PAMPAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021510', N'02', N'0215', N'SANTA ROSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021511', N'02', N'0215', N'TAUCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021601', N'02', N'0216', N'POMABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021602', N'02', N'0216', N'HUAYLLAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021603', N'02', N'0216', N'PAROBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021604', N'02', N'0216', N'QUINUABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021701', N'02', N'0217', N'RECUAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021702', N'02', N'0217', N'CATAC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021703', N'02', N'0217', N'COTAPARACO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021704', N'02', N'0217', N'HUAYLLAPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021705', N'02', N'0217', N'LLACLLIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021706', N'02', N'0217', N'MARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021707', N'02', N'0217', N'PAMPAS CHICO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021708', N'02', N'0217', N'PARARIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021709', N'02', N'0217', N'TAPACOCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021710', N'02', N'0217', N'TICAPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021801', N'02', N'0218', N'CHIMBOTE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021802', N'02', N'0218', N'CACERES DEL PERU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021803', N'02', N'0218', N'COISHCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021804', N'02', N'0218', N'MACATE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021805', N'02', N'0218', N'MORO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021806', N'02', N'0218', N'NEPE+ÆA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021807', N'02', N'0218', N'SAMANCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021808', N'02', N'0218', N'SANTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021809', N'02', N'0218', N'NUEVO CHIMBOTE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021901', N'02', N'0219', N'SIHUAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021902', N'02', N'0219', N'ACOBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021903', N'02', N'0219', N'ALFONSO UGARTE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021904', N'02', N'0219', N'CASHAPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021905', N'02', N'0219', N'CHINGALPO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021906', N'02', N'0219', N'HUAYLLABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021907', N'02', N'0219', N'QUICHES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021908', N'02', N'0219', N'RAGASH')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021909', N'02', N'0219', N'SAN JUAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'021910', N'02', N'0219', N'SICSIBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'022001', N'02', N'0220', N'YUNGAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'022002', N'02', N'0220', N'CASCAPARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'022003', N'02', N'0220', N'MANCOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'022004', N'02', N'0220', N'MATACOTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'022005', N'02', N'0220', N'QUILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'022006', N'02', N'0220', N'RANRAHIRCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'022007', N'02', N'0220', N'SHUPLUY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'022008', N'02', N'0220', N'YANAMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030101', N'03', N'0301', N'ABANCAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030102', N'03', N'0301', N'CHACOCHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030103', N'03', N'0301', N'CIRCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030104', N'03', N'0301', N'CURAHUASI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030105', N'03', N'0301', N'HUANIPACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030106', N'03', N'0301', N'LAMBRAMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030107', N'03', N'0301', N'PICHIRHUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030108', N'03', N'0301', N'SAN PEDRO DE CACHORA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030109', N'03', N'0301', N'TAMBURCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030201', N'03', N'0302', N'ANDAHUAYLAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030202', N'03', N'0302', N'ANDARAPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030203', N'03', N'0302', N'CHIARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030204', N'03', N'0302', N'HUANCARAMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030205', N'03', N'0302', N'HUANCARAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030206', N'03', N'0302', N'HUAYANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030207', N'03', N'0302', N'KISHUARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030208', N'03', N'0302', N'PACOBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030209', N'03', N'0302', N'PACUCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030210', N'03', N'0302', N'PAMPACHIRI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030211', N'03', N'0302', N'POMACOCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030212', N'03', N'0302', N'SAN ANTONIO DE CACHI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030213', N'03', N'0302', N'SAN JERONIMO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030214', N'03', N'0302', N'SAN MIGUEL DE CHACCRAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030215', N'03', N'0302', N'SANTA MARIA DE CHICMO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030216', N'03', N'0302', N'TALAVERA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030217', N'03', N'0302', N'TUMAY HUARACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030218', N'03', N'0302', N'TURPO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030219', N'03', N'0302', N'KAQUIABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030220', N'03', N'0302', N'JOSE MARIA ARGUEDAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030301', N'03', N'0303', N'ANTABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030302', N'03', N'0303', N'EL ORO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030303', N'03', N'0303', N'HUAQUIRCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030304', N'03', N'0303', N'JUAN ESPINOZA MEDRANO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030305', N'03', N'0303', N'OROPESA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030306', N'03', N'0303', N'PACHACONAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030307', N'03', N'0303', N'SABAINO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030401', N'03', N'0304', N'CHALHUANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030402', N'03', N'0304', N'CAPAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030403', N'03', N'0304', N'CARAYBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030404', N'03', N'0304', N'CHAPIMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030405', N'03', N'0304', N'COLCABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030406', N'03', N'0304', N'COTARUSE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030407', N'03', N'0304', N'HUAYLLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030408', N'03', N'0304', N'JUSTO APU SAHUARAURA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030409', N'03', N'0304', N'LUCRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030410', N'03', N'0304', N'POCOHUANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030411', N'03', N'0304', N'SAN JUAN DE CHAC+ÆA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030412', N'03', N'0304', N'SA+ÆAYCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030413', N'03', N'0304', N'SORAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030414', N'03', N'0304', N'TAPAIRIHUA')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030415', N'03', N'0304', N'TINTAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030416', N'03', N'0304', N'TORAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030417', N'03', N'0304', N'YANACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030501', N'03', N'0305', N'TAMBOBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030502', N'03', N'0305', N'COTABAMBAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030503', N'03', N'0305', N'COYLLURQUI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030504', N'03', N'0305', N'HAQUIRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030505', N'03', N'0305', N'MARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030506', N'03', N'0305', N'CHALLHUAHUACHO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030601', N'03', N'0306', N'CHINCHEROS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030602', N'03', N'0306', N'ANCO_HUALLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030603', N'03', N'0306', N'COCHARCAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030604', N'03', N'0306', N'HUACCANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030605', N'03', N'0306', N'OCOBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030606', N'03', N'0306', N'ONGOY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030607', N'03', N'0306', N'URANMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030608', N'03', N'0306', N'RANRACANCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030701', N'03', N'0307', N'CHUQUIBAMBILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030702', N'03', N'0307', N'CURPAHUASI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030703', N'03', N'0307', N'GAMARRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030704', N'03', N'0307', N'HUAYLLATI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030705', N'03', N'0307', N'MAMARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030706', N'03', N'0307', N'MICAELA BASTIDAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030707', N'03', N'0307', N'PATAYPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030708', N'03', N'0307', N'PROGRESO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030709', N'03', N'0307', N'SAN ANTONIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030710', N'03', N'0307', N'SANTA ROSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030711', N'03', N'0307', N'TURPAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030712', N'03', N'0307', N'VILCABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030713', N'03', N'0307', N'VIRUNDO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'030714', N'03', N'0307', N'CURASCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040101', N'04', N'0401', N'AREQUIPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040102', N'04', N'0401', N'ALTO SELVA ALEGRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040103', N'04', N'0401', N'CAYMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040104', N'04', N'0401', N'CERRO COLORADO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040105', N'04', N'0401', N'CHARACATO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040106', N'04', N'0401', N'CHIGUATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040107', N'04', N'0401', N'JACOBO HUNTER')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040108', N'04', N'0401', N'LA JOYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040109', N'04', N'0401', N'MARIANO MELGAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040110', N'04', N'0401', N'MIRAFLORES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040111', N'04', N'0401', N'MOLLEBAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040112', N'04', N'0401', N'PAUCARPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040113', N'04', N'0401', N'POCSI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040114', N'04', N'0401', N'POLOBAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040115', N'04', N'0401', N'QUEQUE+ÆA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040116', N'04', N'0401', N'SABANDIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040117', N'04', N'0401', N'SACHACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040118', N'04', N'0401', N'SAN JUAN DE SIGUAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040119', N'04', N'0401', N'SAN JUAN DE TARUCANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040120', N'04', N'0401', N'SANTA ISABEL DE SIGUAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040121', N'04', N'0401', N'SANTA RITA DE SIGUAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040122', N'04', N'0401', N'SOCABAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040123', N'04', N'0401', N'TIABAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040124', N'04', N'0401', N'UCHUMAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040125', N'04', N'0401', N'VITOR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040126', N'04', N'0401', N'YANAHUARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040127', N'04', N'0401', N'YARABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040128', N'04', N'0401', N'YURA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040129', N'04', N'0401', N'JOSE LUIS BUSTAMANTE Y RIVERO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040201', N'04', N'0402', N'CAMANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040202', N'04', N'0402', N'JOSE MARIA QUIMPER')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040203', N'04', N'0402', N'MARIANO NICOLAS VALCARCEL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040204', N'04', N'0402', N'MARISCAL CACERES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040205', N'04', N'0402', N'NICOLAS DE PIEROLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040206', N'04', N'0402', N'OCO+ÆA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040207', N'04', N'0402', N'QUILCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040208', N'04', N'0402', N'SAMUEL PASTOR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040301', N'04', N'0403', N'CARAVELI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040302', N'04', N'0403', N'ACARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040303', N'04', N'0403', N'ATICO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040304', N'04', N'0403', N'ATIQUIPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040305', N'04', N'0403', N'BELLA UNION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040306', N'04', N'0403', N'CAHUACHO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040307', N'04', N'0403', N'CHALA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040308', N'04', N'0403', N'CHAPARRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040309', N'04', N'0403', N'HUANUHUANU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040310', N'04', N'0403', N'JAQUI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040311', N'04', N'0403', N'LOMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040312', N'04', N'0403', N'QUICACHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040313', N'04', N'0403', N'YAUCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040401', N'04', N'0404', N'APLAO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040402', N'04', N'0404', N'ANDAGUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040403', N'04', N'0404', N'AYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040404', N'04', N'0404', N'CHACHAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040405', N'04', N'0404', N'CHILCAYMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040406', N'04', N'0404', N'CHOCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040407', N'04', N'0404', N'HUANCARQUI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040408', N'04', N'0404', N'MACHAGUAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040409', N'04', N'0404', N'ORCOPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040410', N'04', N'0404', N'PAMPACOLCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040411', N'04', N'0404', N'TIPAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040412', N'04', N'0404', N'U+ÆON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040413', N'04', N'0404', N'URACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040414', N'04', N'0404', N'VIRACO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040501', N'04', N'0405', N'CHIVAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040502', N'04', N'0405', N'ACHOMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040503', N'04', N'0405', N'CABANACONDE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040504', N'04', N'0405', N'CALLALLI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040505', N'04', N'0405', N'CAYLLOMA')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040506', N'04', N'0405', N'COPORAQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040507', N'04', N'0405', N'HUAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040508', N'04', N'0405', N'HUANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040509', N'04', N'0405', N'ICHUPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040510', N'04', N'0405', N'LARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040511', N'04', N'0405', N'LLUTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040512', N'04', N'0405', N'MACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040513', N'04', N'0405', N'MADRIGAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040514', N'04', N'0405', N'SAN ANTONIO DE CHUCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040515', N'04', N'0405', N'SIBAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040516', N'04', N'0405', N'TAPAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040517', N'04', N'0405', N'TISCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040518', N'04', N'0405', N'TUTI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040519', N'04', N'0405', N'YANQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040520', N'04', N'0405', N'MAJES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040601', N'04', N'0406', N'CHUQUIBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040602', N'04', N'0406', N'ANDARAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040603', N'04', N'0406', N'CAYARANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040604', N'04', N'0406', N'CHICHAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040605', N'04', N'0406', N'IRAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040606', N'04', N'0406', N'RIO GRANDE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040607', N'04', N'0406', N'SALAMANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040608', N'04', N'0406', N'YANAQUIHUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040701', N'04', N'0407', N'MOLLENDO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040702', N'04', N'0407', N'COCACHACRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040703', N'04', N'0407', N'DEAN VALDIVIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040704', N'04', N'0407', N'ISLAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040705', N'04', N'0407', N'MEJIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040706', N'04', N'0407', N'PUNTA DE BOMBON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040801', N'04', N'0408', N'COTAHUASI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040802', N'04', N'0408', N'ALCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040803', N'04', N'0408', N'CHARCANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040804', N'04', N'0408', N'HUAYNACOTAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040805', N'04', N'0408', N'PAMPAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040806', N'04', N'0408', N'PUYCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040807', N'04', N'0408', N'QUECHUALLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040808', N'04', N'0408', N'SAYLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040809', N'04', N'0408', N'TAURIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040810', N'04', N'0408', N'TOMEPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'040811', N'04', N'0408', N'TORO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050101', N'05', N'0501', N'AYACUCHO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050102', N'05', N'0501', N'ACOCRO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050103', N'05', N'0501', N'ACOS VINCHOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050104', N'05', N'0501', N'CARMEN ALTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050105', N'05', N'0501', N'CHIARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050106', N'05', N'0501', N'OCROS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050107', N'05', N'0501', N'PACAYCASA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050108', N'05', N'0501', N'QUINUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050109', N'05', N'0501', N'SAN JOSE DE TICLLAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050110', N'05', N'0501', N'SAN JUAN BAUTISTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050111', N'05', N'0501', N'SANTIAGO DE PISCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050112', N'05', N'0501', N'SOCOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050113', N'05', N'0501', N'TAMBILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050114', N'05', N'0501', N'VINCHOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050115', N'05', N'0501', N'JESUS NAZARENO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050116', N'05', N'0501', N'ANDRES AVELINO CACERES DORREGARAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050201', N'05', N'0502', N'CANGALLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050202', N'05', N'0502', N'CHUSCHI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050203', N'05', N'0502', N'LOS MOROCHUCOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050204', N'05', N'0502', N'MARIA PARADO DE BELLIDO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050205', N'05', N'0502', N'PARAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050206', N'05', N'0502', N'TOTOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050301', N'05', N'0503', N'SANCOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050302', N'05', N'0503', N'CARAPO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050303', N'05', N'0503', N'SACSAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050304', N'05', N'0503', N'SANTIAGO DE LUCANAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050401', N'05', N'0504', N'HUANTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050402', N'05', N'0504', N'AYAHUANCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050403', N'05', N'0504', N'HUAMANGUILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050404', N'05', N'0504', N'IGUAIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050405', N'05', N'0504', N'LURICOCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050406', N'05', N'0504', N'SANTILLANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050407', N'05', N'0504', N'SIVIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050408', N'05', N'0504', N'LLOCHEGUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050409', N'05', N'0504', N'CANAYRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050410', N'05', N'0504', N'UCHURACCAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050501', N'05', N'0505', N'SAN MIGUEL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050502', N'05', N'0505', N'ANCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050503', N'05', N'0505', N'AYNA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050504', N'05', N'0505', N'CHILCAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050505', N'05', N'0505', N'CHUNGUI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050506', N'05', N'0505', N'LUIS CARRANZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050507', N'05', N'0505', N'SANTA ROSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050508', N'05', N'0505', N'TAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050509', N'05', N'0505', N'SAMUGARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050510', N'05', N'0505', N'ANCHIHUAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050601', N'05', N'0506', N'PUQUIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050602', N'05', N'0506', N'AUCARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050603', N'05', N'0506', N'CABANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050604', N'05', N'0506', N'CARMEN SALCEDO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050605', N'05', N'0506', N'CHAVI+ÆA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050606', N'05', N'0506', N'CHIPAO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050607', N'05', N'0506', N'HUAC-HUAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050608', N'05', N'0506', N'LARAMATE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050609', N'05', N'0506', N'LEONCIO PRADO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050610', N'05', N'0506', N'LLAUTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050611', N'05', N'0506', N'LUCANAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050612', N'05', N'0506', N'OCA+ÆA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050613', N'05', N'0506', N'OTOCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050614', N'05', N'0506', N'SAISA')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050615', N'05', N'0506', N'SAN CRISTOBAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050616', N'05', N'0506', N'SAN JUAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050617', N'05', N'0506', N'SAN PEDRO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050618', N'05', N'0506', N'SAN PEDRO DE PALCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050619', N'05', N'0506', N'SANCOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050620', N'05', N'0506', N'SANTA ANA DE HUAYCAHUACHO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050621', N'05', N'0506', N'SANTA LUCIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050701', N'05', N'0507', N'CORACORA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050702', N'05', N'0507', N'CHUMPI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050703', N'05', N'0507', N'CORONEL CASTA+ÆEDA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050704', N'05', N'0507', N'PACAPAUSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050705', N'05', N'0507', N'PULLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050706', N'05', N'0507', N'PUYUSCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050707', N'05', N'0507', N'SAN FRANCISCO DE RAVACAYCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050708', N'05', N'0507', N'UPAHUACHO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050801', N'05', N'0508', N'PAUSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050802', N'05', N'0508', N'COLTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050803', N'05', N'0508', N'CORCULLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050804', N'05', N'0508', N'LAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050805', N'05', N'0508', N'MARCABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050806', N'05', N'0508', N'OYOLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050807', N'05', N'0508', N'PARARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050808', N'05', N'0508', N'SAN JAVIER DE ALPABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050809', N'05', N'0508', N'SAN JOSE DE USHUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050810', N'05', N'0508', N'SARA SARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050901', N'05', N'0509', N'QUEROBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050902', N'05', N'0509', N'BELEN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050903', N'05', N'0509', N'CHALCOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050904', N'05', N'0509', N'CHILCAYOC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050905', N'05', N'0509', N'HUACA+ÆA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050906', N'05', N'0509', N'MORCOLLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050907', N'05', N'0509', N'PAICO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050908', N'05', N'0509', N'SAN PEDRO DE LARCAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050909', N'05', N'0509', N'SAN SALVADOR DE QUIJE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050910', N'05', N'0509', N'SANTIAGO DE PAUCARAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'050911', N'05', N'0509', N'SORAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051001', N'05', N'0510', N'HUANCAPI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051002', N'05', N'0510', N'ALCAMENCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051003', N'05', N'0510', N'APONGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051004', N'05', N'0510', N'ASQUIPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051005', N'05', N'0510', N'CANARIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051006', N'05', N'0510', N'CAYARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051007', N'05', N'0510', N'COLCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051008', N'05', N'0510', N'HUAMANQUIQUIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051009', N'05', N'0510', N'HUANCARAYLLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051010', N'05', N'0510', N'HUAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051011', N'05', N'0510', N'SARHUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051012', N'05', N'0510', N'VILCANCHOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051101', N'05', N'0511', N'VILCAS HUAMAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051102', N'05', N'0511', N'ACCOMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051103', N'05', N'0511', N'CARHUANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051104', N'05', N'0511', N'CONCEPCION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051105', N'05', N'0511', N'HUAMBALPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051106', N'05', N'0511', N'INDEPENDENCIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051107', N'05', N'0511', N'SAURAMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'051108', N'05', N'0511', N'VISCHONGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060101', N'06', N'0601', N'CAJAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060102', N'06', N'0601', N'ASUNCION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060103', N'06', N'0601', N'CHETILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060104', N'06', N'0601', N'COSPAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060105', N'06', N'0601', N'ENCA+ÆADA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060106', N'06', N'0601', N'JESUS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060107', N'06', N'0601', N'LLACANORA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060108', N'06', N'0601', N'LOS BA+ÆOS DEL INCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060109', N'06', N'0601', N'MAGDALENA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060110', N'06', N'0601', N'MATARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060111', N'06', N'0601', N'NAMORA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060112', N'06', N'0601', N'SAN JUAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060201', N'06', N'0602', N'CAJABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060202', N'06', N'0602', N'CACHACHI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060203', N'06', N'0602', N'CONDEBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060204', N'06', N'0602', N'SITACOCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060301', N'06', N'0603', N'CELENDIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060302', N'06', N'0603', N'CHUMUCH')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060303', N'06', N'0603', N'CORTEGANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060304', N'06', N'0603', N'HUASMIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060305', N'06', N'0603', N'JORGE CHAVEZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060306', N'06', N'0603', N'JOSE GALVEZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060307', N'06', N'0603', N'MIGUEL IGLESIAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060308', N'06', N'0603', N'OXAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060309', N'06', N'0603', N'SOROCHUCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060310', N'06', N'0603', N'SUCRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060311', N'06', N'0603', N'UTCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060312', N'06', N'0603', N'LA LIBERTAD DE PALLAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060401', N'06', N'0604', N'CHOTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060402', N'06', N'0604', N'ANGUIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060403', N'06', N'0604', N'CHADIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060404', N'06', N'0604', N'CHIGUIRIP')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060405', N'06', N'0604', N'CHIMBAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060406', N'06', N'0604', N'CHOROPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060407', N'06', N'0604', N'COCHABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060408', N'06', N'0604', N'CONCHAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060409', N'06', N'0604', N'HUAMBOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060410', N'06', N'0604', N'LAJAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060411', N'06', N'0604', N'LLAMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060412', N'06', N'0604', N'MIRACOSTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060413', N'06', N'0604', N'PACCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060414', N'06', N'0604', N'PION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060415', N'06', N'0604', N'QUEROCOTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060416', N'06', N'0604', N'SAN JUAN DE LICUPIS')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060417', N'06', N'0604', N'TACABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060418', N'06', N'0604', N'TOCMOCHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060419', N'06', N'0604', N'CHALAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060501', N'06', N'0605', N'CONTUMAZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060502', N'06', N'0605', N'CHILETE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060503', N'06', N'0605', N'CUPISNIQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060504', N'06', N'0605', N'GUZMANGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060505', N'06', N'0605', N'SAN BENITO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060506', N'06', N'0605', N'SANTA CRUZ DE TOLED')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060507', N'06', N'0605', N'TANTARICA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060508', N'06', N'0605', N'YONAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060601', N'06', N'0606', N'CUTERVO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060602', N'06', N'0606', N'CALLAYUC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060603', N'06', N'0606', N'CHOROS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060604', N'06', N'0606', N'CUJILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060605', N'06', N'0606', N'LA RAMADA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060606', N'06', N'0606', N'PIMPINGOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060607', N'06', N'0606', N'QUEROCOTILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060608', N'06', N'0606', N'SAN ANDRES DE CUTERVO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060609', N'06', N'0606', N'SAN JUAN DE CUTERVO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060610', N'06', N'0606', N'SAN LUIS DE LUCMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060611', N'06', N'0606', N'SANTA CRUZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060612', N'06', N'0606', N'SANTO DOMINGO DE LA CAPILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060613', N'06', N'0606', N'SANTO TOMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060614', N'06', N'0606', N'SOCOTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060615', N'06', N'0606', N'TORIBIO CASANOVA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060701', N'06', N'0607', N'BAMBAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060702', N'06', N'0607', N'CHUGUR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060703', N'06', N'0607', N'HUALGAYOC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060801', N'06', N'0608', N'JAEN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060802', N'06', N'0608', N'BELLAVISTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060803', N'06', N'0608', N'CHONTALI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060804', N'06', N'0608', N'COLASAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060805', N'06', N'0608', N'HUABAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060806', N'06', N'0608', N'LAS PIRIAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060807', N'06', N'0608', N'POMAHUACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060808', N'06', N'0608', N'PUCARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060809', N'06', N'0608', N'SALLIQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060810', N'06', N'0608', N'SAN FELIPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060811', N'06', N'0608', N'SAN JOSE DEL ALTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060812', N'06', N'0608', N'SANTA ROSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060901', N'06', N'0609', N'SAN IGNACIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060902', N'06', N'0609', N'CHIRINOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060903', N'06', N'0609', N'HUARANGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060904', N'06', N'0609', N'LA COIPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060905', N'06', N'0609', N'NAMBALLE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060906', N'06', N'0609', N'SAN JOSE DE LOURDES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'060907', N'06', N'0609', N'TABACONAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061001', N'06', N'0610', N'PEDRO GALVEZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061002', N'06', N'0610', N'CHANCAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061003', N'06', N'0610', N'EDUARDO VILLANUEVA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061004', N'06', N'0610', N'GREGORIO PITA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061005', N'06', N'0610', N'ICHOCAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061006', N'06', N'0610', N'JOSE MANUEL QUIROZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061007', N'06', N'0610', N'JOSE SABOGAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061101', N'06', N'0611', N'SAN MIGUEL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061102', N'06', N'0611', N'BOLIVAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061103', N'06', N'0611', N'CALQUIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061104', N'06', N'0611', N'CATILLUC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061105', N'06', N'0611', N'EL PRADO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061106', N'06', N'0611', N'LA FLORIDA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061107', N'06', N'0611', N'LLAPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061108', N'06', N'0611', N'NANCHOC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061109', N'06', N'0611', N'NIEPOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061110', N'06', N'0611', N'SAN GREGORIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061111', N'06', N'0611', N'SAN SILVESTRE DE COCHAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061112', N'06', N'0611', N'TONGOD')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061113', N'06', N'0611', N'UNION AGUA BLANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061201', N'06', N'0612', N'SAN PABLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061202', N'06', N'0612', N'SAN BERNARDINO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061203', N'06', N'0612', N'SAN LUIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061204', N'06', N'0612', N'TUMBADEN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061301', N'06', N'0613', N'SANTA CRUZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061302', N'06', N'0613', N'ANDABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061303', N'06', N'0613', N'CATACHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061304', N'06', N'0613', N'CHANCAYBA+ÆOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061305', N'06', N'0613', N'LA ESPERANZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061306', N'06', N'0613', N'NINABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061307', N'06', N'0613', N'PULAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061308', N'06', N'0613', N'SAUCEPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061309', N'06', N'0613', N'SEXI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061310', N'06', N'0613', N'UTICYACU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'061311', N'06', N'0613', N'YAUYUCAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'070101', N'07', N'0701', N'CALLAO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'070102', N'07', N'0701', N'BELLAVISTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'070103', N'07', N'0701', N'CARMEN DE LA LEGUA REYNOSO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'070104', N'07', N'0701', N'LA PERLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'070105', N'07', N'0701', N'LA PUNTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'070106', N'07', N'0701', N'VENTANILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'070107', N'07', N'0701', N'MI PERU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080101', N'08', N'0801', N'CUSCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080102', N'08', N'0801', N'CCORCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080103', N'08', N'0801', N'POROY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080104', N'08', N'0801', N'SAN JERONIMO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080105', N'08', N'0801', N'SAN SEBASTIAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080106', N'08', N'0801', N'SANTIAGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080107', N'08', N'0801', N'SAYLLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080108', N'08', N'0801', N'WANCHAQ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080201', N'08', N'0802', N'ACOMAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080202', N'08', N'0802', N'ACOPIA')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080203', N'08', N'0802', N'ACOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080204', N'08', N'0802', N'MOSOC LLACTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080205', N'08', N'0802', N'POMACANCHI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080206', N'08', N'0802', N'RONDOCAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080207', N'08', N'0802', N'SANGARARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080301', N'08', N'0803', N'ANTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080302', N'08', N'0803', N'ANCAHUASI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080303', N'08', N'0803', N'CACHIMAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080304', N'08', N'0803', N'CHINCHAYPUJIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080305', N'08', N'0803', N'HUAROCONDO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080306', N'08', N'0803', N'LIMATAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080307', N'08', N'0803', N'MOLLEPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080308', N'08', N'0803', N'PUCYURA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080309', N'08', N'0803', N'ZURITE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080401', N'08', N'0804', N'CALCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080402', N'08', N'0804', N'COYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080403', N'08', N'0804', N'LAMAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080404', N'08', N'0804', N'LARES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080405', N'08', N'0804', N'PISAC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080406', N'08', N'0804', N'SAN SALVADOR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080407', N'08', N'0804', N'TARAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080408', N'08', N'0804', N'YANATILE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080501', N'08', N'0805', N'YANAOCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080502', N'08', N'0805', N'CHECCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080503', N'08', N'0805', N'KUNTURKANKI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080504', N'08', N'0805', N'LANGUI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080505', N'08', N'0805', N'LAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080506', N'08', N'0805', N'PAMPAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080507', N'08', N'0805', N'QUEHUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080508', N'08', N'0805', N'TUPAC AMARU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080601', N'08', N'0806', N'SICUANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080602', N'08', N'0806', N'CHECACUPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080603', N'08', N'0806', N'COMBAPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080604', N'08', N'0806', N'MARANGANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080605', N'08', N'0806', N'PITUMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080606', N'08', N'0806', N'SAN PABLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080607', N'08', N'0806', N'SAN PEDRO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080608', N'08', N'0806', N'TINTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080701', N'08', N'0807', N'SANTO TOMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080702', N'08', N'0807', N'CAPACMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080703', N'08', N'0807', N'CHAMACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080704', N'08', N'0807', N'COLQUEMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080705', N'08', N'0807', N'LIVITACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080706', N'08', N'0807', N'LLUSCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080707', N'08', N'0807', N'QUI+ÆOTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080708', N'08', N'0807', N'VELILLE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080801', N'08', N'0808', N'ESPINAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080802', N'08', N'0808', N'CONDOROMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080803', N'08', N'0808', N'COPORAQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080804', N'08', N'0808', N'OCORURO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080805', N'08', N'0808', N'PALLPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080806', N'08', N'0808', N'PICHIGUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080807', N'08', N'0808', N'SUYCKUTAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080808', N'08', N'0808', N'ALTO PICHIGUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080901', N'08', N'0809', N'SANTA ANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080902', N'08', N'0809', N'ECHARATE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080903', N'08', N'0809', N'HUAYOPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080904', N'08', N'0809', N'MARANURA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080905', N'08', N'0809', N'OCOBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080906', N'08', N'0809', N'QUELLOUNO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080907', N'08', N'0809', N'KIMBIRI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080908', N'08', N'0809', N'SANTA TERESA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080909', N'08', N'0809', N'VILCABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080910', N'08', N'0809', N'PICHARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080911', N'08', N'0809', N'INKAWASI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'080912', N'08', N'0809', N'VILLA VIRGEN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081001', N'08', N'0810', N'PARURO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081002', N'08', N'0810', N'ACCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081003', N'08', N'0810', N'CCAPI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081004', N'08', N'0810', N'COLCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081005', N'08', N'0810', N'HUANOQUITE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081006', N'08', N'0810', N'OMACHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081007', N'08', N'0810', N'PACCARITAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081008', N'08', N'0810', N'PILLPINTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081009', N'08', N'0810', N'YAURISQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081101', N'08', N'0811', N'PAUCARTAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081102', N'08', N'0811', N'CAICAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081103', N'08', N'0811', N'CHALLABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081104', N'08', N'0811', N'COLQUEPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081105', N'08', N'0811', N'HUANCARANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081106', N'08', N'0811', N'KOS+ÆIPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081201', N'08', N'0812', N'URCOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081202', N'08', N'0812', N'ANDAHUAYLILLAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081203', N'08', N'0812', N'CAMANTI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081204', N'08', N'0812', N'CCARHUAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081205', N'08', N'0812', N'CCATCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081206', N'08', N'0812', N'CUSIPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081207', N'08', N'0812', N'HUARO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081208', N'08', N'0812', N'LUCRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081209', N'08', N'0812', N'MARCAPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081210', N'08', N'0812', N'OCONGATE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081211', N'08', N'0812', N'OROPESA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081212', N'08', N'0812', N'QUIQUIJANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081301', N'08', N'0813', N'URUBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081302', N'08', N'0813', N'CHINCHERO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081303', N'08', N'0813', N'HUAYLLABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081304', N'08', N'0813', N'MACHUPICCHU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081305', N'08', N'0813', N'MARAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081306', N'08', N'0813', N'OLLANTAYTAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'081307', N'08', N'0813', N'YUCAY')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090101', N'09', N'0901', N'HUANCAVELICA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090102', N'09', N'0901', N'ACOBAMBILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090103', N'09', N'0901', N'ACORIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090104', N'09', N'0901', N'CONAYCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090105', N'09', N'0901', N'CUENCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090106', N'09', N'0901', N'HUACHOCOLPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090107', N'09', N'0901', N'HUAYLLAHUARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090108', N'09', N'0901', N'IZCUCHACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090109', N'09', N'0901', N'LARIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090110', N'09', N'0901', N'MANTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090111', N'09', N'0901', N'MARISCAL CACERES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090112', N'09', N'0901', N'MOYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090113', N'09', N'0901', N'NUEVO OCCORO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090114', N'09', N'0901', N'PALCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090115', N'09', N'0901', N'PILCHACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090116', N'09', N'0901', N'VILCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090117', N'09', N'0901', N'YAULI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090118', N'09', N'0901', N'ASCENSION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090119', N'09', N'0901', N'HUANDO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090201', N'09', N'0902', N'ACOBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090202', N'09', N'0902', N'ANDABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090203', N'09', N'0902', N'ANTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090204', N'09', N'0902', N'CAJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090205', N'09', N'0902', N'MARCAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090206', N'09', N'0902', N'PAUCARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090207', N'09', N'0902', N'POMACOCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090208', N'09', N'0902', N'ROSARIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090301', N'09', N'0903', N'LIRCAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090302', N'09', N'0903', N'ANCHONGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090303', N'09', N'0903', N'CALLANMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090304', N'09', N'0903', N'CCOCHACCASA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090305', N'09', N'0903', N'CHINCHO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090306', N'09', N'0903', N'CONGALLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090307', N'09', N'0903', N'HUANCA-HUANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090308', N'09', N'0903', N'HUAYLLAY GRANDE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090309', N'09', N'0903', N'JULCAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090310', N'09', N'0903', N'SAN ANTONIO DE ANTAPARCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090311', N'09', N'0903', N'SANTO TOMAS DE PATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090312', N'09', N'0903', N'SECCLLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090401', N'09', N'0904', N'CASTROVIRREYNA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090402', N'09', N'0904', N'ARMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090403', N'09', N'0904', N'AURAHUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090404', N'09', N'0904', N'CAPILLAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090405', N'09', N'0904', N'CHUPAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090406', N'09', N'0904', N'COCAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090407', N'09', N'0904', N'HUACHOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090408', N'09', N'0904', N'HUAMATAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090409', N'09', N'0904', N'MOLLEPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090410', N'09', N'0904', N'SAN JUAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090411', N'09', N'0904', N'SANTA ANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090412', N'09', N'0904', N'TANTARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090413', N'09', N'0904', N'TICRAPO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090501', N'09', N'0905', N'CHURCAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090502', N'09', N'0905', N'ANCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090503', N'09', N'0905', N'CHINCHIHUASI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090504', N'09', N'0905', N'EL CARMEN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090505', N'09', N'0905', N'LA MERCED')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090506', N'09', N'0905', N'LOCROJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090507', N'09', N'0905', N'PAUCARBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090508', N'09', N'0905', N'SAN MIGUEL DE MAYOCC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090509', N'09', N'0905', N'SAN PEDRO DE CORIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090510', N'09', N'0905', N'PACHAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090511', N'09', N'0905', N'COSME')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090601', N'09', N'0906', N'HUAYTARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090602', N'09', N'0906', N'AYAVI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090603', N'09', N'0906', N'CORDOVA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090604', N'09', N'0906', N'HUAYACUNDO ARMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090605', N'09', N'0906', N'LARAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090606', N'09', N'0906', N'OCOYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090607', N'09', N'0906', N'PILPICHACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090608', N'09', N'0906', N'QUERCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090609', N'09', N'0906', N'QUITO-ARMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090610', N'09', N'0906', N'SAN ANTONIO DE CUSICANCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090611', N'09', N'0906', N'SAN FRANCISCO DE SANGAYAICO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090612', N'09', N'0906', N'SAN ISIDRO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090613', N'09', N'0906', N'SANTIAGO DE CHOCORVOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090614', N'09', N'0906', N'SANTIAGO DE QUIRAHUARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090615', N'09', N'0906', N'SANTO DOMINGO DE CAPILLAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090616', N'09', N'0906', N'TAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090701', N'09', N'0907', N'PAMPAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090702', N'09', N'0907', N'ACOSTAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090703', N'09', N'0907', N'ACRAQUIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090704', N'09', N'0907', N'AHUAYCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090705', N'09', N'0907', N'COLCABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090706', N'09', N'0907', N'DANIEL HERNANDEZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090707', N'09', N'0907', N'HUACHOCOLPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090709', N'09', N'0907', N'HUARIBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090710', N'09', N'0907', N'+ÆAHUIMPUQUIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090711', N'09', N'0907', N'PAZOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090713', N'09', N'0907', N'QUISHUAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090714', N'09', N'0907', N'SALCABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090715', N'09', N'0907', N'SALCAHUASI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090716', N'09', N'0907', N'SAN MARCOS DE ROCCHAC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090717', N'09', N'0907', N'SURCUBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090718', N'09', N'0907', N'TINTAY PUNCU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090719', N'09', N'0907', N'QUICHUAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'090720', N'09', N'0907', N'ANDAYMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100101', N'10', N'1001', N'HUANUCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100102', N'10', N'1001', N'AMARILIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100103', N'10', N'1001', N'CHINCHAO')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100104', N'10', N'1001', N'CHURUBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100105', N'10', N'1001', N'MARGOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100106', N'10', N'1001', N'QUISQUI (KICHKI)')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100107', N'10', N'1001', N'SAN FRANCISCO DE CAYRAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100108', N'10', N'1001', N'SAN PEDRO DE CHAULAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100109', N'10', N'1001', N'SANTA MARIA DEL VALLE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100110', N'10', N'1001', N'YARUMAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100111', N'10', N'1001', N'PILLCO MARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100112', N'10', N'1001', N'YACUS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100201', N'10', N'1002', N'AMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100202', N'10', N'1002', N'CAYNA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100203', N'10', N'1002', N'COLPAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100204', N'10', N'1002', N'CONCHAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100205', N'10', N'1002', N'HUACAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100206', N'10', N'1002', N'SAN FRANCISCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100207', N'10', N'1002', N'SAN RAFAEL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100208', N'10', N'1002', N'TOMAY KICHWA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100301', N'10', N'1003', N'LA UNION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100307', N'10', N'1003', N'CHUQUIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100311', N'10', N'1003', N'MARIAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100313', N'10', N'1003', N'PACHAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100316', N'10', N'1003', N'QUIVILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100317', N'10', N'1003', N'RIPAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100321', N'10', N'1003', N'SHUNQUI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100322', N'10', N'1003', N'SILLAPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100323', N'10', N'1003', N'YANAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100401', N'10', N'1004', N'HUACAYBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100402', N'10', N'1004', N'CANCHABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100403', N'10', N'1004', N'COCHABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100404', N'10', N'1004', N'PINRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100501', N'10', N'1005', N'LLATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100502', N'10', N'1005', N'ARANCAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100503', N'10', N'1005', N'CHAVIN DE PARIARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100504', N'10', N'1005', N'JACAS GRANDE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100505', N'10', N'1005', N'JIRCAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100506', N'10', N'1005', N'MIRAFLORES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100507', N'10', N'1005', N'MONZON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100508', N'10', N'1005', N'PUNCHAO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100509', N'10', N'1005', N'PU+ÆOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100510', N'10', N'1005', N'SINGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100511', N'10', N'1005', N'TANTAMAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100601', N'10', N'1006', N'RUPA-RUPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100602', N'10', N'1006', N'DANIEL ALOMIA ROBLES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100603', N'10', N'1006', N'HERMILIO VALDIZAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100604', N'10', N'1006', N'JOSE CRESPO Y CASTILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100605', N'10', N'1006', N'LUYANDO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100606', N'10', N'1006', N'MARIANO DAMASO BERAUN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100701', N'10', N'1007', N'HUACRACHUCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100702', N'10', N'1007', N'CHOLON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100703', N'10', N'1007', N'SAN BUENAVENTURA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100801', N'10', N'1008', N'PANAO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100802', N'10', N'1008', N'CHAGLLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100803', N'10', N'1008', N'MOLINO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100804', N'10', N'1008', N'UMARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100901', N'10', N'1009', N'PUERTO INCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100902', N'10', N'1009', N'CODO DEL POZUZO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100903', N'10', N'1009', N'HONORIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100904', N'10', N'1009', N'TOURNAVISTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'100905', N'10', N'1009', N'YUYAPICHIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101001', N'10', N'1010', N'JESUS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101002', N'10', N'1010', N'BA+ÆOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101003', N'10', N'1010', N'JIVIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101004', N'10', N'1010', N'QUEROPALCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101005', N'10', N'1010', N'RONDOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101006', N'10', N'1010', N'SAN FRANCISCO DE ASIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101007', N'10', N'1010', N'SAN MIGUEL DE CAURI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101101', N'10', N'1011', N'CHAVINILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101102', N'10', N'1011', N'CAHUAC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101103', N'10', N'1011', N'CHACABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101104', N'10', N'1011', N'APARICIO POMARES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101105', N'10', N'1011', N'JACAS CHICO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101106', N'10', N'1011', N'OBAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101107', N'10', N'1011', N'PAMPAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'101108', N'10', N'1011', N'CHORAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110101', N'11', N'1101', N'ICA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110102', N'11', N'1101', N'LA TINGUI+ÆA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110103', N'11', N'1101', N'LOS AQUIJES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110104', N'11', N'1101', N'OCUCAJE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110105', N'11', N'1101', N'PACHACUTEC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110106', N'11', N'1101', N'PARCONA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110107', N'11', N'1101', N'PUEBLO NUEVO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110108', N'11', N'1101', N'SALAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110109', N'11', N'1101', N'SAN JOSE DE LOS MOLINOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110110', N'11', N'1101', N'SAN JUAN BAUTISTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110111', N'11', N'1101', N'SANTIAGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110112', N'11', N'1101', N'SUBTANJALLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110113', N'11', N'1101', N'TATE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110114', N'11', N'1101', N'YAUCA DEL ROSARIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110201', N'11', N'1102', N'CHINCHA ALTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110202', N'11', N'1102', N'ALTO LARAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110203', N'11', N'1102', N'CHAVIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110204', N'11', N'1102', N'CHINCHA BAJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110205', N'11', N'1102', N'EL CARMEN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110206', N'11', N'1102', N'GROCIO PRADO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110207', N'11', N'1102', N'PUEBLO NUEVO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110208', N'11', N'1102', N'SAN JUAN DE YANAC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110209', N'11', N'1102', N'SAN PEDRO DE HUACARPANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110210', N'11', N'1102', N'SUNAMPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110211', N'11', N'1102', N'TAMBO DE MORA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110301', N'11', N'1103', N'NASCA')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110302', N'11', N'1103', N'CHANGUILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110303', N'11', N'1103', N'EL INGENIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110304', N'11', N'1103', N'MARCONA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110305', N'11', N'1103', N'VISTA ALEGRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110401', N'11', N'1104', N'PALPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110402', N'11', N'1104', N'LLIPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110403', N'11', N'1104', N'RIO GRANDE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110404', N'11', N'1104', N'SANTA CRUZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110405', N'11', N'1104', N'TIBILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110501', N'11', N'1105', N'PISCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110502', N'11', N'1105', N'HUANCANO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110503', N'11', N'1105', N'HUMAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110504', N'11', N'1105', N'INDEPENDENCIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110505', N'11', N'1105', N'PARACAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110506', N'11', N'1105', N'SAN ANDRES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110507', N'11', N'1105', N'SAN CLEMENTE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'110508', N'11', N'1105', N'TUPAC AMARU INCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120101', N'12', N'1201', N'HUANCAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120104', N'12', N'1201', N'CARHUACALLANGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120105', N'12', N'1201', N'CHACAPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120106', N'12', N'1201', N'CHICCHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120107', N'12', N'1201', N'CHILCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120108', N'12', N'1201', N'CHONGOS ALTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120111', N'12', N'1201', N'CHUPURO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120112', N'12', N'1201', N'COLCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120113', N'12', N'1201', N'CULLHUAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120114', N'12', N'1201', N'EL TAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120116', N'12', N'1201', N'HUACRAPUQUIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120117', N'12', N'1201', N'HUALHUAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120119', N'12', N'1201', N'HUANCAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120120', N'12', N'1201', N'HUASICANCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120121', N'12', N'1201', N'HUAYUCACHI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120122', N'12', N'1201', N'INGENIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120124', N'12', N'1201', N'PARIAHUANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120125', N'12', N'1201', N'PILCOMAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120126', N'12', N'1201', N'PUCARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120127', N'12', N'1201', N'QUICHUAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120128', N'12', N'1201', N'QUILCAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120129', N'12', N'1201', N'SAN AGUSTIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120130', N'12', N'1201', N'SAN JERONIMO DE TUNAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120132', N'12', N'1201', N'SA+ÆO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120133', N'12', N'1201', N'SAPALLANGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120134', N'12', N'1201', N'SICAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120135', N'12', N'1201', N'SANTO DOMINGO DE ACOBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120136', N'12', N'1201', N'VIQUES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120201', N'12', N'1202', N'CONCEPCION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120202', N'12', N'1202', N'ACO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120203', N'12', N'1202', N'ANDAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120204', N'12', N'1202', N'CHAMBARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120205', N'12', N'1202', N'COCHAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120206', N'12', N'1202', N'COMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120207', N'12', N'1202', N'HEROINAS TOLEDO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120208', N'12', N'1202', N'MANZANARES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120209', N'12', N'1202', N'MARISCAL CASTILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120210', N'12', N'1202', N'MATAHUASI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120211', N'12', N'1202', N'MITO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120212', N'12', N'1202', N'NUEVE DE JULIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120213', N'12', N'1202', N'ORCOTUNA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120214', N'12', N'1202', N'SAN JOSE DE QUERO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120215', N'12', N'1202', N'SANTA ROSA DE OCOPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120301', N'12', N'1203', N'CHANCHAMAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120302', N'12', N'1203', N'PERENE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120303', N'12', N'1203', N'PICHANAQUI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120304', N'12', N'1203', N'SAN LUIS DE SHUARO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120305', N'12', N'1203', N'SAN RAMON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120306', N'12', N'1203', N'VITOC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120401', N'12', N'1204', N'JAUJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120402', N'12', N'1204', N'ACOLLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120403', N'12', N'1204', N'APATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120404', N'12', N'1204', N'ATAURA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120405', N'12', N'1204', N'CANCHAYLLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120406', N'12', N'1204', N'CURICACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120407', N'12', N'1204', N'EL MANTARO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120408', N'12', N'1204', N'HUAMALI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120409', N'12', N'1204', N'HUARIPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120410', N'12', N'1204', N'HUERTAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120411', N'12', N'1204', N'JANJAILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120412', N'12', N'1204', N'JULCAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120413', N'12', N'1204', N'LEONOR ORDO+ÆEZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120414', N'12', N'1204', N'LLOCLLAPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120415', N'12', N'1204', N'MARCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120416', N'12', N'1204', N'MASMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120417', N'12', N'1204', N'MASMA CHICCHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120418', N'12', N'1204', N'MOLINOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120419', N'12', N'1204', N'MONOBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120420', N'12', N'1204', N'MUQUI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120421', N'12', N'1204', N'MUQUIYAUYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120422', N'12', N'1204', N'PACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120423', N'12', N'1204', N'PACCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120424', N'12', N'1204', N'PANCAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120425', N'12', N'1204', N'PARCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120426', N'12', N'1204', N'POMACANCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120427', N'12', N'1204', N'RICRAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120428', N'12', N'1204', N'SAN LORENZO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120429', N'12', N'1204', N'SAN PEDRO DE CHUNAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120430', N'12', N'1204', N'SAUSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120431', N'12', N'1204', N'SINCOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120432', N'12', N'1204', N'TUNAN MARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120433', N'12', N'1204', N'YAULI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120434', N'12', N'1204', N'YAUYOS')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120501', N'12', N'1205', N'JUNIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120502', N'12', N'1205', N'CARHUAMAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120503', N'12', N'1205', N'ONDORES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120504', N'12', N'1205', N'ULCUMAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120601', N'12', N'1206', N'SATIPO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120602', N'12', N'1206', N'COVIRIALI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120603', N'12', N'1206', N'LLAYLLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120604', N'12', N'1206', N'MAZAMARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120605', N'12', N'1206', N'PAMPA HERMOSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120606', N'12', N'1206', N'PANGOA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120607', N'12', N'1206', N'RIO NEGRO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120608', N'12', N'1206', N'RIO TAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120701', N'12', N'1207', N'TARMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120702', N'12', N'1207', N'ACOBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120703', N'12', N'1207', N'HUARICOLCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120704', N'12', N'1207', N'HUASAHUASI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120705', N'12', N'1207', N'LA UNION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120706', N'12', N'1207', N'PALCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120707', N'12', N'1207', N'PALCAMAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120708', N'12', N'1207', N'SAN PEDRO DE CAJAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120709', N'12', N'1207', N'TAPO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120801', N'12', N'1208', N'LA OROYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120802', N'12', N'1208', N'CHACAPALPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120803', N'12', N'1208', N'HUAY-HUAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120804', N'12', N'1208', N'MARCAPOMACOCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120805', N'12', N'1208', N'MOROCOCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120806', N'12', N'1208', N'PACCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120807', N'12', N'1208', N'SANTA BARBARA DE CARHUACAYAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120808', N'12', N'1208', N'SANTA ROSA DE SACCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120809', N'12', N'1208', N'SUITUCANCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120810', N'12', N'1208', N'YAULI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120901', N'12', N'1209', N'CHUPACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120902', N'12', N'1209', N'AHUAC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120903', N'12', N'1209', N'CHONGOS BAJO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120904', N'12', N'1209', N'HUACHAC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120905', N'12', N'1209', N'HUAMANCACA CHICO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120906', N'12', N'1209', N'SAN JUAN DE ISCOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120907', N'12', N'1209', N'SAN JUAN DE JARPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120908', N'12', N'1209', N'TRES DE DICIEMBRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'120909', N'12', N'1209', N'YANACANCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130101', N'13', N'1301', N'TRUJILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130102', N'13', N'1301', N'EL PORVENIR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130103', N'13', N'1301', N'FLORENCIA DE MORA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130104', N'13', N'1301', N'HUANCHACO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130105', N'13', N'1301', N'LA ESPERANZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130106', N'13', N'1301', N'LAREDO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130107', N'13', N'1301', N'MOCHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130108', N'13', N'1301', N'POROTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130109', N'13', N'1301', N'SALAVERRY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130110', N'13', N'1301', N'SIMBAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130111', N'13', N'1301', N'VICTOR LARCO HERRERA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130201', N'13', N'1302', N'ASCOPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130202', N'13', N'1302', N'CHICAMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130203', N'13', N'1302', N'CHOCOPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130204', N'13', N'1302', N'MAGDALENA DE CAO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130205', N'13', N'1302', N'PAIJAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130206', N'13', N'1302', N'RAZURI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130207', N'13', N'1302', N'SANTIAGO DE CAO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130208', N'13', N'1302', N'CASA GRANDE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130301', N'13', N'1303', N'BOLIVAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130302', N'13', N'1303', N'BAMBAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130303', N'13', N'1303', N'CONDORMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130304', N'13', N'1303', N'LONGOTEA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130305', N'13', N'1303', N'UCHUMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130306', N'13', N'1303', N'UCUNCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130401', N'13', N'1304', N'CHEPEN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130402', N'13', N'1304', N'PACANGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130403', N'13', N'1304', N'PUEBLO NUEVO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130501', N'13', N'1305', N'JULCAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130502', N'13', N'1305', N'CALAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130503', N'13', N'1305', N'CARABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130504', N'13', N'1305', N'HUASO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130601', N'13', N'1306', N'OTUZCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130602', N'13', N'1306', N'AGALLPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130604', N'13', N'1306', N'CHARAT')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130605', N'13', N'1306', N'HUARANCHAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130606', N'13', N'1306', N'LA CUESTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130608', N'13', N'1306', N'MACHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130610', N'13', N'1306', N'PARANDAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130611', N'13', N'1306', N'SALPO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130613', N'13', N'1306', N'SINSICAP')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130614', N'13', N'1306', N'USQUIL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130701', N'13', N'1307', N'SAN PEDRO DE LLOC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130702', N'13', N'1307', N'GUADALUPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130703', N'13', N'1307', N'JEQUETEPEQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130704', N'13', N'1307', N'PACASMAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130705', N'13', N'1307', N'SAN JOSE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130801', N'13', N'1308', N'TAYABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130802', N'13', N'1308', N'BULDIBUYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130803', N'13', N'1308', N'CHILLIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130804', N'13', N'1308', N'HUANCASPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130805', N'13', N'1308', N'HUAYLILLAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130806', N'13', N'1308', N'HUAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130807', N'13', N'1308', N'ONGON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130808', N'13', N'1308', N'PARCOY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130809', N'13', N'1308', N'PATAZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130810', N'13', N'1308', N'PIAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130811', N'13', N'1308', N'SANTIAGO DE CHALLAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130812', N'13', N'1308', N'TAURIJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130813', N'13', N'1308', N'URPAY')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130901', N'13', N'1309', N'HUAMACHUCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130902', N'13', N'1309', N'CHUGAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130903', N'13', N'1309', N'COCHORCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130904', N'13', N'1309', N'CURGOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130905', N'13', N'1309', N'MARCABAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130906', N'13', N'1309', N'SANAGORAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130907', N'13', N'1309', N'SARIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'130908', N'13', N'1309', N'SARTIMBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131001', N'13', N'1310', N'SANTIAGO DE CHUCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131002', N'13', N'1310', N'ANGASMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131003', N'13', N'1310', N'CACHICADAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131004', N'13', N'1310', N'MOLLEBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131005', N'13', N'1310', N'MOLLEPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131006', N'13', N'1310', N'QUIRUVILCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131007', N'13', N'1310', N'SANTA CRUZ DE CHUCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131008', N'13', N'1310', N'SITABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131101', N'13', N'1311', N'CASCAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131102', N'13', N'1311', N'LUCMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131103', N'13', N'1311', N'MARMOT')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131104', N'13', N'1311', N'SAYAPULLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131201', N'13', N'1312', N'VIRU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131202', N'13', N'1312', N'CHAO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'131203', N'13', N'1312', N'GUADALUPITO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140101', N'14', N'1401', N'CHICLAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140102', N'14', N'1401', N'CHONGOYAPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140103', N'14', N'1401', N'ETEN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140104', N'14', N'1401', N'ETEN PUERTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140105', N'14', N'1401', N'JOSE LEONARDO ORTIZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140106', N'14', N'1401', N'LA VICTORIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140107', N'14', N'1401', N'LAGUNAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140108', N'14', N'1401', N'MONSEFU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140109', N'14', N'1401', N'NUEVA ARICA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140110', N'14', N'1401', N'OYOTUN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140111', N'14', N'1401', N'PICSI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140112', N'14', N'1401', N'PIMENTEL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140113', N'14', N'1401', N'REQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140114', N'14', N'1401', N'SANTA ROSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140115', N'14', N'1401', N'SA+ÆA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140116', N'14', N'1401', N'CAYALTI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140117', N'14', N'1401', N'PATAPO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140118', N'14', N'1401', N'POMALCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140119', N'14', N'1401', N'PUCALA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140120', N'14', N'1401', N'TUMAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140201', N'14', N'1402', N'FERRE+ÆAFE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140202', N'14', N'1402', N'CA+ÆARIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140203', N'14', N'1402', N'INCAHUASI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140204', N'14', N'1402', N'MANUEL ANTONIO MESONES MURO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140205', N'14', N'1402', N'PITIPO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140206', N'14', N'1402', N'PUEBLO NUEVO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140301', N'14', N'1403', N'LAMBAYEQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140302', N'14', N'1403', N'CHOCHOPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140303', N'14', N'1403', N'ILLIMO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140304', N'14', N'1403', N'JAYANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140305', N'14', N'1403', N'MOCHUMI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140306', N'14', N'1403', N'MORROPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140307', N'14', N'1403', N'MOTUPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140308', N'14', N'1403', N'OLMOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140309', N'14', N'1403', N'PACORA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140310', N'14', N'1403', N'SALAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140311', N'14', N'1403', N'SAN JOSE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'140312', N'14', N'1403', N'TUCUME')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150101', N'15', N'1501', N'LIMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150102', N'15', N'1501', N'ANCON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150103', N'15', N'1501', N'ATE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150104', N'15', N'1501', N'BARRANCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150105', N'15', N'1501', N'BRE+ÆA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150106', N'15', N'1501', N'CARABAYLLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150107', N'15', N'1501', N'CHACLACAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150108', N'15', N'1501', N'CHORRILLOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150109', N'15', N'1501', N'CIENEGUILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150110', N'15', N'1501', N'COMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150111', N'15', N'1501', N'EL AGUSTINO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150112', N'15', N'1501', N'INDEPENDENCIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150113', N'15', N'1501', N'JESUS MARIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150114', N'15', N'1501', N'LA MOLINA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150115', N'15', N'1501', N'LA VICTORIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150116', N'15', N'1501', N'LINCE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150117', N'15', N'1501', N'LOS OLIVOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150118', N'15', N'1501', N'LURIGANCHO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150119', N'15', N'1501', N'LURIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150120', N'15', N'1501', N'MAGDALENA DEL MAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150121', N'15', N'1501', N'PUEBLO LIBRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150122', N'15', N'1501', N'MIRAFLORES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150123', N'15', N'1501', N'PACHACAMAC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150124', N'15', N'1501', N'PUCUSANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150125', N'15', N'1501', N'PUENTE PIEDRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150126', N'15', N'1501', N'PUNTA HERMOSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150127', N'15', N'1501', N'PUNTA NEGRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150128', N'15', N'1501', N'RIMAC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150129', N'15', N'1501', N'SAN BARTOLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150130', N'15', N'1501', N'SAN BORJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150131', N'15', N'1501', N'SAN ISIDRO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150132', N'15', N'1501', N'SAN JUAN DE LURIGANCHO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150133', N'15', N'1501', N'SAN JUAN DE MIRAFLORES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150134', N'15', N'1501', N'SAN LUIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150135', N'15', N'1501', N'SAN MARTIN DE PORRES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150136', N'15', N'1501', N'SAN MIGUEL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150137', N'15', N'1501', N'SANTA ANITA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150138', N'15', N'1501', N'SANTA MARIA DEL MAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150139', N'15', N'1501', N'SANTA ROSA')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150140', N'15', N'1501', N'SANTIAGO DE SURCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150141', N'15', N'1501', N'SURQUILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150142', N'15', N'1501', N'VILLA EL SALVADOR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150143', N'15', N'1501', N'VILLA MARIA DEL TRIUNFO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150201', N'15', N'1502', N'BARRANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150202', N'15', N'1502', N'PARAMONGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150203', N'15', N'1502', N'PATIVILCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150204', N'15', N'1502', N'SUPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150205', N'15', N'1502', N'SUPE PUERTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150301', N'15', N'1503', N'CAJATAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150302', N'15', N'1503', N'COPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150303', N'15', N'1503', N'GORGOR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150304', N'15', N'1503', N'HUANCAPON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150305', N'15', N'1503', N'MANAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150401', N'15', N'1504', N'CANTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150402', N'15', N'1504', N'ARAHUAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150403', N'15', N'1504', N'HUAMANTANGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150404', N'15', N'1504', N'HUAROS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150405', N'15', N'1504', N'LACHAQUI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150406', N'15', N'1504', N'SAN BUENAVENTURA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150407', N'15', N'1504', N'SANTA ROSA DE QUIVES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150501', N'15', N'1505', N'SAN VICENTE DE CA+ÆETE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150502', N'15', N'1505', N'ASIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150503', N'15', N'1505', N'CALANGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150504', N'15', N'1505', N'CERRO AZUL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150505', N'15', N'1505', N'CHILCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150506', N'15', N'1505', N'COAYLLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150507', N'15', N'1505', N'IMPERIAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150508', N'15', N'1505', N'LUNAHUANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150509', N'15', N'1505', N'MALA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150510', N'15', N'1505', N'NUEVO IMPERIAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150511', N'15', N'1505', N'PACARAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150512', N'15', N'1505', N'QUILMANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150513', N'15', N'1505', N'SAN ANTONIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150514', N'15', N'1505', N'SAN LUIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150515', N'15', N'1505', N'SANTA CRUZ DE FLORES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150516', N'15', N'1505', N'ZU+ÆIGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150601', N'15', N'1506', N'HUARAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150602', N'15', N'1506', N'ATAVILLOS ALTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150603', N'15', N'1506', N'ATAVILLOS BAJO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150604', N'15', N'1506', N'AUCALLAMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150605', N'15', N'1506', N'CHANCAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150606', N'15', N'1506', N'IHUARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150607', N'15', N'1506', N'LAMPIAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150608', N'15', N'1506', N'PACARAOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150609', N'15', N'1506', N'SAN MIGUEL DE ACOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150610', N'15', N'1506', N'SANTA CRUZ DE ANDAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150611', N'15', N'1506', N'SUMBILCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150612', N'15', N'1506', N'VEINTISIETE DE NOVIEMBRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150701', N'15', N'1507', N'MATUCANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150702', N'15', N'1507', N'ANTIOQUIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150703', N'15', N'1507', N'CALLAHUANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150704', N'15', N'1507', N'CARAMPOMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150705', N'15', N'1507', N'CHICLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150706', N'15', N'1507', N'CUENCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150707', N'15', N'1507', N'HUACHUPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150708', N'15', N'1507', N'HUANZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150709', N'15', N'1507', N'HUAROCHIRI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150710', N'15', N'1507', N'LAHUAYTAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150711', N'15', N'1507', N'LANGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150712', N'15', N'1507', N'LARAOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150713', N'15', N'1507', N'MARIATANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150714', N'15', N'1507', N'RICARDO PALMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150715', N'15', N'1507', N'SAN ANDRES DE TUPICOCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150716', N'15', N'1507', N'SAN ANTONIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150717', N'15', N'1507', N'SAN BARTOLOME')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150718', N'15', N'1507', N'SAN DAMIAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150719', N'15', N'1507', N'SAN JUAN DE IRIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150720', N'15', N'1507', N'SAN JUAN DE TANTARANCHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150721', N'15', N'1507', N'SAN LORENZO DE QUINTI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150722', N'15', N'1507', N'SAN MATEO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150723', N'15', N'1507', N'SAN MATEO DE OTAO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150724', N'15', N'1507', N'SAN PEDRO DE CASTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150725', N'15', N'1507', N'SAN PEDRO DE HUANCAYRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150726', N'15', N'1507', N'SANGALLAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150727', N'15', N'1507', N'SANTA CRUZ DE COCACHACRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150728', N'15', N'1507', N'SANTA EULALIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150729', N'15', N'1507', N'SANTIAGO DE ANCHUCAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150730', N'15', N'1507', N'SANTIAGO DE TUNA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150731', N'15', N'1507', N'SANTO DOMINGO DE LOS OLLEROS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150732', N'15', N'1507', N'SURCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150801', N'15', N'1508', N'HUACHO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150802', N'15', N'1508', N'AMBAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150803', N'15', N'1508', N'CALETA DE CARQUIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150804', N'15', N'1508', N'CHECRAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150805', N'15', N'1508', N'HUALMAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150806', N'15', N'1508', N'HUAURA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150807', N'15', N'1508', N'LEONCIO PRADO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150808', N'15', N'1508', N'PACCHO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150809', N'15', N'1508', N'SANTA LEONOR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150810', N'15', N'1508', N'SANTA MARIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150811', N'15', N'1508', N'SAYAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150812', N'15', N'1508', N'VEGUETA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150901', N'15', N'1509', N'OYON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150902', N'15', N'1509', N'ANDAJES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150903', N'15', N'1509', N'CAUJUL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150904', N'15', N'1509', N'COCHAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150905', N'15', N'1509', N'NAVAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'150906', N'15', N'1509', N'PACHANGARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151001', N'15', N'1510', N'YAUYOS')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151002', N'15', N'1510', N'ALIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151003', N'15', N'1510', N'ALLAUCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151004', N'15', N'1510', N'AYAVIRI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151005', N'15', N'1510', N'AZANGARO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151006', N'15', N'1510', N'CACRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151007', N'15', N'1510', N'CARANIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151008', N'15', N'1510', N'CATAHUASI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151009', N'15', N'1510', N'CHOCOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151010', N'15', N'1510', N'COCHAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151011', N'15', N'1510', N'COLONIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151012', N'15', N'1510', N'HONGOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151013', N'15', N'1510', N'HUAMPARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151014', N'15', N'1510', N'HUANCAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151015', N'15', N'1510', N'HUANGASCAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151016', N'15', N'1510', N'HUANTAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151017', N'15', N'1510', N'HUA+ÆEC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151018', N'15', N'1510', N'LARAOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151019', N'15', N'1510', N'LINCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151020', N'15', N'1510', N'MADEAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151021', N'15', N'1510', N'MIRAFLORES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151022', N'15', N'1510', N'OMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151023', N'15', N'1510', N'PUTINZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151024', N'15', N'1510', N'QUINCHES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151025', N'15', N'1510', N'QUINOCAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151026', N'15', N'1510', N'SAN JOAQUIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151027', N'15', N'1510', N'SAN PEDRO DE PILAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151028', N'15', N'1510', N'TANTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151029', N'15', N'1510', N'TAURIPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151030', N'15', N'1510', N'TOMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151031', N'15', N'1510', N'TUPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151032', N'15', N'1510', N'VI+ÆAC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'151033', N'15', N'1510', N'VITIS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160101', N'16', N'1601', N'IQUITOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160102', N'16', N'1601', N'ALTO NANAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160103', N'16', N'1601', N'FERNANDO LORES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160104', N'16', N'1601', N'INDIANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160105', N'16', N'1601', N'LAS AMAZONAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160106', N'16', N'1601', N'MAZAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160107', N'16', N'1601', N'NAPO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160108', N'16', N'1601', N'PUNCHANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160110', N'16', N'1601', N'TORRES CAUSANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160112', N'16', N'1601', N'BELEN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160113', N'16', N'1601', N'SAN JUAN BAUTISTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160201', N'16', N'1602', N'YURIMAGUAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160202', N'16', N'1602', N'BALSAPUERTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160205', N'16', N'1602', N'JEBEROS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160206', N'16', N'1602', N'LAGUNAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160210', N'16', N'1602', N'SANTA CRUZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160211', N'16', N'1602', N'TENIENTE CESAR LOPEZ ROJAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160301', N'16', N'1603', N'NAUTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160302', N'16', N'1603', N'PARINARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160303', N'16', N'1603', N'TIGRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160304', N'16', N'1603', N'TROMPETEROS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160305', N'16', N'1603', N'URARINAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160401', N'16', N'1604', N'RAMON CASTILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160402', N'16', N'1604', N'PEBAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160403', N'16', N'1604', N'YAVARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160404', N'16', N'1604', N'SAN PABLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160501', N'16', N'1605', N'REQUENA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160502', N'16', N'1605', N'ALTO TAPICHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160503', N'16', N'1605', N'CAPELO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160504', N'16', N'1605', N'EMILIO SAN MARTIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160505', N'16', N'1605', N'MAQUIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160506', N'16', N'1605', N'PUINAHUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160507', N'16', N'1605', N'SAQUENA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160508', N'16', N'1605', N'SOPLIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160509', N'16', N'1605', N'TAPICHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160510', N'16', N'1605', N'JENARO HERRERA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160511', N'16', N'1605', N'YAQUERANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160601', N'16', N'1606', N'CONTAMANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160602', N'16', N'1606', N'INAHUAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160603', N'16', N'1606', N'PADRE MARQUEZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160604', N'16', N'1606', N'PAMPA HERMOSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160605', N'16', N'1606', N'SARAYACU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160606', N'16', N'1606', N'VARGAS GUERRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160701', N'16', N'1607', N'BARRANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160702', N'16', N'1607', N'CAHUAPANAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160703', N'16', N'1607', N'MANSERICHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160704', N'16', N'1607', N'MORONA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160705', N'16', N'1607', N'PASTAZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160706', N'16', N'1607', N'ANDOAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160801', N'16', N'1608', N'PUTUMAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160802', N'16', N'1608', N'ROSA PANDURO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160803', N'16', N'1608', N'TENIENTE MANUEL CLAVERO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'160804', N'16', N'1608', N'YAGUAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'170101', N'17', N'1701', N'TAMBOPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'170102', N'17', N'1701', N'INAMBARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'170103', N'17', N'1701', N'LAS PIEDRAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'170104', N'17', N'1701', N'LABERINTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'170201', N'17', N'1702', N'MANU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'170202', N'17', N'1702', N'FITZCARRALD')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'170203', N'17', N'1702', N'MADRE DE DIOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'170204', N'17', N'1702', N'HUEPETUHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'170301', N'17', N'1703', N'I+ÆAPARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'170302', N'17', N'1703', N'IBERIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'170303', N'17', N'1703', N'TAHUAMANU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180101', N'18', N'1801', N'MOQUEGUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180102', N'18', N'1801', N'CARUMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180103', N'18', N'1801', N'CUCHUMBAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180104', N'18', N'1801', N'SAMEGUA')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180105', N'18', N'1801', N'SAN CRISTOBAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180106', N'18', N'1801', N'TORATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180201', N'18', N'1802', N'OMATE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180202', N'18', N'1802', N'CHOJATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180203', N'18', N'1802', N'COALAQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180204', N'18', N'1802', N'ICHU+ÆA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180205', N'18', N'1802', N'LA CAPILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180206', N'18', N'1802', N'LLOQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180207', N'18', N'1802', N'MATALAQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180208', N'18', N'1802', N'PUQUINA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180209', N'18', N'1802', N'QUINISTAQUILLAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180210', N'18', N'1802', N'UBINAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180211', N'18', N'1802', N'YUNGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180301', N'18', N'1803', N'ILO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180302', N'18', N'1803', N'EL ALGARROBAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'180303', N'18', N'1803', N'PACOCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190101', N'19', N'1901', N'CHAUPIMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190102', N'19', N'1901', N'HUACHON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190103', N'19', N'1901', N'HUARIACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190104', N'19', N'1901', N'HUAYLLAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190105', N'19', N'1901', N'NINACACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190106', N'19', N'1901', N'PALLANCHACRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190107', N'19', N'1901', N'PAUCARTAMBO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190108', N'19', N'1901', N'SAN FRANCISCO DE ASIS DE YARUSYACAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190109', N'19', N'1901', N'SIMON BOLIVAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190110', N'19', N'1901', N'TICLACAYAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190111', N'19', N'1901', N'TINYAHUARCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190112', N'19', N'1901', N'VICCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190113', N'19', N'1901', N'YANACANCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190201', N'19', N'1902', N'YANAHUANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190202', N'19', N'1902', N'CHACAYAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190203', N'19', N'1902', N'GOYLLARISQUIZGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190204', N'19', N'1902', N'PAUCAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190205', N'19', N'1902', N'SAN PEDRO DE PILLAO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190206', N'19', N'1902', N'SANTA ANA DE TUSI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190207', N'19', N'1902', N'TAPUC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190208', N'19', N'1902', N'VILCABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190301', N'19', N'1903', N'OXAPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190302', N'19', N'1903', N'CHONTABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190303', N'19', N'1903', N'HUANCABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190304', N'19', N'1903', N'PALCAZU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190305', N'19', N'1903', N'POZUZO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190306', N'19', N'1903', N'PUERTO BERMUDEZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190307', N'19', N'1903', N'VILLA RICA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'190308', N'19', N'1903', N'CONSTITUCION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200101', N'20', N'2001', N'PIURA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200104', N'20', N'2001', N'CASTILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200105', N'20', N'2001', N'CATACAOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200107', N'20', N'2001', N'CURA MORI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200108', N'20', N'2001', N'EL TALLAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200109', N'20', N'2001', N'LA ARENA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200110', N'20', N'2001', N'LA UNION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200111', N'20', N'2001', N'LAS LOMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200114', N'20', N'2001', N'TAMBO GRANDE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200115', N'20', N'2001', N'VEINTISEIS DE OCTUBRE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200201', N'20', N'2002', N'AYABACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200202', N'20', N'2002', N'FRIAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200203', N'20', N'2002', N'JILILI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200204', N'20', N'2002', N'LAGUNAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200205', N'20', N'2002', N'MONTERO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200206', N'20', N'2002', N'PACAIPAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200207', N'20', N'2002', N'PAIMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200208', N'20', N'2002', N'SAPILLICA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200209', N'20', N'2002', N'SICCHEZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200210', N'20', N'2002', N'SUYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200301', N'20', N'2003', N'HUANCABAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200302', N'20', N'2003', N'CANCHAQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200303', N'20', N'2003', N'EL CARMEN DE LA FRONTERA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200304', N'20', N'2003', N'HUARMACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200305', N'20', N'2003', N'LALAQUIZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200306', N'20', N'2003', N'SAN MIGUEL DE EL FAIQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200307', N'20', N'2003', N'SONDOR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200308', N'20', N'2003', N'SONDORILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200401', N'20', N'2004', N'CHULUCANAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200402', N'20', N'2004', N'BUENOS AIRES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200403', N'20', N'2004', N'CHALACO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200404', N'20', N'2004', N'LA MATANZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200405', N'20', N'2004', N'MORROPON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200406', N'20', N'2004', N'SALITRAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200407', N'20', N'2004', N'SAN JUAN DE BIGOTE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200408', N'20', N'2004', N'SANTA CATALINA DE MOSSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200409', N'20', N'2004', N'SANTO DOMINGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200410', N'20', N'2004', N'YAMANGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200501', N'20', N'2005', N'PAITA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200502', N'20', N'2005', N'AMOTAPE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200503', N'20', N'2005', N'ARENAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200504', N'20', N'2005', N'COLAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200505', N'20', N'2005', N'LA HUACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200506', N'20', N'2005', N'TAMARINDO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200507', N'20', N'2005', N'VICHAYAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200601', N'20', N'2006', N'SULLANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200602', N'20', N'2006', N'BELLAVISTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200603', N'20', N'2006', N'IGNACIO ESCUDERO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200604', N'20', N'2006', N'LANCONES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200605', N'20', N'2006', N'MARCAVELICA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200606', N'20', N'2006', N'MIGUEL CHECA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200607', N'20', N'2006', N'QUERECOTILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200608', N'20', N'2006', N'SALITRAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200701', N'20', N'2007', N'PARI+ÆAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200702', N'20', N'2007', N'EL ALTO')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200703', N'20', N'2007', N'LA BREA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200704', N'20', N'2007', N'LOBITOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200705', N'20', N'2007', N'LOS ORGANOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200706', N'20', N'2007', N'MANCORA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200801', N'20', N'2008', N'SECHURA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200802', N'20', N'2008', N'BELLAVISTA DE LA UNION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200803', N'20', N'2008', N'BERNAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200804', N'20', N'2008', N'CRISTO NOS VALGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200805', N'20', N'2008', N'VICE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'200806', N'20', N'2008', N'RINCONADA LLICUAR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210101', N'21', N'2101', N'PUNO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210102', N'21', N'2101', N'ACORA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210103', N'21', N'2101', N'AMANTANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210104', N'21', N'2101', N'ATUNCOLLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210105', N'21', N'2101', N'CAPACHICA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210106', N'21', N'2101', N'CHUCUITO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210107', N'21', N'2101', N'COATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210108', N'21', N'2101', N'HUATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210109', N'21', N'2101', N'MA+ÆAZO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210110', N'21', N'2101', N'PAUCARCOLLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210111', N'21', N'2101', N'PICHACANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210112', N'21', N'2101', N'PLATERIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210113', N'21', N'2101', N'SAN ANTONIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210114', N'21', N'2101', N'TIQUILLACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210115', N'21', N'2101', N'VILQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210201', N'21', N'2102', N'AZANGARO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210202', N'21', N'2102', N'ACHAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210203', N'21', N'2102', N'ARAPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210204', N'21', N'2102', N'ASILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210205', N'21', N'2102', N'CAMINACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210206', N'21', N'2102', N'CHUPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210207', N'21', N'2102', N'JOSE DOMINGO CHOQUEHUANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210208', N'21', N'2102', N'MU+ÆANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210209', N'21', N'2102', N'POTONI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210210', N'21', N'2102', N'SAMAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210211', N'21', N'2102', N'SAN ANTON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210212', N'21', N'2102', N'SAN JOSE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210213', N'21', N'2102', N'SAN JUAN DE SALINAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210214', N'21', N'2102', N'SANTIAGO DE PUPUJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210215', N'21', N'2102', N'TIRAPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210301', N'21', N'2103', N'MACUSANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210302', N'21', N'2103', N'AJOYANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210303', N'21', N'2103', N'AYAPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210304', N'21', N'2103', N'COASA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210305', N'21', N'2103', N'CORANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210306', N'21', N'2103', N'CRUCERO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210307', N'21', N'2103', N'ITUATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210308', N'21', N'2103', N'OLLACHEA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210309', N'21', N'2103', N'SAN GABAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210310', N'21', N'2103', N'USICAYOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210401', N'21', N'2104', N'JULI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210402', N'21', N'2104', N'DESAGUADERO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210403', N'21', N'2104', N'HUACULLANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210404', N'21', N'2104', N'KELLUYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210405', N'21', N'2104', N'PISACOMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210406', N'21', N'2104', N'POMATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210407', N'21', N'2104', N'ZEPITA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210501', N'21', N'2105', N'ILAVE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210502', N'21', N'2105', N'CAPAZO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210503', N'21', N'2105', N'PILCUYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210504', N'21', N'2105', N'SANTA ROSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210505', N'21', N'2105', N'CONDURIRI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210601', N'21', N'2106', N'HUANCANE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210602', N'21', N'2106', N'COJATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210603', N'21', N'2106', N'HUATASANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210604', N'21', N'2106', N'INCHUPALLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210605', N'21', N'2106', N'PUSI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210606', N'21', N'2106', N'ROSASPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210607', N'21', N'2106', N'TARACO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210608', N'21', N'2106', N'VILQUE CHICO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210701', N'21', N'2107', N'LAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210702', N'21', N'2107', N'CABANILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210703', N'21', N'2107', N'CALAPUJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210704', N'21', N'2107', N'NICASIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210705', N'21', N'2107', N'OCUVIRI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210706', N'21', N'2107', N'PALCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210707', N'21', N'2107', N'PARATIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210708', N'21', N'2107', N'PUCARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210709', N'21', N'2107', N'SANTA LUCIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210710', N'21', N'2107', N'VILAVILA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210801', N'21', N'2108', N'AYAVIRI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210802', N'21', N'2108', N'ANTAUTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210803', N'21', N'2108', N'CUPI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210804', N'21', N'2108', N'LLALLI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210805', N'21', N'2108', N'MACARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210806', N'21', N'2108', N'NU+ÆOA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210807', N'21', N'2108', N'ORURILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210808', N'21', N'2108', N'SANTA ROSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210809', N'21', N'2108', N'UMACHIRI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210901', N'21', N'2109', N'MOHO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210902', N'21', N'2109', N'CONIMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210903', N'21', N'2109', N'HUAYRAPATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'210904', N'21', N'2109', N'TILALI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211001', N'21', N'2110', N'PUTINA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211002', N'21', N'2110', N'ANANEA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211003', N'21', N'2110', N'PEDRO VILCA APAZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211004', N'21', N'2110', N'QUILCAPUNCU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211005', N'21', N'2110', N'SINA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211101', N'21', N'2111', N'JULIACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211102', N'21', N'2111', N'CABANA')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211103', N'21', N'2111', N'CABANILLAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211104', N'21', N'2111', N'CARACOTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211201', N'21', N'2112', N'SANDIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211202', N'21', N'2112', N'CUYOCUYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211203', N'21', N'2112', N'LIMBANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211204', N'21', N'2112', N'PATAMBUCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211205', N'21', N'2112', N'PHARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211206', N'21', N'2112', N'QUIACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211207', N'21', N'2112', N'SAN JUAN DEL ORO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211208', N'21', N'2112', N'YANAHUAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211209', N'21', N'2112', N'ALTO INAMBARI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211210', N'21', N'2112', N'SAN PEDRO DE PUTINA PUNCO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211301', N'21', N'2113', N'YUNGUYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211302', N'21', N'2113', N'ANAPIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211303', N'21', N'2113', N'COPANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211304', N'21', N'2113', N'CUTURAPI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211305', N'21', N'2113', N'OLLARAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211306', N'21', N'2113', N'TINICACHI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'211307', N'21', N'2113', N'UNICACHI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220101', N'22', N'2201', N'MOYOBAMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220102', N'22', N'2201', N'CALZADA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220103', N'22', N'2201', N'HABANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220104', N'22', N'2201', N'JEPELACIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220105', N'22', N'2201', N'SORITOR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220106', N'22', N'2201', N'YANTALO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220201', N'22', N'2202', N'BELLAVISTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220202', N'22', N'2202', N'ALTO BIAVO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220203', N'22', N'2202', N'BAJO BIAVO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220204', N'22', N'2202', N'HUALLAGA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220205', N'22', N'2202', N'SAN PABLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220206', N'22', N'2202', N'SAN RAFAEL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220301', N'22', N'2203', N'SAN JOSE DE SISA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220302', N'22', N'2203', N'AGUA BLANCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220303', N'22', N'2203', N'SAN MARTIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220304', N'22', N'2203', N'SANTA ROSA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220305', N'22', N'2203', N'SHATOJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220401', N'22', N'2204', N'SAPOSOA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220402', N'22', N'2204', N'ALTO SAPOSOA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220403', N'22', N'2204', N'EL ESLABON')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220404', N'22', N'2204', N'PISCOYACU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220405', N'22', N'2204', N'SACANCHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220406', N'22', N'2204', N'TINGO DE SAPOSOA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220501', N'22', N'2205', N'LAMAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220502', N'22', N'2205', N'ALONSO DE ALVARADO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220503', N'22', N'2205', N'BARRANQUITA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220504', N'22', N'2205', N'CAYNARACHI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220505', N'22', N'2205', N'CU+ÆUMBUQUI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220506', N'22', N'2205', N'PINTO RECODO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220507', N'22', N'2205', N'RUMISAPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220508', N'22', N'2205', N'SAN ROQUE DE CUMBAZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220509', N'22', N'2205', N'SHANAO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220510', N'22', N'2205', N'TABALOSOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220511', N'22', N'2205', N'ZAPATERO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220601', N'22', N'2206', N'JUANJUI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220602', N'22', N'2206', N'CAMPANILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220603', N'22', N'2206', N'HUICUNGO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220604', N'22', N'2206', N'PACHIZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220605', N'22', N'2206', N'PAJARILLO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220701', N'22', N'2207', N'PICOTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220702', N'22', N'2207', N'BUENOS AIRES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220703', N'22', N'2207', N'CASPISAPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220704', N'22', N'2207', N'PILLUANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220705', N'22', N'2207', N'PUCACACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220706', N'22', N'2207', N'SAN CRISTOBAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220707', N'22', N'2207', N'SAN HILARION')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220708', N'22', N'2207', N'SHAMBOYACU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220709', N'22', N'2207', N'TINGO DE PONASA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220710', N'22', N'2207', N'TRES UNIDOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220801', N'22', N'2208', N'RIOJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220802', N'22', N'2208', N'AWAJUN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220803', N'22', N'2208', N'ELIAS SOPLIN VARGAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220804', N'22', N'2208', N'NUEVA CAJAMARCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220805', N'22', N'2208', N'PARDO MIGUEL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220806', N'22', N'2208', N'POSIC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220807', N'22', N'2208', N'SAN FERNANDO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220808', N'22', N'2208', N'YORONGOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220809', N'22', N'2208', N'YURACYACU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220901', N'22', N'2209', N'TARAPOTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220902', N'22', N'2209', N'ALBERTO LEVEAU')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220903', N'22', N'2209', N'CACATACHI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220904', N'22', N'2209', N'CHAZUTA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220905', N'22', N'2209', N'CHIPURANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220906', N'22', N'2209', N'EL PORVENIR')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220907', N'22', N'2209', N'HUIMBAYOC')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220908', N'22', N'2209', N'JUAN GUERRA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220909', N'22', N'2209', N'LA BANDA DE SHILCAYO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220910', N'22', N'2209', N'MORALES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220911', N'22', N'2209', N'PAPAPLAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220912', N'22', N'2209', N'SAN ANTONIO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220913', N'22', N'2209', N'SAUCE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'220914', N'22', N'2209', N'SHAPAJA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'221001', N'22', N'2210', N'TOCACHE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'221002', N'22', N'2210', N'NUEVO PROGRESO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'221003', N'22', N'2210', N'POLVORA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'221004', N'22', N'2210', N'SHUNTE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'221005', N'22', N'2210', N'UCHIZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230101', N'23', N'2301', N'TACNA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230102', N'23', N'2301', N'ALTO DE LA ALIANZA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230103', N'23', N'2301', N'CALANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230104', N'23', N'2301', N'CIUDAD NUEVA')
GO
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230105', N'23', N'2301', N'INCLAN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230106', N'23', N'2301', N'PACHIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230107', N'23', N'2301', N'PALCA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230108', N'23', N'2301', N'POCOLLAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230109', N'23', N'2301', N'SAMA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230110', N'23', N'2301', N'CORONEL GREGORIO ALBARRACIN LANCHIPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230201', N'23', N'2302', N'CANDARAVE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230202', N'23', N'2302', N'CAIRANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230203', N'23', N'2302', N'CAMILACA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230204', N'23', N'2302', N'CURIBAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230205', N'23', N'2302', N'HUANUARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230206', N'23', N'2302', N'QUILAHUANI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230301', N'23', N'2303', N'LOCUMBA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230302', N'23', N'2303', N'ILABAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230303', N'23', N'2303', N'ITE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230401', N'23', N'2304', N'TARATA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230402', N'23', N'2304', N'HEROES ALBARRACIN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230403', N'23', N'2304', N'ESTIQUE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230404', N'23', N'2304', N'ESTIQUE-PAMPA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230405', N'23', N'2304', N'SITAJARA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230406', N'23', N'2304', N'SUSAPAYA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230407', N'23', N'2304', N'TARUCACHI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'230408', N'23', N'2304', N'TICACO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240101', N'24', N'2401', N'TUMBES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240102', N'24', N'2401', N'CORRALES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240103', N'24', N'2401', N'LA CRUZ')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240104', N'24', N'2401', N'PAMPAS DE HOSPITAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240105', N'24', N'2401', N'SAN JACINTO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240106', N'24', N'2401', N'SAN JUAN DE LA VIRGEN')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240201', N'24', N'2402', N'ZORRITOS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240202', N'24', N'2402', N'CASITAS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240203', N'24', N'2402', N'CANOAS DE PUNTA SAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240301', N'24', N'2403', N'ZARUMILLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240302', N'24', N'2403', N'AGUAS VERDES')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240303', N'24', N'2403', N'MATAPALO')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'240304', N'24', N'2403', N'PAPAYAL')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250101', N'25', N'2501', N'CALLERIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250102', N'25', N'2501', N'CAMPOVERDE')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250103', N'25', N'2501', N'IPARIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250104', N'25', N'2501', N'MASISEA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250105', N'25', N'2501', N'YARINACOCHA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250106', N'25', N'2501', N'NUEVA REQUENA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250107', N'25', N'2501', N'MANANTAY')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250201', N'25', N'2502', N'RAYMONDI')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250202', N'25', N'2502', N'SEPAHUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250203', N'25', N'2502', N'TAHUANIA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250204', N'25', N'2502', N'YURUA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250301', N'25', N'2503', N'PADRE ABAD')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250302', N'25', N'2503', N'IRAZOLA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250303', N'25', N'2503', N'CURIMANA')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'250401', N'25', N'2504', N'PURUS')
INSERT [General].[Distrito] ([IdDistrito], [IdDepartamento], [IdProvincia], [Descripcion]) VALUES (N'N0N0N0', N'N0', N'N0N0', N'SIN DATOS')
GO
INSERT [General].[Familia] ([IdFamilia], [Descripcion], [Estado], [EditorUser]) VALUES (N'000001', N'NUEVA FAMILAI DE PRODUCTO', 1, N'Administrador')
INSERT [General].[Familia] ([IdFamilia], [Descripcion], [Estado], [EditorUser]) VALUES (N'000002', N'ANTIGUA FAMILIA', 1, N'Administrador')
INSERT [General].[Familia] ([IdFamilia], [Descripcion], [Estado], [EditorUser]) VALUES (N'000003', N'FAMILIAR  EDITADO', 1, N'Administrador')
GO
INSERT [General].[Marca] ([IdMarca], [NombreMarca], [Descripcion], [Prioridad], [IdProveedor], [Estado], [EditorUser]) VALUES (N'M00001', N'SPORT CENTRE', N'MARCAD EPORTIVA', 2, N'000001', 1, N'Administrador')
INSERT [General].[Marca] ([IdMarca], [NombreMarca], [Descripcion], [Prioridad], [IdProveedor], [Estado], [EditorUser]) VALUES (N'M00002', N'JJPK - EDITED', N'MARCA DE  FUTBOL CCCC', 6, N'000002', 1, N'Administrador')
GO
INSERT [General].[PrecioNivel] ([IdNivel], [Descripcion], [Estado]) VALUES (N'01', N'PRECIO GENERAL', 1)
INSERT [General].[PrecioNivel] ([IdNivel], [Descripcion], [Estado]) VALUES (N'02', N'PRECIO PROVEEDOR', 1)
INSERT [General].[PrecioNivel] ([IdNivel], [Descripcion], [Estado]) VALUES (N'03', N'PRECIO ESPECIAL', 1)
GO
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000001', N'P00001', N'01', CAST(35.50 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000002', N'P00002', N'01', CAST(22.50 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000003', N'P00003', N'01', CAST(75.00 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000004', N'P00004', N'01', CAST(25.00 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000005', N'P00005', N'01', CAST(96.80 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000006', N'P00001', N'02', CAST(33.50 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000007', N'P00002', N'02', CAST(19.50 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000008', N'P00003', N'02', CAST(71.00 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000009', N'P00004', N'02', CAST(23.00 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000010', N'P00005', N'02', CAST(92.50 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000011', N'P00001', N'03', CAST(31.50 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000012', N'P00002', N'03', CAST(16.50 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000013', N'P00003', N'03', CAST(68.00 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000014', N'P00004', N'03', CAST(21.00 AS Decimal(10, 2)), 1)
INSERT [General].[PrecioProducto] ([IdPrecio], [IdProducto], [IdNivel], [Precio], [Estado]) VALUES (N'000015', N'P00005', N'03', CAST(85.60 AS Decimal(10, 2)), 1)
GO
INSERT [General].[Proveedor] ([IdProveedor], [NombreProveedor], [Ubigeo], [Direccion], [Urbanizacion], [TelefFijo], [TelefMovil], [Email], [Estado], [EditorUser]) VALUES (N'000001', N'JUAN CARLOS PEREZ', N'150107', N'CALLE LOS PERDIDOS, 546', N'NONE', N'988462222', N'232322323', N'JCAPEREZ@MAIL.COLM', 1, N'Administrador')
INSERT [General].[Proveedor] ([IdProveedor], [NombreProveedor], [Ubigeo], [Direccion], [Urbanizacion], [TelefFijo], [TelefMovil], [Email], [Estado], [EditorUser]) VALUES (N'000002', N'RICARDO SANCHEZ', N'140303', N'AV. MIGUEL GRAUD 456', N'LAS LOMAS', N'95656565', N'2112121', N'jperes@mail.com', 1, N'Administrador')
GO
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0101', N'01', N'CHACHAPOYAS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0102', N'01', N'BAGUA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0103', N'01', N'BONGARÁ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0104', N'01', N'CONDORCANQUI')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0105', N'01', N'LUYA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0106', N'01', N'RODRÍGUEZ DE MENDOZA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0107', N'01', N'UTCUBAMBA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0201', N'02', N'HUARAZ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0202', N'02', N'AIJA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0203', N'02', N'ANTONIO RAYMONDI')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0204', N'02', N'ASUNCIÓN')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0205', N'02', N'BOLOGNESI')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0206', N'02', N'CARHUAZ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0207', N'02', N'CARLOS FERMÍN FITZCARRALD')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0208', N'02', N'CASMA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0209', N'02', N'CORONGO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0210', N'02', N'HUARI')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0211', N'02', N'HUARMEY')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0212', N'02', N'HUAYLAS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0213', N'02', N'MARISCAL LUZURIAGA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0214', N'02', N'OCROS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0215', N'02', N'PALLASCA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0216', N'02', N'POMABAMBA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0217', N'02', N'RECUAY')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0218', N'02', N'SANTA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0219', N'02', N'SIHUAS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0220', N'02', N'YUNGAY')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0301', N'03', N'ABANCAY')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0302', N'03', N'ANDAHUAYLAS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0303', N'03', N'ANTABAMBA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0304', N'03', N'AYMARAES')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0305', N'03', N'COTABAMBAS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0306', N'03', N'CHINCHEROS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0307', N'03', N'GRAU')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0401', N'04', N'AREQUIPA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0402', N'04', N'CAMANÁ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0403', N'04', N'CARAVELÍ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0404', N'04', N'CASTILLA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0405', N'04', N'CAYLLOMA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0406', N'04', N'CONDESUYOS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0407', N'04', N'ISLAY')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0408', N'04', N'LA UNIÒN')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0501', N'05', N'HUAMANGA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0502', N'05', N'CANGALLO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0503', N'05', N'HUANCA SANCOS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0504', N'05', N'HUANTA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0505', N'05', N'LA MAR')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0506', N'05', N'LUCANAS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0507', N'05', N'PARINACOCHAS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0508', N'05', N'PÀUCAR DEL SARA SARA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0509', N'05', N'SUCRE')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0510', N'05', N'VÍCTOR FAJARDO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0511', N'05', N'VILCAS HUAMÁN')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0601', N'06', N'CAJAMARCA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0602', N'06', N'CAJABAMBA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0603', N'06', N'CELENDÍN')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0604', N'06', N'CHOTA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0605', N'06', N'CONTUMAZÁ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0606', N'06', N'CUTERVO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0607', N'06', N'HUALGAYOC')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0608', N'06', N'JAÉN')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0609', N'06', N'SAN IGNACIO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0610', N'06', N'SAN MARCOS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0611', N'06', N'SAN MIGUEL')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0612', N'06', N'SAN PABLO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0613', N'06', N'SANTA CRUZ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0701', N'07', N'PROV. CONST. DEL CALLAO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0801', N'08', N'CUSCO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0802', N'08', N'ACOMAYO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0803', N'08', N'ANTA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0804', N'08', N'CALCA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0805', N'08', N'CANAS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0806', N'08', N'CANCHIS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0807', N'08', N'CHUMBIVILCAS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0808', N'08', N'ESPINAR')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0809', N'08', N'LA CONVENCIÓN')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0810', N'08', N'PARURO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0811', N'08', N'PAUCARTAMBO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0812', N'08', N'QUISPICANCHI')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0813', N'08', N'URUBAMBA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0901', N'09', N'HUANCAVELICA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0902', N'09', N'ACOBAMBA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0903', N'09', N'ANGARAES')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0904', N'09', N'CASTROVIRREYNA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0905', N'09', N'CHURCAMPA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0906', N'09', N'HUAYTARÁ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'0907', N'09', N'TAYACAJA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1001', N'10', N'HUÁNUCO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1002', N'10', N'AMBO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1003', N'10', N'DOS DE MAYO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1004', N'10', N'HUACAYBAMBA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1005', N'10', N'HUAMALÍES')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1006', N'10', N'LEONCIO PRADO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1007', N'10', N'MARAÑÓN')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1008', N'10', N'PACHITEA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1009', N'10', N'PUERTO INCA')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1010', N'10', N'LAURICOCHA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1011', N'10', N'YAROWILCA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1101', N'11', N'ICA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1102', N'11', N'CHINCHA ')
GO
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1103', N'11', N'NAZCA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1104', N'11', N'PALPA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1105', N'11', N'PISCO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1201', N'12', N'HUANCAYO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1202', N'12', N'CONCEPCIÓN ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1203', N'12', N'CHANCHAMAYO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1204', N'12', N'JAUJA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1205', N'12', N'JUNÍN ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1206', N'12', N'SATIPO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1207', N'12', N'TARMA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1208', N'12', N'YAULI ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1209', N'12', N'CHUPACA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1301', N'13', N'TRUJILLO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1302', N'13', N'ASCOPE ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1303', N'13', N'BOLÍVAR ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1304', N'13', N'CHEPÉN ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1305', N'13', N'JULCÁN ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1306', N'13', N'OTUZCO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1307', N'13', N'PACASMAYO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1308', N'13', N'PATAZ ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1309', N'13', N'SÁNCHEZ CARRIÓN ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1310', N'13', N'SANTIAGO DE CHUCO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1311', N'13', N'GRAN CHIMÚ ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1312', N'13', N'VIRÚ ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1401', N'14', N'CHICLAYO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1402', N'14', N'FERREÑAFE ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1403', N'14', N'LAMBAYEQUE ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1501', N'15', N'LIMA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1502', N'15', N'BARRANCA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1503', N'15', N'CAJATAMBO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1504', N'15', N'CANTA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1505', N'15', N'CAÑETE ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1506', N'15', N'HUARAL ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1507', N'15', N'HUAROCHIRÍ ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1508', N'15', N'HUAURA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1509', N'15', N'OYÓN ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1510', N'15', N'YAUYOS ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1601', N'16', N'MAYNAS ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1602', N'16', N'ALTO AMAZONAS ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1603', N'16', N'LORETO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1604', N'16', N'MARISCAL RAMÓN CASTILLA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1605', N'16', N'REQUENA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1606', N'16', N'UCAYALI ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1607', N'16', N'DATEM DEL MARAÑÓN ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1608', N'16', N'PUTUMAYO')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1701', N'17', N'TAMBOPATA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1702', N'17', N'MANU ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1703', N'17', N'TAHUAMANU ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1801', N'18', N'MARISCAL NIETO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1802', N'18', N'GENERAL SÁNCHEZ CERRO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1803', N'18', N'ILO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1901', N'19', N'PASCO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1902', N'19', N'DANIEL ALCIDES CARRIÓN ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'1903', N'19', N'OXAPAMPA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2001', N'20', N'PIURA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2002', N'20', N'AYABACA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2003', N'20', N'HUANCABAMBA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2004', N'20', N'MORROPÓN ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2005', N'20', N'PAITA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2006', N'20', N'SULLANA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2007', N'20', N'TALARA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2008', N'20', N'SECHURA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2101', N'21', N'PUNO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2102', N'21', N'AZÁNGARO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2103', N'21', N'CARABAYA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2104', N'21', N'CHUCUITO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2105', N'21', N'EL COLLAO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2106', N'21', N'HUANCANÉ ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2107', N'21', N'LAMPA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2108', N'21', N'MELGAR ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2109', N'21', N'MOHO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2110', N'21', N'SAN ANTONIO DE PUTINA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2111', N'21', N'SAN ROMÁN ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2112', N'21', N'SANDIA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2113', N'21', N'YUNGUYO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2201', N'22', N'MOYOBAMBA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2202', N'22', N'BELLAVISTA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2203', N'22', N'EL DORADO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2204', N'22', N'HUALLAGA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2205', N'22', N'LAMAS ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2206', N'22', N'MARISCAL CÁCERES ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2207', N'22', N'PICOTA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2208', N'22', N'RIOJA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2209', N'22', N'SAN MARTÍN ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2210', N'22', N'TOCACHE ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2301', N'23', N'TACNA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2302', N'23', N'CANDARAVE ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2303', N'23', N'JORGE BASADRE ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2304', N'23', N'TARATA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2401', N'24', N'TUMBES ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2402', N'24', N'CONTRALMIRANTE VILLAR ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2403', N'24', N'ZARUMILLA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2501', N'25', N'CORONEL PORTILLO ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2502', N'25', N'ATALAYA ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2503', N'25', N'PADRE ABAD ')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'2504', N'25', N'PURÚS')
INSERT [General].[Provincia] ([IdProvincia], [IdDepartamento], [Descripcion]) VALUES (N'N0N0', N'N0', N'SIN DATOS')
GO
INSERT [General].[SubCategoria] ([IdSubCategoria], [NombreSubCategoria], [Descripcion], [IdCategoria], [Estado], [EditorUser]) VALUES (N'SC001', N'SUB PRUEBA 002 -EDITAFDO', N'DESCERIPCION P002', N'C0002', 1, N'Administrador')
INSERT [General].[SubCategoria] ([IdSubCategoria], [NombreSubCategoria], [Descripcion], [IdCategoria], [Estado], [EditorUser]) VALUES (N'SC002', N'SUB P 0001 - EDIT', N'DES 000 - EDIT', N'C0001', 1, N'Administrador')
INSERT [General].[SubCategoria] ([IdSubCategoria], [NombreSubCategoria], [Descripcion], [IdCategoria], [Estado], [EditorUser]) VALUES (N'SC003', N'3ERA SUB CATEG', N'DDDD', N'C0003', 1, N'Administrador')
INSERT [General].[SubCategoria] ([IdSubCategoria], [NombreSubCategoria], [Descripcion], [IdCategoria], [Estado], [EditorUser]) VALUES (N'SC004', N'CATEG 3ERA PRUEBA', N'XDD', N'C0003', 1, N'Administrador')
GO
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'01', N'000', N'01000', N'TIPO DE CLIENTE', N'TC', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'01', N'001', N'01001', N'PERSONA NATURAL', N'PERSONA', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'01', N'002', N'01002', N'EMPRESA', N'EMPRESA', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'02', N'000', N'02000', N'TIPO DE DOCUMENTO', N'TD', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'02', N'001', N'02001', N'DOC. NACIONAL DE INDENTIDAD', N'D.N.I.', 0, 8)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'02', N'002', N'02002', N'REG. UNICO DE CONTRIBUYENTE', N'R.U.C.', 0, 11)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'02', N'003', N'02003', N'CARNÉ DE EXTRANJERÍA', N'C.EXT.', 0, 9)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'03', N'000', N'03000', N'ENTIDAD BANCARIA', N'EB', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'03', N'001', N'03001', N'BANCO CONTINENTAL', N'BNK. CONT.', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'03', N'002', N'03002', N'BANCO DE CREDITO', N'BNK. CRED.', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'03', N'003', N'03003', N'BANCO DE LA NACION', N'BNK NAC.', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'03', N'004', N'03004', N'BANCO INTERBANK', N'INT. BNK', 0, 8)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'03', N'005', N'03005', N'BANCO SCOTIABANK', N'SCOT. BNK', 0, 11)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'04', N'000', N'04000', N'TIPO MONEDA', N'EB', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'04', N'001', N'04001', N'DOLARES', N'US$', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'04', N'002', N'04002', N'SOLES', N'S/.', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'05', N'000', N'05000', N'MÉTODOS DE PAGOS', N'MP', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'05', N'001', N'05001', N'EFECTIVO', N'US$', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'05', N'002', N'05002', N'CHEQUE', N'S/.', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'05', N'003', N'05003', N'TARJETA', N'US$', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'05', N'004', N'05004', N'CREDITO', N'S/.', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'05', N'005', N'05005', N'CUPON', N'US$', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'05', N'006', N'05006', N'TRANSFERENCIA', N'S/.', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'06', N'000', N'06000', N'TIPOS DE COMPROBANTE', N'TCM', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'06', N'001', N'06001', N'TICKET INTERNO', N'TIC', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'06', N'001', N'06002', N'BOLETA DE VENTA', N'BOL', 0, 0)
INSERT [General].[Tabla] ([NumeroTabla], [Correlativo], [IdTabla], [Descripcion], [Abreviatura], [Valor], [Dimension]) VALUES (N'06', N'001', N'06003', N'FACTURA', N'FAC', 0, 0)
GO
INSERT [General].[Talla] ([IdTalla], [NombreTalla], [Descripcion], [Estado], [EditorUser]) VALUES (N'T001', N'XS', N'EXTRA SAMLL', 1, N'Administrador')
INSERT [General].[Talla] ([IdTalla], [NombreTalla], [Descripcion], [Estado], [EditorUser]) VALUES (N'T002', N'L', N'LARGE', 1, N'Administrador')
INSERT [General].[Talla] ([IdTalla], [NombreTalla], [Descripcion], [Estado], [EditorUser]) VALUES (N'T003', N'XL', N'EXTRA ALRGE', 1, N'Administrador')
INSERT [General].[Talla] ([IdTalla], [NombreTalla], [Descripcion], [Estado], [EditorUser]) VALUES (N'T004', N'M', N'MEDIUM', 1, N'Administrador')
INSERT [General].[Talla] ([IdTalla], [NombreTalla], [Descripcion], [Estado], [EditorUser]) VALUES (N'T005', N'S', N'SMALL', 1, N'Administrador')
GO
INSERT [General].[Tienda] ([IdTienda], [NombreTienda], [Descripcion], [IdResponsable], [Consigna], [EditorUser], [Estado]) VALUES (N'A0001', N'SAN GABRIEL', N'ALAMCEN 001', 6, 0, N'Administrador', 1)
INSERT [General].[Tienda] ([IdTienda], [NombreTienda], [Descripcion], [IdResponsable], [Consigna], [EditorUser], [Estado]) VALUES (N'A0002', N'SAN NICOLAS', N'ALMACEN 002', 6, 0, N'Administrador', 1)
INSERT [General].[Tienda] ([IdTienda], [NombreTienda], [Descripcion], [IdResponsable], [Consigna], [EditorUser], [Estado]) VALUES (N'A0003', N'SAN FERNANDO', N'TIENDA 003', 5, 0, N'Administrador', 1)
GO
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00001', N'VENTA', N'S')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00002', N'TRASLADO', N' ')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00003', N'REPOSICION', N'E')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00004', N'FABRICACION', N'E')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00005', N'SALIDA CIERRE DE TIENDA', N'S')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00006', N'BAJA DE PRODUCTO', N'S')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00007', N'CAMBIO DE PRODUCTO', N'S')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00008', N'STOCK INICIAL', N'E')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00009', N'DEVOLUCION', N'E')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00010', N'RETORNO A PROVEEDOR', N'S')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00011', N'REAJUSTE DE INVENTARIO(SALIDA)', N'S')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00012', N'REAJUSTE DE INVENTARIO(ENTRADA)', N'E')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00013', N'INGRESO CIERRE DE TIENDA', N'E')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00014', N'EXPORTACION', N'S')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00015', N'PRODUCCION - CORTE', N'E')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00016', N'PRODUCCION - MERMA', N'S')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00017', N'COMPRA LOCAL', N'E')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00018', N'FABRICACION -TERCERO', N'E')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00019', N'IMPORTACION', N'E')
INSERT [General].[TipoMovimiento] ([IdTipoMov], [Descripcion], [Movimiento]) VALUES (N'00020', N'CAMBIO DE PRODUCTO - DEVOLUCION', N'E')
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Cliente__A4202588ADB1BF18]    Script Date: 31/01/2022 8:41:24 PM ******/
ALTER TABLE [dbo].[Cliente] ADD UNIQUE NONCLUSTERED 
(
	[NumeroDocumento] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Cliente__AEC642DC6630A71F]    Script Date: 31/01/2022 8:41:24 PM ******/
ALTER TABLE [dbo].[Cliente] ADD UNIQUE NONCLUSTERED 
(
	[RazonSocial] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Cliente__E0DD7E700C4EA04B]    Script Date: 31/01/2022 8:41:24 PM ******/
ALTER TABLE [dbo].[Cliente] ADD UNIQUE NONCLUSTERED 
(
	[CodigoCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Producto__74F263DE4883769F]    Script Date: 31/01/2022 8:41:24 PM ******/
ALTER TABLE [dbo].[Producto] ADD UNIQUE NONCLUSTERED 
(
	[NombreProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Categori__A21FBE9F83B3A61D]    Script Date: 31/01/2022 8:41:24 PM ******/
ALTER TABLE [General].[Categoria] ADD UNIQUE NONCLUSTERED 
(
	[NombreCategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Familia__92C53B6CB6622B5B]    Script Date: 31/01/2022 8:41:24 PM ******/
ALTER TABLE [General].[Familia] ADD UNIQUE NONCLUSTERED 
(
	[Descripcion] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Marca__42FE0ACB2575B98C]    Script Date: 31/01/2022 8:41:24 PM ******/
ALTER TABLE [General].[Marca] ADD UNIQUE NONCLUSTERED 
(
	[NombreMarca] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Proveedo__C20DF553ED414000]    Script Date: 31/01/2022 8:41:24 PM ******/
ALTER TABLE [General].[Proveedor] ADD UNIQUE NONCLUSTERED 
(
	[NombreProveedor] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__SubCateg__60BB2E24366675DA]    Script Date: 31/01/2022 8:41:24 PM ******/
ALTER TABLE [General].[SubCategoria] ADD UNIQUE NONCLUSTERED 
(
	[NombreSubCategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Talla__212212C19D2D987F]    Script Date: 31/01/2022 8:41:24 PM ******/
ALTER TABLE [General].[Talla] ADD UNIQUE NONCLUSTERED 
(
	[NombreTalla] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UQ__Tienda__53C2C7576F1CA51B]    Script Date: 31/01/2022 8:41:24 PM ******/
ALTER TABLE [General].[Tienda] ADD UNIQUE NONCLUSTERED 
(
	[NombreTienda] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Cliente] ADD  DEFAULT ('') FOR [IdNivelPrecio]
GO
ALTER TABLE [dbo].[Cliente] ADD  DEFAULT ((0)) FOR [LineaCredito]
GO
ALTER TABLE [dbo].[Cliente] ADD  DEFAULT ((0)) FOR [FlagCredito]
GO
ALTER TABLE [dbo].[Producto] ADD  DEFAULT ('') FOR [CodigoBarra]
GO
ALTER TABLE [dbo].[Inventario]  WITH CHECK ADD FOREIGN KEY([IdProducto])
REFERENCES [dbo].[Producto] ([IdProducto])
GO
ALTER TABLE [dbo].[Inventario]  WITH CHECK ADD FOREIGN KEY([IdTienda])
REFERENCES [General].[Tienda] ([IdTienda])
GO
ALTER TABLE [dbo].[Inventario]  WITH CHECK ADD FOREIGN KEY([IdTipoMov])
REFERENCES [General].[TipoMovimiento] ([IdTipoMov])
GO
ALTER TABLE [General].[Distrito]  WITH CHECK ADD FOREIGN KEY([IdDepartamento])
REFERENCES [General].[Departamento] ([IdDepartamento])
GO
ALTER TABLE [General].[Distrito]  WITH CHECK ADD FOREIGN KEY([IdProvincia])
REFERENCES [General].[Provincia] ([IdProvincia])
GO
ALTER TABLE [General].[Marca]  WITH CHECK ADD FOREIGN KEY([IdProveedor])
REFERENCES [General].[Proveedor] ([IdProveedor])
GO
ALTER TABLE [General].[Provincia]  WITH CHECK ADD FOREIGN KEY([IdDepartamento])
REFERENCES [General].[Departamento] ([IdDepartamento])
GO
ALTER TABLE [General].[SubCategoria]  WITH CHECK ADD FOREIGN KEY([IdCategoria])
REFERENCES [General].[Categoria] ([IdCategoria])
GO
ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD CHECK  (([CodigoCliente] like 'CD[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[Cliente]  WITH CHECK ADD CHECK  (([Sexo] like '[HME]'))
GO
ALTER TABLE [dbo].[Inventario]  WITH CHECK ADD CHECK  (([Cantidad]>(0)))
GO
ALTER TABLE [dbo].[Inventario]  WITH CHECK ADD CHECK  (([Condicion] like '[CP]'))
GO
ALTER TABLE [dbo].[Inventario]  WITH CHECK ADD CHECK  (([FlagMov] like '[ES]'))
GO
ALTER TABLE [dbo].[OrdenVenta]  WITH CHECK ADD CHECK  (([ImporteParcial]>=(0)))
GO
ALTER TABLE [dbo].[OrdenVenta]  WITH CHECK ADD CHECK  (([Situacion] like '[VOC]'))
GO
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD CHECK  (([Genero] like '[UMF]'))
GO
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD CHECK  (([IdProducto] like '[P][0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD CHECK  (([StokMinimo]>(0)))
GO
ALTER TABLE [dbo].[Stock]  WITH CHECK ADD CHECK  (([StokFinal]>=(0)))
GO
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD CHECK  (([ComprobanteInterno] like '[A-Z][A-Z][A-Z]-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD CHECK  (([ImporteTotal]>(0)))
GO
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD CHECK  (([NumComprobante] like '[A-Z][A-Z][A-Z]-[0-9][0-9][0-9][0-9]-[0-9][0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [dbo].[VentaPago]  WITH CHECK ADD CHECK  (([Importe]>(0)))
GO
ALTER TABLE [dbo].[VentaPago]  WITH CHECK ADD CHECK  (([Moneda] like '[SD]'))
GO
ALTER TABLE [General].[Categoria]  WITH CHECK ADD CHECK  (([IdCategoria] like '[C][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [General].[Familia]  WITH CHECK ADD CHECK  (([IdFamilia] like '[0-9][0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [General].[Marca]  WITH CHECK ADD CHECK  (([IdMarca] like '[M][0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [General].[Proveedor]  WITH CHECK ADD CHECK  (([IdProveedor] like '[0-9][0-9][0-9][0-9][0-9][0-9]'))
GO
ALTER TABLE [General].[SubCategoria]  WITH CHECK ADD CHECK  (([IdSubCategoria] like '[S][C][0-9][0-9][0-9]'))
GO
ALTER TABLE [General].[Talla]  WITH CHECK ADD CHECK  (([IdTalla] like '[T][0-9][0-9][0-9]'))
GO
ALTER TABLE [General].[Tienda]  WITH CHECK ADD CHECK  (([IdTienda] like 'A[0-9][0-9][0-9][0-9]'))
GO
/****** Object:  StoredProcedure [dbo].[SpBitacoraInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpBitacoraInsert](
	@NombreTabla varchar(70) 
	, @Accion varchar(20) 
	, @EditorUser varchar(20) 
	, @Evento varchar(max)
)
as begin
	insert into Bitacora
	(NombreTabla, Accion, EditorUser, FechaRegistro, Evento)
	values(@NombreTabla
		, @Accion
		, @EditorUser
		, convert(datetime, getdate())
		, @Evento
	);
end
GO
/****** Object:  StoredProcedure [dbo].[SpBitacoraList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpBitacoraList](
	@NombreTabla varchar(70)
)
as begin
	select IdBitacora
		, NombreTabla
		, Accion
		, EditorUser
		, FechaRegistro
		, Evento
	from Bitacora
	where NombreTabla=@NombreTabla
end
GO
/****** Object:  StoredProcedure [dbo].[SpClienteById]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpClienteById](
	@IdCliente VARCHAR(5)
)
AS BEGIN
	SELECT
		IdCliente, 
		IdTipoCliente, 
		IdTipoDocumento, 
		NumeroDocumento, 
		CodigoCliente, 
		Nombres, 
		Apellidos, 
		RazonSocial, 
		Sexo, 
		FechaNacimiento, 
		Rol, 
		Estado, 
		EditorUser,
		IdNivelPrecio,
		LineaCredito,
		FlagCredito
	FROM Cliente
	WHERE IdCliente=@IdCliente
END
GO
/****** Object:  StoredProcedure [dbo].[SpClienteList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpClienteList]
as begin
	select c.IdCliente
		, c.IdTipoCliente
		, tc.Abreviatura as TipoCliente
		, c.IdTipoDocumento
		, td.Abreviatura as TipoDocumento
		, c.NumeroDocumento
		, c.CodigoCliente
		, c.Nombres
		, c.Apellidos
		, c.RazonSocial
		, c.Sexo
		, c.FechaNacimiento
		, c.Rol
		, c.Estado
		, c.EditorUser
		, c.IdNivelPrecio+': '+pn.Descripcion AS NivelPrecio
		, c.LineaCredito
		, c.FlagCredito
	from Cliente c
	join General.Tabla tc on tc.IdTabla=c.IdTipoCliente
	join General.Tabla td on td.IdTabla=c.IdTipoDocumento
	LEFT JOIN General.PrecioNivel pn ON pn.IdNivel=c.IdNivelPrecio
end
GO
/****** Object:  StoredProcedure [dbo].[SpClienteMaintenance]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpClienteMaintenance](
	@IdCliente VARCHAR(5)
	, @IdTipoCliente varchar(5)
	, @IdTipoDocumento varchar(5)
	, @NumeroDocumento nvarchar(11)
	, @Nombres varchar(80)
	, @Apellidos varchar(80)
	, @Sexo char(1)
	, @FechaNacimiento date
	, @Rol varchar(max)
	, @EditorUser varchar(20)
	, @IdNivelPrecio VARCHAR(2)
	, @LineaCredito DECIMAL(10,2)
	, @FlagCredito BIT
	, @Option varchar(10) -- Insert, Update, Delete, Restore
)
as begin
	declare @CodigoCliente nvarchar(15);
	set @CodigoCliente=('CD'+right('0000000000000'+@NumeroDocumento,13));
	if(@Option='Insert')
		begin
			-- OBTENER CORRELATIVO
			SET @IdCliente=(
					SELECT RIGHT('00000'+CONVERT(VARCHAR,ISNULL(CONVERT(INT,MAX(IdCliente)),0)+1),5)
					FROM Cliente
				);
				
			Insert into Cliente
			(IdCliente, IdTipoCliente,IdTipoDocumento,NumeroDocumento,CodigoCliente,Nombres,Apellidos,RazonSocial,
			Sexo,FechaNacimiento,Rol,Estado,EditorUser, IdNivelPrecio, LineaCredito, FlagCredito)
			values(@IdCliente
				, @IdTipoCliente
				, @IdTipoDocumento
				, @NumeroDocumento
				, @CodigoCliente
				, @Nombres
				, @Apellidos
				, @Nombres+' '+@Apellidos -- Razon Social
				, @Sexo
				, @FechaNacimiento
				, @Rol
				, 1 --1:Activo, 0:Eliminado
				, @EditorUser
				, @IdNivelPrecio
				, @LineaCredito
				, @FlagCredito
				)
		end
	if(@Option='Update')
		begin
			Update Cliente
			set IdTipoCliente=@IdTipoCliente
				, IdTipoDocumento=@IdTipoDocumento
				, NumeroDocumento=@NumeroDocumento
				, CodigoCliente=@CodigoCliente
				, Nombres=@Nombres
				, Apellidos=@Apellidos
				, RazonSocial=@Nombres+' '+@Apellidos
				, Sexo=@Sexo
				, FechaNacimiento=@FechaNacimiento
				, Rol=@Rol
				, EditorUser=@EditorUser
				, IdNivelPrecio=@IdNivelPrecio
				, LineaCredito=@LineaCredito
				, FlagCredito=@FlagCredito
			where IdCliente=@IdCliente;
		end
	if(@Option='Delete')
		begin
			Update Cliente
			set Estado=0
				, EditorUser=@EditorUser
			where IdCliente=@IdCliente;
		end
	if(@Option='Restore')
		begin
			Update Cliente
			set Estado=1
				, EditorUser=@EditorUser
			where IdCliente=@IdCliente;
		end
end
GO
/****** Object:  StoredProcedure [dbo].[SpClienteNivelPrecioList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpClienteNivelPrecioList]
as begin
	select c.IdCliente
		, c.NumeroDocumento
		, c.CodigoCliente
		, c.RazonSocial
		, c.IdNivelPrecio+': '+pn.Descripcion AS NivelPrecio
	from Cliente c
	JOIN General.PrecioNivel pn ON pn.IdNivel=c.IdNivelPrecio
	WHERE c.Rol LIKE'%Cliente%'
	--join General.Tabla tc on tc.IdTabla=c.IdTipoCliente
	--join General.Tabla td on td.IdTabla=c.IdTipoDocumento
end
GO
/****** Object:  StoredProcedure [dbo].[SpEntradaInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpEntradaInsert](
	@IdEntrada VARCHAR(10) 
	, @IdTienda VARCHAR(5)  
	, @IdProveedor VARCHAR(6) 
	, @Documento VARCHAR(16) 
	, @EditorUser VARCHAR(20)
	--- Parametros para el inventario
	, @IdTipoMov VARCHAR(5) --Entrada: Reposición/Stock incicial, etc.
	, @IdProducto NVARCHAR(6) 
	, @Cantidad DECIMAL(10,3) 
	, @PrecioUnitario DECIMAL(10,2) --
	--, @DocMovimiento VARCHAR(16) 
	--, @Condicion CHAR(1) -- C:Completado / P:Pendiente-- Solo para Ventas
)
AS BEGIN
	BEGIN TRY
	BEGIN TRANSACTION
		-- Validamos si existe registro alguno que corresponda al ID del momento.
		IF((SELECT count(1) FROM Entrada WHERE IdEntrada=@IdEntrada)=0)
			BEGIN
				-- Si es Igual a "0" quiere decir que es el primer registro.
				INSERT INTO Entrada
				(IdEntrada, IdTienda, FechaEntrada,IdProveedor, Documento,EditorUser)
				VALUES(@IdEntrada,
					@IdTienda,
					GETDATE(),
					@IdProveedor,
					@Documento,
					@EditorUser
				)
			END
		-- Insertar Valores de productos en inventario 
		DECLARE @DocMovimiento VARCHAR(16)=@IdEntrada;
		execute SpInventarioInsert @IdTipoMov 
			, @IdTienda
			, @IdProducto 
			, @Cantidad 
			, @PrecioUnitario
			, @DocMovimiento
			, 'C';--@Condicion;
	END TRY
		BEGIN CATCH	
			BEGIN
				--Si Existe algún error, entonces se restablecerá la transacción.
				select (select error_message() as ErrorMessage)+' '+ 
				(Select error_procedure() AS ErrorProcedure)
				+' '+(select error_line() as ErrorLine)
				as OutMessage;

				IF(@@TRANCOUNT>0)
					ROLLBACK TRANSACTION
			END
		END CATCH
			BEGIN
				select '1' as OutMessage;
				-- Si no existe ningún erro, se va a confirmar la transacción.
				IF(@@TRANCOUNT>0)
					COMMIT TRANSACTION
			END

END



-- PR
GO
/****** Object:  StoredProcedure [dbo].[SpEntradaList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpEntradaList](
	@Desde DATE
	, @Hasta DATE
)
AS BEGIN
	SELECT e.IdEntrada,
		t.Descripcion as NombreTienda,
		e.FechaEntrada,
		p.NombreProveedor,
		e.Documento
	FROM Entrada e
	JOIN General.Tienda t ON t.IdTienda=e.IdTienda
	JOIN General.Proveedor p ON p.IdProveedor=e.IdProveedor
	WHERE e.FechaEntrada >= @Desde AND e.FechaEntrada<= @Hasta
END
GO
/****** Object:  StoredProcedure [dbo].[SpGenerateNewId10digits]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpGenerateNewId10digits](
	@tableName VARCHAR(100)
	, @columnIdName VARCHAR(50)
)
AS BEGIN
	DECLARE @Year VARCHAR(4)=(YEAR(GETDATE())), @sql NVARCHAR(MAX);

	SET @sql=('SELECT '''+@Year+'''+RIGHT(''000000''+CONVERT(VARCHAR,ISNULL(CONVERT(INT,MAX(RIGHT('+@columnIdName+',6))),0)+1),6)
	 FROM '+@tableName+'
	 WHERE LEFT('+@columnIdName+',4)='''+@Year+'''
	')
	exec sys.sp_executesql @sql;
END
GO
/****** Object:  StoredProcedure [dbo].[SpInventarioDelete]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInventarioDelete](
	@IdInventario VARCHAR(10)
)
AS BEGIN
	DELETE FROM Inventario
	WHERE IdInventario=@IdInventario
		AND Condicion='P'
END
GO
/****** Object:  StoredProcedure [dbo].[SpInventarioGenerateStock]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInventarioGenerateStock]
AS BEGIN
	-- GENERAR STOCK PARA TODAS LAS TIENDAS
	IF(OBJECT_ID('tempdb..#tmpStock'))IS NOT NULL
    DROP TABLE #tmpStock
	SELECT 
		IdTienda, IdProducto, SUM(Cantidad) as StockFinal
		INTO #tmpStock
	FROM 
	(
		SELECT IdTienda, IdProducto, SUM(Cantidad) as Cantidad
		FROM Inventario
		WHERE FlagMov='E' AND Estado=1
		GROUP BY IdTienda,IdProducto
		UNION
		SELECT IdTienda, IdProducto, SUM(Cantidad)*-1 as Cantidad
		FROM Inventario
		WHERE FlagMov='S' AND Estado=1
		GROUP BY IdTienda,IdProducto
	) AS tmp
	GROUP BY IdTienda, IdProducto

	/*
	-- TABLA TEMPORAL PARA ENTRADAS
	IF(OBJECT_ID('tempdb..#tmpEntrada'))IS NOT NULL
    DROP TABLE #tmpEntrada
	SELECT IdProducto, SUM(Cantidad) as Cantidad
	INTO #tmpEntrada
	FROM Inventario
	WHERE FlagMov='E'
	GROUP BY IdProducto

	-- TABLA TEMPORAL PARA SALIDA
	IF(OBJECT_ID('tempdb..#tmpSalida'))IS NOT NULL
    DROP TABLE #tmpSalida
	SELECT IdProducto, SUM(Cantidad) as Cantidad
	INTO #tmpSalida
	FROM Inventario
	WHERE FlagMov='S'
	GROUP BY IdProducto

	-- TABLA TEMPORAL PARA STOCK
	IF(OBJECT_ID('tempdb..#tmpStock'))IS NOT NULL
    DROP TABLE #tmpStock

	SELECT p.IdProducto
		, SUM(ISNULL(te.Cantidad,0))-SUM(ISNULL(ts.Cantidad,0)) as StockFinal
	INTO #tmpStock
	FROM Producto p
	LEFT JOIN #tmpEntrada te ON te.IdProducto=p.IdProducto
	LEFT JOIN #tmpSalida ts ON ts.IdProducto=p.IdProducto
	GROUP BY p.IdProducto
	*/

	-- ELIMINAR VALORES DEL STOCK
	
	-- INSETAR TABLA STOCK
	DELETE FROM Stock;
	INSERT INTO Stock
	SELECT ts.IdTienda, p.IdProducto, p.NombreProducto, p.PrecioBase as PrecioUnitario,
		p.StokMinimo, ts.StockFinal
	FROM Producto p
	JOIN #tmpStock ts ON ts.IdProducto=p.IdProducto
	--WHERE ts.StockFinal>0-- p.StokMinimo
END
GO
/****** Object:  StoredProcedure [dbo].[SpInventarioInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInventarioInsert](
	@IdTipoMov VARCHAR(5) 
	, @IdTienda VARCHAR(5)
	, @IdProducto NVARCHAR(6) 
	, @Cantidad DECIMAL(10,3) 
	, @PrecioUnitario DECIMAL(10,2) 
	, @DocMovimiento VARCHAR(16) 
	--, @EditorUser VARCHAR(20)
	, @Condicion CHAR(1)
)
AS BEGIN
	DECLARE @Year VARCHAR(4)=(YEAR(GETDATE()));
	DECLARE @IdInventario 
		VARCHAR(10)=(
			SELECT @Year+RIGHT('000000'+CONVERT
				(VARCHAR,ISNULL(CONVERT(INT,MAX(RIGHT(IdInventario,6))),0)+1),6)
			FROM Inventario
			WHERE LEFT(IdInventario,4)=@Year
		);

	DECLARE @FlagMov -- E/S
		CHAR(1)=(
			SELECT Movimiento 
			FROM General.TipoMovimiento
			WHERE IdTipoMov=@IdTipoMov
		);

	INSERT Inventario
	VALUES(
		@IdInventario 
		, @IdTienda
		, GETDATE()
		, @IdTipoMov 
		, @FlagMov 
		, @IdProducto 
		, @Cantidad 
		, @PrecioUnitario 
		, @DocMovimiento 
		--, @EditorUser	
		, @Condicion
		, 1 -- Estado
	);
END
GO
/****** Object:  StoredProcedure [dbo].[SpInventarioMovimiento]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInventarioMovimiento](
	@Documento VARCHAR(16), -- Referencai al Id Enterada y Salida.
	@Movimiento CHAR(1) -- E:ntrada / S:alida
)
AS BEGIN
	SELECT i.IdInventario,
		i.IdTipoMov,
		t.Descripcion as TipoMovimiento,
		p.NombreProducto as Producto,
		i.Cantidad,
		i.PrecioUnitario
	FROM Inventario i
	JOIN General.TipoMovimiento t ON t.IdTipoMov=i.IdTipoMov
	JOIN Producto p ON p.IdProducto=i.IdProducto
	WHERE DocMovimiento=@Documento 
		AND FlagMov=@Movimiento
END
GO
/****** Object:  StoredProcedure [dbo].[SpInventarioUpdate]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInventarioUpdate](
	@IdInventario VARCHAR(10)
	, @Cantidad DECIMAL(10,3) 
)
AS BEGIN
	UPDATE Inventario
	SET Cantidad=@Cantidad
	WHERE IdInventario=@IdInventario
		AND Condicion='P'
END
GO
/****** Object:  StoredProcedure [dbo].[SpInventarioUpdateNivelPrecio]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpInventarioUpdateNivelPrecio](
	@IdNivel VARCHAR(2),
	@IdOrdenVenta VARCHAR(10),
	@IdCliente VARCHAR(5)
)
AS BEGIN
	--- Actualizar precios
	UPDATE Inventario
	SET Inventario.PrecioUnitario=pp.Precio
	FROM Inventario i
	JOIN General.PrecioProducto pp 
		ON pp.IdProducto=i.IdProducto 
		AND pp.IdNivel=@IdNivel
	WHERE i.DocMovimiento=@IdOrdenVenta
	AND i.FlagMov='S'
	AND i.IdTipoMov='00001'

	--- Reasignar Nuevo Cliente
	UPDATE OrdenVenta
	SET IdCliente=@IdCliente
	WHERE IdOrden=@IdOrdenVenta
END
GO
/****** Object:  StoredProcedure [dbo].[SpOrdenVentaInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpOrdenVentaInsert](
	@IdOrden VARCHAR(10),  -- 2021070001
	@IdTienda VARCHAR(5),
	@IdVendedor VARCHAR(5),
	@IdCliente VARCHAR(5), -- Puede ser Cliente General...
	--@Situacion VARCHAR(1), --V:Vendido, O:Orden y C: Cancelado
	@Observacion VARCHAR(200),
	@EditorUser VARCHAR(20)

	--- Parametros para el inventario
	--, @IdTipoMov VARCHAR(5) --Salida: , etc. ::: 00001   Tipo "Venta"
	, @IdProducto NVARCHAR(6) 
	, @Cantidad DECIMAL(10,3) 
	, @PrecioUnitario DECIMAL(10,2) --
	--, @DocMovimiento VARCHAR(16) 
	--, @Condicion CHAR(1) -->P
)
AS BEGIN
	BEGIN TRY
	BEGIN TRANSACTION
		-- Validamos si existe registro alguno que corresponda al ID del momento.
		IF((SELECT count(1) FROM OrdenVenta WHERE IdOrden=@IdOrden)=0)
			BEGIN
				-- Si es Igual a "0" quiere decir que es el primer registro.
				INSERT INTO OrdenVenta
				(IdOrden, IdTienda, FechaRegistro,IdVendedor,IdCliente, Situacion,ImporteParcial, 
				Observacion, EditorUser, Estado)
				VALUES(@IdOrden,
					@IdTienda,
					GETDATE(),
					@IdVendedor,
					@IdCliente, -- Puede el Cliente General...
					'O', --Situación: --V:Vendido, O:Orden y C: Cancelado
					0.00, -- Importe parcial, Será actualizado con Triggers.
					@Observacion,
					@EditorUser,
					1 -- Estado 1: Activo
				)
			END
		-- Insertar Valores de productos en inventario 
		DECLARE @DocMovimiento VARCHAR(16)=@IdOrden;
		execute SpInventarioInsert '00001' -- TipoMov. POR VENTA 
			, @IdTienda
			, @IdProducto 
			, @Cantidad 
			, @PrecioUnitario
			, @DocMovimiento
			, 'P';--@Condicion;
	END TRY
		BEGIN CATCH	
			BEGIN
				--Si Existe algún error, entonces se restablecerá la transacción.
				select (select error_message() as ErrorMessage)+' '+ 
				(Select error_procedure() AS ErrorProcedure)
				+' '+(select error_line() as ErrorLine)
				as OutMessage;

				IF(@@TRANCOUNT>0)
					ROLLBACK TRANSACTION
			END
		END CATCH
			BEGIN
				select '1' as OutMessage;
				-- Si no existe ningún erro, se va a confirmar la transacción.
				IF(@@TRANCOUNT>0)
					COMMIT TRANSACTION
			END
END
GO
/****** Object:  StoredProcedure [dbo].[SpOrdenVentaList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpOrdenVentaList](
	@Desde DATE
	, @Hasta DATE
	, @IdTienda VARCHAR(5)
	, @Situacion VARCHAR(1) -- Para Ventas el valor="O": para Orden valor="";
	, @IdOrden VARCHAR(10)
)
AS BEGIN
	SELECT ov.IdOrden,
		t.NombreTienda AS Tienda,
		ov.FechaRegistro,
		c1.RazonSocial AS Vendedor,
		ov.IdCliente,
		c2.RazonSocial AS Cliente,
		ov.Situacion,
		ov.FechaDespacho,
		c3.RazonSocial as Despachador,
		ov.ImporteParcial
	FROM OrdenVenta ov
	JOIN General.Tienda t ON t.IdTienda=ov.IdTienda
	JOIN Cliente c1 ON c1.IdCliente=ov.IdVendedor
	JOIN Cliente c2 ON c2.IdCliente=ov.IdCliente
	LEFT JOIN Cliente c3 ON c3.IdCliente=ov.IdDespachador
	WHERE ov.FechaRegistro BETWEEN @Desde AND @Hasta
	AND ov.Estado=1 AND ov.IdTienda 
	LIKE'%'+@IdTienda+'%' 
	AND ov.Situacion LIKE'%'+@Situacion+'%'
	AND ov.IdOrden LIKE'%%'+@IdOrden+'%'
END 
GO
/****** Object:  StoredProcedure [dbo].[SpProductoList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpProductoList]
as begin
	Select p.IdProducto
		, p.IdCategoria
		, c.NombreCategoria
		, p.IdSubCategoria
		, s.NombreSubCategoria
		, p.IdMarca
		, m.NombreMarca
		, p.IdTalla
		, t.NombreTalla
		, p.IdFamilia
		, f.Descripcion as NombreFamilia
		, p.NombreProducto
		, p.Descripcion
		, p.Genero
		, p.EnlaceWeb
		, p.Sku
		, p.StokMinimo
		, p.EditorUser
		, p.PrecioBase
		, p.Estado
	from Producto p
	join General.Categoria c on c.IdCategoria=p.IdCategoria
	join General.SubCategoria s on s.IdSubCategoria=p.IdSubCategoria
	join General.Marca m on m.IdMarca=p.IdMarca
	join General.Talla t on t.IdTalla=p.IdTalla
	join General.Familia f on f.IdFamilia=p.IdFamilia
end
GO
/****** Object:  StoredProcedure [dbo].[SpProductoMaintenace]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpProductoMaintenace](
	@IdProducto nvarchar(6) --P00001
	, @IdCategoria nvarchar(5) 
	, @IdSubCategoria nvarchar(5) 
	, @IdMarca nvarchar(6)
	, @IdTalla nvarchar(4)
	, @IdFamilia nvarchar(6)
	, @NombreProducto varchar(100) 
	, @Descripcion varchar(100)	
	, @Genero char(1) 
	, @EnlaceWeb varchar(max)
	, @Sku varchar(100)
	, @PrecioBase DECIMAL(10,2)
	, @StokMinimo decimal(10,3) 
	, @EditorUser varchar(20)
	, @Option varchar(10) -- Insert, Update, Delete, Restore
)
as begin
	if(@Option='Insert')
		begin
			set @IdProducto=(select 'P'+right('00000'+convert(varchar(5),isnull(convert(int,max(right(IdProducto,5))),0)+1),5)  
								from Producto);
			
			DECLARE @CodigoBarra VARCHAR(20);
			SET @CodigoBarra=(@IdProducto+@IdCategoria+@IdMarca);

			insert into Producto
			(IdProducto, IdCategoria, IdSubCategoria, IdMarca, IdTalla, IdFamilia, NombreProducto,Descripcion, Genero
			, EnlaceWeb, Sku, StokMinimo, PrecioBase,Estado,EditorUser, CodigoBarra)
			values(@IdProducto
				, @IdCategoria
				, @IdSubCategoria
				, @IdMarca
				, @IdTalla
				, @IdFamilia
				, @NombreProducto
				, @Descripcion
				, @Genero
				, @EnlaceWeb
				, @Sku
				, @StokMinimo
				, @PrecioBase
				, 1 -- 1: Activo  0: Eliminado
				, @EditorUser
				, @CodigoBarra --CodigoBarra
			);
		end
	if(@Option='Update')
		begin
			update Producto
			set IdCategoria=@IdCategoria
				, IdSubCategoria=@IdSubCategoria
				, IdMarca=@IdMarca
				, IdTalla=@IdTalla
				, IdFamilia=@IdFamilia
				, NombreProducto=@NombreProducto
				, Descripcion=@Descripcion
				, Genero=@Genero
				, EnlaceWeb=@EnlaceWeb
				, Sku=@Sku
				, PrecioBase=@PrecioBase
				, StokMinimo=@StokMinimo
				, EditorUser=@EditorUser
			where IdProducto=@IdProducto;
		end
	if(@Option='Delete')
		begin
			update Producto
				set Estado=0
					, EditorUser=@EditorUser
				where IdProducto=@IdProducto;		
		end
	if(@Option='Restore')
		begin
			update Producto
				set Estado=1
					, EditorUser=@EditorUser
				where IdProducto=@IdProducto
		end
end
GO
/****** Object:  StoredProcedure [dbo].[SpSalidaInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpSalidaInsert](
	@IdSalida VARCHAR(10)
	, @IdTienda VARCHAR(5) 
	, @Documento VARCHAR(16) 
	, @EditorUser VARCHAR(20)
	--- Parametros para el inventario
	, @IdTipoMov VARCHAR(5) --Salida: , etc.
	, @IdProducto NVARCHAR(6) 
	, @Cantidad DECIMAL(10,3) 
	--, @PrecioUnitario DECIMAL(10,2) --
	--, @DocMovimiento VARCHAR(16) 
	--, @Condicion CHAR(1)
)
AS BEGIN
	BEGIN TRY
	BEGIN TRANSACTION
		-- Validamos si existe registro alguno que corresponda al ID del momento.
		IF((SELECT count(1) FROM Salida WHERE IdSalida=@IdSalida)=0)
			BEGIN
				-- Si es Igual a "0" quiere decir que es el primer registro.
				INSERT INTO Salida
				(IdSalida, IdTienda, FechaSalida,Documento,EditorUser)
				VALUES(@IdSalida,
					@IdTienda,
					GETDATE(),
					@Documento,
					@EditorUser
				)
			END
		-- Insertar Valores de productos en inventario 
		DECLARE @DocMovimiento VARCHAR(16)=@IdSalida;
		execute SpInventarioInsert @IdTipoMov 
			, @IdTienda
			, @IdProducto 
			, @Cantidad 
			, 0.00--@PrecioUnitario
			, @DocMovimiento
			, 'C';--@Condicion;
	END TRY
		BEGIN CATCH	
			BEGIN
				--Si Existe algún error, entonces se restablecerá la transacción.
				select (select error_message() as ErrorMessage)+' '+ 
				(Select error_procedure() AS ErrorProcedure)
				+' '+(select error_line() as ErrorLine)
				as OutMessage;

				IF(@@TRANCOUNT>0)
					ROLLBACK TRANSACTION
			END
		END CATCH
			BEGIN
				select '1' as OutMessage;
				-- Si no existe ningún erro, se va a confirmar la transacción.
				IF(@@TRANCOUNT>0)
					COMMIT TRANSACTION
			END
END
GO
/****** Object:  StoredProcedure [dbo].[SpSalidaList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpSalidaList](
	@Desde DATE
	, @Hasta DATE
)
AS BEGIN
	SELECT s.IdSalida,
		t.Descripcion as NombreTienda,
		s.FechaSalida,
		s.Documento
	FROM Salida s
	JOIN General.Tienda t ON t.IdTienda=s.IdTienda
	WHERE s.FechaSalida BETWEEN @Desde AND @Hasta
END 
GO
/****** Object:  StoredProcedure [dbo].[SpStockByStore]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpStockByStore](
	@IdTienda VARCHAR(5)
)
AS BEGIN
	SELECT s.IdProducto,
		s.NombreProducto,
		s.StokFinal
		-- Categoria, Marca.
	FROM Stock s
	WHERE s.IdTienda=@IdTienda
		AND s.StokFinal>0
END
GO
/****** Object:  StoredProcedure [dbo].[SpStockDetail]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpStockDetail](
	@IdTienda VARCHAR(5)
)
AS BEGIN
	SELECT t.NombreTienda as Tienda,
		s.IdProducto,
		s.NombreProducto as Producto,
		c.NombreCategoria as Categoria,
		m.NombreMarca as Marca,
		p.PrecioBase,
		p.StokMinimo,
		s.StokFinal,
		p.Descripcion
	FROM Stock s
	JOIN Producto p ON p.IdProducto=s.IdProducto
	JOIN General.Categoria c ON c.IdCategoria=p.IdCategoria
	JOIN General.Marca m ON m.IdMarca=p.IdMarca
	JOIN General.Tienda t ON t.IdTienda=s.IdTienda
	WHERE s.IdTienda  like'%'+@IdTienda+'%'	 
END
GO
/****** Object:  StoredProcedure [dbo].[SpUsuarioLogin]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpUsuarioLogin](
	@columna varchar(100)
	, @parametro varchar(max)
)
as begin
	declare @sql nvarchar(max), @where nvarchar(100)

	if(LEN(@columna)>0)
		begin
			if(@columna='UserName')begin
				set @where=' where '+@columna+'='''+@parametro+''''; end
			if(@columna='Password')begin
				set @where=' where '+@columna+'='''+@parametro+''''; end
			
			select @sql='select * from Usuario'+@where;
		end
	---------------------------------------------------
	execute sys.sp_executesql @sql
end
GO
/****** Object:  StoredProcedure [dbo].[SpUsuarioLoginPassword]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpUsuarioLoginPassword](
	@UserName varchar(20)
	, @Password varchar(max)
)
as begin
	select *
	from Usuario 
	where UserName=@UserName
	and Password=@Password
end
GO
/****** Object:  StoredProcedure [dbo].[SpUsuarioLoginUser]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[SpUsuarioLoginUser](
	@UserName varchar(20)
)
as begin
	select count(1) as Value from Usuario where UserName=@UserName
end
GO
/****** Object:  StoredProcedure [dbo].[SpVentaGenerarComprobanteCorrelativo]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpVentaGenerarComprobanteCorrelativo](
	@IdComprobante VARCHAR(5)
)
AS BEGIN
	--<> Obteniendo el Prefijo del tipo de comprobante
	DECLARE @prefijo VARCHAR(3)=(
		SELECT Abreviatura
		FROM General.Tabla WHERE IdTabla=@IdComprobante
	);
	--<> Obteniendo el Año en Curso
	DECLARE @year VARCHAR(4)=(YEAR(GETDATE()));
	--<> Obteniendo el Correlativo del comprobante
	DECLARE @correlativo VARCHAR(6)=(
		SELECT FORMAT(ISNULL(MAX(CONVERT(bigint,RIGHT(NumComprobante,6))),0)+1,'000000')
		FROM Venta
		WHERE LEFT(NumComprobante,8)=@prefijo+'-'+@year
	)

	SELECT @prefijo+'-'+@year+'-'+@correlativo AS Correlativo;
END

GO
/****** Object:  StoredProcedure [dbo].[SpVentaInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpVentaInsert](
	@IdTienda VARCHAR(5),
	@IdVenta VARCHAR(10), --2021070001 Si
	@IdOrden VARCHAR(10), --2021070001 Si
	@IdComprobante VARCHAR(5), --{TIC} TICKET(INTERNO), {BOL} BOLETA, {FAC} FACTURA
	@NumComprobante VARCHAR(15), -- 1°-->TIC-2021-000001, 2°-->FAC-2021-000032
	@NivelPrecio VARCHAR(50), -- Descripción del nivel de precio.
	@ImporteTotal DECIMAL(10,2),
	@Observacion VARCHAR(200),	
	@EditorUser VARCHAR(20),
	-- PARAMETROS PARA VentaPago
	@IdFormaPago VARCHAR(5), --TablaGeneral, Forma de pago o metodo de pago.
	@MetodoPago VARCHAR(100),
	@NumOperacion VARCHAR(20),
	@Moneda VARCHAR(1), -- S->Soles,D->Dolares
	@Cambio DECIMAL(10,3), --Tipo de cambio Dolares a Soles
	@Importe DECIMAL(10,2)
)
AS BEGIN TRY
	BEGIN TRANSACTION
		--VALIDACIÓN SI EXISTE EL ID DE LA VENTA A REALIZAR
		IF((SELECT COUNT(1) FROM Venta WHERE IdVenta=@IdVenta)=0)-- SI X=0, ENTONCES SE GENERA NUEVO REGISTRO DE VENTA
			BEGIN
				INSERT INTO Venta
				(IdVenta, IdTienda, IdOrden, FechaRegistrro, IdComprobante, NumComprobante, NivelPrecio, ImporteTotal, Observacion, 
					Facturado, FechaFacturado, IdClienteFinal, CorrelativoVenta, CorrelativoBoleta, CorrelativoFactura, 
					ComprobanteInterno, Anulado, MotivoAnulacion, Estado, EditorUser)
				VALUES(@IdVenta,
					@IdTienda,
					@IdOrden,
					GETDATE(), --FECHA DE REGISTRO
					@IdComprobante,
					@NumComprobante,
					@NivelPrecio,
					@ImporteTotal,
					@Observacion,
					0,-- AÚN NO SE HA FACTURADO, CAMBIAR SI SE VA A EMITIR FACTURACIÓN AL INSTANTE
					'', -- FECHA DE FACTURACION --FechaFacturado
					'', --ID DEL CLIENTE FINAL --IdClienteFinal
					'', -- CORRELATIVO VENTA --CorrelativoVenta
					'', -- CORRELATIVO BOLETA --CorrelativoBoleta
					'', -- CORRELATIVO FACTURA --CorrelativoFactura
					@NumComprobante, -- ES EL COMPROBANTE INTERNO --ComprobanteInterno
					0, -- ANULADO: ESTE REGISTRO SE GUARDA SIN ANULAR --Anulado
					'', --MOTIVO DE ANULACION --MotivoAnulacion
					1, -- ESTADO, SIEMPRE SE GUARDA COMO ACTIVO, --Estado
					@EditorUser 
				);
			END

		-- INSERTAR DETALLES  O VentaPago
		EXECUTE SpVentaPagoInsert @IdVenta, @IdFormaPago, 
			@MetodoPago, @NumOperacion, @Moneda, @Cambio,
			@Importe;

	END TRY
		BEGIN CATCH
			BEGIN
				SELECT( (SELECT ERROR_MESSAGE())+' '+
					(SELECT ERROR_PROCEDURE())+' '+
					(SELECT ERROR_LINE())
				) AS OutMessage; 

				IF(@@TRANCOUNT>0)
					ROLLBACK TRANSACTION;
			END
		END CATCH
			BEGIN
				SELECT '1' AS OutMessage;
				IF(@@TRANCOUNT>0)
					COMMIT TRANSACTION;
			END
GO
/****** Object:  StoredProcedure [dbo].[SpVentaList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpVentaList](
	@Desde DATE,
	@Hasta DATE,
	@IdTienda VARCHAR(5)
)
AS BEGIN
	SELECT v.IdVenta, 
		t.NombreTienda as Tienda,
		v.IdOrden,
		c.RazonSocial AS Cliente,
		d.RazonSocial AS Vendedor,
		v.FechaRegistrro,
		gt.Descripcion as Comprobante,
		v.NumComprobante,
		v.NivelPrecio,
		v.ImporteTotal,
		v.Observacion,
		v.Anulado,
		v.MotivoAnulacion
	FROM Venta v
	JOIN General.Tienda t ON t.IdTienda=v.IdTienda
	JOIN OrdenVenta o ON o.IdOrden=v.IdOrden
	JOIN Cliente c ON c.IdCliente=o.IdCliente
	LEFT JOIN Cliente d ON d.IdCliente=o.IdDespachador
	JOIN General.Tabla gt ON gt.IdTabla=v.IdComprobante
	WHERE v.FechaRegistrro BETWEEN @Desde AND @Hasta
	AND v.IdTienda LIKE'%'+@IdTienda+'%'
END
GO
/****** Object:  StoredProcedure [dbo].[SpVentaPagoInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpVentaPagoInsert](	
	@IdVenta VARCHAR(10), --2021070001
	@IdFormaPago VARCHAR(5), --TablaGeneral, Forma de pago o metodo de pago.
	@MetodoPago VARCHAR(100),
	@NumOperacion VARCHAR(20),
	@Moneda VARCHAR(1), -- S->Soles,D->Dolares
	@Cambio DECIMAL(10,3), --Tipo de cambio Dolares a Soles
	@Importe DECIMAL(10,2)
)
AS BEGIN
	-- GENERAR CORRELATIVO PARA "VentaPago"
	DECLARE @IdPago VARCHAR(10), @year VARCHAR(4), @month VARCHAR(2)
	SET @month=(FORMAT(MONTH(GETDATE()),'00'));
	SET @year=FORMAT(YEAR(GETDATE()),'0000');
	SET @IdPago=@year+@month+FORMAT((
		select ISNULL(max(CONVERT(bigint,RIGHT(IdPago,4))),0)+1 from VentaPago
		WHERE LEFT(IdPago,6)=@year+@month
	),'0000');

	INSERT INTO VentaPago
	(IdPago, IdVenta, IdFormaPago, MetodoPago, NumOperacion, Moneda, Cambio, Importe, Estado)
	VALUES(@IdPago,
		@IdVenta,
		@IdFormaPago,
		@MetodoPago,
		@NumOperacion,
		@Moneda,
		@Cambio,
		@Importe,
		1 -- Estado,  se interta como Activo -->1: Activo, -->0:Eliminado o anulado o cancelado o deshabilitado.
	);
END
GO
/****** Object:  StoredProcedure [dbo].[SpVentaPagoList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SpVentaPagoList](
	@IdVenta VARCHAR(10)
)
AS BEGIN
	SELECT vp.IdPago,
		vp.IdVenta,
		gt.Descripcion as FormaPago,
		vp.MetodoPago,
		vp.NumOperacion,
		vp.Moneda,
		vp.Cambio,
		vp.Importe
	FROM VentaPago vp
	JOIN General.Tabla gt ON gt.IdTabla=vp.IdFormaPago
	WHERE vp.IdVenta=@IdVenta
END

GO
/****** Object:  StoredProcedure [General].[SpCategoriaList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpCategoriaList]
as begin
	Select IdCategoria
		, NombreCategoria
		, Descripcion
		, Prioridad
		, Estado
		, EditorUser
	from General.Categoria
end
GO
/****** Object:  StoredProcedure [General].[SpCategoriaMaintenace]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpCategoriaMaintenace](
	@IdCategoria nvarchar(5) 
	, @NombreCategoria varchar(50) 
	, @Descripcion varchar(100) 
	, @Prioridad int
	, @EditorUser varchar(20)
	, @Option varchar(10) -- Insert, Update, Delete, Restore
)
as begin
	if(@Option='Insert')
		begin
			set @IdCategoria=(select 'C'+right('0000'+convert(varchar(4),isnull(convert(int,max(right(IdCategoria,4))),0)+1),4)  
								from General.Categoria);

			insert into General.Categoria
			(IdCategoria, NombreCategoria, Descripcion, Prioridad, Estado, EditorUser)
			values(@IdCategoria
				, @NombreCategoria
				, @Descripcion
				, @Prioridad
				, 1 --true
				, @EditorUser
			);
		end
	if(@Option='Update')
		begin
			update General.Categoria
			set NombreCategoria=@NombreCategoria
				, Descripcion=@Descripcion
				, Prioridad=@Prioridad
				, EditorUser=@EditorUser
			where IdCategoria=@IdCategoria;
		end
	if(@Option='Delete')
		begin
			update General.Categoria
				set Estado=0
					, EditorUser=@EditorUser
				where IdCategoria=@IdCategoria;		
		end
	if(@Option='Restore')
		begin
			update General.Categoria
				set Estado=1
					, EditorUser=@EditorUser
				where IdCategoria=@IdCategoria;	
		end
end
GO
/****** Object:  StoredProcedure [General].[SpFamiliaList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpFamiliaList]
as begin
	Select IdFamilia
		, Descripcion
		, Estado
		, EditorUser
	from General.Familia;
end
GO
/****** Object:  StoredProcedure [General].[SpFamiliaMaintenace]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpFamiliaMaintenace](
	@IdFamilia nvarchar(6)  --000001
	, @Descripcion varchar(100) 
	, @EditorUser varchar(20)
	, @Option varchar(10) -- Insert, Update, Delete, Restore
)
as begin
	if(@Option='Insert')
		begin
			set @IdFamilia=(select right('000000'+convert(varchar(6),isnull(convert(int,max(IdFamilia)),0)+1),6)  
								from General.Familia); --000001

			insert into General.Familia
			(IdFamilia, Descripcion, Estado, EditorUser)
			values(@IdFamilia
				, @Descripcion
				, 1 --true
				, @EditorUser
			);
		end
	if(@Option='Update')
		begin
			update General.Familia
			set Descripcion=@Descripcion
			where IdFamilia=@IdFamilia;
		end
	if(@Option='Delete')
		begin
			update General.Familia
				set Estado=0
					, EditorUser=@EditorUser
				where IdFamilia=@IdFamilia;
		end
	if(@Option='Restore')
		begin
			update General.Familia
				set Estado=1
					, EditorUser=@EditorUser
				where IdFamilia=@IdFamilia;
		end
end
GO
/****** Object:  StoredProcedure [General].[SpMarcaList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpMarcaList]
as begin
	Select m.IdMarca
		, m.NombreMarca
		, m.Descripcion
		, m.Prioridad
		, m.IdProveedor
		, p.NombreProveedor
		, m.Estado
		, m.EditorUser
	from General.Marca m
	join General.Proveedor p on p.IdProveedor=m.IdProveedor;
end
GO
/****** Object:  StoredProcedure [General].[SpMarcaMaintenace]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpMarcaMaintenace](
	@IdMarca nvarchar(6) --M00001
	, @NombreMarca varchar(50)	
	, @Descripcion varchar(100)
	, @Prioridad	int	
	, @IdProveedor nvarchar(6) 
	, @EditorUser varchar(20)
	, @Option varchar(10) -- Insert, Update, Delete, Restore
)
as begin
	if(@Option='Insert')
		begin
			set @IdMarca=(select 'M'+right('00000'+convert(varchar(5),isnull(convert(int,max(right(IdMarca,5))),0)+1),5)  
								from General.Marca); --M00001

			insert into General.Marca
			(IdMarca, NombreMarca, Descripcion, Prioridad, IdProveedor, Estado, EditorUser)
			values(@IdMarca
				, @NombreMarca
				, @Descripcion
				, @Prioridad
				, @IdProveedor
				, 1 --true
				, @EditorUser
			);
		end
	if(@Option='Update')
		begin
			update General.Marca
			set NombreMarca=@NombreMarca
				, Descripcion=@Descripcion
				, Prioridad=@Prioridad
				, EditorUser=@EditorUser
			where IdMarca=@IdMarca;
		end
	if(@Option='Delete')
		begin
			update General.Marca
				set Estado=0
					, EditorUser=@EditorUser
				where IdMarca=@IdMarca;		
		end
	if(@Option='Restore')
		begin
			update General.Marca
				set Estado=1
					, EditorUser=@EditorUser
				where IdMarca=@IdMarca;	
		end
end
GO
/****** Object:  StoredProcedure [General].[SpPrecioNivelList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [General].[SpPrecioNivelList]
AS BEGIN
	SELECT IdNivel, Descripcion,Estado
	FROM General.PrecioNivel
END
GO
/****** Object:  StoredProcedure [General].[SpPrecioProductoDetail]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [General].[SpPrecioProductoDetail](
	@IdProducto VARCHAR(6)
)
AS BEGIN
	SELECT pp.IdPrecio,
		pn.Descripcion AS NivelPrecio,
		pp.Precio
	FROM General.PrecioProducto pp
	JOIN General.PrecioNivel pn ON pn.IdNivel=pp.IdNivel
	WHERE pp.Estado=1
	AND pp.IdProducto=@IdProducto;
END
GO
/****** Object:  StoredProcedure [General].[SpPrecioProductoList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [General].[SpPrecioProductoList]
AS BEGIN
	SELECT 
	IdPrecio,IdProducto,IdNivel,Precio,Estado
	FROM General.PrecioProducto
END
GO
/****** Object:  StoredProcedure [General].[SpPrecioProductoUpdate]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [General].[SpPrecioProductoUpdate](
	@IdPrecio VARCHAR(6),
	@Precio DECIMAL(10,2)
)
AS BEGIN
	UPDATE General.PrecioProducto
	SET Precio=@Precio
	WHERE IdPrecio=@IdPrecio
END
GO
/****** Object:  StoredProcedure [General].[SpProveedorList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpProveedorList]
as begin
	Select IdProveedor
		, NombreProveedor
		, Ubigeo
		, Direccion
		, Urbanizacion
		, TelefFijo
		, TelefMovil
		, Email
		, Estado
		, EditorUser
	from General.Proveedor
end
GO
/****** Object:  StoredProcedure [General].[SpProveedorMaintenace]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpProveedorMaintenace](
	@IdProveedor nvarchar(6) --000001
	, @NombreProveedor varchar(60) 
	, @Ubigeo nvarchar(6) 
	, @Direccion varchar(250)
	, @Urbanizacion varchar(100) 
	, @TelefFijo nvarchar(10) 
	, @TelefMovil nvarchar(13) 
	, @Email varchar(100) 
	, @EditorUser varchar(20)
	, @Option varchar(10) -- Insert, Update, Delete, Restore
)
as begin
	if(@Option='Insert')
		begin
			set @IdProveedor=(select right('000000'+convert(varchar(6),isnull(convert(int,max(IdProveedor)),0)+1),6)  
								from General.Proveedor);

			insert into General.Proveedor
			(IdProveedor, NombreProveedor, Ubigeo, Direccion, Urbanizacion, TelefFijo
			, TelefMovil, Email, Estado, EditorUser)
			values(@IdProveedor
				, @NombreProveedor
				, @Ubigeo
				, @Direccion
				, @Urbanizacion
				, @TelefFijo
				, @TelefMovil
				, @Email
				, 1 --true
				, @EditorUser
			);
		end
	if(@Option='Update')
		begin
			update General.Proveedor
			set NombreProveedor=@NombreProveedor
				, Ubigeo=@Ubigeo
				, Direccion=@Direccion
				, Urbanizacion=@Urbanizacion
				, TelefFijo=@TelefFijo
				, TelefMovil=@TelefMovil
				, Email=@Email
				, EditorUser=@EditorUser
			where IdProveedor=@IdProveedor;
		end
	if(@Option='Delete')
		begin
			update General.Proveedor
				set Estado=0
					, EditorUser=@EditorUser
				where IdProveedor=@IdProveedor;	
		end
	if(@Option='Restore')
		begin
			update General.Proveedor
				set Estado=1
					, EditorUser=@EditorUser
				where IdProveedor=@IdProveedor;
		end
end
GO
/****** Object:  StoredProcedure [General].[SpSubCategoriaList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpSubCategoriaList]
as begin
	Select sc.IdSubCategoria
		, sc.IdCategoria
		, c.NombreCategoria
		, sc.NombreSubCategoria
		, sc.Descripcion
		, sc.Estado
		, sc.EditorUser
	from General.SubCategoria sc
	join General.Categoria c on c.IdCategoria=sc.IdCategoria;
end
GO
/****** Object:  StoredProcedure [General].[SpSubCategoriaMaintenace]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpSubCategoriaMaintenace](
	@IdSubCategoria nvarchar(5)--SC001
	, @NombreSubCategoria varchar(50)	
	, @Descripcion varchar(100)
	, @IdCategoria nvarchar(5)--C0001
	, @EditorUser varchar(20)
	, @Option varchar(10) -- Insert, Update, Delete, Restore
)
as begin
	if(@Option='Insert')
		begin
			set @IdSubCategoria=(select 'SC'+right('000'+convert(varchar(3),isnull(convert(int,max(right(IdSubCategoria,3))),0)+1),3)  
								from General.SubCategoria); --SC001

			insert into General.SubCategoria
			(IdSubCategoria, NombreSubCategoria, Descripcion, IdCategoria, Estado, EditorUser)
			values(@IdSubCategoria
				, @NombreSubCategoria
				, @Descripcion
				, @IdCategoria
				, 1 --true
				, @EditorUser
			);
		end
	if(@Option='Update')
		begin
			update General.SubCategoria
			set NombreSubCategoria=@NombreSubCategoria
				, Descripcion=@Descripcion
				, EditorUser=@EditorUser
			where IdSubCategoria=@IdSubCategoria;
		end
	if(@Option='Delete')
		begin
			update General.SubCategoria
				set Estado=0
					, EditorUser=@EditorUser
				where IdSubCategoria=@IdSubCategoria;		
		end
	if(@Option='Restore')
		begin
			update General.SubCategoria
				set Estado=1
					, EditorUser=@EditorUser
				where IdSubCategoria=@IdSubCategoria;	
		end
end
GO
/****** Object:  StoredProcedure [General].[SpTablaList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpTablaList](
	@NumeroTabla varchar(2)
)
as begin
	select IdTabla
		, Descripcion
		, Abreviatura
		, Valor
		, Dimension
	from General.Tabla
	where NumeroTabla=@NumeroTabla and Correlativo<>'000'
end
GO
/****** Object:  StoredProcedure [General].[SpTallaList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpTallaList]
as begin
	Select IdTalla
		, NombreTalla
		, Descripcion
		, Estado
		, EditorUser
	from General.Talla;
end
GO
/****** Object:  StoredProcedure [General].[SpTallaMaintenace]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpTallaMaintenace](
	@IdTalla	nvarchar(4)	--T001
	, @NombreTalla varchar(10) 
	, @Descripcion varchar(100)
	, @EditorUser varchar(20)
	, @Option varchar(10) -- Insert, Update, Delete, Restore
)
as begin
	if(@Option='Insert')
		begin
			set @IdTalla=(select 'T'+right('000'+convert(varchar(3),isnull(convert(int,max(right(IdTalla,3))),0)+1),3)  
								from General.Talla); --T001

			insert into General.Talla
			(IdTalla, NombreTalla, Descripcion, Estado, EditorUser)
			values(@IdTalla
				, @NombreTalla
				, @Descripcion
				, 1 --true
				, @EditorUser
			);
		end
	if(@Option='Update')
		begin
			update General.Talla
			set NombreTalla=@NombreTalla
				, Descripcion=@Descripcion
			where IdTalla=@IdTalla;
		end
	if(@Option='Delete')
		begin
			update General.Talla
				set Estado=0
					, EditorUser=@EditorUser
				where IdTalla=@IdTalla;	
		end
	if(@Option='Restore')
		begin
			update General.Talla
				set Estado=1
					, EditorUser=@EditorUser
				where IdTalla=@IdTalla;
		end
end
GO
/****** Object:  StoredProcedure [General].[SpTiendaList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpTiendaList]
as begin
	select t.IdTienda
		, t.NombreTienda
		, t.Descripcion
		, t.IdResponsable
		, c.RazonSocial
		, t.Consigna
		, t.EditorUser
		, t.Estado
	from General.Tienda t
	join Cliente c on c.IdCliente=t.IdResponsable
end
GO
/****** Object:  StoredProcedure [General].[SpTiendaMaintenance]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpTiendaMaintenance](
	@IdTienda varchar(5) --A0001
	, @NombreTienda varchar(80)
	, @Descripcion varchar(100) 
	, @IdResponsable bigint 
	, @Consigna bit 
	, @EditorUser varchar(20) 
	, @Option varchar(10) -- Insert, Update, Delete, Restore
)
as begin
	if(@Option='Insert')
		begin
			set @IdTienda=(select 'A'+right('0000'+convert(varchar(4),isnull(convert(int,max(right(IdTienda,4))),0)+1),4)  
								from General.Tienda);

			insert into General.Tienda
			(IdTienda, NombreTienda, Descripcion, IdResponsable, Consigna, EditorUser, Estado)
			values(@IdTienda
				, @NombreTienda
				, @Descripcion
				, @IdResponsable
				, @Consigna
				, @EditorUser
				, 1 -- 1: Activo  0: Eliminado
			);
		end
	if(@Option='Update')
		begin
			update General.Tienda
			set NombreTienda=@NombreTienda
				, Descripcion=@Descripcion
				, IdResponsable=@IdResponsable
				, Consigna=@Consigna
				, EditorUser=@EditorUser
			where IdTienda=@IdTienda;
		end
	if(@Option='Delete')
		begin
			update General.Tienda
				set Estado=0
					, EditorUser=@EditorUser
				where IdTienda=@IdTienda;		
		end
	if(@Option='Restore')
		begin
			update General.Tienda
				set Estado=1
					, EditorUser=@EditorUser
				where IdTienda=@IdTienda;	
		end
end
GO
/****** Object:  StoredProcedure [General].[SpTipoMovimientoList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpTipoMovimientoList]
as begin
	select IdTipoMov
		, Descripcion
		, Movimiento
	from General.TipoMovimiento
end
GO
/****** Object:  StoredProcedure [General].[SpUbigeoList]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [General].[SpUbigeoList]
as begin
	select gd.IdDistrito as Ubigeo
		, gd.IdDepartamento
		, gdp.Descripcion as Departamento
		, gd.IdProvincia
		, gp.Descripcion as Provincia
		, gd.Descripcion as Distrito
	from General.Distrito gd
	left join General.Provincia gp on gp.IdProvincia=gd.IdProvincia
	left join General.Departamento gdp on gdp.IdDepartamento=gd.IdDepartamento
end
GO
/****** Object:  Trigger [dbo].[TgInventarioDelete]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TgInventarioDelete]
ON [dbo].[Inventario] FOR DELETE
AS BEGIN
	--<> GENERAR STOCK ------------------------------------
	EXEC SpInventarioGenerateStock
END
GO
ALTER TABLE [dbo].[Inventario] ENABLE TRIGGER [TgInventarioDelete]
GO
/****** Object:  Trigger [dbo].[TgInventarioInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TgInventarioInsert]
ON [dbo].[Inventario] FOR INSERT
AS BEGIN
	DECLARE @DocMovimiento VARCHAR(16), @IdTienda VARCHAR(5);

	SELECT @DocMovimiento=i.DocMovimiento,@IdTienda=i.IdTienda
	FROM inserted i;

	/*declare @EditorUser varchar(20), @Evento varchar(max);
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	*/
	--<> GENERAR STOCK ------------------------------------
	EXEC SpInventarioGenerateStock

	--<> ACTUALIZAR IMPORTE DE "OrdenVenta"----------------
	UPDATE OrdenVenta 
	SET ImporteParcial=(
		SELECT ISNULL(SUM((Cantidad*PrecioUnitario)),0.00) AS Valor
		FROM Inventario
		WHERE DocMovimiento=@DocMovimiento
		AND FlagMov='S' AND IdTipoMov='00001' AND IdTienda=@IdTienda
		AND Estado=1
	)
	WHERE IdOrden=LTRIM(RTRIM(@DocMovimiento));	

	--<>---------------------------------------------------
	/*execute SpBitacoraInsert 'Inventario'
	,'Insert',@EditorUser,@Evento*/
END
GO
ALTER TABLE [dbo].[Inventario] ENABLE TRIGGER [TgInventarioInsert]
GO
/****** Object:  Trigger [dbo].[TgInventarioUpdate]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TgInventarioUpdate]
ON [dbo].[Inventario] FOR UPDATE
AS BEGIN
	DECLARE @Documento VARCHAR(20); 
	SELECT @Documento=LTRIM(RTRIM(i.DocMovimiento)) FROM inserted i
	------------------------------------------------
	IF(UPDATE(Cantidad))
	BEGIN
		--<> GENERAR STOCK ------------------------------------
		EXEC SpInventarioGenerateStock
	END

	IF(UPDATE(PrecioUnitario))
	BEGIN
		DECLARE @Importe DECIMAL(10,2);
		SET @Importe=(
			SELECT SUM(Cantidad*PrecioUnitario)
			FROM Inventario
			WHERE DocMovimiento=@Documento
			AND FlagMov='S' AND IdTipoMov='00001'
		);

		UPDATE OrdenVenta
		SET ImporteParcial=@Importe
		WHERE IdOrden=@Documento;
	END

END
GO
ALTER TABLE [dbo].[Inventario] ENABLE TRIGGER [TgInventarioUpdate]
GO
/****** Object:  Trigger [dbo].[TgProductoInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TgProductoInsert]
ON [dbo].[Producto] FOR INSERT
AS BEGIN
	--<> Obtener datos del producto en el instante del insert.
	DECLARE @idProducto NVARCHAR(6), @precio DECIMAL(10,2);
	------------------------------------------------------------
	SELECT @idProducto=x.IdProducto, @precio=x.PrecioBase
	FROM inserted x;

	--<> Precios de productos según nivel de precio
	DECLARE @existe INT=(SELECT COUNT(1) FROM General.PrecioProducto WHERE IdProducto=@idProducto);
	IF(@existe>0)
		BEGIN RETURN;  END
	ELSE
		BEGIN
			DECLARE @idMax INT=(
				SELECT ISNULL(MAX(CONVERT(bigint,IdPrecio)),0)
				FROM General.PrecioProducto
			);

			INSERT INTO General.PrecioProducto
			SELECT 
				FORMAT(@idMax+ROW_NUMBER() OVER(ORDER BY IdNivel ASC),'000000'),
				@idProducto,
				IdNivel,
				@precio,
				1 --Estado
			FROM General.PrecioNivel WHERE Estado=1;
		END
	--<> Historial/Bitacora de eventos

END
GO
ALTER TABLE [dbo].[Producto] ENABLE TRIGGER [TgProductoInsert]
GO
/****** Object:  Trigger [dbo].[TgProductoUpdate]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TgProductoUpdate]
ON [dbo].[Producto] FOR UPDATE
AS BEGIN
	DECLARE @estado BIT, @idProducto NVARCHAR(6);
	SELECT @estado=x.Estado, @idProducto=x.IdProducto
	FROM inserted x;
	---------------------------------------
	IF(UPDATE(Estado))
		BEGIN
			--<> Precio de productos			
			UPDATE General.PrecioProducto
			SET Estado=@estado
			WHERE IdProducto=@idProducto
		END

	--<> Historial/Bitacora de eventos
END
GO
ALTER TABLE [dbo].[Producto] ENABLE TRIGGER [TgProductoUpdate]
GO
/****** Object:  Trigger [dbo].[TgVentaInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [dbo].[TgVentaInsert]
ON [dbo].[Venta] FOR INSERT
AS BEGIN
	DECLARE @idOrden VARCHAR(10);
	SELECT @idOrden=x.IdOrden FROM inserted x;
	---------------------------------------
	UPDATE OrdenVenta
	SET Situacion='V'--Vendido
	WHERE IdOrden=@idOrden

	UPDATE Inventario
	SET Condicion='C'
	WHERE DocMovimiento=@idOrden AND IdTipoMov='00001' AND FlagMov='S';
END

GO
ALTER TABLE [dbo].[Venta] ENABLE TRIGGER [TgVentaInsert]
GO
/****** Object:  Trigger [General].[TgCategoriaInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [General].[TgCategoriaInsert]
on [General].[Categoria] for insert
as begin
	declare @EditorUser varchar(20), @Evento varchar(max);
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	--<>---------------------------------------------------
	execute SpBitacoraInsert 'General.Categoria'
	,'Insert',@EditorUser,@Evento
end
GO
ALTER TABLE [General].[Categoria] ENABLE TRIGGER [TgCategoriaInsert]
GO
/****** Object:  Trigger [General].[TgCategoriaUpdate]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [General].[TgCategoriaUpdate]
on [General].[Categoria] for update
as begin
	declare @EditorUser varchar(20), @Evento varchar(max), @Accion varchar(10)='Update';
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	--<>---------------------------------------------------
	if(update(Estado))
		begin
			set @Accion='Restore';
			if((select Estado from inserted)=0)
				begin
					set @Accion='Delete';
				end
		end

	execute SpBitacoraInsert 'General.Categoria'
	,@Accion,@EditorUser,@Evento;
end
GO
ALTER TABLE [General].[Categoria] ENABLE TRIGGER [TgCategoriaUpdate]
GO
/****** Object:  Trigger [General].[TgFamiliaInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [General].[TgFamiliaInsert]
on [General].[Familia] for insert
as begin
	declare @EditorUser varchar(20), @Evento varchar(max);
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	--<>---------------------------------------------------
	execute SpBitacoraInsert 'General.Familia'
	,'Insert',@EditorUser,@Evento
end
GO
ALTER TABLE [General].[Familia] ENABLE TRIGGER [TgFamiliaInsert]
GO
/****** Object:  Trigger [General].[TgFamiliaUpdate]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [General].[TgFamiliaUpdate]
on [General].[Familia] for update
as begin
	declare @EditorUser varchar(20), @Evento varchar(max), @Accion varchar(10)='Update';
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	--<>---------------------------------------------------
	if(update(Estado))
		begin
			set @Accion='Restore';
			if((select Estado from inserted)=0)
				begin
					set @Accion='Delete';
				end
		end

	execute SpBitacoraInsert 'General.Familia'
	,@Accion,@EditorUser,@Evento;
end
GO
ALTER TABLE [General].[Familia] ENABLE TRIGGER [TgFamiliaUpdate]
GO
/****** Object:  Trigger [General].[TgMarcaInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [General].[TgMarcaInsert]
on [General].[Marca] for insert
as begin
	declare @EditorUser varchar(20), @Evento varchar(max);
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	--<>---------------------------------------------------
	execute SpBitacoraInsert 'General.Marca'
	,'Insert',@EditorUser,@Evento
end
GO
ALTER TABLE [General].[Marca] ENABLE TRIGGER [TgMarcaInsert]
GO
/****** Object:  Trigger [General].[TgMarcaUpdate]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [General].[TgMarcaUpdate]
on [General].[Marca] for update
as begin
	declare @EditorUser varchar(20), @Evento varchar(max), @Accion varchar(10)='Update';
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	--<>---------------------------------------------------
	if(update(Estado))
		begin
			set @Accion='Restore';
			if((select Estado from inserted)=0)
				begin
					set @Accion='Delete';
				end
		end

	execute SpBitacoraInsert 'General.Marca'
	,@Accion,@EditorUser,@Evento;
end
GO
ALTER TABLE [General].[Marca] ENABLE TRIGGER [TgMarcaUpdate]
GO
/****** Object:  Trigger [General].[TgPrecioNivelInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [General].[TgPrecioNivelInsert]
ON [General].[PrecioNivel] FOR INSERT
AS BEGIN
	DECLARE @idNivel VARCHAR(2);
	SELECT @idNivel=x.IdNivel 
	FROM inserted x;
	---------------------------------------

	--<> Precio de productos
	DECLARE @existe INT=(
		SELECT COUNT(1) FROM General.PrecioProducto
		WHERE IdNivel=@idNivel
	);
	IF(@existe>0)
		BEGIN RETURN; END
	ELSE
		BEGIN
			DECLARE @idMax INT=(
				SELECT ISNULL(MAX(CONVERT(BIGINT,IdPrecio)),0)
				FROM General.PrecioProducto
			);

			INSERT INTO General.PrecioProducto
			SELECT
				FORMAT(
					@idMax+ROW_NUMBER() OVER(ORDER BY IdProducto ASC)
				,'000000'),	
				IdProducto,
				@idNivel,
				PrecioBase,
				1 --Estado
			FROM Producto WHERE Estado=1;
		END
END
GO
ALTER TABLE [General].[PrecioNivel] ENABLE TRIGGER [TgPrecioNivelInsert]
GO
/****** Object:  Trigger [General].[TgPrecioNivelUpdate]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TRIGGER [General].[TgPrecioNivelUpdate]
ON [General].[PrecioNivel] FOR UPDATE
AS BEGIN
	DECLARE @idNivel VARCHAR(2), @estado BIT;
	SELECT @idNivel=x.IdNivel, @estado=x.Estado
	FROM inserted x;
	------------------------------------------------
	IF(UPDATE(Estado))
		BEGIN
			--<> Precio de productos			
			UPDATE General.PrecioProducto
			SET Estado=@estado
			WHERE IdNivel=@idNivel
		END

	--<> Historial/Bitacora de eventos
END
GO
ALTER TABLE [General].[PrecioNivel] ENABLE TRIGGER [TgPrecioNivelUpdate]
GO
/****** Object:  Trigger [General].[TgProveedorInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [General].[TgProveedorInsert]
on [General].[Proveedor] for insert
as begin
	declare @EditorUser varchar(20), @Evento varchar(max);
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	--<>---------------------------------------------------
	execute SpBitacoraInsert 'General.Proveedor'
	,'Insert',@EditorUser,@Evento
end
GO
ALTER TABLE [General].[Proveedor] ENABLE TRIGGER [TgProveedorInsert]
GO
/****** Object:  Trigger [General].[TgProveedorUpdate]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [General].[TgProveedorUpdate]
on [General].[Proveedor] for update
as begin
	declare @EditorUser varchar(20), @Evento varchar(max), @Accion varchar(10)='Update';
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	--<>---------------------------------------------------
	if(update(Estado))
		begin
			set @Accion='Restore';
			if((select Estado from inserted)=0)
				begin
					set @Accion='Delete';
				end
		end

	execute SpBitacoraInsert 'General.Proveedor'
	,@Accion,@EditorUser,@Evento;
end
GO
ALTER TABLE [General].[Proveedor] ENABLE TRIGGER [TgProveedorUpdate]
GO
/****** Object:  Trigger [General].[TgSubCategoriaInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [General].[TgSubCategoriaInsert]
on [General].[SubCategoria] for insert
as begin
	declare @EditorUser varchar(20), @Evento varchar(max);
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	--<>---------------------------------------------------
	execute SpBitacoraInsert 'General.SubCategoria'
	,'Insert',@EditorUser,@Evento
end
GO
ALTER TABLE [General].[SubCategoria] ENABLE TRIGGER [TgSubCategoriaInsert]
GO
/****** Object:  Trigger [General].[TgSubCategoriaUpdate]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [General].[TgSubCategoriaUpdate]
on [General].[SubCategoria] for update
as begin
	declare @EditorUser varchar(20), @Evento varchar(max), @Accion varchar(10)='Update';
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	--<>---------------------------------------------------
	if(update(Estado))
		begin
			set @Accion='Restore';
			if((select Estado from inserted)=0)
				begin
					set @Accion='Delete';
				end
		end

	execute SpBitacoraInsert 'General.SubCategoria'
	,@Accion,@EditorUser,@Evento;
end
GO
ALTER TABLE [General].[SubCategoria] ENABLE TRIGGER [TgSubCategoriaUpdate]
GO
/****** Object:  Trigger [General].[TgTallaInsert]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [General].[TgTallaInsert]
on [General].[Talla] for insert
as begin
	declare @EditorUser varchar(20), @Evento varchar(max);
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	--<>---------------------------------------------------
	execute SpBitacoraInsert 'General.Talla'
	,'Insert',@EditorUser,@Evento
end
GO
ALTER TABLE [General].[Talla] ENABLE TRIGGER [TgTallaInsert]
GO
/****** Object:  Trigger [General].[TgTallaUpdate]    Script Date: 31/01/2022 8:41:24 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create trigger [General].[TgTallaUpdate]
on [General].[Talla] for update
as begin
	declare @EditorUser varchar(20), @Evento varchar(max), @Accion varchar(10)='Update';
	set @EditorUser=(select EditorUser from inserted);
	set @Evento=(select * from inserted for xml auto)
	--<>---------------------------------------------------
	if(update(Estado))
		begin
			set @Accion='Restore';
			if((select Estado from inserted)=0)
				begin
					set @Accion='Delete';
				end
		end

	execute SpBitacoraInsert 'General.Talla'
	,@Accion,@EditorUser,@Evento;
end
GO
ALTER TABLE [General].[Talla] ENABLE TRIGGER [TgTallaUpdate]
GO
